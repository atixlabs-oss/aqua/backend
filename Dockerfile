FROM node:12.21.0


# Setting working directory. All the path will be relative to WORKDIR
WORKDIR /usr/src/app

# Installing dependencies
COPY package* ./

RUN npm ci

# Copying source files
COPY . .

EXPOSE 3001

# Configure the contracts deployment in use
ENV CONTRACTS_CHAIN_ID="" \
    CONTRACTS_DEPLOYMENT_NAME=""
ENTRYPOINT ["./scripts/docker-entrypoint.sh"]

# Running the app
CMD ["node", "index"]

