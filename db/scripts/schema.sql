-- Schema

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';

CREATE TYPE public.tx_status AS ENUM (
    'pending',
    'confirmed',
    'failed'
);

CREATE TYPE public.tx_status_old AS ENUM (
    'pending',
    'confirmed',
    'cancelled'
);

CREATE TYPE public.claimstatus AS ENUM (
    'pending',
    'claimable',
    'claimed',
    'transferred'
);

CREATE TYPE public.projectstatus AS ENUM (
  'draft',
  'published',
  'in progress',
  'in review',
  'completed',
  'new',
  'toreview',
  'rejected',
  'deleted',
  'consensus',
  'funding',
  'executing',
  'changingscope',
  'finished',
  'aborted',
  'archived',
  'cancelled',
  'open review',
  'cancelled review',
  'pending published',
  'pending review',
  'pending approve review',
  'pending cancel review'
);

CREATE TYPE public.action_type AS ENUM (
  'create_project',
  'publish_project',
  'send_project_to_review',
  'edit_project_basic_information',
  'edit_project_details',
  'add_user_project',
  'remove_user_project',
  'add_milestone',
  'remove_milestone',
  'add_activity',
  'remove_activity',
  'add_evidence',
  'delete_evidence',
  'reject_activity',
  'approve_activity',
  'activity_to_review',
  'reject_evidence',
  'approve_evidence',
  'project_clone',
  'cancel_review',
  'approve_review',
  'update_milestone',
  'update_activity',
  'send_cancel_to_review'
);

CREATE TYPE public.role_old AS ENUM (
    'admin',
    'entrepreneur',
    'supporter',
    'curator',
    'bankoperator'
);

CREATE TYPE public.evidence_type AS ENUM (
    'transfer',
    'impact'
);

CREATE TYPE public.tx_evidence_status AS ENUM (
    'notsent',
    'sent',
    'confirmed',
    'failed',
    'pending_verification'
);

CREATE TYPE public.evidence_status AS ENUM (
    'new',
    'approved',
    'rejected'
);

CREATE TYPE public.tx_funder_status AS ENUM (
    'reconciliation',
    'pending',
    'sent',
    'failed',
    'cancelled',
    'verified',
    'pending_verification'
);

CREATE TYPE public.activity_status AS ENUM (
    'new',
    'to-review',
    'approved',
    'rejected',
    'in_progress',
    'pending to review',
    'pending to approve',
    'pending to reject'
);

CREATE TYPE public.milestone_status AS ENUM (
    'not started',
    'in progress',
    'approved'
);

CREATE TABLE public."role" (
    id serial PRIMARY KEY,
    description varchar(255) NOT NULL
);

CREATE TABLE public.blockchain_block (
    id serial,
    "blockNumber" integer NOT NULL,
    "transactionHash" character varying(80) NOT NULL,
    "createdAt" timestamp without time zone NOT NULL,
    "updatedAt" timestamp without time zone NOT NULL
);

CREATE TABLE public.blockchain_status (
    id smallint NOT NULL,
    name character varying NOT NULL
);

CREATE TABLE public.configs (
    id serial,
    key character varying NOT NULL,
    value character varying,
    "createdAt" date,
    "updatedAt" date
);

CREATE TABLE public.country (
    id serial,
    name character varying(42) NOT NULL,
    "callingCode" integer
);

CREATE TABLE public.file (
    id serial,
    path character varying NOT NULL,
    name TEXT NOT NULL,
    size integer NOT NULL,
    hash TEXT NOT NULL,
    "createdAt" date,
    "updatedAt" date
);

CREATE TABLE public.milestone (
    id serial,
    "projectId" uuid NOT NULL,
    title varchar(50),
    description text,
    "createdAt" date,
    "claimStatus" public.claimstatus DEFAULT 'pending'::public.claimstatus,
    "claimReceiptPath" character varying(200),
    status public.milestone_status DEFAULT 'not started'::public.milestone_status,
    deleted BOOLEAN NOT NULL DEFAULT false,
    "parentId" integer
);

CREATE TABLE public.milestone_activity_status (
    status integer NOT NULL,
    name character varying NOT NULL
);

CREATE TABLE public.milestone_budget_status (
    id integer NOT NULL,
    name character varying NOT NULL
);

CREATE TABLE public.pass_recovery (
    id serial,
    token character varying(80) NOT NULL,
    email character varying(80) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "expirationDate" timestamp with time zone NOT NULL
);

CREATE TABLE public.project (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    "projectName" character varying(50) NOT NULL,
    "ownerId_old" integer,
    mission text,
    "problemAddressed" text,
    location text,
    timeframe numeric(6,3),
    "timeframeUnit" text,
    "isCancelRequested" boolean NOT NULL DEFAULT false,
    "published" BOOLEAN NOT NULL DEFAULT false,
    "highlighted" BOOLEAN NOT NULL DEFAULT false,
    "dataComplete" integer NOT NULL DEFAULT 0,
    status public.projectstatus DEFAULT 'draft'::public.projectstatus,
    "goalAmount" text NOT NULL DEFAULT '0',
    "currencyType" varchar(50),
    "currency" varchar(50),
    "additionalCurrencyInformation" text,
    "faqLink" character varying,
    "createdAt" timestamp with time zone DEFAULT now(),
    "lastUpdatedStatusAt" timestamp with time zone DEFAULT now(),
    address character varying(42) DEFAULT NULL::character varying,
    "agreementFileHash" character varying(200),
    "proposalFileHash" character varying(200),
    "agreementFilePath" character varying(200),
    "proposalFilePath" character varying(200),
    "txHash" character varying(80) DEFAULT NULL::character varying,
    "coverPhotoPath" character varying(200),
    "cardPhotoPath" character varying(200),
    "milestonePath" character varying(200),
    proposal text,
    "agreementJson" text,
    "rejectionReason" text,
    "ownerId" uuid NOT NULL,
    "ipfsHash" TEXT,
    "proposerId" uuid,
    "parentId" uuid,
    "revision" integer NOT NULL DEFAULT 1,
    step integer NOT NULL DEFAULT 0,
    type TEXT,
    "proposerNonce" integer NULL
);

CREATE TABLE public.project_status (
    status integer NOT NULL,
    name character varying NOT NULL
);

CREATE TABLE public.tx (
    id serial,
    hash character varying(80) NOT NULL,
    status public.tx_status DEFAULT 'pending'::public.tx_status NOT NULL,
    action public.action_type NOT NULL,
    data json
);

CREATE TYPE public.activity_type AS ENUM (
    'funding',
    'spending',
    'payback'
);

CREATE TABLE public.activity (
    id serial,
    "milestoneId" integer NOT NULL,
    title varchar(50) NOT NULL,
    description text NOT NULL,
    "acceptanceCriteria" text NOT NULL,
    budget text NOT NULL,
    "auditorId" uuid  NOT NULL,
    "createdAt" date DEFAULT now(),
    "activityHash" character varying(80) DEFAULT NULL::character varying,
    "proposerId" uuid,
    status public.activity_status DEFAULT 'new'::public.activity_status,
    reason text,
    step integer NOT NULL DEFAULT 0,
    "toSign" json DEFAULT '{}'::json,
    deleted BOOLEAN NOT NULL DEFAULT false,
    type public.activity_type,
    current text NOT NULL DEFAULT '0',
    "parentId" integer,
    "claimCounter" integer NOT NULL DEFAULT 1
);

CREATE TABLE public.activity_evidence (
    id serial,
    title character varying(50) NOT NULL,
    description character varying(500) NOT NULL,
    type public.evidence_type NOT NULL,
    "transferTxHash" text,
    proof text,
    approved boolean,
    "activityId" integer NOT NULL,
    "txHash" character varying(80) DEFAULT NULL::character varying,
    status public.evidence_status DEFAULT 'new'::public.evidence_status,
    "createdAt" timestamp with time zone NOT NULL,
    "auditorId" uuid,
    reason text,
    "userId" uuid,
    amount text NOT NULL DEFAULT '0',
    "destinationAccount" text,
    "parentId" integer,
    "roleDescription" varchar(255) NOT NULL
);

CREATE TABLE public.transaction (
    id serial,
    sender character varying(42) NOT NULL,
    "txHash" character varying(80) NOT NULL,
    nonce integer NOT NULL,
    "createdAt" date DEFAULT now()
);

CREATE TABLE public.transfer_status (
    status integer NOT NULL,
    name character varying NOT NULL
);

CREATE TABLE public."user" (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    id_old serial NOT NULL,
    "firstName" character varying NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    address character varying,
    "createdAt" date,
    role public.role_old DEFAULT 'entrepreneur'::public.role_old NOT NULL,
    "lastName" character varying(50) DEFAULT ''::character varying NOT NULL,
    blocked boolean DEFAULT false NOT NULL,
    "phoneNumber" character varying(80) DEFAULT NULL::character varying,
    company character varying(80) DEFAULT NULL::character varying,
    "countryId" integer,
    "encryptedWallet" json DEFAULT '{}'::json,
    "forcePasswordChange" boolean DEFAULT false NOT NULL,
    mnemonic character varying(200),
    "emailConfirmation" boolean DEFAULT false NOT NULL,
    "isAdmin" BOOLEAN DEFAULT false,
    "first" BOOLEAN DEFAULT true,
    "pin" BOOLEAN DEFAULT false,
    "apiKey" character varying,
    "apiSecret" character varying
);

CREATE TABLE public.user_funder (
    id serial,
    "userId" integer NOT NULL,
    "phoneNumber" character varying(80)
);

CREATE TABLE public.user_project (
    id serial,
    "userId" uuid NOT NULL,
    "projectId" uuid NOT NULL,
    "roleId" integer NOT NULL
);

CREATE TABLE public.user_social_entrepreneur (
    id serial,
    "userId" integer NOT NULL,
    company character varying(80),
    "phoneNumber" character varying(80)
);

CREATE TABLE public.user_wallet (
    id serial,
    "userId_old" integer,
    address character varying(42) NOT NULL,
    "encryptedWallet" json NOT NULL,
    mnemonic character varying(200),
    active boolean NOT NULL,
    "createdAt" date,
    iv character varying(200),
    "userId" uuid NOT NULL
);

ALTER TABLE ONLY public.blockchain_block
    ADD CONSTRAINT "blockchain_block_transactionHash_key" UNIQUE ("transactionHash");

ALTER TABLE ONLY public.configs
    ADD CONSTRAINT configs_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.milestone_activity_status
    ADD CONSTRAINT milestone_activity_status_pkey PRIMARY KEY (status);

ALTER TABLE ONLY public.milestone_budget_status
    ADD CONSTRAINT milestone_budget_status_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT milestone_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.pass_recovery
    ADD CONSTRAINT pass_recovery_email_key UNIQUE (email);

ALTER TABLE ONLY public.pass_recovery
    ADD CONSTRAINT pass_recovery_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.project_status
    ADD CONSTRAINT project_status_pkey PRIMARY KEY (status);

ALTER TABLE ONLY public.activity_evidence
    ADD CONSTRAINT activity_evidence_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.activity_evidence
    ADD CONSTRAINT "activity_evidence_txHash_key" UNIQUE ("txHash");

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.transfer_status
    ADD CONSTRAINT transfer_status_pkey PRIMARY KEY (status);

ALTER TABLE ONLY public.user_wallet
    ADD CONSTRAINT unique_address UNIQUE (address);

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);

ALTER TABLE ONLY public.user_funder
    ADD CONSTRAINT user_funder_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.user_project
    ADD CONSTRAINT user_project_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.user_social_entrepreneur
    ADD CONSTRAINT user_social_entrepreneur_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.user_wallet
    ADD CONSTRAINT user_wallet_pkey PRIMARY KEY (id);

CREATE UNIQUE INDEX "onlyActive" ON public.user_wallet USING btree ("userId_old") WHERE active;

ALTER TABLE ONLY public.user_wallet
    ADD CONSTRAINT fk_user FOREIGN KEY ("userId") REFERENCES public."user"(id);

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT "milestone_projectId_fkey" FOREIGN KEY ("projectId") REFERENCES public.project(id) ON DELETE CASCADE;

ALTER TABLE ONLY public.project
    ADD CONSTRAINT "project_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES public."user"(id);

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT "activity_milestoneId_fkey" FOREIGN KEY ("milestoneId") REFERENCES public.milestone(id) ON DELETE CASCADE;

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "user_countryId_fkey" FOREIGN KEY ("countryId") REFERENCES public.country(id);

ALTER TABLE ONLY public.user_project
    ADD CONSTRAINT "user_project_projectId_fkey" FOREIGN KEY ("projectId") REFERENCES public.project(id);

ALTER TABLE ONLY public.user_project
    ADD CONSTRAINT "user_project_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES public.role(id);

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT "activity_auditorId_fkey" FOREIGN KEY ("auditorId") REFERENCES public."user"(id);

CREATE TABLE public.evidence_file (
    id serial,
    "activityEvidenceId" INTEGER NOT NULL CONSTRAINT "evidence_file_activityEvidenceId_fkey" REFERENCES public.activity_evidence(id),
    "fileId" INTEGER NOT NULL CONSTRAINT "evidence_file_fileId_fkey" REFERENCES public.file(id)
);

CREATE TABLE public.token (
    id serial,
    "name" CHARACTER VARYING(20) NOT NULL,
    "symbol" CHARACTER VARYING(20) NOT NULL,
    "decimals" INTEGER NOT NULL,
    "apiBaseUrl" TEXT NOT NULL,
    "contractAddress" TEXT,
    deleted BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE public.changelog (
    id serial,
    "projectId" uuid CONSTRAINT "changelog_projectId_fkey" REFERENCES public.project(id),
    "revisionId" INTEGER NOT NULL,
    "milestoneId" INTEGER,
    "activityId" INTEGER,
    "evidenceId" INTEGER,
    "userId" UUID,
    "transactionId" TEXT,
    description TEXT,
    "action" public.action_type NOT NULL,
    "extraData" json,
    "milestoneData" jsonb NULL,
    "activityData" jsonb NULL,
    "evidenceData" jsonb NULL,
    "projectData" jsonb NOT NULL,
    "userData" jsonb NOT NULL,
    status public.tx_status DEFAULT 'confirmed'::public.tx_status NOT NULL,
    datetime timestamp with time zone DEFAULT now()
);

ALTER TABLE ONLY public.activity_evidence
    ADD CONSTRAINT "activity_evidence_auditorId_fkey" FOREIGN KEY ("auditorId") REFERENCES public."user"(id);

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT "activity_proposerId_fkey" FOREIGN KEY ("proposerId") REFERENCES public."user"(id);

ALTER TABLE ONLY public.project
    ADD CONSTRAINT "project_proposerId_fkey" FOREIGN KEY ("proposerId") REFERENCES public."user"(id);

ALTER TABLE ONLY public.activity_evidence
    ADD CONSTRAINT "activity_evidence_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."user"(id);

ALTER TABLE ONLY public.tx
    ADD CONSTRAINT tx_pkey PRIMARY KEY (id);
    
