ALTER TYPE "tx_status" RENAME TO tx_status_old;

CREATE TYPE public.tx_status AS ENUM (
    'pending',
    'confirmed',
    'failed'
);

CREATE TABLE public.tx (
    id serial,
    hash character varying(80) NOT NULL,
    status public.tx_status DEFAULT 'pending'::public.tx_status,
    action public.action_type NOT NULL,
    data json
);