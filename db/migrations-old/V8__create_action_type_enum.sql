CREATE TYPE public.action_type AS ENUM (
  'create_project',
  'publish_project',
  'send_project_to_review',
  'edit_project_basic_information',
  'edit_project_details',
  'add_user_project',
  'remove_user_project',
  'add_milestone',
  'remove_milestone',
  'add_activity',
  'remove_activity',
  'add_evidence',
  'reject_activity',
  'approve_activity',
  'activity_to_review',
  'reject_evidence',
  'approve_evidence',
  'project_clone',
  'cancel_review',
  'approve_review',
  'update_milestone',
  'update_activity'
);

ALTER TABLE public.changelog ALTER COLUMN action TYPE public.action_type USING action::text::public.action_type;