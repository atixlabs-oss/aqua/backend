-- ALTER TABLE public.user ADD COLUMN "mnemonic" character varying(200);
-- ALTER TABLE public.user_wallet ADD COLUMN "mnemonic" character varying(200);
-- ALTER TABLE public.user_wallet ADD COLUMN "iv" character varying(200);

-- Remove the column mnemonic
ALTER TABLE public.user DROP COLUMN "mnemonic";
ALTER TABLE public.user_wallet DROP COLUMN "mnemonic";
ALTER TABLE public.user_wallet DROP COLUMN "iv";
