CREATE TYPE public.tx_action AS ENUM (
  'txlist',
  'tokentx'
);

ALTER TABLE public.TOKEN ADD COLUMN "action" public.tx_action NOT NULL;
