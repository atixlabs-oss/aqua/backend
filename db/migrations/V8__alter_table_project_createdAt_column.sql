ALTER TABLE public.project ALTER COLUMN "createdAt" TYPE timestamp with time zone;
ALTER TABLE public.project ALTER COLUMN "createdAt"  SET DEFAULT now();