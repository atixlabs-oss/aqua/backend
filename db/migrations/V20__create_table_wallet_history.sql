CREATE TABLE public.wallet_history (
    id serial,
    "userId" uuid NOT NULL,
    "address" character varying(42) UNIQUE DEFAULT NULL::character varying,
    "createdAt" timestamp without time zone NOT NULL default now()
);

ALTER TABLE ONLY public.wallet_history
    ADD CONSTRAINT fk_user FOREIGN KEY ("userId") REFERENCES public."user"(id);