ALTER TABLE public.milestone ADD COLUMN "publicId" uuid NOT NULL;
ALTER TABLE public.activity ADD COLUMN "publicId" uuid NOT NULL;
ALTER TABLE public.activity_evidence ADD column "publicId" uuid NOT NULL;

ALTER TABLE public.project ADD COLUMN "lastIPFSMetadata" jsonb NULL;
