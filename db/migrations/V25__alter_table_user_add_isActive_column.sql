-- ALTER TABLE public.user DROP COLUMN "isActive";

-- Add the new column with a default value of false
ALTER TABLE public.user ADD COLUMN "isActive" BOOLEAN NOT NULL DEFAULT true;

-- Update existing rows to set the new_column value to true
-- UPDATE public.user SET isActive = true;