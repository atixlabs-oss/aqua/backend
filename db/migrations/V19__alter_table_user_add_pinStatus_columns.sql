CREATE TYPE public.pinStatus AS ENUM (
    'active',
    'in review',
    'approved',
    'rejected',
    'inactive'
)

ALTER TABLE public.user ADD COLUMN "pinStatus" public.pinStatus NOT NULL DEFAULT 'inactive';