-- ALTER TABLE public.user_project DROP COLUMN "confirmed";

-- Add the new column with a default value of false
ALTER TABLE public.user_project ADD COLUMN "confirmed" BOOLEAN NOT NULL DEFAULT false;

-- Update existing rows to set the new_column value to true
-- UPDATE public.user_project SET confirmed = true;