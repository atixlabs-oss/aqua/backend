
-- ALTER TABLE public.project DROP COLUMN "published";
ALTER TABLE public.project ADD COLUMN 
    "published" BOOLEAN NOT NULL DEFAULT false;
-- ALTER TABLE public.project DROP COLUMN "highlighted";
ALTER TABLE public.project ADD COLUMN 
    "highlighted" BOOLEAN NOT NULL DEFAULT false;