ALTER TABLE public.activity ADD COLUMN "approvalDate" timestamp NULL;
ALTER TABLE public.activity ADD COLUMN "revisionDate" timestamp NULL;
ALTER TABLE public.activity_evidence ADD COLUMN "auditedAt" timestamp NULL;