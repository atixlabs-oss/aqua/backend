ALTER TABLE public.movement ADD PRIMARY KEY (id);
ALTER TABLE public.activity_evidence ADD COLUMN "movementId" integer REFERENCES public.movement(id);
ALTER TABLE public.activity_evidence ADD CONSTRAINT fk_activity FOREIGN KEY ("activityId") REFERENCES public."activity"(id);
