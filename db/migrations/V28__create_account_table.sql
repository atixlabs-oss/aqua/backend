CREATE TABLE public.account (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    platform_name character varying(80) NOT NULL,
    currency integer NOT NULL,
    data json,
    middleware_id integer NOT NULL,
    project_id uuid REFERENCES public.project(id) NOT NULL
);

ALTER TABLE ONLY public.account ADD CONSTRAINT account_pkey PRIMARY KEY (id);

-- CREATE TABLE public.account (
--     id serial,
--     platformName character varying(80) NOT NULL,
--     currency integer REFERENCES public.token(id) NOT NULL, the references keyword would be useful but the token table has no PRIMARY KEY
--     data json,
--     middleware_id integer REFERENCES public.middleware(id) NOT NULL, same but middleware table doesn't exist yet
-- );

-- DROP TABLE public.account