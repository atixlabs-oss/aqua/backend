ALTER TABLE public.project DROP COLUMN "isCancelled";
ALTER TABLE public.project ADD COLUMN 
    "isCancelRequested" BOOLEAN NOT NULL DEFAULT false;