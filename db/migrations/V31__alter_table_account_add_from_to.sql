ALTER TABLE public.account ADD COLUMN "date_from" timestamp NOT NULL;
ALTER TABLE public.account ADD COLUMN "date_to" timestamp NOT NULL;

-- Undo
-- ALTER TABLE public.account DROP COLUMN "date_from";
-- ALTER TABLE public.account DROP COLUMN "date_to";
