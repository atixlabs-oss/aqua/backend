CREATE TABLE public.middleware (
    id serial,
    name character varying(80) NOT NULL,
    json_spec json NOT NULL,
    api_key character varying(200) NOT NULL
);

-- DROP TABLE public.middleware;