
-- ALTER TABLE public.project DROP COLUMN "visibilityUpdatedAt";

-- Add the new column with a default value of false
ALTER TABLE public.project ADD COLUMN "visibilityUpdatedAt" timestamp NULL;