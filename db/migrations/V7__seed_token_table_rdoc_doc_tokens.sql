INSERT INTO public.token(name, symbol, decimals, "apiBaseUrl", "contractAddress") 
VALUES ('Dollar on Chain', 'DOC', 18, 'https://blockscout.com/rsk/mainnet/api', '0xE700691Da7B9851F2F35f8b8182C69C53ccad9DB'),
('RIF Dollar on Chain', 'RDOC', 18, 'https://blockscout.com/rsk/mainnet/api', '0x2d919f19D4892381d58EdEbEcA66D5642ceF1A1F');