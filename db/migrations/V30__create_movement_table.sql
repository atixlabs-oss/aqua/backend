CREATE TYPE public.movement_type AS ENUM (
  'incoming',
  'outgoing'
);

CREATE TABLE public.movement (
    id serial,
    external_id character varying(200) NOT NULL,
    display_id character varying(200) NOT NULL,
    amount integer NOT NULL,
    type public.movement_type NOT NULL,
    account_id uuid NOT NULL,
    other_account character varying(200) NOT NULL,
    date timestamp NOT NULL
);

ALTER TABLE ONLY public.movement
    ADD CONSTRAINT "movement_accountId_fkey" FOREIGN KEY ("account_id") REFERENCES public.account(id);

-- DROP TABLE public.movement;