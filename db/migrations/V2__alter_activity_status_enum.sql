ALTER TYPE public.task_status ADD VALUE 'pending to review';
ALTER TYPE public.task_status ADD VALUE 'pending to approve';
ALTER TYPE public.task_status ADD VALUE 'pending to reject';