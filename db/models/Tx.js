/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { TX_STATUS, ACTION_TYPE } = require('../../src/rest/util/constants');

module.exports = {
  identity: 'tx',
  primaryKey: 'id',
  attributes: {
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    hash: { type: 'string', required: true, allowNull: false },
    data: {
      type: 'json',
      required: false
    },
    action: {
      type: 'string',
      required: true,
      validations: {
        isIn: Object.values(ACTION_TYPE)
      }
    },
    status: {
      type: 'string',
      defaultsTo: TX_STATUS.PENDING,
      validations: {
        isIn: Object.values(TX_STATUS)
      }
    }
  }
};
