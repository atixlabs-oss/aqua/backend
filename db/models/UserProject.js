/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @description Represents a relationship between a user and a project
 * @attribute `status`: state in which the user is with respect to a project
 * @attribute `userId`: user id
 * @attribute `projectId`: project id
 */
module.exports = {
  identity: 'user_project',
  primaryKey: 'id',
  attributes: {
    user: {
      columnName: 'userId',
      model: 'user',
      required: true
    },
    project: {
      columnName: 'projectId',
      model: 'project',
      required: true
    },
    role: {
      columnName: 'roleId',
      model: 'role',
      required: true
    },
    confirmed: { 
      columnName: 'confirmed',
      type: 'boolean',
      allowNull: false,
      defaultsTo: false
    },
    id: { type: 'number', autoMigrations: { autoIncrement: true } }
  }
};
