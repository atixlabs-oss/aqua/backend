/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {
  evidenceStatus,
  evidenceTypes,
  rolesTypes
} = require('../../src/rest/util/constants');

module.exports = {
  identity: 'activity_evidence',
  primaryKey: 'id',
  attributes: {
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    title: { type: 'string', required: true },
    description: { type: 'string', required: true },
    type: {
      type: 'string',
      required: true,
      validations: {
        isIn: Object.values(evidenceTypes)
      }
    },
    transferTxHash: { type: 'string', required: false, allowNull: true },
    proof: { type: 'string', required: false, allowNull: true },
    approved: { type: 'boolean', required: false, allowNull: true },
    activity: {
      columnName: 'activityId',
      model: 'activity'
    },
    txHash: { type: 'string', required: false, allowNull: true },
    status: {
      type: 'string',
      defaultsTo: evidenceStatus.NEW,
      validations: {
        isIn: Object.values(evidenceStatus)
      }
    },
    user: {
      columnName: 'userId',
      model: 'user',
      required: false
    },
    auditor: {
      columnName: 'auditorId',
      model: 'user',
      required: false
    },
    reason: {
      type: 'string',
      required: false,
      allowNull: true
    },
    files: {
      collection: 'evidence_file',
      via: 'evidence'
    },
    createdAt: { type: 'string', autoCreatedAt: true },
    amount: { type: 'string', required: false, defaultsTo: '0' },
    destinationAccount: { type: 'string', required: false },
    parent: {
      columnName: 'parentId',
      model: 'activity_evidence',
      required: false
    },
    roleDescription: {
      type: 'string',
      columnName: 'roleDescription',
      required: true,
      validations: {
        isIn: Object.values(rolesTypes)
      }
    },
    auditedAt: { type: 'string', allowNull: true, required: false },
    publicId: { type: 'string', required: true },
    movement: { columnName: 'movementId', model: 'movement' }
  }
};
