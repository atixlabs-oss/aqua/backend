/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @description Represent a user wallet.
 * @attribute `id`: user wallet id.
 * @attribute `userId`: user id owner of wallet .
 * @attribute `encryptedWallet`: wallet associated to user id.
 * @attribute `address`: address associated to wallet and user
 */

module.exports = {
  identity: 'user_wallet',
  primaryKey: 'id',
  attributes: {
    user: {
      columnName: 'userId',
      model: 'user'
    },
    address: { type: 'string', required: true },
    encryptedWallet: { type: 'json', required: false },
    active: { type: 'boolean', defaultsTo: false, required: false },
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    createdAt: { type: 'string', autoCreatedAt: true, required: false }
  }
};
