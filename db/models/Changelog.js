/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { ACTION_TYPE, TX_STATUS } = require('../../src/rest/util/constants');

module.exports = {
  identity: 'changelog',
  primaryKey: 'id',
  attributes: {
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    project: {
      columnName: 'projectId',
      model: 'project',
      required: true
    },
    revision: {
      columnName: 'revisionId',
      type: 'number',
      required: true
    },
    milestone: {
      columnName: 'milestoneId',
      model: 'milestone',
      required: false
    },
    activity: { columnName: 'activityId', model: 'activity', required: false },
    evidence: {
      columnName: 'evidenceId',
      model: 'activity_evidence',
      required: false
    },
    user: { columnName: 'userId', model: 'user', required: false },
    transaction: {
      columnName: 'transactionId',
      type: 'string',
      required: false
    },
    description: { type: 'string', required: false, allowNull: true },
    action: {
      type: 'string',
      required: true,
      validations: {
        isIn: Object.values(ACTION_TYPE)
      }
    },
    extraData: {
      type: 'json',
      required: false
    },
    datetime: {
      type: 'string',
      autoCreatedAt: true,
      required: false
    },
    status: {
      type: 'string',
      defaultsTo: TX_STATUS.CONFIRMED,
      validations: {
        isIn: Object.values(TX_STATUS)
      }
    },
    projectData: {
      type: 'json',
      required: true
    },
    userData: {
      type: 'json',
      required: true
    },
    milestoneData: {
      type: 'json',
      required: false
    },
    activityData: {
      type: 'json',
      required: false
    },
    evidenceData: {
      type: 'json',
      required: false
    }
  }
};
