/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {
  claimMilestoneStatus,
  MILESTONE_STATUS
} = require('../../src/rest/util/constants');

module.exports = {
  identity: 'milestone',
  primaryKey: 'id',
  attributes: {
    title: { type: 'string', required: true },
    description: { type: 'string', required: true },
    claimStatus: { type: 'string', defaultsTo: claimMilestoneStatus.PENDING },
    claimReceiptPath: { type: 'string', required: false, allowNull: true },
    project: {
      columnName: 'projectId',
      model: 'project'
    },
    tasks: {
      collection: 'activity',
      via: 'milestone'
    },
    status: {
      type: 'string',
      defaultsTo: MILESTONE_STATUS.NOT_STARTED,
      validations: {
        isIn: Object.values(MILESTONE_STATUS)
      }
    },
    createdAt: { type: 'string', autoCreatedAt: true },
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    deleted: { type: 'boolean', allowNull: false, defaultsTo: false },
    parent: {
      columnName: 'parentId',
      model: 'milestone',
      required: false
    },
    publicId: { type: 'string', required: true }
  }
};
