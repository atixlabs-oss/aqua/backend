/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @description Represent a user of agua, this can be:
 *              Social Entrepreneur, Project Supporter, Project Curator,
 *              Bank operator, COA Administrator
 * @attribute `id`: user id in the business domain
 * @attribute `name`: name with which the user will be shown
 * @attribute `email`: email with which the user is registered
 * @attribute `pwd`: password with which the user logs
 * @attribute `roles`: role / roles that the user has in the tool
 *            (this can be for example Funder and Oracle at the same time)
 */
const { omit } = require('lodash');
const { userRoles, pinStatus } = require('../../src/rest/util/constants');

module.exports = {
  identity: 'user',
  primaryKey: 'id',
  attributes: {
    firstName: { type: 'string', required: true },
    lastName: { type: 'string', required: true },
    email: { type: 'string', required: true },
    password: { type: 'string', required: true },
    createdAt: { type: 'string', autoCreatedAt: true, required: false },
    id: { type: 'string', required: true },
    role: {
      type: 'string',
      validations: { isIn: Object.values(userRoles) },
      required: true
    },
    blocked: { type: 'boolean', defaultsTo: false, required: false },
    country: {
      columnName: 'countryId',
      model: 'country'
    },
    wallets: {
      collection: 'user_wallet',
      via: 'user'
    },
    phoneNumber: { type: 'string', required: false, allowNull: true },
    company: { type: 'string', required: false, allowNull: true },
    forcePasswordChange: {
      type: 'boolean',
      defaultsTo: false,
      required: false
    },
    emailConfirmation: { type: 'boolean', defaultsTo: false, required: false },
    // Remove once the prod users reovery them passwords
    address: { type: 'string', required: false, allowNull: true },
    encryptedWallet: { type: 'json', required: false },
    roles: {
      collection: 'user_project',
      via: 'user'
    },
    isAdmin: { type: 'boolean', required: true, allowNull: false },
    first: { type: 'boolean', allowNull: false, defaultsTo: true },
    pin: { type: 'boolean', allowNull: false, defaultsTo: false },
    apiKey: { type: 'string', allowNull: true },
    apiSecret: { type: 'string', allowNull: true },
    pinStatus: { 
      type: 'string', 
      validations: {
        isIn: Object.values(pinStatus)
      },
      allowNull: false,
      defaultsTo: pinStatus.INACTIVE
    },
    rejectedPinSeenAt: { type: 'string', allowNull: true, required: false },
    isActive: { type: 'boolean', allowNull: false, defaultsTo: true },
  },
  customToJSON: function toJson() {
    return omit(this, ['password']);
  },
  async findById(id) {
    return this.findOne(id);
  }
};
