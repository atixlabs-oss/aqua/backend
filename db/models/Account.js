/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
    identity: 'account',
    primaryKey: 'id',
    attributes: {
      id: { type: 'string', required: true },
      platformName: { columnName: 'platform_name', type: 'string', required: true },
      currency: {
        columnName: 'currency',
        model: 'token'
      },
      data: {
        type: 'json',
        required: false
      },
      middlewareId: { columnName: 'middleware_id', type: 'number', required: true },
      projectId: { columnName: 'project_id', type: 'string', required: true },
      dateFrom: { columnName: 'date_from', type: 'string', required: true },
      dateTo: { columnName: 'date_to', type: 'string', required: true }
    }
  };
  