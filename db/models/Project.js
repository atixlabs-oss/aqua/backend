/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *@description Represents a project of AGUA
 *@attribute `id`: id of the project in the business domain
 *@attribute `projectName`: name with which the user will be shown
 *@attribute `ownerId`: id of the user who is the creator
 *@attribute `mission`: project mission
 *@attribute `problemAddressed`: problem addressed by the project
 *@attribute `location`: geographical location where the project will be developed
 *@attribute `timeframe`: project time duration
 *@attribute `coverPhoto`: project cover image
 *@attribute `cardPhoto`: project icon
 *@attribute `status`: current project status
 *@attribute `goalAmount`: amount of money needed from the project
 *@attribute `faqLink`: link to the FAQ page
 *@attribute `pitchProposal`: initial proposal of the project
 *@attribute `milestonesFile`: excel file of milestones
 *@attribute `projectAgreement`: project consensus file
 *@attribute `isCancelRequested`: boolean used to identify a cancellation requested
 */

const {
  projectStatuses,
  PROJECT_TYPES
} = require('../../src/rest/util/constants');

module.exports = {
  identity: 'project',
  primaryKey: 'id',
  attributes: {
    projectName: { type: 'string', required: false, allowNull: true },
    mission: { type: 'string', required: false, allowNull: true },
    problemAddressed: { type: 'string', required: false, allowNull: true },
    location: { type: 'string', required: false, allowNull: true },
    timeframe: {
      type: 'ref',
      required: false,
      defaultsTo: 0
    },
    timeframeUnit: { type: 'string', required: false, allowNull: true },
    published: { type: 'boolean', allowNull: false, defaultsTo: false },
    highlighted: { type: 'boolean', allowNull: false, defaultsTo: false },
    dataComplete: {
      type: 'number',
      required: false,
      defaultsTo: 0
    },
    proposal: { type: 'string', required: false, allowNull: true },
    faqLink: { type: 'string', required: false, allowNull: true },
    agreementJson: { type: 'string', required: false, allowNull: true },
    coverPhotoPath: { type: 'string', required: false, allowNull: true },
    cardPhotoPath: { type: 'string', required: false, allowNull: true },
    milestonePath: { type: 'string', required: false, allowNull: true },
    proposalFilePath: { type: 'string', required: false, allowNull: true },
    agreementFilePath: { type: 'string', required: false, allowNull: true },
    agreementFileHash: { type: 'string', required: false, allowNull: true },
    proposalFileHash: { type: 'string', required: false, allowNull: true },
    goalAmount: { type: 'string', required: false, defaultsTo: '0' },
    status: { type: 'string', defaultsTo: projectStatuses.DRAFT },
    owner: {
      columnName: 'ownerId',
      model: 'user'
    },
    createdAt: { type: 'string', autoCreatedAt: true, required: false },
    address: { type: 'string', required: false, allowNull: true },
    milestones: {
      collection: 'milestone',
      via: 'project'
    },
    lastUpdatedStatusAt: {
      type: 'string',
      autoCreatedAt: true,
      required: false
    },
    id: { type: 'string', required: true },
    txHash: { type: 'string', required: false, allowNull: true },
    rejectionReason: { type: 'string', required: false, allowNull: true },
    currencyType: { type: 'string', required: false, allowNull: true },
    currency: { type: 'string', required: false, allowNull: true },
    additionalCurrencyInformation: {
      type: 'string',
      required: false,
      allowNull: true
    },
    ipfsHash: { type: 'string', required: false, allowNull: true },
    proposer: {
      columnName: 'proposerId',
      model: 'user'
    },
    parent: {
      columnName: 'parentId',
      model: 'project',
      required: false
    },
    revision: { type: 'number', required: false, defaultsTo: 1 },
    step: { type: 'number', required: false, defaultsTo: 0 },
    type: {
      type: 'string',
      validations: {
        isIn: Object.values(PROJECT_TYPES)
      },
      allowNull: true
    },
    users: {
      collection: 'user_project',
      via: 'project'
    },
    isCancelRequested: { type: 'boolean', allowNull: false, defaultsTo: false },
    proposerNonce: { type: 'number', allowNull: true },
    visibilityUpdatedAt: {
      type: 'string',
      allowNull: true,
      required: false
    },
    lastIPFSMetadata: {
      type: 'json',
      required: false
    }
  }
};
