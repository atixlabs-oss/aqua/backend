/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @description Represent a user social entrepreneur,
 *              specific information for users with Funder role
 * @attribute `id`: user_social_entrepreneur id in the business domain
 * @attribute `user`: reference to user owner of this information
 * @attribute `company`: company name
 * @attribute `phoneNumber`: phone number
 */
module.exports = {
  identity: 'user_social_entrepreneur',
  primaryKey: 'id',
  attributes: {
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    user: {
      columnName: 'userId',
      model: 'user'
    },
    company: { type: 'string', allowNull: true },
    phoneNumber: { type: 'string', allowNull: true }
  }
};
