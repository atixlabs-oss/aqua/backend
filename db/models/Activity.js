/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const {
  ACTIVITY_STATUS,
  ACTIVITY_TYPES
} = require('../../src/rest/util/constants');

module.exports = {
  identity: 'activity',
  primaryKey: 'id',
  attributes: {
    id: { type: 'number', autoMigrations: { autoIncrement: true } },
    title: { type: 'string', required: true },
    description: { type: 'string', required: true },
    acceptanceCriteria: { type: 'string', required: true },
    budget: { type: 'string', required: true },
    current: { type: 'string', required: false, defaultsTo: '0' },
    milestone: {
      columnName: 'milestoneId',
      model: 'milestone',
      required: true
    },
    auditor: {
      columnName: 'auditorId',
      model: 'user',
      required: true
    },
    activityHash: { type: 'string', required: false, allowNull: true },
    proposer: {
      columnName: 'proposerId',
      model: 'user'
    },
    status: {
      type: 'string',
      defaultsTo: ACTIVITY_STATUS.NEW,
      validations: {
        isIn: Object.values(ACTIVITY_STATUS)
      }
    },
    reason: {
      type: 'string',
      required: false,
      allowNull: true
    },
    createdAt: { type: 'string', autoCreatedAt: true },
    deleted: { type: 'boolean', allowNull: false, defaultsTo: false },
    step: { type: 'number', required: false, defaultsTo: 0 },
    toSign: {
      type: 'json',
      required: false
    },
    type: {
      type: 'string',
      validations: {
        isIn: Object.values(ACTIVITY_TYPES)
      },
      allowNull: true
    },
    parent: {
      columnName: 'parentId',
      model: 'activity',
      required: false
    },
    claimCounter: { type: 'number', required: false, defaultsTo: 1 },
    approvalDate: { type: 'string', allowNull: true, required: false },
    revisionDate: { type: 'string', allowNull: true, required: false },
    publicId: { type: 'string', required: true }
  }
};
