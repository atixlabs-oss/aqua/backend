/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { Pool } = require('pg');
const crypto = require('crypto');
const { Wallet } = require('ethers');
const bcrypt = require('bcrypt');
require('dotenv').config();

const emailClient = require('./src/rest/services/helpers/emailClient');
const { injectDependencies } = require('./src/rest/util/injection');
const { encrypt } = require('./src/rest/util/crypto');
const { encryption } = require('./src/rest/util/constants');

const config = require('./setup-config');
const mailService = require('./src/rest/services/mailService');
const logger = require('./src/rest/logger');

const key = process.env.CRYPTO_KEY;

const GENESIS_ADMIN_PW = 'admin';

const run = async () => {
  const { email, db } = config;
  const pool = new Pool({
    user: db.user,
    database: db.name,
    password: db.password,
    port: db.port,
    host: db.host
  });
  try {
    await pool.connect();
    await pool.query('BEGIN TRANSACTION');
    const usersResult = await pool.query(`SELECT * FROM "user"`);
    if ( usersResult.rows.length > 0 ) {
      await pool.query('COMMIT');
      process.exit(0);
    }
    const wallet = Wallet.createRandom();
    const { mnemonic, address } = wallet;
    const encryptedWallet = await wallet.encrypt(GENESIS_ADMIN_PW);
    const encryptedMnemonic = await encrypt(mnemonic.phrase, key);
    const hashedPassword = await bcrypt.hash(
      GENESIS_ADMIN_PW,
      encryption.saltOrRounds
    );
    if (
      !encryptedMnemonic ||
      !encryptedMnemonic.encryptedData ||
      !encryptedMnemonic.iv
    )
      throw new Error('Mnemonic could not be encrypted');
    const params = [
      'Administrator',
      email,
      hashedPassword,
      address,
      encryptedWallet,
    ];
    logger.info('Creating user...');
    const result = await pool.query(
      `
    INSERT INTO "user"(
      "firstName",
       email, 
       "password", 
       address, 
       "createdAt", 
       role,
       "isAdmin", 
       "lastName", 
       "blocked", 
       "phoneNumber", 
       company, 
       "countryId", 
       "encryptedWallet", 
       "forcePasswordChange", 
       "emailConfirmation", 
       id
    )
    VALUES($1, $2, $3, $4, 
      now(), 'admin'::role_old, true, '', false, NULL::character varying,
      NULL::character varying, 10, $5, true,
      true,uuid_generate_v4())
    RETURNING 
      id_old,
      id`,
      params
    );
    if (result.rows.length === 0) throw new Error('Could not create user');
    logger.info('Creating token...');
    const hash = await crypto.randomBytes(25);
    const token = hash.toString('hex');
    await pool.query(
      `
    INSERT INTO pass_recovery(
      email,
      token,
      "createdAt",
      "expirationDate"
    )
    VALUES($1,$2, NOW(), NOW() + INTERVAL '1 year')
    `,
      [email, token]
    );
    await pool.query('COMMIT');
    injectDependencies(mailService, { emailClient });
    await mailService.sendEmailInitialRecoveryPassword({
      to: email,
      bodyContent: {
        token,
        email
      }
    });
    logger.info('Success run!');
  } catch (error) {
    logger.error('An error occured, rollbacking: ', error);
    await pool.query('ROLLBACK');
  }
  process.exit(0);
};

run();
