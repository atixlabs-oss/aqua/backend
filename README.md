# AGUA - API

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Coverage Status](https://coveralls.io/repos/gitlab/atixlabs-oss/agua/backend/badge.svg?branch=develop)](https://coveralls.io/gitlab/atixlabs-oss/agua/backend?branch=develop)

AGUA is a platform that brings Social Entrepreneurs and Funders around the world closer while ensuring the transparency of investments and donations through blockchain technology, which allows for traceability of operations, tracking and visualization of the real impact that entrepreneurs are generating in their communities.

## Prerequisites

- See [.nvmrc](./.nvmrc) file

- Configured PostgreSQL database

- Configured email provider account. (See external services section)

## Tools and frameworks

- fastify@1.14.3

- solc@0.8.9

- hardhat@2.8.4

## Installation

To install and execute initial setup, please read the related documentation [Installation](docs/installation.md)

## Development

### Setup environment

- Run `npm install` to install the dependencies.
- Run `npm run node` to start a local hardhat node instance in `http://localhost:8545`.
- Create the database either manually as stated above or with Docker by running `cd docker && docker-compose up -d` (this is only for the development environment!!).

### Start the server

- Run `npm start` to start the server in `http://localhost:3001`.

### Linter

- Run `npm run lint:contracts` to run the linter on the .sol files
  - Most problems detected can be automatically fixed by running `npm run prettier:contracts`
- Run `npm run lint` to run the linter on the remainder of the files.

### Testing

- Run `npm test` to run all the API tests.
- Run `npm run test:contracts` to run all the smart contracts tests.
- Run `npm run coverage:contracts` to obtain the coverage results of the smart contracts.

## External dependencies

The external dependencies of this project are described in [this other document](./docs/EXTERNAL_DEPENDENCIES.md).


## Contributing

You could find more information about how contribute in this project in [CONTRIBUTING](CONTRIBUTING.md)

## Architecture

The architecture's documentation can be found in [this link](./docs/ARCHITECTURE.md).

# Contacts

-
