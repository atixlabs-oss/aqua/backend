/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const config = require('config');

require('@nomiclabs/hardhat-ethers');
require('@nomiclabs/hardhat-web3');
require('@nomiclabs/hardhat-solhint');
require('@openzeppelin/hardhat-upgrades');
require('solidity-coverage');

const chainIds = {
  hardhat: 31337,
  // RSK
  rskMainnet: 30,
  rskTestnet: 31,
  // ETH
  ethMainnet: 1,
  ethGoerli: 5,
  ethSepolia: 11155111,
};

const createConfig = (network) => {
  return {
    url: config.hardhat.node_url,
    accounts: [config.hardhat.blockchain_priv_key],
    timeout: 8 * 60 * 1000,
    chainId: chainIds[network]
  }
}

module.exports = {
  paths: {
    artifacts: "./artifacts",
    cache: "./cache",
    tests: "./src/tests/contracts",
    sources: "./src/contracts"
  },
  defaultNetwork: config.hardhat.defaultNetwork || "hardhat",
  networks: {
    // Local deployments
    localhost: {
      chainId: chainIds.hardhat,
      gasPrice: 10,
      initialBaseFeePerGas: 0,
      blockGasLimit: 999999999999999,
      loggingEnabled: true,
      throwOnTransactionFailures: true,
      url: "http://127.0.0.1:8545"
    },
    local_docker: {
      accounts: [
        config.hardhat.blockchain_priv_key || "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80",
        "0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d",
        "0x5de4111afa1a4b94908f83103eb1f1706367c2e68ca870fc3fb9a804cdab365a",
        "0x7c852118294e51e653712a81e05800f419141751be58f605c371e15141b007a6",
      ],
      chainId: chainIds.hardhat,
      gasPrice: 10,
      initialBaseFeePerGas: 0,
      blockGasLimit: 999999999999999,
      loggingEnabled: true,
      throwOnTransactionFailures: true,
      url: config.hardhat.node_url,
    },
    // For tests & coverage, is started automatically
    hardhat: {
      accounts: [
        {privateKey: "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80", balance: "10000000000000000000000"},
        {privateKey: "0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d", balance: "10000000000000000000000"},
        {privateKey: "0x5de4111afa1a4b94908f83103eb1f1706367c2e68ca870fc3fb9a804cdab365a", balance: "10000000000000000000000"},
        {privateKey: "0x7c852118294e51e653712a81e05800f419141751be58f605c371e15141b007a6", balance: "10000000000000000000000"}
      ],
      chainId: chainIds.hardhat,
      gasPrice: 10,
      initialBaseFeePerGas: 0,
      blockGasLimit: 999999999999999,
      loggingEnabled: true,
      throwOnTransactionFailures: true,
    },
    ethSepolia: createConfig("ethSepolia"),
    rskTestnet: createConfig("rskTestnet"),
    rskMainnet: createConfig("rskMainnet"),
    ethMainnet: createConfig("ethMainnet"),
  },
  solidity: {
    version: "0.8.9",
    settings: {
      // From RSK slack's there seem to be a lot EIP's from berlin not currently implemented by them.
      // In the RIF project running on RSK
      // (https://github.com/rsksmart/rif-relay-contracts/blob/03d58d8fd5630235a275318fa482b582e1aed33a/truffle.js#L77)
      // the instanbul version is being used (the previous one to berlin), seemingly making it the latest evmVersion
      // supported by both RSK and ETH networks.
      evmVersion: "istanbul",
      optimizer: {
        enabled: true,
        runs: 200,
        // No-ops added by solidity-coverage were causing the compilation of the contracts to fail
        // This config as solution was obtained from: https://github.com/ethereum/solidity/issues/10354#issuecomment-847407103
        details: {
          yul: true,
          yulDetails: {
            stackAllocation: true,
          },
        }
      }  
    }
  }
};
