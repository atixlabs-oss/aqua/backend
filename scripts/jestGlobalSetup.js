/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const Web3 = require('web3');

const { exec, execSync } = require('child_process');
const { testConfig } = require('config');
const Logger = require('../src/rest/logger');

async function waitUntilNodeIsUp(host) {
  const web3 = new Web3(new Web3.providers.HttpProvider(host));

  do {
    try {
      // eslint-disable-next-line no-await-in-loop
      if (await web3.eth.net.isListening()) {
        break;
      } else {
        Logger.error('Not connected for some reason');
      }
    } catch (e) {
      execSync('sleep 0.5');
    }
  } while (true);
}

async function runNode() {
  exec('npm run node >> /dev/null');

  await waitUntilNodeIsUp('http://localhost:8545');
}

module.exports = async () => {
  Logger.info('Running jest global setup');
  if (testConfig.hardhatNode.runOnTest) {
    await runNode();
  }
  Logger.info('Jest global setup finished');
};
