/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const { parse } = require('csv-parse');
const fetch = require("node-fetch");
const { MAXIMUM_MOVEMENTS_PER_REQUEST } = require('../src/rest/util/constants')
require('dotenv').config();

const logger = require('../src/rest/logger');

const readCSVFile = async (path, page) => {
  const p = new Promise(async (resolve, reject) => {
    let data = [];
    const offset = page === 0 ? 0 : 1;
    fs.createReadStream(path)
      .pipe(parse({
        delimiter: ",",
        columns: true,
        ltrim: true,
        from: MAXIMUM_MOVEMENTS_PER_REQUEST * page + offset,
        to: MAXIMUM_MOVEMENTS_PER_REQUEST * (page + 1)
      }))
      .on("data", row => { data.push(row) })
      .on("error", error => { reject(error) })
      .on("end", () => {
        resolve(data);
    });
  });
  return p;
};

const fileExists = (filepath) =>
  new Promise(resolve =>
    fs.access(filepath, fs.constants.F_OK, error => {
      if (error) resolve(false);
      resolve(true);
    }),
  );

const getMiddlewareAccounts = async (accountsUrl, middlewareId, apiKey) => {
  const formattedAccountsUrl = `${accountsUrl}/middleware?middlewareKey=${apiKey}&middlewareId=${middlewareId}`;
  const accountsByMiddleware = await fetch(
    formattedAccountsUrl,
    { 
      method: 'GET',
      headers: { 
        'Content-type': 'application/json'
      }
    }
  ).then(response => response.json());
  return accountsByMiddleware.map(account => account.id);
};

const uploadMovements = async (apiKey, uploadUrl, accountsUrl, accountId, csvPath) => {
  if (!(await fileExists(csvPath))) {
    throw new Error(`Please add the csv file under ${csvPath}`);
  }
  let page = 0;
  let isDataMaximumLength = true;
  const middlewareId = 1; // this will be updated when we can fetch the middlewareId by accounts
  const formattedUploadUrl = `${uploadUrl}?middlewareKey=${apiKey}&middlewareId=${middlewareId}`;
  const accountIds = await getMiddlewareAccounts(accountsUrl, middlewareId, apiKey);
  while (isDataMaximumLength) {
    const csvData = await readCSVFile(csvPath, page);
    const filteredData = csvData.filter(movement => accountIds.includes(movement.accountId));
    if (filteredData.length > 0) {
      const body = {
        movements: filteredData
      };
    
      console.log(filteredData);
  
      const response = await fetch(formattedUploadUrl, { 
        method: 'PUT',
        headers: { 
          'Content-type': 'application/json'
        },
        body: JSON.stringify(body)
      });
      console.log(response);
    }

    isDataMaximumLength = csvData.length == MAXIMUM_MOVEMENTS_PER_REQUEST;
    page++;
  }
}

const run = async () => {
  if (!process.env.MOVEMENTS_URL) {
    throw new Error(`Missing MOVEMENTS_URL variable`);
  }
  if (!process.env.CSV_MIDDLEWARE_KEY) {
    throw new Error(`Missing CSV_MIDDLEWARE_KEY variable`);
  }
  try {
    const csvPath = `${process.cwd()}/movements/demo_movement_1.csv`;
    // accountId parameter should be updated once middlewares and accounts have been properly connected
    await uploadMovements(process.env.CSV_MIDDLEWARE_KEY, process.env.MOVEMENTS_URL, process.env.ACCOUNTS_URL, 1, csvPath);
    logger.info('Success run!');
  } catch (error) {
    console.log(error);
  }
  process.exit(0);
};

run();
