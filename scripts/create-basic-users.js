/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { Pool } = require('pg');
const { Wallet } = require('ethers');
const bcrypt = require('bcrypt');
require('dotenv').config();

const { encrypt } = require('../src/rest/util/crypto');
const { encryption } = require('../src/rest/util/constants');

const logger = require('../src/rest/logger');

const key = process.env.CRYPTO_KEY;

const db =  {
  "name": process.env.DB_NAME || "name",
  "password": process.env.DB_PASSWORD || "password",
  "port": process.env.DB_PORT || 5432,
  "host": process.env.DB_HOST || "localhost",
  "user": process.env.DB_USER || "postgres"
};

const investor = {
  "firstName": "investor",
  "lastName": "investor",
  "email": "investor@agua.com",
  "password": "Aaaaaaaa2"
};
const beneficiary = {
  "firstName": "beneficiary",
  "lastName": "beneficiary",
  "email": "beneficiary@agua.com",
  "password": "Aaaaaaaa2"
};
const auditor = {
  "firstName": "auditor",
  "lastName": "auditor",
  "email": "auditor@agua.com",
  "password": "Aaaaaaaa2"
};

const createUser = async (user, pool) => {
  const usersResult = await pool.query(`SELECT * FROM "user" WHERE email=$1`, [user.email]);
  if ( usersResult.rows.length > 0 ) {
    await pool.query('COMMIT');
    process.exit(0);
  }
  logger.info('Creating wallet...');
  const wallet = Wallet.createRandom();
  const { mnemonic, address } = wallet;
  const encryptedWallet = await wallet.encrypt(user.password);
  const encryptedMnemonic = await encrypt(mnemonic.phrase, key);
  const hashedPassword = await bcrypt.hash(
    user.password,
    encryption.saltOrRounds
  );
  if (
    !encryptedMnemonic ||
    !encryptedMnemonic.encryptedData ||
    !encryptedMnemonic.iv
  )
    throw new Error('Mnemonic could not be encrypted');
  const params = [
    user.firstName,
    user.lastName,
    user.email,
    hashedPassword,
    address,
    encryptedWallet,
  ];
  logger.info('Creating user...');
  const result = await pool.query(
    `
  INSERT INTO "user"(
    "firstName",
    "lastName",
     email, 
     "password", 
     address, 
     "createdAt", 
     role,
     "isAdmin", 
     "blocked", 
     "phoneNumber", 
     company, 
     "countryId", 
     "encryptedWallet", 
     "forcePasswordChange", 
     "emailConfirmation", 
     id,
     first
  )
  VALUES($1, $2, $3, $4, $5, 
    now(), 'entrepreneur'::role_old, false, false, NULL::character varying,
    NULL::character varying, 10, $6, true,
    true,uuid_generate_v4(), false)
  RETURNING 
    id_old,
    id`,
    params
  );
  if (result.rows.length === 0) throw new Error('Could not create user');
}

const run = async () => {
  const pool = new Pool({
    user: db.user,
    database: db.name,
    password: db.password,
    port: db.port,
    host: db.host
  });
  try {
    await pool.connect();
    await pool.query('BEGIN TRANSACTION');
    await createUser(investor, pool);
    await createUser(auditor, pool);
    await createUser(beneficiary, pool);
    await pool.query('COMMIT');
    logger.info('Success run!');
  } catch (error) {
    logger.error('An error occured, rollbacking: ', error);
    await pool.query('ROLLBACK');
  }
  process.exit(0);
};

run();
