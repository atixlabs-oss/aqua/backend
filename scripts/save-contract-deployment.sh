#!/bin/bash

deploymentName=$1
chainId=$2

deploymentFolder="./deployments/${deploymentName}"
echo "Creating folder ${deploymentFolder}";
mkdir $deploymentFolder

echo "Copying artifacts and contract state to ${deploymentFolder}"
cp -r ./artifacts $deploymentFolder/artifacts
cp state.json $deploymentFolder/state.json

cp .openzeppelin/unknown-${chainId}.json .openzeppelin/${deploymentName}-${chainId}.json
