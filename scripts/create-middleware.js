/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Pool } = require('pg');
const fs = require('fs');
const uuid = require('uuid');
const bcrypt = require('bcrypt');
require('dotenv').config();

const logger = require('../src/rest/logger');
const { encryption } = require('../src/rest/util/constants');

const db =  {
  "name": process.env.DB_NAME || "name",
  "password": process.env.DB_PASSWORD || "password",
  "port": process.env.DB_PORT || 5432,
  "host": process.env.DB_HOST || "localhost",
  "user": process.env.DB_USER || "postgres"
};

const readJsonFile = (path) => {
  const file = fs.readFileSync(path, 'utf-8');
  return JSON.parse(file);
};

const fileExists = (filepath) =>
  new Promise(resolve =>
    fs.access(filepath, fs.constants.F_OK, error => {
      if (error) resolve(false);
      resolve(true);
    }),
  );

const createMiddleware = async (name, pool) => {
  const usersResult = await pool.query(`SELECT * FROM "middleware" WHERE name=$1`, [name]);
  if ( usersResult.rows.length > 0 ) {
    await pool.query('COMMIT');
    process.exit(0);
  }
  logger.info('Creating middleware...');
  const currentDirectory = process.cwd();
  const middlewareSpec = `${currentDirectory}/middlewares/${name}.json`;
  if (!(await fileExists(middlewareSpec))) {
    throw new Error(`Please add the middleware's json spec file under ${middlewareSpec}`);
  }
  const specFile = readJsonFile(middlewareSpec);
  const jsonSpec = JSON.stringify(specFile);
  const apiKey = uuid.v4(); // we should store this somewhere
  console.log("Middleware API Key: ", apiKey);
  const hashedKey = await bcrypt.hash(
    apiKey,
    encryption.saltOrRounds
  );
  const params = [
    name,
    jsonSpec,
    hashedKey
  ];
  const result = await pool.query(
    `INSERT INTO "middleware"(
      name,
      "json_spec",
      "api_key"
    ) VALUES($1, $2, $3)
    RETURNING id`,
    params
  );
  if (result.rows.length === 0) throw new Error('Could not create middleware');
}

const run = async () => {
  const pool = new Pool({
    user: db.user,
    database: db.name,
    password: db.password,
    port: db.port,
    host: db.host
  });
  try {
    await pool.connect();
    await pool.query('BEGIN TRANSACTION');
    // The name for the created Middleware should be changed.
    await createMiddleware("test", pool);
    await pool.query('COMMIT');
    logger.info('Success run!');
  } catch (error) {
    logger.error('An error occured, rollbacking: ', error);
    await pool.query('ROLLBACK');
  }
  process.exit(0);
};

run();