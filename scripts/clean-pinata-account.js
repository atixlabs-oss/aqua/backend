
const pinataSDK = require('@pinata/sdk');
const config = require('config');

const { apiKey, secretKey } = config.ipfsStorage.pinataOptions;


const untilDate = "2023-08-25T00:00:00.000Z";
const cleanPinataAccount = async () => {
    console.log(apiKey, secretKey)
    const pinata = new pinataSDK({ pinataApiKey: apiKey, pinataSecretApiKey: secretKey });
    const list = await pinata.pinList({ pageLimit: 1000 }).catch(e=> e)
    
    console.log(list.rows);
    

    console.log(await Promise.all(list.rows.filter((f)=> f.date_pinned <= untilDate).map((f)=> pinata.unpin(f.ipfs_pin_hash).catch(e => e))));
}

cleanPinataAccount();


