/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { exec } = require('child_process');
const { testConfig } = require('config');
const logger = require('../src/rest/logger');

async function stopNode() {
  // Kill the proccess that is running in the hardhat's node port
  exec(
    `pid=$(lsof -t -i :${
      testConfig.hardhatNode.port
    } -s TCP:LISTEN) && kill -9 $pid >> /dev/null`
  );
}

module.exports = async () => {
  logger.info('Running jest global teardown');
  if (testConfig.hardhatNode.runOnTest) {
    await stopNode();
  }
  logger.info('Jest global teardown finished');
};
