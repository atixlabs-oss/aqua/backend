#!/bin/sh

if [ $CREATE_ADMIN ]; then
    node ./initial-setup.js
fi

if [ $CREATE_BASIC_USERS ]; then
    node ./scripts/create-basic-users.js
fi

if [ $DEPLOY_CONTRACTS_LOCAL ]; then
    if [ ! -d "./deployments/local" ]; then
        npm run build
        npm run deploy -- --network local_docker
        yes | ./scripts/save-contract-deployment.sh local 31337
    fi
    yes | ./scripts/load-contract-deployment.sh local 31337
else
    # Load contract deployment
    yes | ./scripts/load-contract-deployment.sh "$CONTRACTS_DEPLOYMENT_NAME" "$CONTRACTS_CHAIN_ID"
fi


# Continue the execution of the following dockerfile commands
exec "$@"