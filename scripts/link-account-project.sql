-- project_id must ALWAYS be a parent project id and not a clone
INSERT INTO public.account(id, platform_name, currency, data, middleware_id, project_id) 
VALUES (uuid_generate_v4(), 'platform_name', 0, '{"data": "data"}', 0, 'project_uuid');