# Installation

## Before to install
Software base.

- git-core
- docker
- nvm
- node v12.22.12


## Backend
### Creating the database

The schema for the `coadb` database can be found in [schema.sql](./db/scripts/schema.sql).
Execute this script by running `psql -d postgres -a -f schema.sql` to create the database.

### Steps

- Clone backend repository.
```bash
$ git clone https://gitlab.com/atixlabs-oss/agua/backend.git
$ cd backend
$ npm install --force
```

- Configure env vars. Check [example](../.env.example)

- Complete seed admin email in setup file [setup-config](../setup-config.json)

- Check prerequisites to

```bash
cd scripts/ && ./verify-initial-setup.sh
```

- Install dependencies

```bash
npm i
```

- Run initial setup script

```bash
node initial-setup.js
```

- Start server

```bash
npm run index
```

## Development mode.
Follow the next steps for development mode.


### Setup Database

```bash
$ cd docker
$ sudo docker compose up -d
```

### ganache-cli

```bash
$ npm install -g ganache-cli
ganache-cli &
```

### Database Migrations
```bash
$ ./get_flyway.sh
```

#### Running migrations
```
$ ~/flyway-9.4.0/flyway -configFiles=flyway.conf -locations=filesystem:db/migrations migrate -outOfOrder=true
```

## .env file
```
JWT_SECRET=some_secret
JWT_EXPIRATION_TIME=3

# Database
DB_NAME=postgres
DB_USER=postgres
DB_PASSWORD=postgres
DB_HOST=localhost
DB_PORT=5432
DB_DIALECT=postgres

# Server
SERVER_HOST=api.yourhost.com
SERVER_PORT=3001
FRONTEND_URL=https://frontend.youhost.com


# Email
EMAIL_HOST=smtp.sendgrid.net
EMAIL_PORT=587
EMAIL_USER=user@mail.com
EMAIL_PASS=xxxxxxxx
EMAIL_FROM=noreply@mail.com
EMAIL_DISABLED=false
EMAIL_API_KEY=xxxxxxxxx

# Support
SUPPORT_RECOVERY_TIME=24

# File server
FILE_SERVER_PATH=.
FILE_SERVER_MAX_FILE_SIZE=20000000

# Crons
# checkContractBalancesCron
MAIN_ACCOUNT_BALANCE_EMAIL=from@example.com

# RSK testnet
BLOCKCHAIN_PRIV_KEY=0x0000000000000000000000000000000000000000
NODE_URL=http://localhost:4444

CRYPTO_KEY=xxxxxxxxxx
```