# Smart Contracts documentation


## Tools and frameworks

- solc@0.5.8
- hardhat@2.8.4

### Linter: solhint

- Run `npm run lint:contracts` to run the linter on the .sol files
- Most problems detected can be automatically fixed by running `npm run prettier:contracts`

### Testing w/coverage (solcover)

- Run `npm run test:contracts` to run all the smart contracts tests.
- Run `npm run coverage:contracts` to obtain the coverage results of the smart contracts.


## Using Hardhat

### Configuration

- The `hardhat` configuration can be found in [hardhat.config.js](../hardhat.config.js).
- Modify the `network` object inside this file to include any other blockchain configuration you may need.
- Use the `--network` option along with `npx hardhat` commands to use a different network (e.g, `npx hardhat deploy --network rskTestnet` deploys to the RSK testnet). If this flag is not passed then `hardhat` will be used. Current configured networks are:
  - _hardhat_: a local hardhat node used for running the tests and getting the contracts coverage.
  - _localhost_: a local hardhat node used for local deployments.
  - _rskTestnet_: the RSK testnet.
  - _rskMainnet_: the RSK mainnet.
  - _ethSepolia_: Sepolia, one of the ETH's testnets.
  - _ethMainnet_: the ETH mainnet.
- More logs can be obtained on the commands by configuring `hideLogs=false` on `config/($DEPLOYMENT).js`.

#### Mainnet/testnet configuration

Various environment variables have to be configured on the `.env` file for interacting with these networks:
- __BLOCKCHAIN_PRIV_KEY__: the private key of the wallet you want to interact with
- __NODE_URL__: the URL for the RPC API
- __BLOCKCHAIN_NETWORK__: the name of the blockchain you are going to use, optional (hardhat will be used if this envvar is not set)


### Tasks

Several hardhat tasks were included for interacting from CLI with the deployed contracts:
- __src/rest/services/helpers/hardhatClaimTasks.js__: tasks for interacting with the claim registry
- __src/rest/services/helpers/hardhatProjectTasks.js__: tasks for interacting with the projects registry
- __src/rest/services/helpers/hardhatTasks.js__: deployment tasks and other remainder

For example, a project can be created by running:
```bash
$ npx hardhat create-project --id 44 --ipfs-hash "0x1234"
```


## Deployment

**Smart contract deployment files**

Deployment are described through 3 files:
- `state.json`: contains the addresses of the deployed contracts per network.
- `.openzeppelin/${DEPLOYMENT_NAME}-${CHAIN_ID}.json_`_: openzeppelin information of the deployment
- `artifacts/`: artifacts resulting from the compilation of the contracts, with their ABI and bytecode

This files are both used by:
1. Hardhat: when running any of it's scripts or the tests.
2. Backend: for detecting where the smart contracts are currently deployed.

The scripts `./scripts/save-contract-deployment.sh` and `./scripts/load-contract-deployment.sh` allow switching from one deployment to the other without requiring manual configuration.

### New deployment

If the deployment is to mainnet/testnet, this requires having the [mainnet/testnet configurations](#mainnettestnet-configuration) done.

Given a desired NETWORK:
1. If NETWORK is `develop`, then start a hardhat node `npm run node`.
2. Compile the smart contracts by running `npx hardhat compile`.
3. Deploy the compiled contracts to the local network by running `npx hardhat deploy --network $NETWORK`.
   This will fail if the contracts were already deployed on the network, a new deployment can be forced by using `npx hardhat deploy --network $NETWORK --reset-states true`.
4. If this is a mainnet/testnet deployment, make sure of saving it by running `./scripts/save-contract-deployment.sh $DEPLOYMENT_NAME $CHAIN_ID`. Our recommended and used deployment names are `$DATE-$ENVIRONMENT-dep-$COMMIT`.

### Upgrade only the code from deployment

If the deployment is to mainnet/testnet, this requires having the [mainnet/testnet configurations](#mainnettestnet-configuration) done.

Given a desired NETWORK:
1. If NETWORK is `develop`, then start a hardhat node `npm run node`.
2. Load the deploy `./scripts/load-contract-deployment.sh $DEPLOYMENT_NAME`.
3. Compile the new version of the smart contracts by running `npx hardhat compile`.
4. Upgrade the currently the compiled contracts to the local network by running `npx hardhat upgradeToCurrentImpl --network $NETWORK`.
5. If this is a mainnet/testnet deployment, make sure of saving it by running `./scripts/save-contract-deployment.sh $DEPLOYMENT_NAME $CHAIN_ID`. Our recommended and used deployment names are `$DATE-$ENVIRONMENT-upg-$COMMIT`.


### Load mainnet/testnet deployment

Requires having the [mainnet/testnet configurations](#mainnettestnet-configuration) done.

A deployment with of a given DEPLOYMENT_NAME can be loaded by running the script `./scripts/load-contract-deployment.sh $DEPLOYMENT_NAME`.
Note that this overrides the current deployment, so it should be saved (as described on the last step of both deployment alternatives).
