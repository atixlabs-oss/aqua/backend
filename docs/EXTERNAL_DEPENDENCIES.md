# External dependencies

This documents describes the different external services used as a service, what are they used for and gives some guidance on how it could be changed.


## IPFS Node

The system uses IPFS network to store files with decentralization and immutablility warranties.
To do this, we could run a node by ourselves or we could use an external service like Pinata, which is the option we took.

To chose how to connect to IPFS, you should set the envvar `IPFS_CONNECTION_TYPE` to a value (currently we only support, either Pinata or have it disabled which does not upload the file to IPFS and may break some flows as the audit flow) of the following:

- `pinata`
- `disabled` (only intended for development)


### Pinata option

If you chose to use Pinata, you have to set the envvar `IPFS_CONNECTION_TYPE` to `pinata` and set the following envvars with the corresponding keys:

- PINATA_API_KEY=XXXXX
- PINATA_SECRET_KEY=XXXXX

### Implmenting a new option

To use a new option, you will have to create a new file inside the folder `./src/rest/ports/external_services/ipfs/implementations/` implementing the interface defined in `./src/rest/ports/external_services/ipfs/interface.js` and add the option to the switch based on the envvar `IPFS_CONNECTION_TYPE` defined in the factory at `./src/rest/ports/external_services/ipfs/factory.js`.

You should also explain here what envvars are needed for that option.

## Email

The system sends emails in various instances of the projects. To do this we use a mailing service that could be changed depending on the organization that is running the system. To change which mailing service it's being used, you should set the `EMAIL_CONNECTION_TYPE` to one of the allowed values:

- `sendgrid`
- `print` (only intended for development)
- `disabled` (only intended for development)

### Sendgrid

To use Sendgrid as the mailing service set `EMAIL_CONNECTION_TYPE` to `sendgrid`. On top of that you will need to set the following envars with the corresponding values to connect to Sendgrid's service:

- EMAIL_HOST=smtp.example.com
- EMAIL_PORT=587
- EMAIL_USER=username
- EMAIL_PASS=password
- EMAIL_FROM=from@example.com
- EMAIL_API_KEY=XXXX

### Print

This option will print to console the links of each email that is being "sent". This option is very useful if you want to develop locally as this will give you an easy way to access the links to activate new users.

### Implmenting a new option

To use a new option, you will have to create a new file inside the folder `./src/rest/ports/external_services/email/implementations/` implementing the interface defined in `./src/rest/ports/external_services/email/interface.js` and add the option to the switch based on the envvar `EMAIL_CONNECTION_TYPE` defined in the factory at `./src/rest/ports/external_services/email/factory.js`.

You should also explain here what envvars are needed for that option.

## Blockchain node/Blockchain

Since blockchain nodes follow a standard interface and that the node is absolutely critical, the only option is to set the connection string to the node you are chosing. This could either be through Alchemy, Infura, a local node, or any other option that follows the Eth JSON RPC standard.

To set the connection string, you will need to set the envvar `NODE_URL`, [deploy and use the contracts for that network](./smartcontracts.md), set the private key in `BLOCKCHAIN_PRIV_KEY`, and set the network you are going to use in `BLOCKCHAIN_NETWORK`(note that the value there should be a network configured in hardhat.base.config.js for it to be a valid option). 