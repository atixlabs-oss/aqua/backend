# Abstract

This document presents some aspects of the architecture of AGUA. It mainly focuses on the non-smart contract side of it. If you want to read about the Smart contracts, please head to its [own document](./smartcontracts.md).


# Table of Contents
1. [Application Diagram](#application-diagram)
3. [State Diagrams](#state-diagrams)
4. [Sequence Diagrams](#sequence-diagrams)
5. [Notorization](#notarization)
6. [Audit Page](#audit-page)

# Application Diagram


In the following diagram you will see the usual deployment of this solution.

![Alt text](./assets/Agua.png)

First of all, we have a proxy(traefik) which redirects calls to HTTPS and serves three different services:

1. Frontend
1. Backend
1. Wordpress



## Wordpress

The wordpress server is just a landing page which is intended to contain news about the platform. This is just a static file that has a widget that is inserted from the fronted. This widget shows highlighted projects.

## Frontend

The frontend (with the exception of the audit page, which is described in the following subsection) is a web-app that allows the user to explore, create, and edit a project as well as to add and audit activities and evidences inside the project. This is achieved through having a built-in wallet whose private key/mnemonic is NEVER sent to the backend unencrypyted. It is sent to the backend encrypted with a PIN when it is created and sent back to the frontend to be decrypted when needed. The interaction with the blockchain is made completely through the backend which acts as an indexer and as a transaction sender.

In the frontend, there are NO transactions signed. The signatures created in the frontend just sign the version of th project to be notarized. This signature is later embedded in a transaction paid by the backend's account.

### Audit page

The audit page is a webapp (which is now deployed as part of the frontend process, but could be made independent later) which allows anyone to verify, without the use of any other piece of infrastructure owned by the platform. It directly asks for the notarization events to a series of blockchain nodes and IPFS gateways.


This is more thorougly described in [this section](#audit-page-1)

## Backend

The backend is just a simple REST API created using Fastify and Node.js. 

The backend owns an account which is used to send notarization signatures to the contracts in every notarization event.

In turn, the data is stored in a PostgreSQL DB.


# State Diagrams

The following diagram represent the different states and transition that some entities in the system have.

## Project state diagram

Note that this is conceptual diagram (what the user ends up seeing), it may differ on the actual diagram because the clones and the parents don't share the same state. In the future, we will add a section on how clones work.

This diagram represents the state of each project. First, when a project is created, it is in `draft` state. Once it is all setup, the admin will publish it. This will transition it to a temporary state where it will wait for the transaction to be confirmed. Once it is confirmed, it will transition to `published`.


Note that if the transaction fails, the project will revert to draft. Also note that in the diagram, two different state are merged together (`published` and `in progress`). The only difference between these two is that at least one activity is `in progress`, but apart from that there is no other difference.

Once it is `published`/`in progress`, someone could want to edit or cancel the project. This will put the project in `open for review` state which is similar to the `draft` state in the sense that this change is not notarized.

Once the change is done and the user wants to publish, it is first moved to `pending review` temporarily and finally to `in review`.

After that, the admin might approve it, in any case, the project could be (after another temporary state) `completed` (if the missing activities are removed), `cancelled` (if it is cancelled) or `in progress` again (if neither of the previous).

![Alt text](./assets/projectStateDiagram.png)


## Milestones state diagram

Milestones have a pretty straightforward diagram. Its state directly depends on the states of its child activities.

![Alt text](./assets/milestoneStateDiagram.png)


## Activity state diagram

Activities are `new` when created and become `in progress` when the first evidence is added. After the user sends it to review, it becomes `pending to review` until the transaction is confirmed. If it is confirmed, it is moved to `to review` state. 


After that the auditor can either approve it (completing the activity after the blockchain confirmation) or rejecting it which after blockchain confirmation can be sent to be reviewed once again.




![Alt text](./assets/activityStateDiagram.png)

## Evidence state diagram

![Alt text](./assets/evidenceStateDiagram.png)



# Sequence Diagrams

![Alt text](./assets/publishProjectSequence.png)

![Alt text](./assets/editProjectSequence.png)

![Alt text](./assets/submitProjectSequence.png)

![Alt text](./assets/sendReviewActivity.png)


# Notarization

Notarization is performed through a structured JSON in which all metadata associated with a project is stored in the form of a history log.

This logic resides in the project's service, within the functions 'mapProjectToPublish' and 'uploadProjectMetadataToIPFS'.

The first function is responsible for fetching all current data from the 'projects' table, including milestones and activities.

The second function takes the output of the first function as an argument and does the following:

1. Retrieves the latest notarization of the project, saved as JSON in the project table.
2. Retrieves the current users assigned to the project.
3. Maps each user to the defined structure.
4. Maps each milestone to the defined structure.
5. For each milestone, maps each activity to the defined structure.
At this point, it checks against the last stored notarization if there is existing data about the proposer or the auditor. If affirmative, it is copied. If negative, it checks if that data is currently in the DB. In that case, it is added; otherwise, it remains null.
The purpose of this is to preserve the user's state at a specific moment, meaning when the action is carried out. Since the user might be unassigned or change some of their data in the future, such as the public key, we would lose part of the information.
6. For each milestone/activity, it looks for the evidence, which is mapped to the desired format. In this case, it also searches for the creator or auditor user in the last notarization, applying the mentioned criteria for the activity.
7. At each of these steps, possible intermediate states (when sent to the blockchain) are mapped to their final states. For example, a project that is 'pending published' at the time of notarization is mapped to 'published' to represent the real final state.
8. The last notarization (as we don't want it to be part of the notarization) is removed from the object, and the IDs are replaced by the public IDs of each entity. This way, regardless of the clone version we are viewing, the notarization will always show the same ID for the same entity.
9. Finally, it checks if the operation will result in a general project state change. For example, in the case of activity approval, if it completes the milestone and the project, changing their states, these last two states change.

Notarization structure

![Alt text](./assets/notarizationDiagram.png)

# Audit Page

Every time an action is taken in the project, its state is stored in an IPFS service, which is then recorded on the blockchain. This data is public and accessible to any user at any time. However, it is presented in a user-unfriendly format. For this reason, an audit page has been implemented on the frontend.

![Alt text](./assets/ipfsData.png)

The application features the audit page, which always compares two versions (notarizations) of a project. Through it, the project's history is clearly visualized, showing every change made with a date stamp and the involved users.

This page checks several events emitted by the contract using private or public nodes:

1. ProjectCreated (create a project)
2. ProjectEditProposed (request an edit/cancel for a project)
3. ProjectEditAudited (approve or reject an edit/cancel request)
4. ClaimProposed (send to review an activity)
5. ClaimAudited (approve or reject an activity)

From each of these events, the IPFS hash is extracted. Using this hash, the associated notarization is fetched through a pin provider. Then, the selected version is compared to its previous one, highlighting the modifications.

![Alt text](./assets/notarizationVersion0.png "A request for a change of project name")

In cases where the version prior to the selected one is an edit/cancellation rejected by the project, it is decided to skip this version. This is because it was a potential change that never materialized and therefore does not represent a real previous state of the project.
Nevertheless, it is possible to access the canceled version by selecting it and observing which changes were not made.

![Alt text](./assets/notarizationVersion1.png)

![Alt text](./assets/notarizationVersion2.png)

# Notarization schema

```yaml
Project:
  id: uuid
  projectName: string
  mission: string
  problemAddressed: string
  agreementFileHash: string
  proposalFileHash: string
  revision: number
  type: string
  isCancelRequested: boolean
  currency: string
  currencyType: string
  goalAmount: string
  timeframe: string
  timeframeUnit: string
  location: string
  status: string
  users:
    0:
      firstName: string
      lastName: string
      email: string
      createdAt: string
      id: string
      publicKey: string
      country: string
      role: string
    1:
      ...
    2:
      ...
  milestones:
    0:
      id: string
      title: string
      description: string
      status: string
      activities:
        0:
          id: string
          title: string
          description: string
          acceptanceCriteria: string
          type: string
          budget: string
          status: string
          auditedBy:
            firstName: string
            lastName: string
            email: string
            createdAt: string
            id: string
            publicKey: string
            country: string
            role: string
            auditedAt: string
          assignedAuditor:
            firstName: string
            lastName: string
            email: string
            createdAt: string
            id: string
            publicKey: string
            country: string
            role: string
          proposer:
            firstName: string
            lastName: string
            email: string
            createdAt: string
            id: string
            publicKey: string
            country: string
            role: string
          sentToReviewAt: string
          evidences:
            0:
              id: string
              title: string
              description: string
              type: string
              status: string
              createdAt: string
              createdBy:
                firstName: string
                lastName: string
                email: string
                createdAt: string
                id: string
                publicKey: string
                country: string
                role: string
              amount: string
              destinationAccount: string
              auditedAt: string
              auditor:
                firstName: string
                lastName: string
                email: string
                createdAt: string
                id: string
                publicKey: string
                country: string
                role: string
              hashEvidence: string
              linkEvidence: string
            1:
              ...
        1:
          ...
    1:
      ...

@endyaml
```