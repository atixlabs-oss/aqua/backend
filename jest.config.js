/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  rootDir: 'src',
  globalSetup: '../scripts/jestGlobalSetup.js',
  globalTeardown: '../scripts/jestGlobalTearDown.js',
  testMatch: ['<rootDir>/tests/**/*.js', '<rootDir>/plugins/tests/**/*.js'],
  testPathIgnorePatterns: [
    '<rootDir>/tests/contracts',
    '<rootDir>/tests/mockFiles',
    '<rootDir>/tests/testHelper.js',
    '<rootDir>/tests/mockModels.js',
    '<rootDir>/tests/externalApiResponse.mock.js'
  ],
  collectCoverageFrom: ['<rootDir>/rest/services/**'],
  coveragePathIgnorePatterns: [
    '<rootDir>/rest/services/eth/',
    '<rootDir>/rest/services/cronjob/',
    '<rootDir>/rest/services/helpers/hardhatTasks.js',
    '<rootDir>/rest/services/helpers/hardhatClaimTasks.js',
    '<rootDir>/rest/services/helpers/hardhatProjectTasks.js',
    '<rootDir>/rest/services/helpers/hardhatTaskHelpers.js',
    '<rootDir>/rest/services/helper.js',
    '<rootDir>/rest/services/helpers/emailClient.js'
  ],
  testTimeout: 60000
};
