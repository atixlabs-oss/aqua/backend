# Summary

Date : 2023-08-09 16:19:59

Directory /Users/gonzalo.petraglia/projects/agua/backend/src/contracts

Total : 10 files,  371 codes, 411 comments, 95 blanks, all 877 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Solidity | 10 | 371 | 411 | 95 | 877 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 10 | 371 | 411 | 95 | 877 |
| . (Files) | 2 | 224 | 105 | 42 | 371 |
| interfaces | 2 | 70 | 140 | 16 | 226 |
| mocks | 3 | 29 | 64 | 14 | 107 |
| mocks/upgrades | 3 | 29 | 64 | 14 | 107 |
| utils | 3 | 48 | 102 | 23 | 173 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)