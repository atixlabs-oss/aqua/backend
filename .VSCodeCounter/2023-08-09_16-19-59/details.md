# Details

Date : 2023-08-09 16:19:59

Directory /Users/gonzalo.petraglia/projects/agua/backend/src/contracts

Total : 10 files,  371 codes, 411 comments, 95 blanks, all 877 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/contracts/ClaimsRegistry.sol](/src/contracts/ClaimsRegistry.sol) | Solidity | 135 | 51 | 20 | 206 |
| [src/contracts/ProjectsRegistry.sol](/src/contracts/ProjectsRegistry.sol) | Solidity | 89 | 54 | 22 | 165 |
| [src/contracts/interfaces/IClaimsRegistry.sol](/src/contracts/interfaces/IClaimsRegistry.sol) | Solidity | 48 | 79 | 9 | 136 |
| [src/contracts/interfaces/IProjectsRegistry.sol](/src/contracts/interfaces/IProjectsRegistry.sol) | Solidity | 22 | 61 | 7 | 90 |
| [src/contracts/mocks/upgrades/MockClaimsRegistryV1.sol](/src/contracts/mocks/upgrades/MockClaimsRegistryV1.sol) | Solidity | 11 | 21 | 5 | 37 |
| [src/contracts/mocks/upgrades/MockProjectsRegistryV1.sol](/src/contracts/mocks/upgrades/MockProjectsRegistryV1.sol) | Solidity | 11 | 21 | 5 | 37 |
| [src/contracts/mocks/upgrades/VariableStorage.sol](/src/contracts/mocks/upgrades/VariableStorage.sol) | Solidity | 7 | 22 | 4 | 33 |
| [src/contracts/utils/SignatureVerifier.sol](/src/contracts/utils/SignatureVerifier.sol) | Solidity | 17 | 39 | 10 | 66 |
| [src/contracts/utils/StringUtils.sol](/src/contracts/utils/StringUtils.sol) | Solidity | 10 | 17 | 3 | 30 |
| [src/contracts/utils/UpgradeableToV1.sol](/src/contracts/utils/UpgradeableToV1.sol) | Solidity | 21 | 46 | 10 | 77 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)