/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = {
  fileServer: {
    filePath: require('path').join(__dirname, '../src/tests/mockFiles'),
    maxFileSize: 5000000
  },
  frontendUrl: '//test',
  crypto: {
    key: '3c50cffcdce9a802a26f5293aa4dc689' // added to run tests
  },
  balancesConfig: {
    email: 'a@fake.email',
    default: {
      targetBalance: '4',
      balanceThreshold: '2'
    },
    coa: {
      targetBalance: '1.6',
      balanceThreshold: '0.8'
    },
    claimRegistry: {
      targetBalance: '0.4',
      balanceThreshold: '0.2'
    },
    daos: {
      targetBalance: '2',
      balanceThreshold: '1'
    }
  }
};
