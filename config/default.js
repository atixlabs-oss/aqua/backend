/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const routeTags = require('../src/rest/util/routeTags');
const cronExpressions = require('../src/rest/services/cronjob/cronExpressions');

const SECONDS_IN_A_DAY = 86400;

require('dotenv').config();

module.exports = {
  server: {
    host: process.env.SERVER_HOST,
    port: process.env.SERVER_PORT,
    headers: {
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Headers': [
        'Origin, X-Requested-With, Content-Type, Accept'
      ]
    },
    isHttps: false,
    domain: 'localhost',
    hideLogs: false
  },
  frontendUrl: process.env.FRONTEND_URL,
  organizationName: process.env.ORGANIZATION_NAME,
  email: {
    connectionType: process.env.EMAIL_CONNECTION_TYPE,
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
    pool: true,
    debug: false,
    log: false,
    apiKey: process.env.EMAIL_API_KEY, // sendgrid apikey, when undefined uses smtp
    from: process.env.EMAIL_FROM,
    disabled: process.env.EMAIL_DISABLED === 'true',
    print: process.env.EMAIL_PRINT === 'true'
  },
  support: {
    recoveryTime: process.env.SUPPORT_RECOVERY_TIME // in hours
  },

  jwt: {
    secret: process.env.JWT_SECRET,
    expirationTime: process.env.JWT_EXPIRATION_TIME // in months
  },

  database: {
    adapter: require('sails-postgresql'),
    adapterType: 'postgresql',
    database: {
      name: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT
    },
    decoratorName: 'models',
    modelPath: require('path').join(__dirname, '../db/models'),
    modelDefaults: {
      datastore: 'default',
      fetchRecordsOnCreate: true,
      fetchRecordsOnUpdate: true
    }
  },

  fileServer: {
    filePath: process.env.FILE_SERVER_PATH || '.',
    maxFileSize: process.env.FILE_SERVER_MAX_FILE_SIZE
  },

  swagger: {
    routePrefix: '/documentation',
    exposeRoute: true,
    swagger: {
      info: {
        title: 'AGUA API',
        description: 'AGUA API Documentation',
        version: '0.1.0'
      },
      host: 'localhost:3001',
      schemes: ['http', 'json'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: Object.values(routeTags)
    }
  },

  crons: {
    disableAll: false,
    transitionProjectStatusJob: {
      cronTime: cronExpressions.EVERYDAY_AT_MIDNIGHT,
      disabled: false,
      runOnInit: false,
      timezone: undefined
    },
    checkFailedTransactionsJob: {
      cronTime: cronExpressions.EVERY_HOUR,
      disabled: false,
      runOnInit: false,
      timezone: undefined
    },
  },
  balancesConfig: {
    email: process.env.MAIN_ACCOUNT_BALANCE_EMAIL,
    default: {
      targetBalance: '0.01',
      balanceThreshold: '0.005'
    },
    coa: {
      targetBalance: '0.01',
      balanceThreshold: '0.005'
    },
    claimRegistry: {
      targetBalance: '0.001',
      balanceThreshold: '0.0005'
    },
    daos: {
      targetBalance: '0.005',
      balanceThreshold: '0.0025'
    }
  },
  daoPeriodConfig: {
    periodDuration: 17280,
    votingPeriodLength: 35,
    gracePeriodLength: 35
  },

  defaultProjectTimes: {
    minimumUnit: 'days',
    consensusSeconds: 10 * SECONDS_IN_A_DAY, // TODO: define this
    fundingSeconds: 10 * SECONDS_IN_A_DAY // TODO: define this
  },

  hardhat: {
    defaultNetwork: process.env.BLOCKCHAIN_NETWORK || 'hardhat',
    node_url: process.env.NODE_URL || '',
    blockchain_priv_key: process.env.BLOCKCHAIN_PRIV_KEY || '0x0000000000000000000000000000000000000000000000000000000000000000'
  },
  explorerLink: 'https://explorer.testnet.rsk.co',
  crypto: {
    key: process.env.CRYPTO_KEY
  },
  ipfsStorage: {
    connectionType: process.env.IPFS_CONNECTION_TYPE,
    pinataOptions: {
      apiKey: process.env.PINATA_API_KEY,
      secretKey: process.env.PINATA_SECRET_KEY,
    },
  },
  testConfig: {
    contractTestTimeoutMilliseconds: 10 * 60 * 1000,
    hardhatNode: {
      runOnTest: false,
      port: 8545
    },
    relayer: {
      runOnTest: false,
      port: 8546,
      devMode: true,
      quiet: true
    }
  },
  movement: {
    validationLimits: {
      accounts: 60,
      txid: 100,
      decimals: 18
    }
  }
};
