/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const axios = require('axios');
const blockchainService = require('../../rest/services/blockchainService');
const tokenService = require('../../rest/services/tokenService');
const { txTypes } = require('../../rest/util/constants');
const { API_RESPONSE } = require('../externalApiResponse.mock');

jest.mock('../../rest/services/tokenService', () => ({
  getTokenBySymbol: jest.fn(() =>
    Promise.resolve({
      id: 1,
      name: 'ETC',
      symbol: 'ETC',
      decimals: 18
    })
  )
}));

jest.mock('axios');

describe('Testing blockchainService', () => {
  const ADDRESS = '0x166c8dbcd7447c1fcd265130d3d278d47a3bc7b2';
  axios.get.mockResolvedValue(API_RESPONSE);
  describe('Testing getTransactions', () => {
    it('Should return all sent transactions', async () => {
      const response = await blockchainService.getTransactions({
        currency: 'ETC',
        address: ADDRESS,
        type: txTypes.SENT
      });
      expect(response.transactions).toMatchSnapshot();
      expect(
        response.transactions.every(transaction => transaction.value !== '0')
      ).toBeTruthy();
      expect(
        response.transactions.every(transaction => transaction.from === ADDRESS)
      ).toBeTruthy();
    });

    it('Should return all received transactions', async () => {
      const response = await blockchainService.getTransactions({
        currency: 'ETC',
        address: ADDRESS,
        type: txTypes.RECEIVED
      });
      expect(response.transactions).toMatchSnapshot();
      expect(
        response.transactions.every(transaction => transaction.value !== '0')
      ).toBeTruthy();
      expect(
        response.transactions.every(transaction => transaction.to === ADDRESS)
      ).toBeTruthy();
    });
  });

  describe('Testing isNativeToken', () => {
    const ETH = {
      id: 1,
      name: 'ETH',
      symbol: 'ETH',
      decimals: 18,
      apiBaseUrl: 'https://eth-goerli.blockscout.com/api',
      contractAddress: null,
      deleted: false
    };

    const USDT = {
      id: 3,
      name: 'Tether USD',
      symbol: 'USDT',
      decimals: 6,
      apiBaseUrl: 'https://eth-goerli.blockscout.com/api',
      contractAddress: '0xdAC17F958D2ee523a2206206994597C13D831ec7',
      deleted: false
    };
    it('Should return when is native token', async () => {
      const getTokenBySymbolSpy = jest
        .spyOn(tokenService, 'getTokenBySymbol')
        .mockResolvedValue(ETH);

      const booleanResponse = await blockchainService.isNativeToken('ETH');
      expect(booleanResponse).toBeTruthy();
      expect(getTokenBySymbolSpy).toHaveBeenCalledWith('ETH');
    });

    it('Should return when is not native token', async () => {
      const getTokenBySymbolSpy = jest
        .spyOn(tokenService, 'getTokenBySymbol')
        .mockResolvedValue(USDT);

      const booleanResponse = await blockchainService.isNativeToken('USDT');
      expect(booleanResponse).toBeFalsy();
      expect(getTokenBySymbolSpy).toHaveBeenCalledWith('USDT');
    });
  });
});
