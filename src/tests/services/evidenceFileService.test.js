const { injectMocks } = require('../../rest/util/injection');
const evidenceFileService = require('../../rest/services/evidenceFileService');

describe('Testing evidenceFileService', () => {
  const evidenceFileDao = {
    saveEvidenceFile: jest.fn()
  };
  describe('saveEvidenceFile', () => {
    beforeAll(() => {
      injectMocks(evidenceFileService, { evidenceFileDao });
    });
    it('should successfully save evidence file', async () => {
      const saveEvidenceFileSpy = jest.spyOn(
        evidenceFileDao,
        'saveEvidenceFile'
      );
      await evidenceFileService.saveEvidenceFile('evidenceFile');
      expect(saveEvidenceFileSpy).toHaveBeenCalled();
    });
  });
});
