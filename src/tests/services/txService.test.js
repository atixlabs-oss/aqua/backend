/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { coa } = require('hardhat');
const { injectMocks } = require('../../rest/util/injection');
const originalTxService = require('../../rest/services/txService');
const { TX_STATUS, ACTION_TYPE } = require('../../rest/util/constants');
const approveActivityConfirmation = require('../../rest/services/transactions/success-handler/approveActvityConfirmation');
const approveOrRejectActivityRollback = require('../../rest/services/transactions/fail-handler/approveOrRejectActivityRollback');

jest.mock(
  '../../rest/services/transactions/success-handler/approveActvityConfirmation'
);
jest.mock(
  '../../rest/services/transactions/fail-handler/approveOrRejectActivityRollback'
);

let txService = Object.assign({}, originalTxService);
const restoreTxService = () => {
  txService = Object.assign({}, originalTxService);
};

describe('Testing txService', () => {
  const txDao = {
    updateTransaction: jest.fn(),
    createTx: jest.fn(),
    findAllPendingTransactions: jest.fn()
  };

  const changelogService = {
    updateChangelog: jest.fn()
  };

  const mailService = {
    sendTxFailedMail: jest.fn()
  };

  describe('Testing findAllPendingTransactions', () => {
    beforeAll(() => {
      restoreTxService();
      injectMocks(txService, { txDao });
    });
    it('should call findAllPendingTransactions', async () => {
      const findAllPendingTransactionsSpy = jest.spyOn(
        txDao,
        'findAllPendingTransactions'
      );
      await txService.findAllPendingTransactions();
      expect(findAllPendingTransactionsSpy).toHaveBeenCalled();
    });
  });
  describe('Testing hasFailed', () => {
    const txReceipt = { status: 1 };
    beforeAll(() => {
      restoreTxService();
      injectMocks(txService, { txDao });
      coa.getTransactionReceipt = jest.fn(() => txReceipt);
    });
    it('should resolve false', async () => {
      await expect(txService.hasFailed('txHash')).resolves.toEqual(false);
    });
    it('should resolve true', async () => {
      coa.getTransactionReceipt = jest.fn(() => undefined);
      await expect(txService.hasFailed('txHash')).resolves.toEqual(true);
    });
    it('should resolve true', async () => {
      coa.getTransactionReceipt = jest.fn(() => ({ status: 0 }));
      await expect(txService.hasFailed('txHash')).resolves.toEqual(true);
    });
  });
  describe('Testing hasVerified', () => {
    const previousBlocks = process.env.BLOCKS_NUMBER_TO_WAIT;
    beforeAll(() => {
      restoreTxService();
      injectMocks(txService, { txDao });
      process.env.BLOCKS_NUMBER_TO_WAIT = 2;
    });
    afterAll(() => {
      process.env.BLOCKS_NUMBER_TO_WAIT = previousBlocks;
    });
    it('should resolve true', async () => {
      await expect(txService.hasVerified(3000, 4010)).resolves.toEqual(true);
    });
    it('should resolve false', async () => {
      await expect(txService.hasVerified(3000, 3000)).resolves.toEqual(false);
    });
  });
  describe('Testing updateTransaction', () => {
    beforeAll(() => {
      restoreTxService();
      injectMocks(txService, { txDao });
    });
    it('should update transaction', async () => {
      const updateTransactionSpy = jest.spyOn(txDao, 'updateTransaction');
      await txService.updateTransaction({ id: 1, status: TX_STATUS.CONFIRMED });
      expect(updateTransactionSpy).toHaveBeenCalled();
    });
  });
  describe('Testing createTransaction', () => {
    beforeAll(() => {
      restoreTxService();
      injectMocks(txService, { txDao });
    });
    it('should create transaction', async () => {
      const createTxSpy = jest.spyOn(txDao, 'createTx');
      await txService.createTransaction({
        hash: 'hash',
        action: ACTION_TYPE.ADD_ACTIVITY,
        status: TX_STATUS.CONFIRMED,
        data: {}
      });
      expect(createTxSpy).toHaveBeenCalled();
    });
  });
  describe('Testing processPendingTransactions', () => {
    const transactionsMock = [
      { id: 1, hash: 'hash', action: ACTION_TYPE.APPROVE_ACTIVITY, data: {} }
    ];
    beforeAll(() => {
      restoreTxService();
      injectMocks(txService, { txDao, changelogService, mailService });
      coa.getTransactionResponse = jest.fn(() => ({ blockNumber: 3000 }));
      jest
        .spyOn(txService, 'findAllPendingTransactions')
        .mockReturnValue(transactionsMock);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    const currentBlock = 3000;
    it('should confirm the action', async () => {
      jest.spyOn(txService, 'hasVerified').mockReturnValue(true);
      jest.spyOn(txService, 'hasFailed').mockReturnValue(false);
      const updateChangelogSpy = jest.spyOn(
        changelogService,
        'updateChangelog'
      );
      approveActivityConfirmation.approveActivityConfirmation.mockImplementation(
        () => Promise.resolve()
      );
      const approveActivityConfirmationSpy = jest.spyOn(
        approveActivityConfirmation,
        'approveActivityConfirmation'
      );
      txService.updateTransaction = jest.fn();
      const updateTransactionSpy = jest.spyOn(txService, 'updateTransaction');
      await txService.processPendingTransactions(currentBlock);

      expect(updateChangelogSpy).toHaveBeenCalled();
      expect(updateTransactionSpy).toHaveBeenCalled();
      expect(approveActivityConfirmationSpy).toHaveBeenCalled();
    });
    it('should rollback the action', async () => {
      jest.spyOn(txService, 'hasVerified').mockReturnValue(true);
      jest.spyOn(txService, 'hasFailed').mockReturnValue(true);
      const updateChangelogSpy = jest.spyOn(
        changelogService,
        'updateChangelog'
      );
      const sendTxFailedMailSpy = jest.spyOn(mailService, 'sendTxFailedMail');
      approveOrRejectActivityRollback.approveOrRejectActivityRollback.mockImplementation(
        () => Promise.resolve()
      );
      const rollbackSpy = jest.spyOn(
        approveOrRejectActivityRollback,
        'approveOrRejectActivityRollback'
      );
      txService.updateTransaction = jest.fn();
      const updateTransactionSpy = jest.spyOn(txService, 'updateTransaction');
      await txService.processPendingTransactions(currentBlock);

      expect(updateChangelogSpy).toHaveBeenCalled();
      expect(updateTransactionSpy).toHaveBeenCalled();
      expect(sendTxFailedMailSpy).toHaveBeenCalled();
      expect(rollbackSpy).toHaveBeenCalled();
    });
    it('should do nothing if the transaction is not verified', async () => {
      jest.spyOn(txService, 'hasVerified').mockReturnValue(false);
      await txService.processPendingTransactions(currentBlock);
      const updateTransactionSpy = jest.spyOn(txService, 'updateTransaction');
      const updateChangelogSpy = jest.spyOn(
        changelogService,
        'updateChangelog'
      );
      expect(updateTransactionSpy).not.toHaveBeenCalled();
      expect(updateChangelogSpy).not.toHaveBeenCalled();
    });
  });
});
