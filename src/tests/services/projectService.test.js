/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require('jest-fetch-mock').enableMocks();
const { run, coa } = require('hardhat');
const COAError = require('../../rest/errors/COAError');
const {
  userRoles,
  projectStatuses,
  rolesTypes,
  ACTION_TYPE,
  PROJECT_TYPES,
  ACTIVITY_TYPES,
  TX_STATUS,
  ACTIVITY_STATUS,
  PROJECT_STEPS,
  MILESTONE_STATUS,
  pinStatus
} = require('../../rest/util/constants');
const errors = require('../../rest/errors/exporter/ErrorExporter');
const validateMtype = require('../../rest/services/helpers/validateMtype');
const validatePhotoSize = require('../../rest/services/helpers/validatePhotoSize');
const { getMessageHash } = require('../../rest/services/helpers/hardhatTaskHelpers');

const { injectMocks } = require('../../rest/util/injection');

const storage = require('../../rest/util/storage');
const files = require('../../rest/util/files');
const originalProjectService = require('../../rest/services/projectService');
const { default: BigNumber } = require('bignumber.js');
const originalCountryService = require('../../rest/services/countryService');

let projectService = Object.assign({}, originalProjectService);
const restoreProjectService = () => {
  projectService = Object.assign({}, originalProjectService);
};

const projectName = 'validProjectName';
const location = 'Argentina';
const timeframe = '12';
const timeframeUnit = '30';
const highlighted = 'false';
const published = 'true';
const goalAmount = 124123;
const mission = 'mission';
const problemAddressed = 'the problem';
const currencyType = 'Crypto';
const currency = 'ETH';
const additionalCurrencyInformation =
  '0x32Be343B94f860124dC4fEe278FDCBD38C102D88';
const coverPhotoPath = 'detail.jpeg';
const proposal = 'proposal';
const ownerId = 2;
const file = { name: 'project.jpeg', size: 1234 };
const task1 = {
  id: 1,
  oracle: '0x11111111',
  description: 'Task 1 Description',
  reviewCriteria: 'Task 1 Review',
  category: 'Task 1 Category',
  keyPersonnel: 'Task 1 KeyPersonnel',
  budget: 3500
};
const task2 = {
  id: 2,
  oracle: '0x22222222',
  description: 'Task 2 Description',
  reviewCriteria: 'Task 2 Review',
  category: 'Task 2 Category',
  keyPersonnel: 'Task 2 KeyPersonnel',
  budget: 1000
};
const task3 = {
  id: 3,
  oracle: '0x33333333',
  description: 'Task 3 Description',
  reviewCriteria: 'Task 3 Review',
  category: 'Task 3 Category',
  keyPersonnel: 'Task 3 KeyPersonnel',
  budget: 500
};

const task4 = {
  id: 4,
  oracle: '0x33333333',
  description: 'Task 3 Description',
  reviewCriteria: 'Task 3 Review',
  category: 'Task 3 Category',
  keyPersonnel: 'Task 3 KeyPersonnel',
  budget: 500,
  status: ACTIVITY_STATUS.PENDING_TO_REVIEW
};

const milestone = {
  id: 2,
  projectId: 21,
  description: 'Milestone description',
  tasks: [task1, task2, task3]
};

const milestoneOfExecutingProject = {
  id: 3,
  projectId: 17,
  description: 'Milestone description',
  tasks: [task1, task2, task3]
};

const milestoneWithPendingStatusTask = {
  id: 5,
  projectId: 30,
  description: 'Milestone description',
  tasks: [task4]
};

const evidenceOfTask1 = {
  id: 1,
  task: task1.id,
  description: 'evidenceOfTask1'
};

const evidenceOfTask2 = {
  id: 2,
  task: task2.id,
  description: 'evidenceOfTask2'
};

const evidence2OfTask2 = {
  id: 3,
  task: task2.id,
  description: 'evidence2OfTask2'
};

// evidence files
const fileOfevidenceOfTask1 = {
  fileId: 1,
  evidence: evidenceOfTask1.id,
  id: 1
};

const pdfFile = { name: 'file.pdf', size: 1234 };

const genericUser = {
  id: 1,
  firstName: 'Pablo',
  lastName: 'Perez',
  email: 'pablo.perez@globant.com',
  address: '0x02222222',
  isAdmin: false
};

const adminUser = {
  id: 2,
  firstName: 'Admin',
  lastName: 'user',
  role: userRoles.COA_ADMIN,
  email: 'admin@email.com',
  address: '0x02222222',
  isAdmin: true
};

const pendingProject = {
  id: 3,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'cardPhotoPath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.IN_REVIEW,
  milestones: [milestone],
  milestonePath: 'milestonePath',
  dataComplete: 1
};

const draftProjectWithMilestone = {
  id: 10,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'cardPhotoPath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.NEW,
  milestones: [milestone],
  milestonePath: 'milestonePath'
};

const draftProject = {
  id: 1,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'cardPhotoPath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.IN_PROGRESS,
  parent: null
};

const draftProjectFirstUpdate = {
  id: 20,
  projectName,
  goalAmount: 0,
  dataComplete: 0,
  owner: 4,
  status: projectStatuses.DRAFT
};

const draftProjectSecondUpdate = {
  id: 21,
  projectName,
  location,
  timeframe,
  timeframeUnit,
  goalAmount,
  dataComplete: 15,
  owner: 4,
  cardPhotoPath: 'cardPhotoPath',
  agreementFilePath: 'agreementFileHash',
  proposalFilePath: 'proposalFilePath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.DRAFT
};

const executingProject = {
  id: 15,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'path/to/cardPhoto.jpg',
  coverPhotoPath: 'path/to/coverPhoto.jpg',
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.DRAFT,
  milestones: [milestone],
  milestonePath: 'path/to/milestone.xls',
  txHash: '0x151515',
  address: '0x151515'
};

const inprogressProject = {
  id: 17,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'path/to/cardPhoto.jpg',
  coverPhotoPath: 'path/to/coverPhoto.jpg',
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.IN_PROGRESS,
  milestones: [milestone],
  milestonePath: 'path/to/milestone.xls',
  txHash: '0x151515',
  address: '0x151515'
};

const nonGenesisProject = {
  id: 16,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'path/to/cardPhoto.jpg',
  coverPhotoPath: 'path/to/coverPhoto.jpg',
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.DRAFT,
  milestones: [milestone],
  milestonePath: 'path/to/milestone.xls',
  txHash: '0x151515',
  address: '0x151515',
  parent: 1,
  revision: 1
};

const inReviewProject = {
  id: 18,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'path/to/cardPhoto.jpg',
  coverPhotoPath: 'path/to/coverPhoto.jpg',
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.IN_REVIEW,
  milestones: [milestone],
  milestonePath: 'path/to/milestone.xls',
  txHash: '0x151515',
  address: '0x151515',
  parent: inprogressProject.id,
  review: 2
};

const projectWithPendingStatusActivity = {
  id: 30,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'path/to/cardPhoto.jpg',
  coverPhotoPath: 'path/to/coverPhoto.jpg',
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.IN_PROGRESS,
  milestones: [milestoneWithPendingStatusTask],
  milestonePath: 'path/to/milestone.xls',
  txHash: '0x151515',
  address: '0x151515',
  parent: null,
  review: 2
};

const consensusProject = {
  id: 4,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'cardPhotoPath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.CONSENSUS
};

const projectWithTransfer = {
  id: 6,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'cardPhotoPath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.FUNDING
};

const toReviewProject = {
  id: 8,
  projectName,
  location,
  timeframe,
  goalAmount,
  owner: ownerId,
  cardPhotoPath: 'cardPhotoPath',
  coverPhotoPath,
  problemAddressed,
  proposal,
  mission,
  status: projectStatuses.IN_REVIEW
};

const openReviewProject = {
  id: 22,
  status: projectStatuses.OPEN_REVIEW,
  revision: 1,
  parent: '1',
  step: 0
};

const beneficiaryRole = {
  id: 1,
  description: rolesTypes.BENEFICIARY
};

const userService = {
  getUserById: id => {
    if (id === 2 || id === 3) {
      return {
        id,
        role: userRoles.ENTREPRENEUR,
        firstName: 'pepe',
        lastName: 'pepe'
      };
    }
    if (id === 4) {
      return {
        id,
        role: userRoles.COA_ADMIN
      };
    }
    throw new COAError(errors.common.CantFindModelWithId('user', id));
  }
};

const projectDao = {
  saveProject: project => {
    if (
      project.projectName === 'validProjectName' ||
      project.projectName === 'Untitled'
    ) {
      return {
        id: 1,
        revision: 1
      };
    }
    return undefined;
  },
  updateProject: (fields, projectId) => {
    if (
      projectId === 1 ||
      projectId === 3 ||
      projectId === 4 ||
      projectId === 8 ||
      projectId === 10 ||
      projectId === 20 ||
      projectId === 21
    ) {
      return {
        projectName: 'projectUpdateado',
        ...fields,
        id: projectId
      };
    }
    return undefined;
  },
  findById: id => {
    if (id === 1) return draftProject;
    if (id === 3) return pendingProject;
    if (id === 4) return consensusProject;
    if (id === 10) return draftProjectWithMilestone;
    if (id === 15) return executingProject;
    if (id === 8) return toReviewProject;
    if (id === 20) return draftProjectFirstUpdate;
    if (id === 21) return draftProjectSecondUpdate;
    if (id === 22) return openReviewProject;
    return undefined;
  },
  findProjectsWithTransfers: () => [projectWithTransfer]
};

const walletHistoryDao = {
  findAddressesByUser: jest.fn(() => Promise.resolve([]))
}

const milestoneService = {
  createMilestones: (milestonePath, projectId) => {
    if (projectId === 1) {
      return [milestone];
    }
    if (projectId === 10) {
      return [milestone];
    }
  },
  getAllMilestonesByProject: projectId => {
    if (projectId === 3 || projectId === 15) {
      return [milestone];
    }
    return undefined;
  },

  removeMilestonesFromProject: projectId => {
    if (projectId === 10) {
      return [milestone];
    }
  }
};

const mailService = {
  sendProjectStatusChangeMail: jest.fn(),
  sendEmails: jest.fn()
};

const changelogService = {
  createChangelog: jest.fn(() => Promise.resolve({ id: 1 })),
  getChangelog: jest.fn(() => [{ project: { id: 1 } }]),
  deleteProjectChangelogs: jest.fn()
};

const storageService = {
  saveStorageData: jest.fn()
};

const activityService = {
  thereArePendingActivityTransactions: jest.fn()
};

describe('Project Service Test', () => {
  let dbRole = [];
  let dbUserProject = [];
  let dbProject = [];
  let dbUser = [];
  let dbMilestone = [];
  let dbTask = [];
  let dbEvidence = [];
  let dbFileEvidence = [];

  const resetDb = () => {
    dbRole = [];
    dbUserProject = [];
    dbProject = [];
    dbUser = [];
    dbMilestone = [];
    dbTask = [];
    dbEvidence = [];
    dbFileEvidence = [];
  };

  const roleService = {
    getRoleByDescription: description =>
      Promise.resolve(dbRole.find(role => role.description === description))
  };

  const userProjectService = {
    getBeneficiaryByProjectId: ({ projectId }) => {
      const role = dbRole.find(r => r.description === rolesTypes.BENEFICIARY);
      const userProject = dbUserProject.find(
        up => up.project === projectId && up.role === role.id
      );
      if (!userProject) return undefined;
      const user = dbUser.find(u => u.id === userProject.user);
      return {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName
      };
    },
    validateUserWithRoleInProject: jest.fn(),
    getUserProjectFromRoleDescription: jest.fn()
  };

  beforeEach(() => resetDb());

  beforeAll(async () => {
    files.saveFile = jest.fn();
    files.validateAndSaveFile = jest.fn((type, fileToSave) => {
      validateMtype(type, fileToSave);
      validatePhotoSize(fileToSave);
      return '/path/to/file';
    });
    storage.generateStorageHash = jest.fn((fileToSave) => {
      return 'fileHash';
    });
    coa.getTransactionResponse = jest.fn(() => null);
    coa.getBlock = jest.fn();
    await run('deploy', { resetStates: true });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('Update project', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, { projectDao });
    });

    it('Whenever there is no update, an error should be thrown', async () => {
      expect(
        projectService.updateProject(0, {
          field: 'field1',
          field2: 'field2'
        })
      ).rejects.toThrow(errors.project.CantUpdateProject(0));
    });
    it('When an update is done, it should return the id of the updated project', async () => {
      const projectUpdated = await projectService.updateProject(1, {
        field: 'field1',
        field2: 'field2'
      });
      expect(
        projectService.updateProject(1, {
          field: 'field1',
          field2: 'field2'
        })
      ).resolves.not.toThrow(COAError);
      expect(projectUpdated).toEqual(1);
    });
  });

  describe('Save project', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, { projectDao });
    });

    it('Whenever a project is saved, it should return the id of the project', async () => {
      const id = await projectService.saveProject({
        projectName: 'validProjectName'
      });
      expect(id).toEqual({ id: 1, revision: 1 });
    });

    it('Whenever an error occurs and the project cant be saved, an error should be thrown', () => {
      expect(
        projectService.saveProject({ projectName: 'invalidProject' })
      ).rejects.toThrow(errors.project.CantSaveProject);
    });
  });

  describe('Basic information', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao,
        userService,
        changelogService
      });
    });

    describe('Create project', () => {
      const adminUserId = 4;
      it('Should create a new project when all the fields are valid', async () => {
        const createChangelogSpy = jest.spyOn(
          changelogService,
          'createChangelog'
        );
        const { projectId } = await projectService.createProject({
          ownerId: adminUserId
        });
        expect(projectId).toEqual(1);
        expect(createChangelogSpy).toHaveBeenCalledWith({
          project: projectId,
          action: ACTION_TYPE.CREATE_PROJECT,
          revision: 1,
          user: 4
        });
      });

      it('Should not create a project when the owner does not exist and throw an error', async () => {
        await expect(
          projectService.createProject({
            projectName,
            location,
            timeframe,
            timeframeUnit,
            ownerId: 34,
            file
          })
        ).rejects.toThrow(errors.common.CantFindModelWithId('user', 34));
      });
    });

    describe('Update basic project information', () => {
      it('Should update the project whenever the fields are valid and the project already exists', async () => {
        const createChangelogSpy = jest.spyOn(
          changelogService,
          'createChangelog'
        );
        const {
          projectId
        } = await projectService.updateBasicProjectInformation({
          projectId: 20,
          projectName,
          location,
          timeframe,
          timeframeUnit,
          file,
          user: adminUser
        });
        expect(projectId).toEqual(20);
        expect(createChangelogSpy).toHaveBeenCalled();
      });

      const fileWithInvalidSize = {
        name: 'file.jpeg',
        size: Number('90000000')
      };

      it('Should not update the project when timeframe is equal to 0 and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframe: 0,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(errors.project.InvalidTimeframe());
      });

      it('Should not update the project when timeframe is less than 0 and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframe: -10,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(errors.project.InvalidTimeframe());
      });

      it('Should not update the project whenever the fields are valid but the project is in executing status', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: pendingProject.id,
            projectName,
            location,
            timeframe,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.project.ProjectCantBeUpdated(pendingProject.status)
        );
      });

      it('Should not update the project whenever the fields are valid but the project does not exist and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 2,
            projectName,
            location,
            timeframe,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(errors.common.CantFindModelWithId('project', 2));
      });

      it('Should not update the project whenever the photo has an invalid file type and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframe,
            timeframeUnit,
            file: { name: 'file.json', size: 1234 },
            user: adminUser
          })
        ).rejects.toThrow(errors.file.ImgFileTyPeNotValid);
      });

      it('Should not update the project whenever the photo has an invalid size and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframe,
            timeframeUnit,
            file: fileWithInvalidSize,
            user: adminUser
          })
        ).rejects.toThrow(errors.file.ImgSizeBiggerThanAllowed);
      });

      it('Should update the project although file field is missing after the first update', async () => {
        const {
          projectId
        } = await projectService.updateBasicProjectInformation({
          projectId: 21,
          projectName,
          location,
          timeframe,
          timeframeUnit,
          user: adminUser
        });
        expect(projectId).toEqual(21);
      });

      it('Should not update a project when projectName field is missing and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            location,
            timeframe,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateBasicProjectInformation')
        );
      });


      it('Should not update a project when highlighted field is missing and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            location,
            timeframe,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateBasicProjectInformation')
        );
      });

      it('Should not update a project when published field is missing and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            location,
            timeframe,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateBasicProjectInformation')
        );
      });

      it('Should not update a project when timeframe field is missing and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframeUnit,
            file,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateBasicProjectInformation')
        );
      });

      it('Should not update a project when timeframeUnit field is missing and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframe,
            file,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateBasicProjectInformation')
        );
      });

      it('Should not update a project when file field is missing in the first update and throw an error', async () => {
        await expect(
          projectService.updateBasicProjectInformation({
            projectId: 20,
            projectName,
            location,
            timeframe,
            timeframeUnit,
            user: adminUser
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateBasicProjectInformation')
        );
      });
    });
  });

  describe('Project details', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao,
        userService,
        changelogService
      });
    });

    describe('Update project details', () => {
      it('Should update the project if it exists and all fields are valid', async () => {
        const { projectId } = await projectService.updateProjectDetails({
          projectId: 20,
          mission,
          problemAddressed,
          currencyType,
          currency,
          additionalCurrencyInformation,
          legalAgreementFile: pdfFile,
          projectProposalFile: pdfFile,
          user: adminUser,
          type: PROJECT_TYPES.GRANT
        });
        expect(projectId).toEqual(20);
      });

      it('Should not update the project whenever the fields are valid but the project is in executing status', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: pendingProject.id,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.LOAN
          })
        ).rejects.toThrow(
          errors.project.ProjectCantBeUpdated(pendingProject.status)
        );
      });

      it('Should update the project if it exists and have all the fields valids and files are missing after the first update', async () => {
        const { projectId } = await projectService.updateProjectDetails({
          projectId: 21,
          mission,
          problemAddressed,
          currencyType,
          currency,
          additionalCurrencyInformation,
          user: adminUser,
          type: PROJECT_TYPES.GRANT
        });
        expect(projectId).toEqual(21);
      });

      it('Should not update the project if it does not exists, and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 2,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.LOAN
          })
        ).rejects.toThrow(errors.common.CantFindModelWithId('project', 2));
      });

      it('Should not update the project if it exists but mission param is missing and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but problemAddressed param is missing and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but currencyType param is missing and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but currency param is missing and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but additionalCurrencyInformation param is missing and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but legalAgreementFile param is missing in the first update and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but projectProposalFile param is missing in the first update and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists but type param is missing in the first update and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(
          errors.common.RequiredParamsMissing('updateProjectDetails')
        );
      });

      it('Should not update the project if it exists and have all valid fields but legal agreement file size is bigger than allowed', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: {
              name: 'legalAgreementFile.pdf',
              size: 1231239992
            },
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(errors.file.ImgSizeBiggerThanAllowed);
      });

      it('Should not update the project if it exists and have all valid fields but legal agreement file type is not a valid one', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: {
              name: 'legalAgreementFile.json',
              size: 4123
            },
            projectProposalFile: pdfFile,
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(errors.file.DocFileTypeNotValid);
      });

      it('Should not update project detail when the proposal file type is not valid and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: { name: 'proposalFile.json' },
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(errors.file.DocFileTyPeNotValid);
      });

      it('Should not update project detail when the proposal size is bigger than allowed and throw an error', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: {
              name: 'proposalFile.pdf',
              size: Number('12319023')
            },
            user: adminUser,
            type: PROJECT_TYPES.GRANT
          })
        ).rejects.toThrow(errors.file.ImgSizeBiggerThanAllowed);
      });

      it('Should throw error when type is invalid', async () => {
        await expect(
          projectService.updateProjectDetails({
            projectId: 20,
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            legalAgreementFile: pdfFile,
            projectProposalFile: pdfFile,
            user: adminUser,
            type: 'invalidType'
          })
        ).rejects.toThrow(errors.project.InvalidProjectType);
      });
    });
  });

  describe('Get projects', () => {
    beforeEach(() => {
      dbUserProject.push({
        id: 1,
        project: pendingProject.id,
        role: beneficiaryRole.id,
        user: 1
      });
      dbRole.push(beneficiaryRole);
      dbUser.push({
        id: 1,
        firstName: 'name',
        lastName: 'lastName'
      });
    });

    it('Should return an empty list if there are no existing projects', async () => {
      beforeAll(() => restoreProjectService());
      injectMocks(projectService, {
        projectDao: Object.assign({}, projectDao, {
          findGenesisProjects: () => [],
          getLastValidReview: jest.fn()
        }),
        roleService,
        userProjectService
      });
      await expect(projectService.getProjects()).resolves.toHaveLength(0);
    });
    it('Should return an array of projects if there is any project', async () => {
      beforeAll(() => restoreProjectService());
      injectMocks(projectService, {
        projectDao: Object.assign({}, projectDao, {
          findGenesisProjects: () => [pendingProject],
          getLastValidReview: () => pendingProject,
          findActiveProjectClone: jest.fn()
        }),
        roleService,
        userProjectService
      });
      const projects = await projectService.getProjects();
      expect(projects).toHaveLength(1);
      expect(projects).toMatchSnapshot();
    });
    it('Should return projects with no present beneficiary', async () => {
      beforeAll(() => restoreProjectService());
      injectMocks(projectService, {
        projectDao: Object.assign({}, projectDao, {
          findGenesisProjects: () => [consensusProject],
          getLastValidReview: () => consensusProject,
          findActiveProjectClone: jest.fn()
        }),
        roleService,
        userProjectService
      });
      const projects = await projectService.getProjects();
      expect(projects).toHaveLength(1);
      expect(projects).toMatchSnapshot();
    });
  });

  describe('Get project milestones', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        milestoneService,
        projectDao,
        userService
      });
    });

    it('Should return project milestones of an existent project', async () => {
      const milestones = await projectService.getProjectMilestones(3);
      expect(milestones).toHaveLength(1);
      expect(milestones[0].id).toEqual(2);
    });
    it('Should not return project milestones of a non-existent project, and throw an error', async () => {
      await expect(projectService.getProjectMilestones(0)).rejects.toThrow(
        errors.common.CantFindModelWithId('project', 0)
      );
    });
  });
  // TODO whenever mail is answered describe('Project milestone activities', () => {});

  describe('Send project to review', () => {
    const executingProjectToReview = {
      id: 1,
      status: projectStatuses.DRAFT,
      revision: 1,
      parent: null,
      step: 0
    };
    const openReviewProjectToReview = {
      id: 2,
      status: projectStatuses.OPEN_REVIEW,
      revision: 1,
      parent: 1,
      step: 0
    };
    const _milestoneService = {
      getAllMilestonesByProject: projectId =>
        dbMilestone.filter(m => m.projectId === projectId)
    };
    const proposerEmail = 'pedro.gonzalez@email.com';

    const users = [
      {
        id: 'd2739728-6dcc-4cf6-b0e9-5b8430e7f139',
        firstName: 'Juan',
        lastName: 'Rojas',
        email: 'juan.rojas@globant.com',
        country: 10,
        first: true
      },
      {
        id: '71386471-f95a-4d8b-a14c-fd6207aaaa14',
        firstName: 'Pedro',
        lastName: 'Sanchez',
        sanchezemail: 'pedro.sanchez@globant.com',
        country: 1,
        first: false
      },
      {
        id: 1,
        firstName: 'Pedro',
        lastName: 'Gonzalez',
        email: proposerEmail,
        wallet: {
          address: '0xEa51CfB26e6547725835b4138ba96C0b5de9E54A'
        }
      }
    ];

    beforeEach(() => {
      dbProject = [];
    });

    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: Object.assign(
          {},
          {
            findById: projectId =>
              dbProject.find(project => project.id === projectId),
            updateProject: (toUpdate, id) => {
              const found = dbProject.find(project => project.id === id);
              if (!found) return;
              const updated = { ...found, ...toUpdate };
              dbProject[dbProject.indexOf(found)] = updated;
              return updated;
            }
          }
        ),
        userProjectService,
        changelogService,
        mailService,
        storageService,
        milestoneService: _milestoneService,
        userProjectDao: {
          getUserProjects: () => [
            { id: 1, role: { description: 'auditor' }, user: { id: 1, country: 1 } },
            { id: 2, role: { description: 'beneficiary' }, user: { id: 2, country: 1 } }
          ],
          getRolesOfUser: () => [
            {
              role: {
                description: rolesTypes.BENEFICIARY
              }
            }
          ]
        },
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });

      injectMocks(originalCountryService, {
        countryDao: {
          findAllByProps: () => [
            { id: 1, name: 'argentina' }
          ]
        }
      })
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it('should change project status to review successfully', async () => {
      dbProject.push(openReviewProjectToReview);

      jest.spyOn(mailService, 'sendEmails').mockResolvedValue();

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      const proposedIpfsHash = 'ipfsHashTest'
      jest
        .spyOn(storageService, 'saveStorageData')
        .mockResolvedValue(proposedIpfsHash);

      const updateProjectStub = jest.spyOn(projectService, 'updateProject');

      const proposerEmail = 'pedro.gonzalez@email.com';
      const projectParentId = openReviewProjectToReview.parent;

      const response = await projectService.sendProjectToReview({
        user: {
          id: 1,
          firstName: 'Pedro',
          lastName: 'Gonzalez',
          email: proposerEmail,
          wallet: {
            address: '0xEa51CfB26e6547725835b4138ba96C0b5de9E54A'
          }
        },
        projectId: 2
      });

      const toSign = getMessageHash(
        ['uint256', 'string', 'string', 'string'],
        [updateProjectStub.mock.calls[0][1].proposerNonce, projectParentId, proposedIpfsHash, proposerEmail]
      )
      expect(response).toEqual({
        success: true,
        toSign
      });
    });

    it('should change project status to review successfully with correct proposer', async () => {
      const openReviewProjectWithProposer = {
        ...openReviewProjectToReview,
        proposer: 1
      };
      dbProject.push(openReviewProjectWithProposer);

      jest.spyOn(mailService, 'sendEmails').mockResolvedValue();

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      const proposedIpfsHash = 'ipfsHashTest'
      jest
        .spyOn(storageService, 'saveStorageData')
        .mockResolvedValue(proposedIpfsHash);

      const updateProjectStub = jest.spyOn(projectService, 'updateProject');

      const proposerEmail = 'pedro.gonzalez@email.com';
      const projectParentId = openReviewProjectWithProposer.parent;

      const response = await projectService.sendProjectToReview({
        user: {
          id: 1,
          firstName: 'Pedro',
          lastName: 'Gonzalez',
          email: proposerEmail,
          wallet: {
            address: '0xEa51CfB26e6547725835b4138ba96C0b5de9E54A'
          }
        },
        projectId: 2
      });

      const toSign = getMessageHash(
        ['uint256', 'string', 'string', 'string'],
        [updateProjectStub.mock.calls[0][1].proposerNonce, projectParentId, proposedIpfsHash, proposerEmail]
      )
      expect(response).toEqual({
        success: true,
        toSign
      });
    });

    it('should change project status to review successfully with wrong proposer', async () => {
      const openReviewProjectWithProposer = {
        ...openReviewProjectToReview,
        proposer: 2
      };
      dbProject.push(openReviewProjectWithProposer);

      jest.spyOn(mailService, 'sendEmails').mockResolvedValue();

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      const proposedIpfsHash = 'ipfsHashTest'
      jest
        .spyOn(storageService, 'saveStorageData')
        .mockResolvedValue(proposedIpfsHash);

      const updateProjectStub = jest.spyOn(projectService, 'updateProject');

      const proposerEmail = 'pedro.gonzalez@email.com';
      const projectParentId = openReviewProjectWithProposer.parent;

      await expect(projectService.sendProjectToReview({
        user: {
          id: 1,
          firstName: 'Pedro',
          lastName: 'Gonzalez',
          email: proposerEmail,
          wallet: {
            address: '0xEa51CfB26e6547725835b4138ba96C0b5de9E54A'
          }
        },
        projectId: 2
      })).rejects.toThrow(errors.project.OnlyProposerCanSendProposeProjectEditTransaction);
    });

    it('should throw an error if transition is not valid', async () => {
      dbProject.push(executingProjectToReview);

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      await expect(
        projectService.sendProjectToReview({
          user: { id: 1, firstName: 'Pedro', lastName: 'Gonzalez' },
          projectId: 1
        })
      ).rejects.toThrow(errors.project.InvalidProjectTransition);
    });

    it('should throw an error if user is not beneficiary or investor of the project', async () => {
      dbProject.push(openReviewProjectToReview);

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockImplementation(({ error }) => {
          throw new COAError(error);
        });

      await expect(
        projectService.sendProjectToReview({
          user: { id: 1, firstName: 'Pedro', lastName: 'Gonzalez' },
          projectId: 2
        })
      ).rejects.toThrow(errors.project.UserCanNotMoveProjectToReview);
    });
    it('should throw when user has approved or inactive pinStatus', async () => {
      const openReviewProjectWithProposer = {
        ...openReviewProjectToReview,
        proposer: 2
      };
      dbProject.push(openReviewProjectWithProposer);
      injectMocks(projectService, {
        userDao: {
          findById: () => Promise.resolve({ id: 'proposerId', pinStatus: pinStatus.APPROVED }),
        },
      });
      
      await expect(
        projectService.sendProjectToReview({
          user: { id: 2, firstName: 'Pedro', lastName: 'Gonzalez', pinStatus: pinStatus.APPROVED },
          projectId: 2
        })
      ).rejects.toThrow(errors.user.InvalidPinStatus);
    });
  });

  describe('Cancel project review', () => {
    const executingProjectToReview = {
      id: 1,
      status: projectStatuses.DRAFT,
      revision: 1,
      parent: null,
      step: 0
    };
    const openReviewProjectToReview = {
      id: 2,
      status: projectStatuses.OPEN_REVIEW,
      revision: 1,
      parent: null,
      step: 0
    };
    const _milestoneService = {
      getAllMilestonesByProject: projectId =>
        dbMilestone.filter(m => m.projectId === projectId)
    };

    beforeEach(() => {
      dbProject = [];
    });

    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: Object.assign(
          {},
          {
            findById: projectId =>
              dbProject.find(project => project.id === projectId),
            updateProject: (toUpdate, id) => {
              const found = dbProject.find(project => project.id === id);
              if (!found) return;
              const updated = { ...found, ...toUpdate };
              dbProject[dbProject.indexOf(found)] = updated;
              return updated;
            }
          }
        ),
        userProjectService,
        changelogService,
        milestoneService: _milestoneService,
      });
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it('should change project status to cancelled review successfully', async () => {
      dbProject.push(openReviewProjectToReview);

      jest.spyOn(projectService, 'getUsersByProjectId').mockResolvedValue();

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      const response = await projectService.cancelProjectReview({
        user: { id: 1, firstName: 'Pedro', lastName: 'Gonzalez' },
        projectId: 2
      });

      expect(response).toMatchObject({
        success: true
      });
    });

    it('should throw an error if transition is not valid', async () => {
      dbProject.push(executingProjectToReview);

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      await expect(
        projectService.cancelProjectReview({
          user: { id: 1, firstName: 'Pedro', lastName: 'Gonzalez' },
          projectId: 1
        })
      ).rejects.toThrow(errors.project.InvalidProjectTransition);
    });

    it('should throw an error if user is not beneficiary or investor of the project', async () => {
      dbProject.push(openReviewProjectToReview);

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockImplementation(({ error }) => {
          throw new COAError(error);
        });

      await expect(
        projectService.cancelProjectReview({
          user: { id: 1, firstName: 'Pedro', lastName: 'Gonzalez' },
          projectId: 2
        })
      ).rejects.toThrow(errors.project.UserCanNotMoveProjectToCancelReview);
    });
  });

  describe('Get project', () => {
    const ENTREPENEUR_ID = 2;
    const DRAFT_PROJECT_ID = 1;
    const EXECUTING_PROJECT_ID = 2;
    const SECOND_DRAFT_PROJECT_ID = 3;
    const users = [
      {
        id: 'd2739728-6dcc-4cf6-b0e9-5b8430e7f139',
        firstName: 'Juan',
        lastName: 'Rojas',
        email: 'juan.rojas@globant.com',
        country: 10,
        first: true
      },
      {
        id: '71386471-f95a-4d8b-a14c-fd6207aaaa14',
        firstName: 'Pedro',
        lastName: 'Sanchez',
        sanchezemail: 'pedro.sanchez@globant.com',
        country: 1,
        first: false
      }
    ];
    const DRAFT_PROJECT = {
      dataComplete: 1,
      proposal: 'proposal',
      faqLink: 'faqLink',
      agreementJson: '{}',
      coverPhotoPath:
        '/files/projects/coverPhotos/8/82ded2f7b23f99fdd6e1d7cd7c6f84c7.jpeg',
      milestonePath: '/milestone',
      proposalFilePath: '/path',
      agreementFileHash: 'hash',
      status: 'draft',
      owner: {},
      createdAt: '2022-10-11T03:00:00.000Z',
      address: 'address',
      consensusSeconds: 864000,
      fundingSeconds: 864000,
      lastUpdatedStatusAt: '2022-10-11T15:49:35.825Z',
      id: DRAFT_PROJECT_ID,
      txHash: 'hash',
      rejectionReason: 'reason',
      basicInformation: {
        projectName: 'Test project',
        location: 'Argentina',
        timeframe: '232',
        timeframeUnit: 'unit',
        cardPhotoPath:
          '/files/projects/cardPhotos/8/82ded2f7b23f99fdd6e1d7cd7c6f84c7.jpeg'
      },
      details: {
        mission: 'Our mission is to have a mission',
        problemAddressed:
          'We have invented a new problem just to create this project',
        currency: 'usd',
        currencyType: 'fiat',
        budget: '0'
      },
      users: [
        {
          role: '1',
          users: [
            {
              id: 'd2739728-6dcc-4cf6-b0e9-5b8430e7f139',
              firstName: 'Juan',
              lastName: 'Rojas',
              email: 'juan.rojas@globant.com',
              country: 10,
              first: true
            },
            {
              id: '71386471-f95a-4d8b-a14c-fd6207aaaa14',
              firstName: 'Pedro',
              lastName: 'Sanchez',
              sanchezemail: 'pedro.sanchez@globant.com',
              country: 1,
              first: false
            }
          ]
        },
        {
          role: '2',
          users: [
            {
              id: '71386471-f95a-4d8b-a14c-fd6207aaaa14',
              firstName: 'Pedro',
              lastName: 'Sanchez',
              sanchezemail: 'pedro.sanchez@globant.com',
              country: 1,
              first: false
            }
          ]
        },
        {
          role: '3',
          users: [
            {
              id: 'd2739728-6dcc-4cf6-b0e9-5b8430e7f139',
              firstName: 'Juan',
              lastName: 'Rojas',
              email: 'juan.rojas@globant.com',
              country: 10,
              first: true
            }
          ]
        }
      ],
      milestones: [
        {
          id: 1,
          title: 'Title test',
          description: 'Description test',
          budget: '200',
          activities: [
            {
              id: 1,
              title: 'Title',
              description: 'Description',
              acceptanceCriteria: 'Criteria',
              budget: '100',
              currency: 'USD',
              auditor: {
                id: 'd2739728-6dcc-4cf6-b0e9-5b8430e7f139',
                firstName: 'Juan',
                lastName: 'Rojas'
              }
            },
            {
              id: 2,
              title: 'Title',
              description: 'Description',
              acceptanceCriteria: 'Criteria',
              budget: '100',
              currency: 'USD',
              auditor: {
                id: 'd2739728-6dcc-4cf6-b0e9-5b8430e7f139',
                firstName: 'Juan',
                lastName: 'Rojas'
              }
            }
          ]
        },
        {
          id: 2,
          title: 'Title test',
          description: 'Description test',
          budget: '0',
          activities: []
        }
      ],
      revision: 1,
      parent: null
    };
    const EXECUTING_PROJECT = {
      ...draftProject,
      id: EXECUTING_PROJECT_ID,
      milestones: [
        {
          activities: [
            {
              evidences: [
                {
                  status: 'new'
                }
              ]
            }
          ]
        }
      ],
      status: 'executing',
      users: [],
      basicInformation: {
        projectName: 'Project 1'
      }
    };
    const SECOND_DRAFT_PROJECT = {
      ...draftProject,
      id: SECOND_DRAFT_PROJECT_ID
    };
    const projects = [DRAFT_PROJECT, EXECUTING_PROJECT, SECOND_DRAFT_PROJECT];
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        userProjectDao: {
          getProjectsOfUser: () => [
            { project: { id: DRAFT_PROJECT_ID }, userId: ENTREPENEUR_ID },
            { project: { id: EXECUTING_PROJECT_ID }, userId: ENTREPENEUR_ID }
          ]
        },
        projectDao: {
          findById: id => Promise.resolve(projects.find(p => p.id === id)),
          getProjectWithAllData: id =>
            Promise.resolve(projects.find(p => p.id === id)),
          getLastPublicRevisionProject: jest.fn()
        },
        walletHistoryDao,
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });
    });
    it('should retrieve all data of the project when the user is an admin', async () => {
      await expect(
        projectService.getProject(DRAFT_PROJECT_ID, {
          isAdmin: true,
          id: 1
        })
      ).resolves.toEqual(DRAFT_PROJECT);
    });
    it('should retrieve all data expect sensitive data when the user is regular', async () => {
      await expect(
        projectService.getProject(EXECUTING_PROJECT_ID, {
          isAdmin: false,
          id: ENTREPENEUR_ID
        })
      ).resolves.toEqual(EXECUTING_PROJECT);
    });
    it('should retrieve only public data when ther is no given user', async () => {
      await expect(
        projectService.getProject(EXECUTING_PROJECT_ID)
      ).resolves.toEqual({
        id: 2,
        status: EXECUTING_PROJECT.status,
        basicInformation: EXECUTING_PROJECT.basicInformation,
        milestones: [{ activities: [{ evidences: [] }] }],
        users: [],
        parent: null
      });
    });
    it('should retrieve only basic information when the project is in draft and it is logged in', async () => {
      await expect(
        projectService.getProject(DRAFT_PROJECT_ID, {
          role: userRoles.ENTREPRENEUR,
          id: ENTREPENEUR_ID
        })
      ).resolves.toEqual({
        id: DRAFT_PROJECT_ID,
        status: projectStatuses.DRAFT,
        basicInformation: DRAFT_PROJECT.basicInformation,
        revision: DRAFT_PROJECT.revision
      });
    });
    it('should throw when there is no project with given id', async () => {
      const NON_EXISTENT_PROJECT_ID = 9999;
      await expect(
        projectService.getProject(NON_EXISTENT_PROJECT_ID, {
          role: userRoles.ENTREPRENEUR,
          id: ENTREPENEUR_ID
        })
      ).rejects.toEqual(
        new COAError(
          errors.common.CantFindModelWithId('project', NON_EXISTENT_PROJECT_ID)
        )
      );
    });
    it('should throw when user is not related to the project', async () => {
      await expect(
        projectService.getProject(SECOND_DRAFT_PROJECT_ID, {
          role: userRoles.ENTREPRENEUR,
          id: ENTREPENEUR_ID
        })
      ).rejects.toEqual(new COAError(errors.user.UserNotRelatedToTheProject));
    });
  });

  describe('Delete project', () => {
    beforeEach(() => {
      resetDb();
      dbProject.push(
        draftProjectSecondUpdate,
        executingProject,
        pendingProject
      );
      dbUserProject.push({
        id: 1,
        roleId: 1,
        userId: 1,
        projectId: draftProjectSecondUpdate.id
      });
      dbMilestone.push(milestone);
      dbTask.push(task1, task2, task3);
      dbEvidence.push(evidenceOfTask1, evidenceOfTask2, evidence2OfTask2);
    });
    afterEach(() => jest.clearAllMocks());
    const userProjectDao = {
      getUserProjects: projectId =>
        dbUserProject.filter(up => up.projectId === projectId),
      removeUserProject: userProjectId => {
        dbUserProject = dbUserProject.filter(up => up.id === userProjectId);
      }
    };
    const _projectDao = {
      findById: id => dbProject.find(project => project.id === id),
      deleteProject: ({ projectId }) => {
        dbProject = dbProject.filter(p => p.id === projectId);
        return dbProject.find(p => p.id === projectId);
      }
    };
    const _milestoneDao = {
      deleteMilestone: milestoneId => {
        dbMilestone = dbMilestone.filter(m => m.id === milestoneId);
      }
    };
    const _milestoneService = {
      getAllMilestonesByProject: projectId =>
        dbMilestone.filter(m => m.projectId === projectId)
    };
    const activityDao = {
      deleteActivity: id => {
        dbTask = dbTask.filter(t => t.id === id);
      }
    };
    const activityEvidenceDao = {
      deleteEvidence: id => {
        dbEvidence = dbEvidence.filter(e => e.id === id);
      },
      getEvidencesByTaskId: taskId => dbEvidence.filter(e => e.task === taskId)
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        userProjectDao,
        projectDao: _projectDao,
        milestoneDao: _milestoneDao,
        milestoneService: _milestoneService,
        activityDao,
        activityEvidenceDao,
        changelogService
      });
    });
    it(`should successfully delete the project alongside with 
        the user project, activities and milestones`, async () => {
      const projectDaoDeleteSpy = jest.spyOn(_projectDao, 'deleteProject');
      const userProjectDeleteSpy = jest.spyOn(
        userProjectDao,
        'removeUserProject'
      );
      const milestoneDeleteSpy = jest.spyOn(_milestoneDao, 'deleteMilestone');
      const activityDeleteSpy = jest.spyOn(activityDao, 'deleteActivity');
      const evidenceDeleteSpy = jest.spyOn(
        activityEvidenceDao,
        'deleteEvidence'
      );
      const deleteProjectChangelogsSpy = jest.spyOn(
        changelogService,
        'deleteProjectChangelogs'
      );
      await expect(
        projectService.deleteProject(draftProjectSecondUpdate.id)
      ).resolves.toEqual(draftProjectSecondUpdate);
      expect(projectDaoDeleteSpy).toHaveBeenCalledTimes(1);
      expect(milestoneDeleteSpy).toHaveBeenCalledTimes(1);
      expect(userProjectDeleteSpy).toHaveBeenCalledTimes(1);
      expect(activityDeleteSpy).toHaveBeenCalledTimes(3);
      expect(evidenceDeleteSpy).toHaveBeenCalledTimes(3);
      expect(deleteProjectChangelogsSpy).toHaveBeenCalledTimes(1);
    });
    it('should throw when the project is not in draft', async () => {
      await expect(
        projectService.deleteProject(pendingProject.id)
      ).rejects.toThrow(errors.project.ProjectInvalidStatus(pendingProject.id));
    });
    it('should throw when project does not exist', async () => {
      await expect(
        projectService.deleteProject(draftProjectFirstUpdate.id)
      ).rejects.toThrow(
        errors.common.CantFindModelWithId('project', draftProjectFirstUpdate.id)
      );
    });
    it('should throw when there was an error deleting project', async () => {
      jest.spyOn(_projectDao, 'deleteProject').mockReturnValue(undefined);
      await expect(
        projectService.deleteProject(draftProjectSecondUpdate.id)
      ).rejects.toThrow(errors.common.ErrorDeleting('project'));
    });
  });

  describe('Testing cloneProject', () => {
    const _projectDao = {
      findById: id => dbProject.find(p => p.id === id),
      getLastProjectWithValidStatus: projectId =>
        dbProject.find(project => project.id === projectId),
      saveProject: _project => {
        dbProject.push(_project);
        return { id: dbProject.length, ..._project };
      },
      getLastReview: _ => ({}),
      findActiveProjectClone: _ => ({})
    };
    const _milestoneDao = {
      createMilestone: _milestone => {
        dbMilestone.push(_milestone);
        return { id: dbMilestone.length, ..._milestone };
      },
      getMilestonesByProjectId: projectId =>
        dbMilestone.filter(_milestone => _milestone.projectId === projectId)
    };
    const _userProjectDao = {
      getUserProject: ({ project }) =>
        dbUserProject.filter(up => up.projectId === project),
      createUserProject: up => dbUserProject.push(up)
    };
    const _activityDao = {
      createActivity: activity => dbTask.push(activity)
    };
    const _activityEvidenceDao = {
      getEvidencesByTaskId: taskId =>
        dbEvidence.filter(evidence => evidence.task === taskId),
      addActivityEvidence: _evidence => dbEvidence.push(_evidence)
    };
    const _evidenceFileDao = {
      getEvidenceFilesByEvidenceId: evidenceId =>
        dbFileEvidence.filter(_file => _file.evidence === evidenceId),
      saveEvidenceFile: evidenceFile => dbFileEvidence.push(evidenceFile)
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        userProjectDao: _userProjectDao,
        projectDao: _projectDao,
        milestoneDao: _milestoneDao,
        activityDao: _activityDao,
        activityEvidenceDao: _activityEvidenceDao,
        evidenceFileDao: _evidenceFileDao,
        changelogService,
        userProjectService,
        activityService
      });
    });
    beforeEach(() => {
      resetDb();
      dbProject.push(
        draftProjectSecondUpdate,
        inprogressProject,
        executingProject,
        nonGenesisProject,
        projectWithPendingStatusActivity
      );
      dbUserProject.push({
        id: 1,
        roleId: 1,
        userId: 1,
        projectId: inprogressProject.id
      });
      dbMilestone.push(
        milestone,
        milestoneOfExecutingProject,
        milestoneWithPendingStatusTask
      );
      dbTask.push(task1, task2, task3, task4);
      dbEvidence.push(evidenceOfTask1, evidenceOfTask2, evidence2OfTask2);
      dbFileEvidence.push(fileOfevidenceOfTask1);
    });
    it('should successfully clone the project', async () => {
      const saveProjectSpy = jest.spyOn(_projectDao, 'saveProject');
      jest
        .spyOn(_projectDao, 'getLastReview')
        .mockReturnValue(inprogressProject);
      const createMilestoneSpy = jest.spyOn(_milestoneDao, 'createMilestone');
      const createActivitySpy = jest.spyOn(_activityDao, 'createActivity');
      const addActivityEvidenceSpy = jest.spyOn(
        _activityEvidenceDao,
        'addActivityEvidence'
      );
      const saveEvidenceFileSpy = jest.spyOn(
        _evidenceFileDao,
        'saveEvidenceFile'
      );
      const createUserProjectSpy = jest.spyOn(
        _userProjectDao,
        'createUserProject'
      );
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      jest.spyOn(_projectDao, 'findActiveProjectClone').mockReturnValue();
      jest
        .spyOn(activityService, 'thereArePendingActivityTransactions')
        .mockReturnValue(false);

      await expect(
        projectService.cloneProject({
          user: genericUser,
          projectId: inprogressProject.id
        })
      ).resolves.toEqual({ projectId: dbProject.length + 1 });
      expect(saveProjectSpy).toHaveBeenCalledTimes(1);
      expect(createMilestoneSpy).toHaveBeenCalledTimes(1);
      expect(createActivitySpy).toHaveBeenCalledTimes(3);
      expect(addActivityEvidenceSpy).toHaveBeenCalledTimes(3);
      expect(saveEvidenceFileSpy).toHaveBeenCalledTimes(1);
      expect(createUserProjectSpy).toHaveBeenCalledTimes(1);
      expect(createChangelogSpy).toHaveBeenCalledTimes(1);
    });
    it('should throw when project is at invalid status', async () => {
      await expect(
        projectService.cloneProject({
          user: genericUser,
          projectId: executingProject.id
        })
      ).rejects.toThrow(
        errors.project.ProjectInvalidStatus(executingProject.id)
      );
    });
    it('should throw when project has an active clone', async () => {
      jest
        .spyOn(_projectDao, 'findActiveProjectClone')
        .mockReturnValue({ id: 99 });
      await expect(
        projectService.cloneProject({
          user: genericUser,
          projectId: inprogressProject.id
        })
      ).rejects.toThrow(
        errors.project.CloneAlreadyExists(inprogressProject.id)
      );
    });
    it('should throw when project does not exist', async () => {
      const unexistentProjectId = 99;
      await expect(
        projectService.cloneProject({
          user: genericUser,
          projectId: unexistentProjectId
        })
      ).rejects.toThrow(
        errors.common.CantFindModelWithId('project', unexistentProjectId)
      );
    });
    it('should throw when project is not genesis', async () => {
      await expect(
        projectService.cloneProject({
          user: genericUser,
          projectId: nonGenesisProject.id
        })
      ).rejects.toThrow(errors.project.ProjectNotGenesis);
    });
    it('should throw when project has some activity in pending status', async () => {
      jest.spyOn(_projectDao, 'findActiveProjectClone').mockReturnValue();
      jest
        .spyOn(activityService, 'thereArePendingActivityTransactions')
        .mockReturnValue(true);
      await expect(
        projectService.cloneProject({
          user: genericUser,
          projectId: projectWithPendingStatusActivity.id
        })
      ).rejects.toThrow(errors.project.PendingTransaction);
    });
  });

  describe('Testing updateProjectReview', () => {
    const _projectDao = {
      findById: id => dbProject.find(p => p.id === id),
      updateProject: (toUpdate, projectId) => ({ ...toUpdate, projectId }),
      getProjectWithProposer: id => dbProject.find(p => p.id === id)
    };
    const txDao = {
      createTx: jest.fn()
    };
    const _milestoneService = {
      getAllMilestonesByProject: projectId =>
        dbMilestone.filter(m => m.projectId === projectId)
    };

    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        changelogService,
        userProjectService,
        projectDao: _projectDao,
        mailService,
        storageService,
        txDao,
        milestoneService: _milestoneService
      });
      coa.submitProjectEditAuditResult = jest.fn(() => ({ hash: '0x01' }));

      injectMocks(originalCountryService, {
        countryDao: {
          findAllByProps: () => [
            { id: 1, name: 'argentina' }
          ]
        }
      })
    });
    beforeEach(() => {
      const { parent, ...inReviewGenesisProject } = inReviewProject;
      resetDb();
      dbProject.push(
        { ...inReviewProject, proposer: { id: 1, address: 'addressTest' } },
        { ...inReviewGenesisProject, id: 98 },
        { ...inprogressProject, parent: 99, id: 99 },
        inprogressProject
      );
    });
    xit('should successfully cancel a review', async () => {
      const getUsersByProjectIdSpy = jest
        .spyOn(projectService, 'getUsersByProjectId')
        .mockResolvedValue([]);
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      const saveStorageDataSpy = jest.spyOn(storageService, 'saveStorageData');
      await expect(
        projectService.updateProjectReview({
          userId: 1,
          approved: false,
          projectId: inReviewProject.id,
          reason: 'reason of reject review'
        })
      ).resolves.toEqual({ projectId: inReviewProject.id });
      expect(saveStorageDataSpy).toHaveBeenCalled();
      expect(getUsersByProjectIdSpy).toHaveBeenCalledWith({
        projectId: inReviewProject.parent
      });
      expect(createChangelogSpy).toHaveBeenCalledWith({
        project: inReviewProject.parent,
        revision: inReviewProject.revision,
        action: ACTION_TYPE.CANCEL_REVIEW,
        user: 1,
        extraData: { reason: 'reason of reject review' },
        transaction: '0x01',
        status: TX_STATUS.PENDING
      });
    });
    it('should successfully approve a review', async () => {
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      const publishProjectSpy = jest
        .spyOn(projectService, 'publishProject')
        .mockReturnValue({});
      const getLastRevisionStatusSpy = jest
        .spyOn(projectService, 'getLastRevisionStatus')
        .mockResolvedValue(projectStatuses.IN_PROGRESS);
      await expect(
        projectService.updateProjectReview({
          userId: 1,
          approved: true,
          projectId: inReviewProject.id
        })
      ).resolves.toEqual({ projectId: inReviewProject.id });
      expect(createChangelogSpy).toHaveBeenCalledWith({
        project: inReviewProject.parent,
        revision: inReviewProject.revision,
        action: ACTION_TYPE.APPROVE_REVIEW,
        user: 1,
        extraData: {
          isCancelRequested: undefined
        },
        transaction: '0x01',
        status: TX_STATUS.PENDING
      });
      expect(publishProjectSpy).toHaveBeenCalledWith({
        projectId: inReviewProject.id,
        userId: 1,
        previousStatus: inprogressProject.status
      });
      expect(getLastRevisionStatusSpy).toHaveBeenCalledWith(
        inReviewProject.parent
      );
    });
    it('should throw when given id is not a clone', async () => {
      await expect(
        projectService.updateProjectReview({
          userId: 1,
          approved: true,
          projectId: 98
        })
      ).rejects.toThrow(errors.project.GivenProjectIsNotAClone(98));
    });

    it('should throw when given project is not in a valid status', async () => {
      await expect(
        projectService.updateProjectReview({
          userId: 1,
          approved: true,
          projectId: 99
        })
      ).rejects.toThrow(
        errors.project.CantUpdateReview(inprogressProject.status)
      );
    });
  });

  describe('getProjectEvidences', () => {
    const _projectDao = {
      findById: id => dbProject.find(p => p.id === id),
      updateProject: (toUpdate, projectId) => ({ ...toUpdate, projectId }),
      getProjectWithProposer: id => dbProject.find(p => p.id === id)
    };
    const currentDate = new Date();
    const _activityService = {
      getApprovedEvidencesByProject: () => [
        {
          amount: '1000',
          activity: { type: ACTIVITY_TYPES.FUNDING },
          destinationAccount: 'account',
          user: { id: 'id', firstName: 'firstName', lastName: 'lastName' },
          createdAt: currentDate,
          roleDescription: rolesTypes.BENEFICIARY
        }
      ],
      thereArePendingActivityTransactions: jest.fn()
    };
    const userProjectDao = {
      getRolesOfUser: ({ user, project }) =>
        dbUserProject.filter(up => up.project === project && up.user === user)
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        activityService: _activityService,
        userProjectDao,
        projectDao: _projectDao
      });
    });
    beforeEach(() => {
      dbUserProject.push({
        user: 'id',
        project: 'projectId',
        role: { description: rolesTypes.BENEFICIARY }
      });
      dbProject.push({ id: 'projectId', parent: 1 });
    });
    it('should successfully bring project evidences', async () => {
      await expect(
        projectService.getProjectEvidences({ projectId: 'projectId' })
      ).resolves.toEqual({
        evidences: [
          {
            activityType: 'funding',
            amount: '1000',
            date: currentDate,
            destinationAccount: 'account',
            role: rolesTypes.BENEFICIARY,
            userName: 'firstName lastName'
          }
        ]
      });
    });
    it('should successfully bring project evidences when limit is used', async () => {
      await expect(
        projectService.getProjectEvidences({ projectId: 'projectId', limit: 5 })
      ).resolves.toEqual({
        evidences: [
          {
            activityType: 'funding',
            amount: '1000',
            date: currentDate,
            destinationAccount: 'account',
            role: rolesTypes.BENEFICIARY,
            userName: 'firstName lastName'
          }
        ]
      });
    });
  });
  describe('Testing getProjectById', () => {
    const _projectDao = {
      findById: id => dbProject.find(p => p.id === id)
    };
    beforeEach(() => {
      dbProject.push({ id: 'projectId', parent: 1 });
    });
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao
      });
    });
    it('should successfully get project by id', async () => {
      await expect(projectService.getProjectById('projectId')).resolves.toEqual(
        { id: 'projectId', parent: 1 }
      );
    });
  });

  describe('Testing cloneMilestoneActivitiesAndEvidences', () => {
    const _activityDao = {
      createActivity: jest.fn(() => ({ id: 1 }))
    };
    const _activityEvidenceDao = {
      getEvidencesByTaskId: jest.fn(() => [
        { id: 1 },
        { id: 2 },
        { id: 3, parent: 1 }
      ]),
      addActivityEvidence: jest.fn(() => ({ id: 1 }))
    };
    const _evidenceFileDao = {
      getEvidenceFilesByEvidenceId: jest.fn(() => [{ id: 1 }]),
      saveEvidenceFile: jest.fn()
    };
    const _milestoneDao = {
      createMilestone: jest.fn(() => ({ id: 1 }))
    };
    beforeEach(() => {
      dbProject.push({ id: 'projectId', parent: 1 });
    });
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        activityDao: _activityDao,
        activityEvidenceDao: _activityEvidenceDao,
        evidenceFileDao: _evidenceFileDao,
        milestoneDao: _milestoneDao
      });
    });
    it('should successfully clone milestones, activities and evidences', async () => {
      const createActivitySpy = jest.spyOn(_activityDao, 'createActivity');
      const getEvidencesByTaskIdSpy = jest.spyOn(
        _activityEvidenceDao,
        'getEvidencesByTaskId'
      );
      const addActivityEvidenceSpy = jest.spyOn(
        _activityEvidenceDao,
        'addActivityEvidence'
      );
      const getEvidenceFilesByEvidenceIdSpy = jest.spyOn(
        _evidenceFileDao,
        'getEvidenceFilesByEvidenceId'
      );
      const saveEvidenceFileSpy = jest.spyOn(
        _evidenceFileDao,
        'saveEvidenceFile'
      );
      const createMilestoneSpy = jest.spyOn(_milestoneDao, 'createMilestone');

      await projectService.cloneMilestoneActivitiesAndEvidences({
        projectId: 'projectId',
        milestones: [
          { id: 1, tasks: [{ id: 1 }, { id: 2 }, { id: 3, parent: 1 }] },
          { id: 2, parent: 1, tasks: [] }
        ]
      });
      expect(createActivitySpy).toHaveBeenCalled();
      expect(getEvidencesByTaskIdSpy).toHaveBeenCalled();
      expect(addActivityEvidenceSpy).toHaveBeenCalled();
      expect(getEvidenceFilesByEvidenceIdSpy).toHaveBeenCalled();
      expect(saveEvidenceFileSpy).toHaveBeenCalled();
      expect(createMilestoneSpy).toHaveBeenCalled();
    });
  });
  describe('Testing compareProjectDetailsFieldsAndCreateChangelog', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        changelogService
      });
    });
    it('should compareProjectDetailsFieldsAndCreateChangelog ', async () => {
      const compareFieldsAndCreateChangelogSpy = jest
        .spyOn(projectService, 'compareFieldsAndCreateChangelog')
        .mockResolvedValue();
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      await projectService.compareProjectDetailsFieldsAndCreateChangelog({
        project: 'projectId',
        mission: 'mission',
        problemAddressed: 'problemAddressed',
        currencyType,
        currency,
        additionalCurrencyInformation,
        legalAgreementFile: 'file',
        projectProposalFile: 'file',
        user: 'user'
      });
      expect(compareFieldsAndCreateChangelogSpy).toHaveBeenCalled();
      expect(createChangelogSpy).toHaveBeenCalled();
    });
  });

  describe('Testing compareFieldsAndCreateChangelog', () => {
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        changelogService
      });
    });
    it('should successfully compare fields and create changelog', async () => {
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      await projectService.compareFieldsAndCreateChangelog({
        project: {
          key: 'value'
        },
        fields: {
          key: 'value2'
        },
        action: ACTION_TYPE.ADD_ACTIVITY,
        user: { id: 1 }
      });
      expect(createChangelogSpy).toHaveBeenCalled();
    });
    it('should successfully compare fields and not create changelog', async () => {
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      await projectService.compareFieldsAndCreateChangelog({
        project: {
          key: 'value'
        },
        fields: {
          key: 'value'
        },
        action: ACTION_TYPE.ADD_ACTIVITY,
        user: { id: 1 }
      });
      expect(createChangelogSpy).not.toHaveBeenCalled();
    });
  });

  describe('Testing getUsersByProjectId', () => {
    const _userProjectDao = {
      getUserProjects: jest.fn(() => [{ user: 'userId' }])
    };

    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        userProjectDao: _userProjectDao,
      });
    });
    it('should successfully bring user projects', async () => {
      const getUserProjectsSpy = jest.spyOn(_userProjectDao, 'getUserProjects');

      await projectService.getUsersByProjectId('projectId');
      expect(getUserProjectsSpy).toHaveBeenCalled();
    });
  });
  describe('Testing getProjectChangelog', () => {
    const _projectDao = {
      getProjectWithAllData: jest.fn(() => ({ id: 'projectId' })),
      getAllProjectsByParentIdWithAllData: jest.fn(() => [])
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao,
        changelogService
      });
    });
    it('should successfully get project changelog ', async () => {
      const getProjectWithAllDataSpy = jest.spyOn(
        _projectDao,
        'getProjectWithAllData'
      );
      const getAllProjectsByParentIdWithAllDataSpy = jest.spyOn(
        _projectDao,
        'getAllProjectsByParentIdWithAllData'
      );
      await projectService.getProjectChangelog({
        id: 1
      });
      expect(getProjectWithAllDataSpy).toHaveBeenCalled();
      expect(getAllProjectsByParentIdWithAllDataSpy).toHaveBeenCalled();
    });
  });
  describe('Testing publishProject', () => {
    const _projectDao = {
      findById: jest.fn(() => ({
        id: 1,
        dataComplete: 11,
        status: projectStatuses.DRAFT
      })),
      updateProject: jest.fn(() => ({ id: 1 }))
    };
    const _storageService = {
      saveStorageData: jest.fn()
    };
    const _txDao = {
      createTx: jest.fn()
    };
    const _milestoneService = {
      getAllMilestonesByProject: projectId =>
        dbMilestone.filter(m => m.projectId === projectId)
    };

    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao,
        changelogService,
        storageService: _storageService,
        txDao: _txDao,
        milestoneService: _milestoneService,
        userProjectDao: {
          getUserProjects: () => [
            { id: 1, role: { description: 'auditor' }, user: { id: 1, country: 1 } },
            { id: 2, role: { description: 'beneficiary' }, user: { id: 2, country: 1 } }
          ],
          getRolesOfUser: () => [
            {
              role: {
                description: rolesTypes.BENEFICIARY
              }
            }
          ]
        },
      });

      injectMocks(originalCountryService, {
        countryDao: {
          findAllByProps: () => [
            { id: 1, name: 'argentina' }
          ]
        }
      })

      jest.spyOn(projectService, 'validateProjectBudget').mockResolvedValue();
      files.saveProjectMetadataFile = jest.fn();
      files.getFileFromPath = jest.fn();
      coa.createProject = jest.fn(() => ({ hash: 'hash' }));
    });
    it('should successfully publish the project', async () => {
      const updateProjectStub = jest.spyOn(_projectDao, 'updateProject');
      const createTxStub = jest.spyOn(_txDao, 'createTx');
      const createChangelogStub = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      await projectService.publishProject({
        projectId: 'projectId',
        userId: 'userId',
        userEmail: 'test@email.com'
      });
      expect(updateProjectStub).toHaveBeenCalled();
      expect(createTxStub).toHaveBeenCalled();
      expect(createChangelogStub).toHaveBeenCalled();
    });
    it('should successfully publish the project when no previous status is given', async () => {
      const saveStorageDataSpy = jest.spyOn(_storageService, 'saveStorageData');
      const saveProjectMetadataFileSpy = jest.spyOn(
        files,
        'saveProjectMetadataFile'
      );
      await projectService.publishProject({
        projectId: 'projectId',
        userId: 'userId',
        userEmail: 'test@email.com',
        previousStatus: projectStatuses.DRAFT
      });
      expect(saveStorageDataSpy).toHaveBeenCalledTimes(3);
      expect(saveProjectMetadataFileSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('Testing validateProjectBudget', () => {
    const _activityService = {
      getActivitiesByProject: jest.fn(() => []),
      getActivitiesBudget: jest.fn(() => BigNumber(1000))
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        activityService: _activityService
      });
    });
    it('should not throw', async () => {
      await expect(
        projectService.validateProjectBudget('projectId')
      ).resolves.not.toThrow();
    });
    it('should throw', async () => {
      _activityService.getActivitiesBudget
        .mockReturnValueOnce(BigNumber(1000))
        .mockReturnValueOnce(BigNumber(999));
      await expect(
        projectService.validateProjectBudget('projectId')
      ).rejects.toThrow(errors.project.InvalidActivitiesBudget);
    });
  });
  describe('Testing getLastRevisionStatus', () => {
    const _projectDao = {
      getLastProjectWithValidStatus: jest.fn(() => ({
        status: projectStatuses.DRAFT
      }))
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao
      });
    });
    it('should bring last review', async () => {
      await expect(
        projectService.getLastRevisionStatus('projectId')
      ).resolves.not.toThrow();
    });
  });
  describe('Testing sendProjectReviewTransaction', () => {
    const _projectDao = {
      getProjectWithProposer: jest.fn(() => ({
        status: projectStatuses.PENDING_REVIEW,
        step: PROJECT_STEPS.PENDING_SIGNATURE_AUTHORIZATION,
        proposer: {
          id: 'proposerId'
        }
      })),
      updateProject: jest.fn(() => ({ id: 1 }))
    };
    const _txDao = {
      createTx: jest.fn()
    };
    const users = [{ id: 'proposerId' },{ id: 'notProposerId' }];
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao,
        changelogService,
        txDao: _txDao,
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });
      coa.proposeProjectEdit = jest.fn(() => ({ hash: 'hash' }));
    });
    it('should successfully send project review transaction', async () => {
      const createChangelogSpy = jest.spyOn(
        changelogService,
        'createChangelog'
      );
      const createTxSpy = jest.spyOn(_txDao, 'createTx');
      await projectService.sendProjectReviewTransaction({
        user: { id: 'proposerId' },
        projectId: 'projectId',
        authorizationSignature: 'authSignature'
      });
      expect(createChangelogSpy).toHaveBeenCalled();
      expect(createTxSpy).toHaveBeenCalled();
    });
    it('should throw when project is in invalid status', async () => {
      _projectDao.getProjectWithProposer.mockReturnValue({
        status: projectStatuses.OPEN_REVIEW,
        step: PROJECT_STEPS.PENDING_SIGNATURE_AUTHORIZATION,
        proposer: {
          id: 'proposerId'
        }
      });
      await expect(
        projectService.sendProjectReviewTransaction({
          user: { id: 'proposerId' },
          projectId: 'projectId',
          authorizationSignature: 'authSignature'
        })
      ).rejects.toThrow(errors.project.CantSendProposeProjectEditTransaction);
    });
    it('should throw when users are not equal', async () => {
      await expect(
        projectService.sendProjectReviewTransaction({
          user: { id: 'notProposerId' },
          projectId: 'projectId',
          authorizationSignature: 'authSignature'
        })
      ).rejects.toThrow(errors.project.CantSendProposeProjectEditTransaction);
    });
    it('should throw when user has approved or inactive pinStatus', async () => {
      injectMocks(projectService, {
        userDao: {
          findById: () => Promise.resolve({ id: 'proposerId', pinStatus: pinStatus.APPROVED }),
        },
      });

      await expect(
        projectService.sendProjectReviewTransaction({
          user: { id: 'proposerId', pinStatus: pinStatus.APPROVED },
          projectId: 'projectId',
          authorizationSignature: 'authSignature'
        })
      ).rejects.toThrow(errors.user.InvalidPinStatus);
    });
  });
  describe('Testing isProjectComplete', () => {
    const _milestoneService = {
      getMilestonesByProject: jest.fn(() => [
        { status: MILESTONE_STATUS.APPROVED }
      ])
    };
    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        milestoneService: _milestoneService
      });
      coa.proposeProjectEdit = jest.fn(() => ({ hash: 'hash' }));
    });
    it('should return true', async () => {
      await expect(
        projectService.isProjectComplete('projectId')
      ).resolves.toEqual(true);
    });
    it('should return false', async () => {
      _milestoneService.getMilestonesByProject.mockImplementation(() => [
        { status: MILESTONE_STATUS.IN_PROGRESS }
      ]);
      await expect(
        projectService.isProjectComplete('projectId')
      ).resolves.toEqual(false);
    });
  });

  describe('Testing CancelProject', () => {
    const user = { id: 1, email: 'email', firstName: 'Pedro', lastName: 'Gonzalez', isAdmin: false, wallet: { address: '0x00' } };
    const users = [user, { id: 'id', pinStatus: pinStatus.APPROVED }];
    const dbProject = [openReviewProject]
    const _projectDao = {
      findById: id => dbProject.find(p => p.id === id),
      updateProject: (toUpdate, projectId) => ({ ...toUpdate, id: projectId }),
      getProjectWithProposer: id => dbProject.find(p => p.id === id)
    };

    beforeAll(() => {
      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao,
        userProjectService,
        userService,
        changelogService,
        storageService,
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });
      // coa.getProjectRegistryUserNonce = jest.fn(() => Promise.resolve(1));
    });

    it('Should update project with cancel in database', async () => {
      jest.spyOn(projectService, 'getUsersByProjectId').mockResolvedValue();
      jest.spyOn(userProjectService, 'validateUserWithRoleInProject').mockResolvedValue();
      await expect(
        projectService.cancelProject({ projectId: openReviewProject.id, user })
      ).resolves.toEqual({ projectId: openReviewProject.id })
    });

    it('should throw when user has approved or inactive pinStatus', async () => {
      await expect(
        projectService.cancelProject({
          user: { id: 'id', pinStatus: pinStatus.APPROVED },
          projectId: 'projectId',
        })
      ).rejects.toThrow(errors.user.InvalidPinStatus);
    });
  });

  describe('Testing getPublicProjects', () => {
    const publicProject = {
      ...draftProject,
      id: 2,
      published: true,
      users: [{ role: 1, user: 2 }],
      visibilityUpdatedAt: Date.now() - 10000
    }
    const highlightedProject = {
      ...publicProject,
      id: 3,
      highlighted: true,
      users: [{ role: 1, user: 2 }],
      visibilityUpdatedAt: Date.now()
    };

    const highlightedProject2 = {
      ...publicProject,
      id: 4,
      highlighted: true,
      users: [{ role: 1, user: 2 }],
      visibilityUpdatedAt: Date.now() + 10000
    };

    const _projectDao = {
      findGenesisProjects: () => { },
      getLastProjectByFilter: () => { },
    };

    beforeEach(() => {
      jest.spyOn(_projectDao, 'findGenesisProjects').mockReturnValue([draftProject, publicProject, highlightedProject]);
      jest.spyOn(_projectDao, 'getLastProjectByFilter')
        .mockReturnValueOnce(publicProject)
        .mockReturnValueOnce(highlightedProject2)
        .mockReturnValueOnce(highlightedProject);

      jest.spyOn(roleService, 'getRoleByDescription').mockReturnValue({ id: 1 });

      restoreProjectService();
      injectMocks(projectService, {
        projectDao: _projectDao,
        roleService,
        userService
      });
    });

    it('should successfully get public projects', async () => {
      const response = await projectService.getPublicProjects(false, 10);
      // Test amount fetched
      await expect(response.length).toEqual(3);
      // Test order
      await expect(response.map(x => x.id)).toEqual([highlightedProject2.id, highlightedProject.id, publicProject.id]);
    });

    it('should successfully get highlighted projects', async () => {
      const response = await projectService.getPublicProjects(true, 10);
      // Test amount fetched
      await expect(response.length).toEqual(2);
      // Test order
      await expect(response.map(x => x.id)).toEqual([highlightedProject2.id, highlightedProject.id]);
    });
  })

  describe('Testing setProjectVisibility', () => {
    const publicProject = {
      ...draftProject,
      id: 2,
      published: true,
      users: [{ role: 1, user: 2 }]
    }
    const highlightedProject = {
      ...publicProject,
      id: 3,
      highlighted: true,
      users: [{ role: 1, user: 2 }]
    };

    const _projectDao = {
      findById: id => dbProject.find(p => p.id === id),
      updateProject: (toUpdate, projectId) => ({ ...toUpdate, id: projectId }),
    };

    beforeEach(() => {
      restoreProjectService();
      dbProject.push(publicProject);
      dbProject.push(highlightedProject);
      injectMocks(projectService, { projectDao: _projectDao, });
    });

    it('Should update a public project to highlighted and returns success', async () => {
      const updateProjectSpy = jest.spyOn(_projectDao, 'updateProject');

      const response = await projectService.setProjectVisibility(publicProject.id, 'highlighted')
      expect(response.success).toBeTruthy;
      expect(updateProjectSpy).toHaveBeenCalled()
    });

    it('Should not update a highlighted project to highlighted and returns success', async () => {
      const updateProjectSpy = jest.spyOn(_projectDao, 'updateProject');

      const response = await projectService.setProjectVisibility(highlightedProject.id, 'highlighted')
      expect(response.success).toBeTruthy;
      expect(updateProjectSpy).not.toHaveBeenCalled()
    });

    it('Should throw an error if the visibility prop is invalid', async () => {
      expect(projectService.setProjectVisibility(highlightedProject.id, 'highgligh')).rejects.toThrow(errors.project.CantUpdateProject(highlightedProject.id))
    });
  });
});