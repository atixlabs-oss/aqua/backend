/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const config = require('config');
const { injectMocks } = require('../../rest/util/injection');
const errors = require('../../rest/errors/exporter/ErrorExporter');
const originalMailService = require('../../rest/services/mailService');
const templateParser = require('../../rest/services/helpers/templateParser');
const { templateNames } = require('../../rest/services/helpers/templateLoader');
const languages = require('../..//languages/default.json');
const { ACTION_TYPE } = require('../../rest/util/constants');

let mailService = Object.assign({}, originalMailService);
const restoreMailService = () => {
  mailService = Object.assign({}, originalMailService);
};

const invalidEmail = 'invalid@notfound.com';

const FRONTEND_URL = config.frontendUrl;
const IMAGES_URL = `${FRONTEND_URL}/static/images`;
const URL_LOGO = `${IMAGES_URL}/logo-email.png`;
const URL_LOCKED_WINDOW = `${IMAGES_URL}/locked-window.png`;
const URL_UPLOAD_TO_CLOUD = `${IMAGES_URL}/upload-to-cloud.png`;

describe('Testing mailService', () => {
  const email = {
    to: 'user@test.com',
    from: 'coa@support.com',
    subject: 'Hello from COA',
    text: 'Welcome to',
    html: '<b>COA</b>'
  };

  beforeAll(() => {
    templateParser.completeTemplate = jest.fn();
  });

  beforeEach(() => jest.clearAllMocks());

  afterEach(() => restoreMailService());

  const emailClient = {
    sendMail: args => {
      if (args.to === invalidEmail) return { rejected: 'rejected' };
      return args;
    },
    isNodeMailer: () => true
  };

  describe('Test sendMail method', () => {
    beforeEach(() => {
      injectMocks(mailService, {
        emailClient
      });
    });

    it('should resolve and return the info', async () => {
      await expect(mailService.sendMail(email)).resolves.toEqual(
        expect.anything()
      );
    });

    it('should throw an error if any required param is missing', async () => {
      await expect(mailService.sendMail({ text: 'optional' })).rejects.toThrow(
        errors.common.RequiredParamsMissing('sendMail')
      );
    });

    it('should throw an error if the email was rejected', async () => {
      await expect(
        mailService.sendMail({ ...email, to: invalidEmail })
      ).rejects.toThrow(errors.mail.EmailNotSent);
    });
  });

  describe('Test sendProjectStatusChangeMail method', () => {
    beforeEach(() => {
      injectMocks(mailService, {
        sendMail: jest.fn()
      });
    });

    it('should resolve and call completeTemplate and sendMail', async () => {
      const bodyContent = { param: 'Email Param' };
      await expect(
        mailService.sendProjectStatusChangeMail({
          ...email,
          bodyContent
        })
      ).resolves.toBeUndefined();

      expect(templateParser.completeTemplate).toHaveBeenCalled();
      expect(mailService.sendMail).toHaveBeenCalled();
    });

    it('should throw an error if any required param is missing', async () => {
      await expect(
        mailService.sendProjectStatusChangeMail({ text: 'optional' })
      ).rejects.toThrow(
        errors.common.RequiredParamsMissing('sendProjectStatusChangeMail')
      );
    });

    it('should call sendEmail with right parameters', async () => {
      const bodyContent = { param: 'Email Param' };
      templateParser.completeTemplate.mockResolvedValueOnce('abc');
      await expect(
        mailService.sendProjectStatusChangeMail({
          ...email,
          bodyContent
        })
      ).resolves.toBeUndefined();

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to: email.to,
        subject: email.subject,
        text: email.text,
        html: 'abc'
      });
    });
  });

  describe('Test sendEmailVerification method', () => {
    beforeEach(() => {
      injectMocks(mailService, {
        sendMail: jest.fn()
      });
    });

    it('should resolve and call completeTemplate and sendMail', async () => {
      const bodyContent = { param: 'Email Param' };
      await expect(
        mailService.sendEmailVerification({
          ...email,
          bodyContent
        })
      ).resolves.toBeUndefined();

      expect(templateParser.completeTemplate).toHaveBeenCalled();
      expect(mailService.sendMail).toHaveBeenCalled();
    });

    it('should throw an error if any required param is missing', async () => {
      await expect(
        mailService.sendEmailVerification({ text: 'optional' })
      ).rejects.toThrow(
        errors.common.RequiredParamsMissing('sendEmailVerification')
      );
    });

    it('should call sendEmail with right parameters', async () => {
      const bodyContent = { param: 'Email Param' };
      templateParser.completeTemplate.mockResolvedValueOnce('abc');
      await expect(
        mailService.sendProjectStatusChangeMail({
          ...email,
          bodyContent
        })
      ).resolves.toBeUndefined();

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to: email.to,
        subject: email.subject,
        text: email.text,
        html: 'abc'
      });
    });
  });

  describe('Test sendEmailRecoveryPassword method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailRecoveryPassword({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.RECOVERY_PASSWORD,
          objectData: {
            ...bodyContent,
            ...languages.resetPasswordEmail,
            frontendUrl: `${FRONTEND_URL}/u`,
            URL_LOGO,
            URL_LOCKED_WINDOW
          }
        })
      });
    });
  });

  describe('Test sendEmailInitialRecoveryPassword method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailInitialRecoveryPassword({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.WELCOME,
          objectData: {
            ...bodyContent,
            frontendUrl: `${FRONTEND_URL}/u`,
            URL_LOGO,
            URL_LOCKED_WINDOW,
            ...languages.welcomeEmail
          }
        })
      });
    });
  });

  describe('Test sendInitialUserResetPassword method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendInitialUserResetPassword({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.WELCOME_USER,
          objectData: {
            ...bodyContent,
            ...languages.welcomeUserEmail,
            frontendUrl: `${FRONTEND_URL}/my-projects`,
            URL_LOGO,
            URL_LOCKED_WINDOW
          }
        })
      });
    });
  });

  describe('Test sendPublishProject method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendPublishProject({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.PUBLISH_PROJECT,
          objectData: {
            ...bodyContent,
            ...languages.publishedProjectEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_UPLOAD_TO_CLOUD
          }
        })
      });
    });
  });

  describe('Test sendEmailCloneInReview method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailCloneInReview({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.CLONE_IN_REVIEW,
          objectData: {
            ...bodyContent,
            ...languages.cloneInReviewEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_UPLOAD_TO_CLOUD
          }
        })
      });
    });
  });

  describe('Test sendEmails method', () => {
    beforeEach(async () => {
      mailService.sendEmailApprovedReview = jest.fn().mockResolvedValue();
    });

    test('Should call sendEmailApprovedReview method 2 times', async () => {
      const project = { projectName: 'some name', id: 'some-id' };
      const action = ACTION_TYPE.APPROVE_REVIEW;
      const users = [{ email: 'email1' }, { email: 'email2' }];
      await mailService.sendEmails({ project, action, users });
      expect(mailService.sendEmailApprovedReview).toHaveBeenCalledTimes(2);
      expect(mailService.sendEmailApprovedReview).toHaveBeenNthCalledWith(1, {
        to: users[0].email,
        bodyContent: {
          projectName: project.projectName,
          projectId: project.id
        }
      });

      expect(mailService.sendEmailApprovedReview).toHaveBeenNthCalledWith(2, {
        to: users[1].email,
        bodyContent: {
          projectName: project.projectName,
          projectId: project.id
        }
      });
    });
  });

  describe('Test sendEmailApprovedReview method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailApprovedReview({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.REVIEW_APPROVED,
          objectData: {
            ...bodyContent,
            ...languages.cloneApprovedEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_UPLOAD_TO_CLOUD
          }
        })
      });
    });
  });

  describe('Test sendEmailRejectedReview method', () => {
    const bodyContent = { msg: 'some body' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailRejectedReview({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.REVIEW_REJECTED,
          objectData: {
            ...bodyContent,
            ...languages.cloneRejectedEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_UPLOAD_TO_CLOUD
          }
        })
      });
    });
  });

  describe('Test sendTxFailedMail method', () => {
    test('Should call sendMail with right parameters', async () => {
      const bodyContent = { msg: 'some body' };
      const to = 'test@test.test';
      const text = 'some text';
      const subject = 'some subject';
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
      await mailService.sendTxFailedMail({ to, text, bodyContent, subject });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.TX_FAILED,
          objectData: {
            ...bodyContent,
            URL_LOGO,
            URL_LOCKED_WINDOW
          }
        })
      });
    });
  });

  describe('Test sendEmailRequestNewPinForUser method', () => {
    const bodyContent = { username: 'username' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailRequestNewPinForUser({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.REQUEST_NEW_PIN_USER,
          objectData: {
            ...bodyContent,
            ...languages.requestNewPinEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_UPLOAD_TO_CLOUD
          }
        })
      });
    });
  });

  describe('Test sendEmailDigitalSignatureApproved method', () => {
    test('Should call sendMail with right parameters', async () => {
      const bodyContent = { msg: 'some body' };
      const to = 'test@test.test';
      const text = 'some text';
      const subject = 'some subject';
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
      await mailService.sendEmailDigitalSignatureApproved({ to, text, bodyContent, subject });
      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.DIGITAL_SIGNATURE_REQUEST_STATUS,
          objectData: {
            ...bodyContent,
            ...languages.digitalSignatureApprovedEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_LOCKED_WINDOW
          }
        })
      });
    });
  });

  describe('Test sendEmailDigitalSignatureRejected method', () => {
    test('Should call sendMail with right parameters', async () => {
      const bodyContent = { msg: 'some body' };
      const to = 'test@test.test';
      const text = 'some text';
      const subject = 'some subject';
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
      await mailService.sendEmailDigitalSignatureRejected({ to, text, bodyContent, subject });
      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.DIGITAL_SIGNATURE_REQUEST_STATUS,
          objectData: {
            ...bodyContent,
            ...languages.digitalSignatureRejectedEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_LOCKED_WINDOW
          }
        })
      });
    });
  });

  describe('Test sendEmailRequestNewPinAdmin method', () => {
    const bodyContent = { username: 'adminUsername' };
    const to = 'test@test.test';
    const text = 'some text';
    const subject = 'some subject';

    beforeEach(async () => {
      mailService.sendMail = jest.fn().mockResolvedValueOnce();
    });

    test('Should call sendMail with right parameters', async () => {
      await mailService.sendEmailRequestNewPinAdmin({
        to,
        subject,
        text,
        bodyContent
      });

      expect(mailService.sendMail).toHaveBeenCalledWith({
        to,
        subject,
        text,
        html: mailService.getHTMLFromMJML({
          mjmlFileName: templateNames.REQUEST_NEW_PIN,
          objectData: {
            ...bodyContent,
            ...languages.requestNewPinEmail,
            frontendUrl: FRONTEND_URL,
            URL_LOGO,
            URL_UPLOAD_TO_CLOUD
          }
        })
      });
    });
  });
});
