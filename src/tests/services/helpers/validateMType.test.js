/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const validateMType = require('../../../rest/services/helpers/validateMtype');
const COAError = require('../../../rest/errors/COAError');
const errors = require('.../../../rest/errors/exporter/ErrorExporter');

describe('Testing validateMType', () => {
  describe('imgValidator', () => {
    const filesToVerify = [
      ['coverPhoto'],
      ['thumbnail'],
      ['claims'],
      ['transferClaims'],
      ['experiencePhoto'],
      ['transferReceipt'],
      ['milestoneClaim']
    ];

    it.each(filesToVerify)('Validate %s with png extension', mType => {
      const file = new File([], 'valid.png');
      validateMType(mType, file);
    });

    it.each(filesToVerify)('Validate %s with jpeg extension', mType => {
      const file = new File([], 'valid.jpeg');
      validateMType(mType, file);
    });

    it.each(filesToVerify)('Validate %s with wrong extension', mType => {
      const file = new File([], 'invalid.invalid');
      expect.assertions(1);
      try {
        return validateMType(mType, file);
      } catch (error) {
        expect(error).toStrictEqual(
          new COAError(errors.file.ImgFileTyPeNotValid)
        );
      }
    });
  });

  describe('xlsValidator', () => {
    const mType = 'milestones';
    it('Validate milestones with xls extension', () => {
      const file = new File([], 'valid.xls');
      validateMType(mType, file);
    });

    it('Validate milestones with xlsx extension', () => {
      const file = new File([], 'valid.xlsx');
      validateMType(mType, file);
    });

    it('Validate milestones with wrong extension', () => {
      const file = new File([], 'invalid.png');
      expect.assertions(1);
      try {
        return validateMType(mType, file);
      } catch (error) {
        expect(error).toStrictEqual(
          new COAError(errors.file.MilestoneFileTypeNotValid)
        );
      }
    });
  });

  describe('docValidator', () => {
    const filesToVerify = [['agreementFile'], ['proposalFile']];

    it.each(filesToVerify)('Validate %s with doc extension', mType => {
      const file = new File([], 'valid.doc');
      validateMType(mType, file);
    });

    it.each(filesToVerify)('Validate %s with docx extension', mType => {
      const file = new File([], 'valid.docx');
      validateMType(mType, file);
    });

    it.each(filesToVerify)('Validate %s with pdf extension', mType => {
      const file = new File([], 'valid.pdf');
      validateMType(mType, file);
    });

    it.each(filesToVerify)('Validate %s with wrong extension', mType => {
      const file = new File([], 'invalid.png');
      expect.assertions(1);
      try {
        return validateMType(mType, file);
      } catch (error) {
        expect(error).toStrictEqual(
          new COAError(errors.file.DocFileTypeNotValid)
        );
      }
    });
  });

  describe('pdfValidator', () => {
    const filesToVerify = [['legalAgreementFile'], ['projectProposalFile']];

    it.each(filesToVerify)('Validate %s with pdf extension', mType => {
      const file = new File([], 'valid.pdf');
      validateMType(mType, file);
    });

    it.each(filesToVerify)('Validate %s with wrong extension', mType => {
      const file = new File([], 'invalid.doc');
      expect.assertions(1);
      try {
        return validateMType(mType, file);
      } catch (error) {
        expect(error).toStrictEqual(
          new COAError(errors.file.DocFileTypeNotValid)
        );
      }
    });
  });

  describe('evidenceValidator', () => {
    const mType = 'evidence';
    it('Validate evidence with png extension', () => {
      const file = new File([], 'valid.png');
      validateMType(mType, file);
    });

    it('Validate evidence with jpeg extension', () => {
      const file = new File([], 'valid.jpeg');
      validateMType(mType, file);
    });

    it('Validate evidence with pdf extension', () => {
      const file = new File([], 'valid.pdf');
      validateMType(mType, file);
    });

    it('Validate evidence with wrong extension', () => {
      const file = new File([], 'invalid.doc');
      expect.assertions(1);
      try {
        return validateMType(mType, file);
      } catch (error) {
        expect(error).toStrictEqual(
          new COAError(errors.file.EvidenceFileTypeNotValid)
        );
      }
    });
  });
});
