/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const templateParser = require('../../../rest/services/helpers/templateParser');
const {
  templateNames
} = require('../../../rest/services/helpers/templateLoader');

describe('Testing templateParser helper', () => {
  const template = '{{param}}';
  const mockedLoader = jest.fn(() => Buffer.from(template));
  const data = {
    param: 'value'
  };

  describe('Test completeTemplate method', () => {
    it('should return the template string with the completed values', async () => {
      await expect(
        templateParser.completeTemplate(
          data,
          templateNames.GENERIC,
          mockedLoader
        )
      ).resolves.toBe(data.param);
      expect(mockedLoader).toHaveBeenCalledWith(templateNames.GENERIC);
    });
  });
});
