/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const {
  completeStep
} = require('../../../rest/services/helpers/dataCompleteUtil');

describe('Testing completeStep helper', () => {
  it('should update empty dataComplete when step 1 (basic information) is completed', async () => {
    expect(completeStep({ dataComplete: 0, step: 1 })).toEqual(1);
  });
  it('should update dataComplete when step 2 (project details) is completed', async () => {
    expect(completeStep({ dataComplete: 1, step: 2 })).toEqual(3);
  });
  it('should update dataComplete when step 3 (project users) is completed', async () => {
    expect(completeStep({ dataComplete: 3, step: 3 })).toEqual(7);
  });
  it('should update dataComplete when step 4 (project milestones) is completed', async () => {
    expect(completeStep({ dataComplete: 7, step: 4 })).toEqual(15);
  });

  it('should update step 3 (project users) with basic information completed previously', async () => {
    expect(completeStep({ dataComplete: 1, step: 3 })).toEqual(5);
  });

  it('should update step 4 (project users) with basic information completed previosly', async () => {
    expect(completeStep({ dataComplete: 1, step: 4 })).toEqual(9);
  });

  it('should update step 3 (project users) with basic information, project details and project milestones completed previously', async () => {
    expect(completeStep({ dataComplete: 11, step: 3 })).toEqual(15);
  });
});
