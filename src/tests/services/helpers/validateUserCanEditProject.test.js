/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const validateUserCanEditProject = require('../../../rest/services/helpers/validateUserCanEditProject');
const { projectStatuses } = require('../../../rest/util/constants');
const originalUserService = require('../../../rest/services/userProjectService');

describe('Testing validateUserCanEditProject', () => {
  const draftProject = {
    id: 1,
    status: projectStatuses.DRAFT
  };
  const inProgressProject = {
    id: 2,
    status: projectStatuses.IN_PROGRESS
  };
  const openReviewProject = {
    id: 3,
    status: projectStatuses.OPEN_REVIEW
  };
  const adminUser = {
    id: 1,
    isAdmin: true
  };
  const regularUser = {
    id: 2,
    isAdmin: false
  };
  const customError = status => new Error(status);

  it('should pass when its admin and proper project status ', async () => {
    jest
      .spyOn(originalUserService, 'getUserProjectFromRoleDescription')
      .mockReturnValue({});
    await expect(
      validateUserCanEditProject({
        project: draftProject,
        user: adminUser,
        error: customError
      })
    ).resolves.not.toThrow();
  });

  it('should pass when its regular user and proper project status ', async () => {
    jest
      .spyOn(originalUserService, 'getUserProjectFromRoleDescription')
      .mockReturnValue({});
    await expect(
      validateUserCanEditProject({
        project: openReviewProject,
        user: regularUser,
        error: customError
      })
    ).resolves.not.toThrow();
  });

  it('should throw when its regular user and invalid project status ', async () => {
    await expect(
      validateUserCanEditProject({
        project: inProgressProject,
        user: regularUser,
        error: customError
      })
    ).rejects.toThrow(customError(inProgressProject.status));
  });

  it('should throw when its admin and invalid project status ', async () => {
    await expect(
      validateUserCanEditProject({
        project: inProgressProject,
        user: adminUser,
        error: customError
      })
    ).rejects.toThrow(customError(inProgressProject.status));
  });
});
