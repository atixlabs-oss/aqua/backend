/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const COAError = require('../../../rest/errors/COAError');
const {
  validateMtype,
  validatePhotoSize,
  xslValidator,
  imgValidator
} = require('../../../rest/services/helpers/projectServiceHelper');

describe('Project service helper', () => {
  // here are the variables of dependencies to inject
  describe('- ValidateMtype', () => {
    const validImgFile = { mtype: 'image/', name: 'file.png' };
    const invalidFile = { mtype: 'application/json', name: 'file.json' };
    const validMilestoneFile = {
      mtype: 'application/vnd.ms-excel',
      name: 'file.xlsx'
    };

    it('If cover photo file has a valid mtype, it should not throw an error', () => {
      expect(() => validateMtype('coverPhoto')(validImgFile)).not.toThrow(
        Error
      );
    });

    it('If cover photo file has an invalid mtype, it should throw an error', () => {
      expect(() => validateMtype('coverPhoto')(invalidFile)).toThrow(COAError);
    });

    it('If thumbnail file has a valid mtype, it should not throw an error', () => {
      expect(() => validateMtype('thumbnail')(validImgFile)).not.toThrow(Error);
    });

    it('If thumbnail file has an invalid mtype, it should throw an error', () => {
      expect(() => validateMtype('thumbnail')(invalidFile)).toThrow(COAError);
    });

    it('If milestones file has a valid mtype, it should not throw an error', () => {
      expect(() => validateMtype('milestones')(validMilestoneFile)).not.toThrow(
        Error
      );
    });

    it('If milestones file has an invalid mtype, it should throw an error', () => {
      expect(() => validateMtype('milestones')(invalidFile)).toThrow(COAError);
    });
  });

  describe('- ValidatePhotoSize', () => {
    const bigPhotoFile = { size: 700000 };
    const equalMaxPhotoFile = { size: 500000 };
    const validPhotoFile = { size: 12314 };

    it('If the photo file has a bigger size than allowed, it should throw an error', () => {
      expect(() => validatePhotoSize(bigPhotoFile)).toThrow(COAError);
    });

    it('If the photo file has a lower size than allowed, it should not throw an error', () => {
      expect(() => validatePhotoSize(validPhotoFile)).not.toThrow(Error);
    });

    it('If the photo file has an equal size than allowed, it should not throw an error', () => {
      expect(() => validatePhotoSize(equalMaxPhotoFile)).not.toThrow(COAError);
    });
  });

  describe('- XlslValidator', () => {
    const invalidFile = { mtype: 'application/json', name: 'file.json' };
    const validMilestoneFile = {
      mtype: 'application/vnd.ms-excel',
      name: 'file.xlsx'
    };

    it('If the file passed to xlslValidator has a valid type, it should not throw an error', () => {
      expect(() => xslValidator(validMilestoneFile)).not.toThrow(COAError);
    });
    it('If the file passed to xlslValidator has not a valid type, it should throw an error', () => {
      expect(() => xslValidator(invalidFile)).toThrow(COAError);
    });
  });

  describe('- ImgValidator', () => {
    const validImgFile = { mtype: 'image/', name: 'file.png' };
    const invalidFile = { mtype: 'application/json', name: 'file.json' };

    it('If the file passed to imgValidator is not a valid img file, it should throw an error', () => {
      expect(() => imgValidator(validImgFile)).not.toThrow(COAError);
    });
    it('If the file passed to imgValidator is a valid img file, it should not throw an error', () => {
      expect(() => imgValidator(invalidFile)).toThrow(COAError);
    });
  });
});
