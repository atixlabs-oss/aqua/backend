/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const validateUsersAreEqualsOrThrowError = require('../../../rest/services/helpers/validateUsersAreEqualsOrThrowError');
const COAError = require('../../../rest/errors/COAError');

describe('Testing validateUsersAreEqualsOrThrowError', () => {
  const error = 'some error';
  it('Should throw error for different user ids', () => {
    expect.assertions(1);
    try {
      validateUsersAreEqualsOrThrowError({
        firstUserId: 1,
        secondUserId: 2,
        error
      });
    } catch (thrownError) {
      expect(thrownError).toStrictEqual(new COAError(error));
    }
  });

  it('Should not throw for same user ids', () =>
    validateUsersAreEqualsOrThrowError({
      firstUserId: 2,
      secondUserId: 2,
      error
    }));
});
