/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mapFieldAndSum = require('../../../rest/services/helpers/mapFieldAndSum');

describe('Testing mapFieldAndSum', () => {
  it('Should return right sum', () => {
    expect(
      mapFieldAndSum({
        array: [{ a: 1, b: 2 }, { a: 4, b: 2 }],
        field: 'a'
      }).toString()
    ).toBe('5');
  });

  it('Should return NaN if field not present', () => {
    expect(
      mapFieldAndSum({
        array: [{ a: 1, b: 2 }, { a: 4, b: 2 }],
        field: 'd'
      }).toString()
    ).toBe('NaN');
  });

  it('Should work with strings', () => {
    expect(
      mapFieldAndSum({
        array: [{ a: '1', b: '2' }, { a: '4', b: '2' }],
        field: 'b'
      }).toString()
    ).toBe('4');
  });
});
