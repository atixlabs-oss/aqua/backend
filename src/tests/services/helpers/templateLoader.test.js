/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const errors = require('../../../rest/errors/exporter/ErrorExporter');
const templateLoader = require('../../../rest/services/helpers/templateLoader');

const { templateNames, getTemplatePath } = templateLoader;

describe('Testing templateLoader helper', () => {
  const mockedRead = jest.fn();

  describe('Test loadTemplate method', () => {
    it('should return the file for the indicated template', async () => {
      await expect(
        templateLoader.loadTemplate(templateNames.GENERIC, mockedRead)
      ).resolves.toBeUndefined();
      expect(mockedRead).toHaveBeenCalledWith(
        getTemplatePath(templateNames.GENERIC)
      );
    });
    it('should throw an error if the template does not exist', async () => {
      await expect(templateLoader.loadTemplate('', mockedRead)).rejects.toThrow(
        errors.mail.TemplateNotFound
      );
    });
  });
});
