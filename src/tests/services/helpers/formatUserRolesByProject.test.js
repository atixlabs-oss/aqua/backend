/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const formatUserRolesByProject = require('../../../rest/services/helpers/formatUserRolesByProject');

describe('Testing formatUserRoles helper', () => {
  const user = {
    id: 3,
    firstName: 'Pablo',
    lastName: 'Perez',
    email: 'admin@test.com',
    emailConfirmation: true,
    roles: [
      {
        project: '1',
        role: 1,
        user: 3
      },
      {
        project: '1',
        role: 2,
        user: 3
      },
      {
        project: '2',
        role: 2,
        user: 3
      }
    ]
  };
  describe('Test formatUserRolesByProject method', () => {
    it('should return the same user properties with roles formated', () => {
      expect(formatUserRolesByProject(user)).toMatchObject({
        id: 3,
        firstName: 'Pablo',
        lastName: 'Perez',
        email: 'admin@test.com',
        emailConfirmation: true,
        projects: [
          {
            projectId: '1',
            roles: [1, 2]
          },
          {
            projectId: '2',
            roles: [2]
          }
        ]
      });
    });

    it('should return the same user properties because roles field is empty array', () => {
      expect(formatUserRolesByProject({ ...user, roles: [] })).toMatchObject({
        id: 3,
        firstName: 'Pablo',
        lastName: 'Perez',
        email: 'admin@test.com',
        emailConfirmation: true,
        projects: []
      });
    });
  });
});
