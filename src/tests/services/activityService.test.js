/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { BigNumber } = require('bignumber.js');
const { run, coa } = require('hardhat');
const files = require('../../rest/util/files');
const {
  projectStatuses,
  userRoles,
  txEvidenceStatus,
  evidenceTypes,
  validStatusToChange,
  evidenceStatus,
  ACTIVITY_STATUS,
  rolesTypes,
  MILESTONE_STATUS,
  ACTIVITY_STEPS,
  ACTIVITY_TYPES,
  PROJECT_TYPES,
  pinStatus
} = require('../../rest/util/constants');
const { injectMocks } = require('../../rest/util/injection');
const utilFiles = require('../../rest/util/files');
const COAError = require('../../rest/errors/COAError');
const errors = require('../../rest/errors/exporter/ErrorExporter');
const originalActivityService = require('../../rest/services/activityService');
const originalUserService = require('../../rest/services/userProjectService');
const originalProjectService = require('../../rest/services/projectService');
const validateMtype = require('../../rest/services/helpers/validateMtype');
const validatePhotoSize = require('../../rest/services/helpers/validatePhotoSize');
const { getMessageHash } = require('../../rest/services/helpers/hardhatTaskHelpers');
const originalCountryService = require('../../rest/services/countryService');

let activityService = Object.assign({}, originalActivityService);
const restoreActivityService = () => {
  activityService = Object.assign({}, originalActivityService);
};

describe('Testing activityService', () => {
  let dbTask = [];
  let dbTaskEvidence = [];
  let dbMilestone = [];
  let dbProject = [];
  let dbUser = [];
  let dbRole = [];
  let dbUserProject = [];
  let dbTxActivity = [];

  const resetDb = () => {
    dbTask = [];
    dbTaskEvidence = [];
    dbMilestone = [];
    dbProject = [];
    dbUser = [];
    dbRole = [];
    dbUserProject = [];
    dbTxActivity = [];
  };

  const evidenceFile = { name: 'evidence.jpg', size: 20000, data: 'data' };

  const mockedDescription = 'Testing description';

  const newActivity = {
    title: 'Title',
    description: 'Description',
    acceptanceCriteria: 'Acceptance criteria',
    budget: '1000',
    auditor: 3,
    status: ACTIVITY_STATUS.NEW,
    type: ACTIVITY_TYPES.FUNDING
  };

  // ROLES
  const auditorRole = {
    id: 1,
    description: rolesTypes.AUDITOR
  };
  const beneficiaryRole = {
    id: 2,
    description: rolesTypes.BENEFICIARY
  };

  // USERS
  const userEntrepreneur = {
    id: "1",
    role: userRoles.ENTREPRENEUR
  };

  const userSupporter = {
    id: 2,
    firstName: 'User',
    lastName: 'Supporter',
    role: userRoles.PROJECT_SUPPORTER
  };

  const regularUser = {
    id: 3,
    firstName: 'test',
    lastName: 'test',
    email: 'test@test.com'
  };

  const regularUser2 = {
    id: 4,
    firstName: 'test2',
    lastName: 'test2',
    email: 'test2@test.com'
  };

  const auditorUser = {
    id: 3,
    firstName: 'test',
    lastName: 'test',
    email: 'auditorUser@email.com',
    address: '0x000000000000a000000000000000000000000000'
  };

  const beneficiaryUser = {
    id: 4,
    firstName: 'test',
    lastName: 'test',
    email: 'beneficiaryUser@email.com',
    address: '0x0000000000000000000000000000000000000000',
    pinStatus: pinStatus.ACTIVE
  };

  const adminUser = {
    id: 5,
    firstName: 'admin',
    lastName: 'admin',
    isAdmin: true
  };

  // PROJECTS
  const newProject = {
    id: 1,
    status: projectStatuses.DRAFT,
    owner: userEntrepreneur.id,
    goalAmount: 5000,
    type: PROJECT_TYPES.GRANT
  };

  const executingProject = {
    id: 2,
    status: projectStatuses.EXECUTING,
    owner: userEntrepreneur.id,
    goalAmount: 5000
  };

  const draftProject = {
    id: 10,
    status: projectStatuses.DRAFT,
    owner: 3,
    goalAmount: 5000,
    dataComplete: 1
  };

  const publishedProject = {
    id: 11,
    status: projectStatuses.PUBLISHED,
    owner: 3,
    goalAmount: 5000,
    dataComplete: 11,
    currency: 'ETH'
  };

  // USER PROJECT
  const auditorRegularUser = {
    id: 1,
    user: regularUser.id,
    project: executingProject.id,
    role: auditorRole.id
  };

  const beneficiaryRegularUser2 = {
    id: 2,
    user: regularUser2.id,
    project: executingProject.id,
    role: beneficiaryRole.id
  };
  // USER PROJECTS
  const auditorUserProject = {
    role: auditorRole.id,
    project: newProject.id,
    user: auditorUser.id
  };
  const beneficiaryUserProject = {
    role: beneficiaryRole.id,
    project: newProject.id,
    user: beneficiaryUser.id
  };

  // MILESTONES
  const updatableMilestone = {
    id: 1,
    project: newProject.id
  };

  const nonUpdatableMilestone = {
    id: 2,
    project: executingProject.id
  };

  // TASKS
  const updatableTask = {
    id: 1,
    description: 'TaskDescription',
    budget: '5000',
    milestone: updatableMilestone.id,
    status: ACTIVITY_STATUS.IN_PROGRESS,
    step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
  };

  const nonUpdatableTask = {
    id: 2,
    milestone: nonUpdatableMilestone.id,
    auditor: regularUser.id,
    status: ACTIVITY_STATUS.NEW
  };

  const newUdaptableTask = {
    id: 3,
    description: 'TaskDescription',
    budget: '5000',
    milestone: 10,
    status: ACTIVITY_STATUS.NEW,
    type: ACTIVITY_TYPES.FUNDING
  };

  const taskInReview = {
    id: 4,
    description: 'TaskDescription',
    budget: '5000',
    status: ACTIVITY_STATUS.IN_REVIEW,
    milestone: 11,
    proposer: 4,
    step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS,
    activityHash: 'activityHashTest'
  };

  const taskWithNoEvidences = {
    id: 5,
    description: 'TaskDescription',
    budget: '5000',
    status: ACTIVITY_STATUS.IN_REVIEW,
    milestone: updatableMilestone.id,
    step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
  };

  const taskWithNewStatus = {
    id: 6,
    description: 'TaskDescription',
    budget: '5000',
    status: ACTIVITY_STATUS.NEW,
    milestone: updatableMilestone.id,
    step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
  };

  const approvedTask = {
    id: 7,
    description: 'TaskDescription',
    budget: '5000',
    status: ACTIVITY_STATUS.APPROVED,
    milestone: updatableMilestone.id
  };

  const inReviewTask = {
    id: 8,
    description: 'TaskDescription',
    budget: '5000',
    status: ACTIVITY_STATUS.IN_REVIEW,
    milestone: updatableMilestone.id
  };

  const rejectedTask = {
    id: 9,
    milestone: nonUpdatableMilestone.id,
    auditor: regularUser.id,
    status: ACTIVITY_STATUS.REJECTED
  };

  const newUpdatableMilestone = {
    id: 10,
    project: draftProject.id,
    tasks: [newUdaptableTask]
  };

  const milestoneWithEmptyTasks = {
    id: 11,
    project: draftProject.id,
    tasks: [taskInReview]
  };

  const approvedMilestone = {
    id: 11,
    tasks: [newUdaptableTask],
    project: draftProject.id,
    status: MILESTONE_STATUS.APPROVED
  };

  // EVIDENCES
  const taskEvidence = {
    id: 1,
    createdAt: '2020-02-13',
    description: mockedDescription,
    proof: '/file/taskEvidence',
    approved: true,
    task: nonUpdatableTask.id,
    txHash: '0x111',
    status: txEvidenceStatus.SENT,
    auditor: {
      id: auditorUser.id,
      firstName: auditorUser.firstName,
      lastName: auditorUser.lastName
    },
    activity: {
      id: 1,
      title: 'Activity title',
      milestone: 1,
      auditor: auditorUser.id,
      status: 'in_progress'
    },
    files: [],
    user: {
      id: 12,
      firstName: 'Beneficiary firstName',
      lastName: 'Beneficiary lastName'
    }
  };

  const newTaskEvidence = {
    id: 1,
    createdAt: '2020-02-13',
    description: mockedDescription,
    proof: '/file/taskEvidence',
    approved: true,
    activity: { id: nonUpdatableTask.id },
    type: evidenceTypes.TRANSFER,
    txHash: '0x111',
    status: evidenceStatus.NEW
  };

  const newRejectedTaskEvidence = {
    id: 2,
    createdAt: '2020-02-13',
    description: mockedDescription,
    proof: '/file/taskEvidence',
    approved: true,
    activity: { id: nonUpdatableTask.id },
    txHash: '0x111',
    status: evidenceStatus.REJECTED
  };

  const updatableEvidenceTask = {
    id: 3,
    task: updatableTask.id,
    status: evidenceStatus.APPROVED
  };
  const nonUpdaptableEvidenceTask = {
    id: 4,
    status: evidenceStatus.APPROVED,
    task: nonUpdatableTask.id
  };
  const newUpdatableEvidenceTask = {
    id: 5,
    status: evidenceStatus.REJECTED,
    task: newUdaptableTask.id
  };
  const taskInReviewEvidenceTask = {
    id: 6,
    status: evidenceStatus.REJECTED,
    task: taskInReview.id
  };

  const activityDao = {
    findById: id => dbTask.find(task => task.id === id),
    updateActivity: (params, activityId) => {
      const found = dbTask.find(task => task.id === activityId);
      if (!found) return;
      const updated = { ...found, ...params };
      dbTask[dbTask.indexOf(found)] = updated;
      return updated;
    },
    deleteActivity: id => {
      const found = dbTask.find(task => task.id === id);
      if (!found) return;
      dbTask.splice(dbTask.indexOf(found), 1);
      return found;
    },
    saveActivity: (activity, milestoneId) => {
      const newTaskId =
        dbTask.length > 0 ? dbTask[dbTask.length - 1].id + 1 : 1;
      const newTask = {
        milestone: milestoneId,
        id: newTaskId,
        ...activity
      };
      dbTask.push(newTask);
      return newTask;
    },
    getActivityByIdWithMilestone: taskId => {
      const found = dbTask.find(task => task.id === taskId);
      if (!found) return;
      const populatedMilestone = dbMilestone.find(
        milestone => milestone.id === found.milestone
      );
      return {
        ...found,
        milestone: populatedMilestone
      };
    },
    getTasksByMilestone: milestoneId =>
      dbTask.filter(task => task.milestone === milestoneId),
    getActivitiesByMilestones: jest.fn()
  };

  const activityEvidenceDao = {
    findById: id => dbTaskEvidence.find(evidence => evidence.id === id),
    findByTxHash: hash =>
      dbTaskEvidence.find(evidence => evidence.txHash === hash),
    addActivityEvidence: ({ description, proof, approved, task }) => {
      const newTaskEvidenceId =
        dbTaskEvidence.length > 0
          ? dbTaskEvidence[dbTaskEvidence.length - 1].id + 1
          : 1;

      const _newTaskEvidence = {
        id: newTaskEvidenceId,
        task,
        description,
        proof,
        approved
      };

      dbTaskEvidence.push(_newTaskEvidence);
      return _newTaskEvidence;
    },
    getEvidencesByTaskId: taskId => {
      const evidences = dbTaskEvidence.filter(
        evidence => evidence.task === taskId
      );

      return evidences;
    },
    updateTaskEvidence: (id, { status }) => {
      const found = dbTaskEvidence.find(e => e.id === id);
      if (!found) return;
      const updated = { ...found, status };
      dbTaskEvidence[dbTaskEvidence.indexOf(found)] = updated;
      return updated;
    },
    findAllSentTxs: () =>
      dbTaskEvidence
        .filter(ev => ev.status === txEvidenceStatus.SENT)
        .map(({ id, txHash }) => ({ id, txHash })),
    getEvidencesByActivityId: jest.fn(),
    getApprovedEvidences: jest.fn()
  };

  const milestoneService = {
    getProjectFromMilestone: id => {
      const found = dbMilestone.find(milestone => milestone.id === id);
      if (!found)
        throw new COAError(errors.common.CantFindModelWithId('milestone', id));
      return dbProject.find(project => project.id === found.project);
    },
    getAllMilestonesByProject: projectId =>
      dbMilestone.filter(milestone => milestone.project === projectId),
    getMilestoneById: jest.fn()
  };

  const userService = {
    getUserById: id => {
      const found = dbUser.find(user => user.id === id);
      if (!found)
        throw new COAError(errors.common.CantFindModelWithId('user', id));
      return found;
    },
    getUserByAddress: address => {
      const found = dbUser.find(user => user.address === address);
      if (!found)
        throw new COAError(
          errors.common.CantFindModelWithAddress('user', address)
        );
      return found;
    }
  };

  const projectService = {
    ...originalProjectService,
    isOracleCandidate: jest.fn(),
    updateProject: (projectId, params) => {
      const found = dbProject.find(task => task.id === projectId);
      if (!found) return;
      const updated = { ...found, ...params };
      dbProject[dbProject.indexOf(found)] = updated;
      return updated;
    },
    getProjectById: id => {
      const found = dbProject.find(project => project.id === id);
      if (!found)
        throw new COAError(errors.common.CantFindModelWithId('project', id));
      return found;
    }
  };

  const userProjectService = {
    getUserProjectFromRoleDescription: ({
      projectId,
      roleDescriptions,
      userId
    }) => {
      const roleFound = dbRole.find(role =>
        roleDescriptions.includes(role.description)
      );
      const found = dbUserProject.find(
        up =>
          up.role === roleFound.id &&
          up.project === projectId &&
          up.user === userId
      );
      if (!found)
        throw new COAError(errors.user.UserNotRelatedToTheProjectAndRole);
      return found;
    },
    getBeneficiaryByProjectId: jest.fn(),
    validateUserWithRoleInProject: jest.fn()
  };

  const userProjectDao = {
    findUserProject: ({ user, project, role }) =>
      dbUserProject.find(
        up => up.user === user && up.project === project && up.role === role
      )
  };

  const milestoneDao = {
    findById: id => dbMilestone.find(milestone => milestone.id === id),
    updateMilestone: (fields, milestoneId) => {
      const found = dbMilestone.find(milestone => milestone.id === milestoneId);
      if (!found) return undefined;
      const updated = { ...found, ...fields };
      dbMilestone[dbMilestone.indexOf(found)] = updated;
      return updated;
    },
    getMilestonesByProjectId: jest.fn()
  };

  const projectDao = {
    findById: id => dbProject.find(p => p.id === id)
  };

  const roleDao = {
    getRoleByDescription: description =>
      dbRole.find(role => role.description === description)
  };

  const roleService = {
    getRoleByDescription: jest.fn(),
    getRolesByDescriptionIn: jest.fn()
  };

  const fileService = {
    saveFile: jest.fn(),
    getFileById: jest.fn()
  };

  const evidenceFileService = {
    saveEvidenceFile: jest.fn()
  };

  const txActivityDao = {
    createTxActivity: txActivity => dbTxActivity.push(txActivity)
  };

  const storageService = {
    saveStorageData: jest.fn(() => Promise.resolve('proofHashTest'))
  };

  const changelogService = {
    createChangelog: jest.fn(() => Promise.resolve()),
    findDeletedEvidenceInChangelog: jest.fn(() => Promise.resolve([]))
  };

  const blockchainService = {
    getTransaction: jest.fn(() =>
      Promise.resolve({
        value: '75300000000000000',
        tokenSymbol: 'ETH',
        decimals: 18,
        to: 'address'
      })
    ),
    isNativeToken: jest.fn(() => true)
  };

  beforeAll(async () => {
    restoreActivityService();
    files.validateAndSaveFile = jest.fn((type, file) => {
      validateMtype(type, file);
      validatePhotoSize(file);
      return '/dir/path';
    });
    files.saveFile = jest.fn(() => '/dir/path');
    files.getSaveFilePath = jest.fn(() => '/dir/path');
    coa.sendNewTransaction = jest.fn();
    coa.getTransactionResponse = jest.fn(() => null);
    coa.getBlock = jest.fn();
    await run('deploy', { resetStates: true });
  });

  beforeEach(() => resetDb());
  afterAll(() => {
    jest.clearAllMocks();
  });

  describe('Testing updateActivity', () => {
    const users = [adminUser, userEntrepreneur];
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        milestoneService,
        projectService,
        roleDao,
        userProjectDao,
        changelogService,
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });
    });

    beforeEach(() => {
      dbProject.push(draftProject, executingProject, newProject);
      dbTask.push(
        { id: 10, ...newActivity, milestone: newUpdatableMilestone.id },
        nonUpdatableTask,
        approvedTask
      );
      dbMilestone.push(
        newUpdatableMilestone,
        updatableMilestone,
        nonUpdatableMilestone
      );
      dbUser.push(userEntrepreneur);
    });

    afterEach(() => jest.restoreAllMocks());

    afterAll(() => restoreActivityService());

    const activityToUpdate = {
      title: 'Updated title',
      description: 'Updated description',
      acceptanceCriteria: 'Updated acceptance criteria',
      budget: 1.5,
      auditor: 3,
      status: ACTIVITY_STATUS.NEW,
      milestone: newUpdatableMilestone,
      type: ACTIVITY_TYPES.SPENDING
    };

    it('should update the activity and return its id', async () => {
      jest
        .spyOn(activityService, 'validateAuditorIsInProject')
        .mockImplementation();

      jest
        .spyOn(originalUserService, 'getUserProjectFromRoleDescription')
        .mockReturnValue({});
      const response = await activityService.updateActivity({
        activityId: 10,
        ...activityToUpdate,
        user: adminUser
      });
      expect(response).toEqual({ activityId: 10 });
      const updated = dbTask.find(
        activity => activity.id === response.activityId
      );
      expect(updated.title).toEqual(activityToUpdate.title);
      expect(updated.description).toEqual(activityToUpdate.description);
      expect(updated.acceptanceCriteria).toEqual(
        activityToUpdate.acceptanceCriteria
      );
      expect(
        BigNumber(updated.budget).eq(activityToUpdate.budget)
      ).toBeTruthy();
      expect(updated.auditor).toEqual(activityToUpdate.auditor);
    });

    it('should update the activity and update the goal amount project and return the activity id', async () => {
      const initialGoalAmount = BigNumber(1000);

      jest
        .spyOn(activityService, 'validateAuditorIsInProject')
        .mockImplementation();

      dbProject = [{ ...draftProject, goalAmount: initialGoalAmount }];
      const response = await activityService.updateActivity({
        activityId: 10,
        ...activityToUpdate,
        budget: '500',
        user: adminUser
      });
      const updatedActivity = dbTask.find(
        activity => activity.id === response.activityId
      );
      const updatedProject = dbProject.find(
        project => project.id === draftProject.id
      );
      expect(response).toEqual({ activityId: 10 });
      expect(updatedActivity.title).toEqual(activityToUpdate.title);
      expect(updatedActivity.description).toEqual(activityToUpdate.description);
      expect(updatedActivity.acceptanceCriteria).toEqual(
        activityToUpdate.acceptanceCriteria
      );
      expect(BigNumber(updatedProject.goalAmount).eq(0)).toBeTruthy();
    });

    it('should throw when activity status is not editable', async () => {
      jest
        .spyOn(activityService, 'validateAuditorIsInProject')
        .mockImplementation();

      jest
        .spyOn(originalUserService, 'getUserProjectFromRoleDescription')
        .mockReturnValue({});
      await expect(
        activityService.updateActivity({
          activityId: approvedTask.id,
          ...activityToUpdate,
          user: adminUser
        })
      ).rejects.toThrow(
        errors.task.CantUpdateTaskWithStatus(approvedTask.status)
      );
    });

    it('should throw an error if the activity type is not valid', async () => {
      await expect(
        activityService.updateActivity({
          activityId: 10,
          user: adminUser,
          ...newActivity,
          type: 'InvalidType'
        })
      ).rejects.toThrow(errors.task.InvalidActivityType);
    });

    it('should throw an error if the project status is not valid', async () => {
      await expect(
        activityService.updateActivity({
          activityId: 2,
          user: adminUser,
          ...newActivity
        })
      ).rejects.toThrow(
        errors.task.UpdateWithInvalidProjectStatus(projectStatuses.EXECUTING)
      );
    });

    it('should throw an error if the auditor param received does not have auditor role in project', async () => {
      jest
        .spyOn(roleDao, 'getRoleByDescription')
        .mockImplementation(async () =>
          Promise.resolve({ id: 3, description: 'auditor' })
        );

      jest
        .spyOn(userProjectDao, 'findUserProject')
        .mockImplementation(async () => Promise.resolve(undefined));

      await expect(
        activityService.updateActivity({
          activityId: 10,
          user: adminUser,
          ...newActivity
        })
      ).rejects.toThrow(
        errors.task.UserIsNotAuditorInProject(3, draftProject.id)
      );
    });
  });

  describe('Testing createActivity', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        milestoneService,
        projectService,
        roleDao,
        userProjectDao,
        milestoneDao,
        changelogService
      });
    });

    beforeEach(() => {
      dbProject.push(draftProject, executingProject, newProject);
      dbMilestone.push(
        newUpdatableMilestone,
        updatableMilestone,
        nonUpdatableMilestone,
        approvedMilestone
      );
      dbUser.push(userEntrepreneur);
    });

    afterEach(() => jest.restoreAllMocks());

    afterAll(() => restoreActivityService());

    it('should create the activity and return its id', async () => {
      jest
        .spyOn(activityService, 'validateAuditorIsInProject')
        .mockImplementation();

      const response = await activityService.createActivity({
        milestoneId: newUpdatableMilestone.id,
        user: adminUser,
        ...newActivity
      });
      const createdActivity = dbTask.find(
        task => task.id === response.activityId
      );
      expect(response).toHaveProperty('activityId');
      expect(response.activityId).toBeDefined();
      expect(createdActivity).toHaveProperty('id', response.activityId);
      expect(createdActivity).toHaveProperty(
        'milestone',
        newUpdatableMilestone.id
      );
      expect(createdActivity).toHaveProperty('title', 'Title');
      expect(createdActivity).toHaveProperty('description', 'Description');
      expect(createdActivity).toHaveProperty(
        'acceptanceCriteria',
        'Acceptance criteria'
      );
      expect(createdActivity).toHaveProperty('budget', '1000');
      expect(createdActivity).toHaveProperty('auditor', 3);
    });

    it('should create the activity, add the budget to the project goal amount and return the activity id', async () => {
      const initialGoalAmount = BigNumber(1000);

      jest
        .spyOn(activityService, 'validateAuditorIsInProject')
        .mockImplementation();

      dbProject = [{ ...draftProject, goalAmount: initialGoalAmount }];
      const response = await activityService.createActivity({
        milestoneId: newUpdatableMilestone.id,
        ...newActivity,
        user: adminUser
      });
      const createdActivity = dbTask.find(
        task => task.id === response.activityId
      );
      const updatedProject = dbProject.find(
        project => project.id === draftProject.id
      );
      expect(createdActivity).toHaveProperty('title', 'Title');
      expect(createdActivity).toHaveProperty('description', 'Description');
      expect(createdActivity).toHaveProperty(
        'acceptanceCriteria',
        'Acceptance criteria'
      );
      expect(createdActivity).toHaveProperty('budget', '1000');
      expect(createdActivity).toHaveProperty('auditor', 3);
      expect(
        BigNumber(updatedProject.goalAmount).eq(
          initialGoalAmount.plus(newActivity.budget)
        )
      ).toBeTruthy();
    });

    it('should throw an error when milestone status is approved', async () => {
      await expect(
        activityService.createActivity({
          milestoneId: approvedMilestone.id,
          ...newActivity,
          user: adminUser
        })
      ).rejects.toThrow(errors.milestone.MilestoneIsApproved);
    });

    it('should throw an error if a milestoneId is not received', async () => {
      await expect(
        activityService.createActivity({
          ...newActivity,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a title is not received', async () => {
      const { title, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a description is not received', async () => {
      const { description, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a acceptanceCriteria is not received', async () => {
      const { acceptanceCriteria, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a acceptanceCriteria is not received', async () => {
      const { acceptanceCriteria, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a budget is not received', async () => {
      const { budget, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a auditor is not received', async () => {
      const { auditor, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if a type is not received', async () => {
      const { type, ...rest } = newActivity;
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...rest,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('createActivity'));
    });

    it('should throw an error if the activity type is not valid', async () => {
      await expect(
        activityService.createActivity({
          milestoneId: 0,
          ...newActivity,
          user: adminUser,
          type: 'invalidType'
        })
      ).rejects.toThrow(errors.task.InvalidActivityType);
    });

    it('should throw an error if try to add a paypack activity type to grant project type', async () => {
      await expect(
        activityService.createActivity({
          milestoneId: 1,
          ...newActivity,
          user: adminUser,
          type: ACTIVITY_TYPES.PAYBACK
        })
      ).rejects.toThrow(
        errors.task.InvalidActivityTypeInProjectType(ACTIVITY_TYPES.PAYBACK)
      );
    });

    it('should throw an error if the milestone does not exist', async () => {
      await expect(
        activityService.createActivity({
          milestoneId: 0,
          ...newActivity,
          user: adminUser
        })
      ).rejects.toThrow(errors.common.CantFindModelWithId('milestone', 0));
    });

    it('should throw an error if the project status is not valid', async () => {
      await expect(
        activityService.createActivity({
          milestoneId: nonUpdatableMilestone.id,
          ...newActivity,
          user: adminUser
        })
      ).rejects.toThrow(
        errors.task.CreateWithInvalidProjectStatus(projectStatuses.EXECUTING)
      );
    });

    it('should throw an error if the auditor param receveid does not have auditor role in project', async () => {
      jest
        .spyOn(roleDao, 'getRoleByDescription')
        .mockImplementation(async () =>
          Promise.resolve({ id: 3, description: 'auditor' })
        );

      jest
        .spyOn(userProjectDao, 'findUserProject')
        .mockImplementation(async () => Promise.resolve(undefined));

      await expect(
        activityService.createActivity({
          milestoneId: newUpdatableMilestone.id,
          ...newActivity,
          user: adminUser
        })
      ).rejects.toThrow(
        errors.task.UserIsNotAuditorInProject(3, draftProject.id)
      );
    });
  });

  describe('Testing deleteActivity', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        milestoneService,
        projectService,
        changelogService
      });
    });

    beforeEach(() => {
      dbProject.push(newProject, executingProject, draftProject);
      dbTask.push(
        updatableTask,
        nonUpdatableTask,
        newUdaptableTask,
        approvedTask
      );
      dbMilestone.push(
        updatableMilestone,
        nonUpdatableMilestone,
        newUpdatableMilestone
      );
      dbUser.push(userEntrepreneur);
    });

    afterAll(() => restoreActivityService());
    it(
      'should delete the task, substract the budget from the project goal amount ' +
        'and return the task id',
      async () => {
        const updateProjectSpy = jest
          .spyOn(projectService, 'updateProject')
          .mockImplementation((_, fields) =>
            Promise.resolve({
              ...draftProject,
              ...fields
            })
          );
        const response = await activityService.deleteActivity(
          newUdaptableTask.id,
          adminUser
        );
        const updatedTask = dbTask.find(task => task.id === response.taskId);
        expect(response).toEqual({ taskId: newUdaptableTask.id });
        expect(updatedTask).toEqual(undefined);
        expect(updateProjectSpy).toHaveBeenCalledWith(draftProject.id, {
          goalAmount: `${draftProject.goalAmount - newUdaptableTask.budget}`
        });
      }
    );
    it('should throw when activity status is not NEW', async () => {
      jest
        .spyOn(projectService, 'updateProject')
        .mockImplementation((_, fields) =>
          Promise.resolve({
            ...draftProject,
            ...fields
          })
        );
      await expect(
        activityService.deleteActivity(approvedTask.id, adminUser)
      ).rejects.toThrow(
        errors.task.CantDeleteTaskWithStatus(approvedTask.status)
      );
    });
    it('should throw an error if parameters are not valid', async () => {
      await expect(activityService.deleteActivity()).rejects.toThrow(
        errors.common.RequiredParamsMissing('deleteActivity')
      );
    });

    it('should throw an error if task does not exist', async () => {
      await expect(
        activityService.deleteActivity(0, userEntrepreneur.id)
      ).rejects.toThrow(errors.common.CantFindModelWithId('activity', 0));
    });

    it('should throw an error if the project status is not DRAFT', async () => {
      await expect(
        activityService.deleteActivity(nonUpdatableTask.id, userEntrepreneur.id)
      ).rejects.toThrow(
        errors.task.DeleteWithInvalidProjectStatus(projectStatuses.EXECUTING)
      );
    });
  });
  describe('Testing addEvidence', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        activityEvidenceDao,
        projectService,
        roleService,
        userProjectDao,
        fileService,
        evidenceFileService,
        milestoneDao,
        blockchainService,
        changelogService,
        userProjectService
      });
    });

    beforeEach(async () => {
      dbUser.push(userEntrepreneur);
      dbTask.push(
        {
          ...nonUpdatableTask,
          oracle: userEntrepreneur.id
        },
        approvedTask,
        inReviewTask,
        rejectedTask
      );
      dbMilestone.push(nonUpdatableMilestone);
      dbProject.push({
        ...executingProject,
        address: '0xEa51CfB26e6547725835b4138ba96C0b5de9E54A'
      });
    });

    afterEach(() => jest.restoreAllMocks());

    afterAll(() => restoreActivityService());

    xit('should add the evidence to ativity and return its id', async () => {
      jest.spyOn(activityDao, 'getTasksByMilestone').mockResolvedValue([]);
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() =>
          Promise.resolve({ project: 1, id: nonUpdatableMilestone.id })
        );

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.PUBLISHED
        })
      );

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      jest
        .spyOn(files, 'saveFile')
        .mockImplementation(() => Promise.resolve('testPath'));

      jest
        .spyOn(fileService, 'saveFile')
        .mockImplementation(file => Promise.resolve({ id: 1, ...file }));

      jest
        .spyOn(activityEvidenceDao, 'addActivityEvidence')
        .mockImplementation(evidence =>
          Promise.resolve({ id: 1, ...evidence })
        );

      jest.spyOn(evidenceFileService, 'saveEvidenceFile').mockImplementation();

      const updateMilestoneSpy = jest.spyOn(milestoneDao, 'updateMilestone');

      const response = await activityService.addEvidence({
        activityId: nonUpdatableTask.id,
        userId: userEntrepreneur.id,
        title: 'Evidence title',
        description: 'Evidence description',
        type: evidenceTypes.IMPACT,
        files: { evidenceFile }
      });

      expect(response).toEqual({ evidenceId: 1 });
      expect(updateActivitySpy).toHaveBeenCalledWith(
        {
          status: ACTIVITY_STATUS.IN_PROGRESS
        },
        nonUpdatableTask.id
      );
      expect(updateMilestoneSpy).toHaveBeenCalledWith(
        { status: MILESTONE_STATUS.IN_PROGRESS },
        nonUpdatableMilestone.id
      );
    });

    xit('should add the evidence to ativity and update activity to in progress status when previously was rejected', async () => {
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');
      jest
        .spyOn(activityEvidenceDao, 'getEvidencesByTaskId')
        .mockResolvedValue([newTaskEvidence]);
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() =>
          Promise.resolve({ project: 1, id: nonUpdatableMilestone.id })
        );

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.PUBLISHED
        })
      );

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      jest
        .spyOn(files, 'saveFile')
        .mockImplementation(() => Promise.resolve('testPath'));

      jest
        .spyOn(fileService, 'saveFile')
        .mockImplementation(file => Promise.resolve({ id: 1, ...file }));

      jest
        .spyOn(activityEvidenceDao, 'addActivityEvidence')
        .mockImplementation(evidence =>
          Promise.resolve({ id: 1, ...evidence })
        );

      jest.spyOn(evidenceFileService, 'saveEvidenceFile').mockImplementation();

      const response = await activityService.addEvidence({
        activityId: rejectedTask.id,
        userId: userEntrepreneur.id,
        title: 'Evidence title',
        description: 'Evidence description',
        type: evidenceTypes.IMPACT,
        files: { evidenceFile }
      });

      expect(response).toEqual({ evidenceId: 1 });
      expect(updateActivitySpy).toHaveBeenCalledWith(
        {
          status: ACTIVITY_STATUS.IN_PROGRESS
        },
        rejectedTask.id
      );
    });

    it('should throw an error if activity is in approved status', async () => {
      await expect(
        activityService.addEvidence({
          activityId: approvedTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile }
        })
      ).rejects.toThrow(
        errors.task.ActivityIsApprovedOrInProgress(approvedTask.status)
      );
    });

    it('should throw an error if activity is in in-review status', async () => {
      await expect(
        activityService.addEvidence({
          activityId: inReviewTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile }
        })
      ).rejects.toThrow(
        errors.task.ActivityIsApprovedOrInProgress(inReviewTask.status)
      );
    });

    it('should throw error if activityId required param is missing', async () => {
      await expect(
        activityService.addEvidence({
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile }
        })
      ).rejects.toThrow("[ActivityService] :: There was an error: ValidationError: \"activityId\" is required");
    });

    it('should throw error if userId required param is missing', async () => {
      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile }
        })
      ).rejects.toThrow("[ActivityService] :: There was an error: ValidationError: \"userId\" is required");
    });

    it('should throw error if title required param is missing', async () => {
      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile }
        })
      ).rejects.toThrow("[ActivityService] :: There was an error: ValidationError: \"title\" is required");
    });

    it('should throw error if description required param is missing', async () => {
      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile }
        })
      ).rejects.toThrow("[ActivityService] :: There was an error: ValidationError: \"description\" is required");
    });

    it('should throw error if type required param is missing', async () => {
      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          files: { evidenceFile }
        })
      ).rejects.toThrow("[ActivityService] :: There was an error: ValidationError: \"type\" is required");
    });

    it('should throw error if files required param is missing in impact evidence type', async () => {
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() => Promise.resolve({ project: 1 }));

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.PUBLISHED
        })
      );

      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('addEvidence'));
    });

    it('should throw error if files required param is missing in transfer fiat evidence type', async () => {
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() => Promise.resolve({ project: 1 }));

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'fiat',
          status: projectStatuses.PUBLISHED
        })
      );

      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.TRANSFER,
          amount: '123'
        })
      ).rejects.toThrow(errors.common.RequiredParamsMissing('addEvidence'));
    });

    it('should throw error if invalid type is passed', async () => {
      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: 'invalidtype',
          files: { evidenceFile }
        })
      ).rejects.toThrow(errors.task.InvalidEvidenceType('invalidtype'));
    });

    it('should throw an error if the project is not in executing status', async () => {
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() => Promise.resolve({ project: 1 }));

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.DRAFT,
          currency: 'ETH',
          additionalCurrencyInformation: 'address'
        })
      );

      jest
        .spyOn(roleService, 'getRolesByDescriptionIn')
        .mockImplementation(() =>
          Promise.resolve([
            { id: 1, description: 'beneficiary' },
            { id: 2, description: 'founder' }
          ])
        );

      jest
        .spyOn(userProjectDao, 'findUserProject')
        .mockImplementation(() =>
          Promise.resolve({ id: 1, project: 1, user: 1, role: 1 })
        );

      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.TRANSFER,
          amount: '123',
          transferTxHash: 'txHash'
        })
      ).rejects.toThrow(
        errors.project.InvalidStatusForEvidenceUpload(projectStatuses.DRAFT)
      );
    });

    it('should throw an error if user is not the beneficiary', async () => {
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() => Promise.resolve({ project: 1 }));

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.IN_PROGRESS,
          currency: 'ETH',
          additionalCurrencyInformation: 'address'
        })
      );

      jest
        .spyOn(roleService, 'getRolesByDescriptionIn')
        .mockImplementation(() =>
          Promise.resolve([
            { id: 1, description: 'beneficiary' },
            { id: 2, description: 'founder' }
          ])
        );

      jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockImplementation(({ error }) => {
          throw new COAError(error);
        });

      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.TRANSFER,
          amount: '123',
          transferTxHash: 'txHash'
        })
      ).rejects.toThrow(
        errors.task.UserCanNotAddEvidenceToProject({
          userId: userEntrepreneur.id,
          activityId: nonUpdatableTask.id,
          projectId: 1
        })
      );
    });

    it('should throw an error if the file mtype is invalid', async () => {
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() => Promise.resolve({ project: 1 }));

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.PUBLISHED
        })
      );

      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile: { name: 'invalidclaim.exe', size: 2000 } }
        })
      ).rejects.toThrow(errors.file.EvidenceFileTypeNotValid);
    });

    it('should throw an error if the file has an invalid size', async () => {
      jest
        .spyOn(activityService, 'getMilestoneFromActivityId')
        .mockImplementation(() => Promise.resolve({ project: 1 }));

      jest.spyOn(projectService, 'getProjectById').mockImplementation(() =>
        Promise.resolve({
          id: 1,
          currencyType: 'crypto',
          status: projectStatuses.IN_PROGRESS
        })
      );

      await expect(
        activityService.addEvidence({
          activityId: nonUpdatableTask.id,
          userId: userEntrepreneur.id,
          title: 'Evidence title',
          description: 'Evidence description',
          type: evidenceTypes.IMPACT,
          files: { evidenceFile: { name: 'imbig.jpg', size: 999999999999 } }
        })
      ).rejects.toThrow(errors.file.ImgSizeBiggerThanAllowed);
    });
  });
  describe('Testing updateEvidenceStatus', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        activityEvidenceDao,
        roleDao,
        userProjectDao,
        projectService,
        changelogService
      });
    });
    beforeEach(() => {
      resetDb();
      dbTaskEvidence.push(newTaskEvidence, newRejectedTaskEvidence);
      dbUserProject.push(auditorRegularUser, beneficiaryRegularUser2);
      dbRole.push(auditorRole);
      dbTask.push(nonUpdatableTask);
      dbMilestone.push(nonUpdatableMilestone);
      dbProject.push(executingProject);
    });
    afterEach(() => jest.clearAllMocks());
    afterAll(() => restoreActivityService());
    it('should successfully update evidence status', async () => {
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: newTaskEvidence.id,
          newStatus: validStatusToChange[0],
          userId: regularUser.id
        })
      ).resolves.toEqual({ success: true });
    });
    it('should throw when the given status is not a valid one', async () => {
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: newTaskEvidence.id,
          newStatus: evidenceStatus.NEW,
          userId: regularUser.id
        })
      ).rejects.toThrow(errors.task.EvidenceStatusNotValid(evidenceStatus.NEW));
    });
    it('should throw when evidence status is not NEW', async () => {
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: newRejectedTaskEvidence.id,
          newStatus: validStatusToChange[0],
          userId: regularUser.id
        })
      ).rejects.toThrow(
        errors.task.EvidenceStatusCannotChange(newRejectedTaskEvidence.status)
      );
    });
    it('should throw when the given evidence doesnt exist', async () => {
      const unexistentEvidenceId = 99999;
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: unexistentEvidenceId,
          newStatus: validStatusToChange[0],
          userId: regularUser.id
        })
      ).rejects.toThrow(
        errors.common.CantFindModelWithId('evidence', unexistentEvidenceId)
      );
    });
    it('should throw when the user doesnt have an auditor role in the project', async () => {
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: newTaskEvidence.id,
          newStatus: validStatusToChange[0],
          userId: regularUser2.id
        })
      ).rejects.toThrow(errors.task.UserCantUpdateEvidence);
    });
    it('should throw when evidence couldnt be updated', async () => {
      jest
        .spyOn(activityEvidenceDao, 'updateTaskEvidence')
        .mockReturnValue(undefined);
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: newTaskEvidence.id,
          newStatus: validStatusToChange[0],
          userId: regularUser.id
        })
      ).rejects.toThrow(errors.task.EvidenceUpdateError);
    });
    it('should throw when the auditor role doesnt exist', async () => {
      jest.spyOn(roleDao, 'getRoleByDescription').mockReturnValue(undefined);
      await expect(
        activityService.updateEvidenceStatus({
          evidenceId: newTaskEvidence.id,
          newStatus: validStatusToChange[0],
          userId: regularUser.id
        })
      ).rejects.toThrow(errors.common.ErrorGetting('role'));
    });
  });
  describe('Testing updateActivityStatus', () => {
    const users = [beneficiaryUser, auditorUser];
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        userProjectService,
        txActivityDao,
        storageService,
        activityEvidenceDao,
        milestoneDao,
        projectDao,
        changelogService,
        userService,
        projectService: {
          ...projectService,
          mapProjectToPublish: jest.fn().mockResolvedValue({ ...newProject, users: [], milestones: [] }),
          getUsersByProjectId: jest.fn().mockResolvedValue([
            { role: 'auditor', country: 1  },
            { role: 'beneficiary', country: 1 }
          ]),
          uploadProjectMetadataToIPFS: jest.fn().mockResolvedValue({ metadataHash: '', metadataFile: {} })
        },
        milestoneService,
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });

      injectMocks(originalCountryService, {
        countryDao: {
         findAllByProps: () => [
           { id: 1, name: 'argentina'}
         ]
       }
     })
     jest.spyOn(Date.prototype, 'toISOString').mockReturnValue('2023-09-18T20:00:29.309Z');
    });

    beforeEach(() => {
      dbProject.push(newProject, executingProject, draftProject);
      dbTask.push(
        updatableTask,
        nonUpdatableTask,
        newUdaptableTask,
        taskInReview,
        taskWithNoEvidences,
        taskWithNewStatus
      );
      dbMilestone.push(
        milestoneWithEmptyTasks,
        updatableMilestone,
        nonUpdatableMilestone,
        newUpdatableMilestone
      );
      dbUser.push(auditorUser, beneficiaryUser);
      dbRole.push(auditorRole, beneficiaryRole);
      dbUserProject.push(auditorUserProject, beneficiaryUserProject);
      dbTaskEvidence.push(
        updatableEvidenceTask,
        nonUpdaptableEvidenceTask,
        newUpdatableEvidenceTask,
        taskInReviewEvidenceTask
      );
    });
    afterEach(() => jest.clearAllMocks());
    afterAll(() => {
      restoreActivityService()
      dateNowSpy.mockRestore();
    });

    it("should successfully update activity status to 'in-review' status", async () => {
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');

      const proposedIpfsHash = 'ipfsHashTest'
      jest
        .spyOn(storageService, 'saveStorageData')
        .mockResolvedValue(proposedIpfsHash);

      const response = await activityService.updateActivityStatus({
        activityId: updatableTask.id,
        user: beneficiaryUser,
        status: ACTIVITY_STATUS.IN_REVIEW,
        txId: 'txId'
      });

      const messageHash = getMessageHash(
        ['uint256', 'string', 'bytes32', 'string', 'uint256', 'string'],
          [
            updateActivitySpy.mock.calls[1][0].toSign.nonce,
            newProject.id,
            updateActivitySpy.mock.calls[1][0].toSign.claimHash,
            updateActivitySpy.mock.calls[1][0].toSign.proofHash,
            updatableTask.id,
            beneficiaryUser.email
          ]
      )

      expect(response).toEqual({
        success: true,
        toSign: messageHash
      });
    });
    it("should successfully update activity status to 'rejected' status", async () => {
      jest
        .spyOn(userProjectService, 'getUserProjectFromRoleDescription')
        .mockReturnValue({});
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');

      const response = await activityService.updateActivityStatus({
        activityId: taskInReview.id,
        user: auditorUser,
        status: ACTIVITY_STATUS.REJECTED,
        txId: 'txId'
      });

      const call = updateActivitySpy.mock.calls[1][0].toSign;
      const messageHash = getMessageHash(
        [
          'uint256',
          'string',
          'bytes32',
          'string',
          'address',
          'string',
          'string',
          'bool'
        ],
        [
          call.nonce,
          draftProject.id,
          call.claimHash,
          call.proposalProofHash,
          beneficiaryUser.address,
          auditorUser.email,
          call.auditProofHash,
          false
        ]
      )

      expect(response).toEqual({
        success: true,
        toSign: messageHash
      });
    });
    it("should successfully update activity status to 'approved' status", async () => {
      jest
        .spyOn(userProjectService, 'getUserProjectFromRoleDescription')
        .mockReturnValue({});
      jest.spyOn(utilFiles, 'getFileFromPath').mockReturnValue({});
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');

      const response = await activityService.updateActivityStatus({
        activityId: taskInReview.id,
        user: auditorUser,
        status: ACTIVITY_STATUS.APPROVED
      });

      const call = updateActivitySpy.mock.calls[1][0].toSign;
      const messageHash = getMessageHash(
        [
          'uint256',
          'string',
          'bytes32',
          'string',
          'address',
          'string',
          'string',
          'bool'
        ],
        [
          call.nonce,
          draftProject.id,
          call.claimHash,
          call.proposalProofHash,
          beneficiaryUser.address,
          auditorUser.email,
          call.auditProofHash,
          true
        ]
      )

      expect(response).toEqual({
        success: true,
        toSign:messageHash
      });
    });
    it("should successfully update activity status to 'rejected' status with a reason", async () => {
      jest.clearAllMocks();
      jest.spyOn(utilFiles, 'getFileFromPath').mockReturnValue({});
      jest
        .spyOn(userProjectService, 'getUserProjectFromRoleDescription')
        .mockReturnValue({});
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');
      const reason = 'activity does not accomplish the requirements';
      const response = await activityService.updateActivityStatus({
        activityId: taskInReview.id,
        user: auditorUser,
        status: ACTIVITY_STATUS.REJECTED,
        reason
      });

      const call = updateActivitySpy.mock.calls[1][0].toSign;
      const messageHash = getMessageHash(
        [
          'uint256',
          'string',
          'bytes32',
          'string',
          'address',
          'string',
          'string',
          'bool'
        ],
        [
          call.nonce,
          draftProject.id,
          call.claimHash,
          call.proposalProofHash,
          beneficiaryUser.address,
          auditorUser.email,
          call.auditProofHash,
          false
        ]
      )

      expect(response).toEqual({
        success: true,
        toSign: messageHash
      });
      expect(updateActivitySpy).toHaveBeenCalledWith(
        {
          approvalDate: '2023-09-18T20:00:29.309Z',
          auditorId: auditorUser.id,
          status: ACTIVITY_STATUS.PENDING_TO_REJECT,
          reason,
          step: ACTIVITY_STEPS.WAITING_SIGNATURE_AUTHORIZATION
        },
        taskInReview.id
      );
    });
    it("should ignore reason param when status is not 'rejected'", async () => {
      jest.clearAllMocks();
      jest.spyOn(utilFiles, 'getFileFromPath').mockReturnValue({});
      jest
        .spyOn(userProjectService, 'getUserProjectFromRoleDescription')
        .mockReturnValue({});
      const updateActivitySpy = jest.spyOn(activityDao, 'updateActivity');
      const reason = 'activity does not accomplish the requirements';
      const response = await activityService.updateActivityStatus({
        activityId: taskInReview.id,
        user: auditorUser,
        status: ACTIVITY_STATUS.APPROVED,
        reason
      });

      const call = updateActivitySpy.mock.calls[1][0].toSign;
      const messageHash = getMessageHash(
        [
          'uint256',
          'string',
          'bytes32',
          'string',
          'address',
          'string',
          'string',
          'bool'
        ],
        [
          call.nonce,
          draftProject.id,
          call.claimHash,
          call.proposalProofHash,
          beneficiaryUser.address,
          auditorUser.email,
          call.auditProofHash,
          true
        ]
      )

      expect(response).toEqual({
        success: true,
        toSign: messageHash
      });
      expect(updateActivitySpy).toHaveBeenCalledWith(
        {
          approvalDate: '2023-09-18T20:00:29.309Z',
          auditorId: auditorUser.id,
          status: ACTIVITY_STATUS.PENDING_TO_APPROVE,
          step: ACTIVITY_STEPS.WAITING_SIGNATURE_AUTHORIZATION
        },
        taskInReview.id
      );
    });
    it("should fail when trying to update from 'new' status", async () => {
      jest.clearAllMocks();
      jest.spyOn(utilFiles, 'getFileFromPath').mockReturnValue({});
      const saveStorageDataSpy = jest.spyOn(storageService, 'saveStorageData');
      await expect(
        activityService.updateActivityStatus({
          activityId: taskWithNewStatus.id,
          user: auditorUser,
          status: ACTIVITY_STATUS.IN_REVIEW
        })
      ).rejects.toThrow(errors.task.InvalidStatusTransition);
      expect(saveStorageDataSpy).not.toHaveBeenCalled();
    });
    it("should throw when new status is approved/rejected and not all of the evidences are 'rejected' or 'approved'", async () => {
      jest.clearAllMocks();
      jest.spyOn(utilFiles, 'getFileFromPath').mockReturnValue({});
      const saveStorageDataSpy = jest.spyOn(storageService, 'saveStorageData');
      await expect(
        activityService.updateActivityStatus({
          activityId: taskWithNoEvidences.id,
          user: auditorUser,
          status: ACTIVITY_STATUS.REJECTED
        })
      ).rejects.toThrow(errors.task.TaskNotReady);
      expect(saveStorageDataSpy).not.toHaveBeenCalled();
    });
    it('should fail when trying to update to an invalid status ', async () => {
      const invalidStatus = 'invalidStatus';
      await expect(
        activityService.updateActivityStatus({
          activityId: updatableTask.id,
          user: auditorUser,
          status: invalidStatus,
          txId: 'txI'
        })
      ).rejects.toThrow(errors.task.InvalidStatus(invalidStatus));
    });
    it('should fail when trying to update an invalid transition', async () => {
      await expect(
        activityService.updateActivityStatus({
          activityId: updatableTask.id,
          status: ACTIVITY_STATUS.REJECTED,
          user: auditorUser
        })
      ).rejects.toThrow(errors.task.InvalidStatusTransition);
    });
    it('should fail when couldnt update activity status', async () => {
      jest.spyOn(activityDao, 'updateActivity').mockReturnValue(undefined);
      await expect(
        activityService.updateActivityStatus({
          activityId: updatableTask.id,
          user: beneficiaryUser,
          status: ACTIVITY_STATUS.IN_REVIEW
        })
      ).rejects.toThrow(errors.task.ActivityStatusCantBeUpdated);
    });
    it('should fail when couldnt update activity status', async () => {
      jest.spyOn(activityDao, 'updateActivity').mockReturnValue(undefined);
      await expect(
        activityService.updateActivityStatus({
          activityId: updatableTask.id,
          user: beneficiaryUser,
          status: ACTIVITY_STATUS.IN_REVIEW
        })
      ).rejects.toThrow(errors.task.ActivityStatusCantBeUpdated);
    });
    it('should fail when couldnt update activity status', async () => {
      const user = {
        ...beneficiaryUser,
        pinStatus: pinStatus.APPROVED
      };
      injectMocks(activityService, {
        userDao: {
          findById: () => Promise.resolve({ id: 'proposerId', pinStatus: pinStatus.APPROVED }),
        },
      });
      
      await expect(
        activityService.updateActivityStatus({
          activityId: updatableTask.id,
          user,
          status: ACTIVITY_STATUS.IN_REVIEW
        })
      ).rejects.toThrow(errors.user.InvalidPinStatus);
    });
  });

  describe('Testing getActivityEvidences', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        activityEvidenceDao,
        fileService,
        milestoneService
      });
    });

    beforeEach(() => {
      jest.spyOn(activityDao, 'findById').mockImplementationOnce(() => ({
        id: 1,
        title: 'Activity title',
        description: 'Activity description',
        toSign: {
          messageHash: 'messageHashTest'
        }
      }));

      jest
        .spyOn(activityEvidenceDao, 'getEvidencesByActivityId')
        .mockImplementationOnce(() => [
          {
            id: 1,
            title: 'Title evidence',
            description: 'Evidence',
            type: 'transfer',
            transferTxHash: null,
            proof: null,
            approved: null,
            txHash: null,
            status: 'approved',
            createdAt: '2022-11-25T15:29:06.590Z',
            activity: 4,
            files: [{ id: 1, evidence: 1, file: 1 }]
          },
          {
            id: 2,
            title: 'Title evidence',
            description: 'Evidence',
            type: 'transfer',
            transferTxHash: null,
            proof: null,
            approved: null,
            txHash: null,
            status: 'approved',
            createdAt: '2022-11-25T16:52:37.799Z',
            activity: 4,
            files: []
          }
        ]);

      jest.spyOn(fileService, 'getFileById').mockImplementationOnce(() => ({
        id: 1,
        name: 'File test',
        path: '/path/test.pdf'
      }));
    });

    afterAll(() => restoreActivityService());

    it('should return a list of all task evidences', async () => {
      jest
        .spyOn(milestoneService, 'getMilestoneById')
        .mockImplementation(() =>
          Promise.resolve({ id: 1, title: 'Milestone title' })
        );

      const response = await activityService.getActivityEvidences({
        activityId: nonUpdatableTask.id
      });
      expect(response.evidences).toHaveLength(2);
      expect(response).toMatchObject({
        milestone: { id: 1, title: 'Milestone title' },
        activity: {
          id: 1,
          title: 'Activity title',
          description: 'Activity description'
        },
        evidences: [
          {
            id: 1,
            title: 'Title evidence',
            description: 'Evidence',
            type: 'transfer',
            transferTxHash: null,
            proof: null,
            approved: null,
            txHash: null,
            status: 'approved',
            createdAt: '2022-11-25T15:29:06.590Z',
            activity: 4,
            files: [{ id: 1, name: 'File test', path: '/path/test.pdf' }]
          },
          {
            id: 2,
            title: 'Title evidence',
            description: 'Evidence',
            type: 'transfer',
            transferTxHash: null,
            proof: null,
            approved: null,
            txHash: null,
            status: 'approved',
            createdAt: '2022-11-25T16:52:37.799Z',
            activity: 4,
            files: []
          }
        ]
      });
    });
  });

  describe('Testing get evidence', () => {
    const userWalletDao = {
      findActiveByUserId: () => 'address'
    };
    beforeAll(() => {
      injectMocks(activityService, {
        activityEvidenceDao,
        userService,
        userProjectService,
        milestoneService,
        projectService,
        userWalletDao,
        changelogService
      });
    });
    beforeEach(() => {
      dbTaskEvidence.push(taskEvidence);
      dbUser.push(auditorUser);
      dbProject.push(publishedProject);
    });
    afterAll(() => restoreActivityService());

    it('should successfully bring an evidence', async () => {
      jest
        .spyOn(milestoneService, 'getMilestoneById')
        .mockImplementation(milestoneId =>
          Promise.resolve({
            id: milestoneId,
            title: 'Milestone title',
            project: 11
          })
        );

      await expect(
        activityService.getEvidence(taskEvidence.id)
      ).resolves.toMatchObject({
        id: 1,
        createdAt: '2020-02-13',
        description: mockedDescription,
        proof: '/file/taskEvidence',
        approved: true,
        task: nonUpdatableTask.id,
        txHash: '0x111',
        status: txEvidenceStatus.SENT,
        currency: 'ETH',
        activity: {
          id: 1,
          title: 'Activity title',
          status: 'in_progress'
        },
        milestone: {
          id: 1,
          title: 'Milestone title'
        },
        auditor: {
          id: auditorUser.id,
          firstName: auditorUser.firstName,
          lastName: auditorUser.lastName
        },
        user: {
          id: 12,
          firstName: 'Beneficiary firstName',
          lastName: 'Beneficiary lastName'
        }
      });
    });
    it('should throw when the evidence does not exist', async () => {
      const nonExistentEvidenceId = taskEvidence.id + 1;
      await expect(
        activityService.getEvidence(nonExistentEvidenceId)
      ).rejects.toThrow(
        errors.common.CantFindModelWithId('evidence', nonExistentEvidenceId)
      );
    });
    it('should successfully bring a deleted evidence', async () => {
      jest
        .spyOn(changelogService, 'findDeletedEvidenceInChangelog')
        .mockImplementation(evidenceId =>
          Promise.resolve({
            id: evidenceId,
            evidenceTitle: 'Evidence title',
            activityTitle: 'Activity title',
            milestoneTitle: 'Milestone title',
            deleted: true,
          })
        );

      await expect(
        activityService.getEvidence(taskEvidence.id)
      ).resolves.toMatchObject({
        id: 1
      });
    });
  });

  describe('Testing thereArePendingActivityTransactions', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        activityDao,
        milestoneService
      });
    });

    beforeEach(() => {
      jest
        .spyOn(milestoneService, 'getAllMilestonesByProject')
        .mockResolvedValue([
          {
            id: 3,
            title: 'Milestone 3',
            description: 'Milestone description'
          },
          {
            id: 4,
            title: 'Milestone 4',
            description: 'Milestone description'
          }
        ]);
    });

    it('should return true when there are some activity with pending status', async () => {
      const getActivitiesByMilestonesSpy = jest
        .spyOn(activityDao, 'getActivitiesByMilestones')
        .mockResolvedValue([
          {
            id: 1,
            title: 'Activity 1',
            status: ACTIVITY_STATUS.IN_PROGRESS
          },
          {
            id: 2,
            title: 'Activity 2',
            status: ACTIVITY_STATUS.PENDING_TO_REVIEW
          }
        ]);

      const booleanResponse = await activityService.thereArePendingActivityTransactions(
        1
      );
      expect(booleanResponse).toBeTruthy();
      expect(getActivitiesByMilestonesSpy).toHaveBeenCalledWith([3, 4]);
    });

    it('should return false when there are some activity with pending status', async () => {
      const getActivitiesByMilestonesSpy = jest
        .spyOn(activityDao, 'getActivitiesByMilestones')
        .mockResolvedValue([
          {
            id: 1,
            title: 'Activity 1',
            status: ACTIVITY_STATUS.IN_PROGRESS
          },
          {
            id: 2,
            title: 'Activity 2',
            status: ACTIVITY_STATUS.APPROVED
          }
        ]);

      const booleanResponse = await activityService.thereArePendingActivityTransactions(
        1
      );
      expect(booleanResponse).toBeFalsy();
      expect(getActivitiesByMilestonesSpy).toHaveBeenCalledWith([3, 4]);
    });
  });

  describe('Testing validateUserCanAddEvidence', () => {
    beforeAll(() => {
      injectMocks(activityService, {
        userProjectService
      });
    });

    it('should call validateUserWithRoleInProject with investor role description', async () => {
      const validateUserWithRoleInProjectSpy = jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      await activityService.validateUserCanAddEvidence({
        userId: 1,
        activityId: 2,
        projectId: 'projectHash',
        roleDescription: rolesTypes.INVESTOR
      });
      expect(validateUserWithRoleInProjectSpy).toHaveBeenCalledWith({
        user: 1,
        descriptionRoles: [rolesTypes.INVESTOR],
        project: 'projectHash',
        error: errors.task.UserCanNotAddEvidenceToProject({
          userId: 1,
          activityId: 2,
          projectId: 'projectHash'
        })
      });
    });

    it('should call validateUserWithRoleInProject with beneficiary role description', async () => {
      jest.clearAllMocks();
      const validateUserWithRoleInProjectSpy = jest
        .spyOn(userProjectService, 'validateUserWithRoleInProject')
        .mockResolvedValue();

      await activityService.validateUserCanAddEvidence({
        userId: 3,
        activityId: 2,
        projectId: 'projectHash2',
        roleDescription: rolesTypes.BENEFICIARY
      });
      expect(validateUserWithRoleInProjectSpy).toHaveBeenCalledWith({
        user: 3,
        descriptionRoles: [rolesTypes.BENEFICIARY],
        project: 'projectHash2',
        error: errors.task.UserCanNotAddEvidenceToProject({
          userId: 3,
          activityId: 2,
          projectId: 'projectHash2'
        })
      });
    });
  });

  describe('Testing getActivitiesBudget', () => {
    const activities = [
      { id: 1, budget: '1000', type: ACTIVITY_TYPES.FUNDING },
      { id: 2, budget: '1000', type: ACTIVITY_TYPES.SPENDING },
      { id: 3, budget: '500', type: ACTIVITY_TYPES.SPENDING },
      { id: 4, budget: '300', type: ACTIVITY_TYPES.FUNDING }
    ];
    it('should sum budget of funding acitivites', async () => {
      expect(
        activityService.getActivitiesBudget({
          activities,
          type: ACTIVITY_TYPES.FUNDING
        })
      ).toEqual(BigNumber('1300'));
    });

    it('should sum budget of spending acitivites', async () => {
      expect(
        activityService.getActivitiesBudget({
          activities,
          type: ACTIVITY_TYPES.SPENDING
        })
      ).toEqual(BigNumber('1500'));
    });
  });

  describe('Testing validateTransaction', () => {
    const transaction = {
      txHash:
        '0xb5a94a017aa9957d6768f3594b78146331da41f2184a339a4ae7367c13a87825',
      value: 50,
      tokenSymbol: 'USDT',
      from: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      to: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
      timestamp: 'Feb-17-2023 12:39:12 PM+UTC'
    };

    const address = '0xac4d2ac230876f8bab54144cee315bc22250661f';
    it('should pass validation without throw error', async () => {
      expect(() =>
        activityService.validateTransaction({
          transaction,
          address
        })
      ).not.toThrowError();
    });

    it('should throw error because address is not a sender or receiver of transaction', async () => {
      expect(() =>
        activityService.validateTransaction({
          transaction,
          address: 'invalidAddress'
        })
      ).toThrowError(errors.task.TransactionIsNotRelatedToProjectAddress);
    });
  });

  describe('Testing sendActivityTransaction', () => {
    const user = {
      ...beneficiaryUser,
      pinStatus: pinStatus.APPROVED
    };

    beforeAll(() => {
      injectMocks(activityService, {
        userDao: {
          findById: id => Promise.resolve(users.find(u => u.id === id)),
        },
      });
    });

    beforeEach(() => {
      dbTask.push(
        { id: 10, ...newActivity, milestone: newUpdatableMilestone.id },
        nonUpdatableTask,
        approvedTask
      );
    });

    afterEach(() => jest.restoreAllMocks());

    afterAll(() => restoreActivityService());

    it('should throw an error if the activity type is not valid', async () => {
      injectMocks(activityService, {
        userDao: {
          findById: () => Promise.resolve({ id: 'proposerId', pinStatus: pinStatus.APPROVED }),
        },
      });

      await expect(
        activityService.sendActivityTransaction({
          user,
          activityId: 10,
          authorizationSignature: 'authSignature'
        })
      ).rejects.toThrow(errors.user.InvalidPinStatus);
    });
  });
});
