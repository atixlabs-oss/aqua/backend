/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { injectMocks } = require('../../rest/util/injection');

jest.mock('../../rest/services/helpers/checkExistence.js');
const countryService = require('../../rest/services/countryService');

describe('Testing countryService', () => {
  let dbCountry = [];

  const countryDao = {
    findAllByProps: filter =>
      dbCountry.filter(country =>
        Object.keys(filter).every(key => country[key] === filter[key])
      )
  };

  describe('Testing countryService getAll', () => {
    beforeAll(() => {
      injectMocks(countryService, { countryDao });
    });

    beforeEach(() => {
      dbCountry = [];
      dbCountry.push({
        id: 1,
        name: 'Argentina'
      });
    });

    it('should get all the countries', async () => {
      const response = await countryService.getAll({});
      expect(response.length).toEqual(dbCountry.length);
    });
  });
  describe('Testing countryService getById', () => {
    beforeEach(() => {
      jest.resetModules();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should get all the countries', async () => {
      const response = await countryService.getCountryById(1);
      expect(response).toBeDefined();
      expect(response).toEqual({
        country: {
          id: 1,
          name: 'Argentina'
        }
      });
    });
  });
});
