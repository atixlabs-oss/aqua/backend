/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const tokenService = require('../../rest/services/tokenService');
const { injectMocks } = require('../../rest/util/injection');

describe('Testing tokenService', () => {
  let dbToken = [];
  const resetDb = () => {
    dbToken = [];
  };

  const ETH = {
    id: 1,
    name: 'Ether',
    symbol: 'ETH',
    deleted: false
  };

  const ETC = {
    id: 1,
    name: 'Etherereum Classic',
    symbol: 'ETC',
    deleted: true
  };

  const USDT = {
    id: 1,
    name: 'Tether USD',
    symbol: 'USDT',
    deleted: false
  };

  const tokenDao = {
    getTokens: () => dbToken.filter(token => !token.deleted)
  };

  describe('Testing getTokens', () => {
    beforeAll(() => {
      injectMocks(tokenService, {
        tokenDao
      });
    });

    beforeEach(() => {
      dbToken.push(ETH, ETC, USDT);
    });

    it('should return all tokens that are not removed', async () => {
      const response = await tokenService.getTokens();
      expect(response.length).toEqual(2);
      expect(response).toMatchObject([ETH, USDT]);
    });
  });
});
