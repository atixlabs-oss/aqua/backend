/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { injectMocks } = require('../../rest/util/injection');
const COAError = require('../../rest/errors/COAError');
const errors = require('../../rest/errors/exporter/ErrorExporter');
const originalChangelogService = require('../../rest/services/changelogService');

const changelogService = Object.assign({}, originalChangelogService);

const changelogDao = {
  getChangelogBy: jest.fn(),
  createChangelog: jest.fn(),
  deleteProjectChangelogs: jest.fn(),
  updateChangelog: jest.fn()
};

const userProjectService = {
  getRolesOfUser: jest.fn()
};

const userService = {
  getUserById: jest.fn()
};

const milestoneService = {
  getMilestoneById: jest.fn()
};

const activityService = {
  getActivityById: jest.fn(),
  getEvidenceById: jest.fn()
};

const projectService = {
  getProjectById: jest.fn()
};

describe('Testing changelogService', () => {
  beforeAll(() => {
    injectMocks(changelogService, {
      changelogDao,
      userProjectService,
      userService,
      milestoneService,
      activityService,
      projectService
    });
  });

  beforeEach(() => jest.clearAllMocks());

  describe('changelogFilter', () => {
    const activity = { id: 1, parent: 2 };
    const milestone = { id: 3, parent: 4 };
    const evidence = { id: 5, parent: 6 };
    describe('Filter by milestoneId', () => {
      test('Should return false if changelog has no milestone', () => {
        expect(
          changelogService.changelogFilter({ activity }, { milestoneId: 4 })
        ).toBe(false);
      });

      test('Should return false if ids doesnt match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { milestoneId: milestone.id + 1 }
          )
        ).toBe(false);
      });

      test('Should return false if ids doesnt match even if milestone has no parent', () => {
        expect(
          changelogService.changelogFilter(
            { milestone: { id: milestone.id } },
            { milestoneId: milestone.id + 1 }
          )
        ).toBe(false);
      });

      test('Should return true if ids match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { milestoneId: milestone.id }
          )
        ).toBe(true);
      });
    });

    describe('Filter by milestoneId and parentMilestoneId', () => {
      test('Should return true if id match with param milestoneId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { milestoneId: milestone.id }
          )
        ).toBe(true);
      });

      test('Should return true if id match with param parentMilestoneId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { milestoneId: milestone.id + 1, parentMilestoneId: milestone.id }
          )
        ).toBe(true);
      });

      test('Should return true if parent match with param parentMilestoneId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            {
              milestoneId: milestone.id + 1,
              parentMilestoneId: milestone.parent
            }
          )
        ).toBe(true);
      });

      test('Should return false if ids doesnt match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            {
              milestoneId: milestone.id + 1,
              parentMilestoneId: milestone.parent + 1
            }
          )
        ).toBe(false);
      });
    });

    describe('Filter by activityId', () => {
      test('Should return false if changelog has no activity', () => {
        expect(
          changelogService.changelogFilter({ milestone }, { activityId: 4 })
        ).toBe(false);
      });

      test('Should return false if ids doesnt match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { activityId: activity.id + 1 }
          )
        ).toBe(false);
      });

      test('Should return false if ids doesnt match even if activity has no parent', () => {
        expect(
          changelogService.changelogFilter(
            { activity: { id: activity.id } },
            { activityId: activity.id + 1 }
          )
        ).toBe(false);
      });

      test('Should return true if ids match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { activityId: activity.id }
          )
        ).toBe(true);
      });
    });

    describe('Filter by milestoneId and parentMilestoneId', () => {
      test('Should return true if id match with param activityId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { activityId: activity.id, parentActivityId: activity.id + 1 }
          )
        ).toBe(true);
      });

      test('Should return true if id match with param parentActivityId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { activityId: activity.id + 1, parentActivityId: activity.id }
          )
        ).toBe(true);
      });

      test('Should return true if parent match with param parentMilestoneId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            { activityId: activity.id + 1, parentActivityId: activity.parent }
          )
        ).toBe(true);
      });

      test('Should return false if ids doesnt match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, activity },
            {
              activityId: activity.id + 1,
              parentActivityId: activity.parent + 1
            }
          )
        ).toBe(false);
      });
    });

    describe('Filter by evidenceId', () => {
      test('Should return false if changelog has no evidence', () => {
        expect(
          changelogService.changelogFilter({ milestone }, { evidenceId: 4 })
        ).toBe(false);
      });

      test('Should return false if ids doesnt match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, evidence },
            { evidenceId: evidence.id + 1 }
          )
        ).toBe(false);
      });

      test('Should return false if ids doesnt match even if evidence has no parent', () => {
        expect(
          changelogService.changelogFilter(
            { evidence: { id: evidence.id } },
            { evidenceId: evidence.id + 1 }
          )
        ).toBe(false);
      });

      test('Should return true if ids match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, evidenceData: { ...evidence } },
            { evidenceId: evidence.id }
          )
        ).toBe(true);
      });
    });

    describe('Filter by evidenceId and parentEvidenceId', () => {
      test('Should return true if id match with param evidenceId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, evidenceData: { ...evidence } },
            { evidenceId: evidence.id }
          )
        ).toBe(true);
      });

      test('Should return true if id match with param parentEvidenceId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, evidenceData: { ...evidence } },
            { evidenceId: evidence.id + 1, parentEvidenceId: evidence.id }
          )
        ).toBe(true);
      });

      test('Should return true if parent match with param parentEvidenceId', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, evidenceData: { ...evidence } },
            {
              evidenceId: evidence.id + 1,
              parentEvidenceId: evidence.parent
            }
          )
        ).toBe(true);
      });

      test('Should return false if ids doesnt match', () => {
        expect(
          changelogService.changelogFilter(
            { milestone, evidence },
            {
              evidenceId: evidence.id + 1,
              parentEvidenceId: evidence.parent + 1
            }
          )
        ).toBe(false);
      });
    });
  });

  describe('getChangelog', () => {
    const changelog = {
      milestone: { id: 3 },
      milestoneData: { title: 'milestone3Title', id: 3 },
      activity: { id: 2 },
      activityData: { title: 'activity2Title', id: 2, auditor: {
        firstName: 'auditorFirstName',
        lastName: 'auditorLastName',
        id: 'idAuditor'
      } },
      user: { id: 'some-id' },
      userData: { id: 'some-id' },
      project: 1,
      projectData: {
        projectName: 'projectName',
        id: 1
      }
    };

    const changelogWithoutUserAndActivity = {
      milestone: { id: 3 },
      milestoneData: { title: 'milestone3Title', id: 3 },
      user: { id: 'some-id' },
      userData: { id: 'some-id' },
      project: 1,
      projectData: {
        projectName: 'projectName',
        id: 1
      }
    };
    test('Without childrens. Should populate changelog with user roles and auditors', async () => {
      changelogDao.getChangelogBy.mockResolvedValueOnce([changelog]);
      const response = await changelogService.getChangelog(1, [], {
        milestoneId: 3
      });

      expect(response).toStrictEqual([
        changelog
      ]);
    });

    test('With childrens. Should build where clause correctly', async () => {
      changelogDao.getChangelogBy.mockResolvedValueOnce([changelog]);
      const response = await changelogService.getChangelog(1, [2, 3], {
        milestoneId: 3
      });

      console.log(response)
      expect(changelogDao.getChangelogBy).toHaveBeenCalledWith({
        or: [{ project: 1 }, { project: 2 }, { project: 3 }]
      });
    });

    test('Should not map user roles and auditors if they are not found', async () => {
      changelogDao.getChangelogBy.mockResolvedValueOnce([
        changelogWithoutUserAndActivity
      ]);
      const response = await changelogService.getChangelog(1, [], {
        milestoneId: 3
      });

      expect(response).toStrictEqual([changelogWithoutUserAndActivity]);
    });
  });

  describe('createChangelog', () => {
    test('Should call createChangelog from changelog dao with right parameters', async () => {
      const newChangelog = {
        project: 'some-project',
        revision: 2,
        milestone: 3,
        activity: 4,
        evidence: 5,
        user: 'some-user',
        transaction: 'some-tx',
        description: 'some-descriptions',
        action: 'some-action',
        extraData: {},
        status: 'PENDING'
      };

      const milestoneData = {
        title: 'milestone3Title',
        id: newChangelog.milestone
      };
      const activityData = {
        title: 'activity4Title',
        auditor: 'auditorId',
        id: newChangelog.activity
      }
      const evidenceData = {
        title: 'evidence5Title',
        id: newChangelog.evidence
      }
      const projectData = {
        projectName: 'projectName',
        id: newChangelog.project
      }
      const roleData = [{
          description: 'roleName'
      }]
      const auditorData = {
        firstName: 'auditorFirstName',
        lastName: 'auditorLastName',
        id: activityData.auditor
      };
      const userData = {
        firstName: 'userFirstName',
        lastName: 'userLastName',
        isAdmin: false,
        id: newChangelog.user
      }

      milestoneService.getMilestoneById.mockResolvedValueOnce(milestoneData);

      activityService.getActivityById.mockResolvedValueOnce(activityData);

      userService.getUserById.mockResolvedValueOnce(auditorData).mockResolvedValueOnce(userData);

      activityService.getEvidenceById.mockResolvedValueOnce(evidenceData);

      projectService.getProjectById.mockResolvedValueOnce(projectData);

      userProjectService.getRolesOfUser.mockResolvedValueOnce(roleData);

      await changelogService.createChangelog(newChangelog);

      expect(changelogDao.createChangelog).toHaveBeenCalledWith({
        ...newChangelog,
        milestoneData,
        activityData: {
          ...activityData,
          auditor: auditorData
        },
        userData: {
          ...userData,
          role: roleData[0].description
        },
        projectData,
        evidenceData
      });
    });
  });

  describe('deleteProjectChangelogs', () => {
    test('Should call deleteProjectChangelogs from changelog dao with right parameters', async () => {
      const projectId = 'some-project';
      await changelogService.deleteProjectChangelogs(projectId);

      expect(changelogDao.deleteProjectChangelogs).toHaveBeenCalledWith(
        projectId
      );
    });
  });

  describe('updateChangelog', () => {
    test('Should call updateChangelog from changelog dao with right parameters', async () => {
      const request = {
        changelogId: 3,
        toUpdate: { status: 'PENDING' }
      };

      changelogDao.updateChangelog.mockResolvedValueOnce({ status: 'PENDING' });

      await changelogService.updateChangelog(request);

      expect(changelogDao.updateChangelog).toHaveBeenCalledWith({
        id: request.changelogId,
        toUpdate: request.toUpdate
      });
    });

    test('Should throw correct error if update failed', async () => {
      const request = {
        changelogId: 3,
        toUpdate: { status: 'PENDING' }
      };

      changelogDao.updateChangelog.mockResolvedValueOnce(null);
      expect.assertions(1);

      await changelogService.updateChangelog(request).catch(error => {
        expect(error).toStrictEqual(
          new COAError(
            errors.changelog.CantUpdateChangelog(request.changelogId)
          )
        );
      });
    });
  });
});
