/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mailService = require('../../../../rest/services/mailService');
const projectService = require('../../../../rest/services/projectService');
const rejectProjectReviewConfirmation = require('../../../../rest/services/transactions/success-handler/rejectProjectReviewConfirmation');
const userProjectService = require('../../../../rest/services/userProjectService');
const {
  projectStatuses,
  ACTION_TYPE
} = require('../../../../rest/util/constants');

describe('Testing rejectProjectReviewConfirmation', () => {
  const project = {
    id: 'projectHash2',
    projectName: 'Project 1 rev 2',
    parent: 'projectHashParent',
    revision: 2
  };

  const users = [
    { id: 1, firstName: 'Pablo', lastName: 'Perez' },
    { id: 2, firstName: 'Federico', lastName: 'Gonzalez' }
  ];

  const updateProjectSpy = jest
    .spyOn(projectService, 'updateProject')
    .mockResolvedValue('projectHash2');

  const getProjectByIdSpy = jest
    .spyOn(projectService, 'getProjectById')
    .mockResolvedValue(project);

  const getUsersOfProjectSpy = jest
    .spyOn(userProjectService, 'getUsersOfProject')
    .mockResolvedValue(users);

  const sendEmailsSpy = jest
    .spyOn(mailService, 'sendEmails')
    .mockResolvedValue();

  it('should call rejectProjectReviewConfirmation successfully', async () => {
    await rejectProjectReviewConfirmation({
      projectId: 'projectHash2'
    });

    expect(getProjectByIdSpy).toHaveBeenCalledWith('projectHash2');
    expect(updateProjectSpy).toHaveBeenCalledWith('projectHash2', {
      status: projectStatuses.CANCELLED_REVIEW,
      revision: 1
    });
    expect(getUsersOfProjectSpy).toHaveBeenCalledWith('projectHash2');
    expect(sendEmailsSpy).toHaveBeenCalledWith({
      action: ACTION_TYPE.CANCEL_REVIEW,
      users,
      project
    });
  });
});
