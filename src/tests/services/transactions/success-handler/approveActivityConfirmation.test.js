/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const activityDao = require('../../../../rest/dao/activityDao');
const projectService = require('../../../../rest/services/projectService');
const milestoneService = require('../../../../rest/services/milestoneService');
const {
  approveActivityConfirmation
} = require('../../../../rest/services/transactions/success-handler/approveActvityConfirmation');
const { ACTIVITY_STATUS } = require('../../../../rest/util/constants');

describe('Testing approveActvityConfirmation', () => {
  const updateActivitySpy = jest
    .spyOn(activityDao, 'updateActivity')
    .mockResolvedValue('projectHash');

  const getActivityByIdWithMilestoneSpy = jest
    .spyOn(activityDao, 'getActivityByIdWithMilestone')
    .mockResolvedValue({ id: 1, milestone: { id: 1, project: 'projectHash' } });

  const updateStatusIfMilestoneIsCompleteSpy = jest
    .spyOn(milestoneService, 'updateStatusIfMilestoneIsComplete')
    .mockResolvedValue();

  const updateStatusIfProjectIsCompleteSpy = jest
    .spyOn(projectService, 'updateStatusIfProjectIsComplete')
    .mockResolvedValue();

  it('should call approveActvityConfirmation successfully', async () => {
    await approveActivityConfirmation({
      activityId: 1
    });

    expect(updateActivitySpy).toHaveBeenCalledWith(
      {
        status: ACTIVITY_STATUS.APPROVED
      },
      1
    );
    expect(getActivityByIdWithMilestoneSpy).toHaveBeenCalledWith(1);
    expect(updateStatusIfMilestoneIsCompleteSpy).toHaveBeenCalledWith(1);
    expect(updateStatusIfProjectIsCompleteSpy).toHaveBeenCalledWith(
      'projectHash'
    );
  });
});
