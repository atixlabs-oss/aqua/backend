/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const activityDao = require('../../../../rest/dao/activityDao');
const rejectActivityConfirmation = require('../../../../rest/services/transactions/success-handler/rejectActivityConfirmation');
const {
  ACTIVITY_STATUS,
  ACTIVITY_STEPS
} = require('../../../../rest/util/constants');

describe('Testing rejectActivityConfirmation', () => {
  const findByIdSpy = jest
    .spyOn(activityDao, 'findById')
    .mockResolvedValue({ id: 1, title: 'Activity title', claimCounter: 1 });

  const updateActivitySpy = jest
    .spyOn(activityDao, 'updateActivity')
    .mockResolvedValue('projectHash');

  it('should call rejectActivityConfirmation successfully', async () => {
    await rejectActivityConfirmation({
      activityId: 1
    });

    expect(findByIdSpy).toHaveBeenCalledWith(1);
    expect(updateActivitySpy).toHaveBeenCalledWith(
      {
        status: ACTIVITY_STATUS.REJECTED,
        step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS,
        claimCounter: 2
      },
      1
    );
  });
});
