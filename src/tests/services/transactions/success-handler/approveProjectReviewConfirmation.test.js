/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mailService = require('../../../../rest/services/mailService');
const projectService = require('../../../../rest/services/projectService');
const milestoneService = require('../../../../rest/services/milestoneService');
const approveProjectReviewConfirmation = require('../../../../rest/services/transactions/success-handler/approveProjectReviewConfirmation');
const userProjectService = require('../../../../rest/services/userProjectService');
const {
  projectStatuses,
  ACTION_TYPE
} = require('../../../../rest/util/constants');

describe('Testing approveProjectReviewConfirmation', () => {
  const project = {
    id: 'projectHash2',
    projectName: 'Project 1 rev 2',
    parent: 'projectHashParent'
  };

  const users = [
    { id: 1, firstName: 'Pablo', lastName: 'Perez' },
    { id: 2, firstName: 'Federico', lastName: 'Gonzalez' }
  ];

  const updateProjectSpy = jest
    .spyOn(projectService, 'updateProject')
    .mockResolvedValue('projectHash2');

  const getProjectByIdSpy = jest
    .spyOn(projectService, 'getProjectById')
    .mockResolvedValue(project);

  const getLastRevisionStatusSpy = jest
    .spyOn(projectService, 'getLastRevisionStatus')
    .mockResolvedValue(projectStatuses.IN_PROGRESS);

  const getUsersOfProjectSpy = jest
    .spyOn(userProjectService, 'getUsersOfProject')
    .mockResolvedValue(users);

  const sendEmailsSpy = jest
    .spyOn(mailService, 'sendEmails')
    .mockResolvedValue();

  jest
    .spyOn(milestoneService, 'getAllMilestonesByProject')
    .mockResolvedValue([
      {
        id: 1
      },
      {
        id: 2
      }
    ]);

  const updateStatusIfMilestoneIsCompleteSpy = jest
    .spyOn(milestoneService, 'updateStatusIfMilestoneIsComplete')
    .mockResolvedValue();

  jest
    .spyOn(projectService, 'updateStatusIfProjectIsComplete')
    .mockResolvedValue();

  it('should call approveProjectReviewConfirmation successfully', async () => {
    await approveProjectReviewConfirmation({
      projectId: 'projectHash2'
    });

    expect(getProjectByIdSpy).toHaveBeenCalledWith('projectHash2');
    expect(getLastRevisionStatusSpy).toHaveBeenCalledWith('projectHashParent');
    expect(updateProjectSpy).toHaveBeenCalledWith('projectHash2', {
      status: projectStatuses.IN_PROGRESS
    });
    expect(updateStatusIfMilestoneIsCompleteSpy).toHaveBeenCalledTimes(2);
    expect(getUsersOfProjectSpy).toHaveBeenCalledWith('projectHash2');
    expect(sendEmailsSpy).toHaveBeenCalledWith({
      action: ACTION_TYPE.APPROVE_REVIEW,
      users,
      project
    });
  });

  it('should call approveProjectReviewConfirmation for a cancel operation successfully', async () => {
    jest
      .spyOn(projectService, 'getProjectById')
      .mockResolvedValue({
        ...project,
        isCancelRequested: true
      });

    await approveProjectReviewConfirmation({
      projectId: 'projectHash2'
    });

    expect(getProjectByIdSpy).toHaveBeenCalledWith('projectHash2');
    expect(getUsersOfProjectSpy).toHaveBeenCalledWith('projectHash2');
    expect(updateProjectSpy).toHaveBeenLastCalledWith('projectHash2', {
      status: projectStatuses.CANCELLED,
      isCancelRequested: false
    });
    expect(sendEmailsSpy).toHaveBeenLastCalledWith({
      action: ACTION_TYPE.APPROVE_CANCELLATION,
      users,
      project: { ...project, isCancelRequested: true }
    });
  });
});
