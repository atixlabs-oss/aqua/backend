/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mailService = require('../../../../rest/services/mailService');
const projectService = require('../../../../rest/services/projectService');
const userProjectService = require('../../../../rest/services/userProjectService');
const sendProjectToReviewConfirmation = require('../../../../rest/services/transactions/success-handler/sendProjectToReviewConfirmation');
const {
  ACTION_TYPE,
  projectStatuses
} = require('../../../../rest/util/constants');

describe('Testing sendProjectToReviewConfirmation', () => {
  const project = {
    projectId: 'projectHash',
    changelogId: 1
  };

  const users = [
    { id: 1, firstName: 'Pablo', lastName: 'Perez' },
    { id: 2, firstName: 'Federico', lastName: 'Gonzalez' }
  ];

  const updateProjectSpy = jest
    .spyOn(projectService, 'updateProject')
    .mockResolvedValue('projectHash');

  const getProjectByIdSpy = jest
    .spyOn(projectService, 'getProjectById')
    .mockResolvedValue(project);

  const getUsersByProjectIdSpy = jest
    .spyOn(projectService, 'getUsersByProjectId')
    .mockResolvedValue(users);

  const sendEmailsSpy = jest
    .spyOn(mailService, 'sendEmails')
    .mockResolvedValue();

  it('should call sendProjectToReviewConfirmation successfully', async () => {
    await sendProjectToReviewConfirmation({
      projectId: 'projectHash'
    });

    expect(updateProjectSpy).toHaveBeenCalledWith('projectHash', {
      status: projectStatuses.IN_REVIEW
    });
    expect(getProjectByIdSpy).toHaveBeenCalledWith('projectHash');
    expect(getUsersByProjectIdSpy).toHaveBeenCalledWith({
      projectId: 'projectHash'
    });
    expect(sendEmailsSpy).toHaveBeenCalledWith({
      action: ACTION_TYPE.SEND_PROJECT_TO_REVIEW,
      users,
      project
    });
  });
});
