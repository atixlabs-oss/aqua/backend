/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const projectService = require('../../../../rest/services/projectService');
const sendProjectToReviewRollback = require('../../../../rest/services/transactions/fail-handler/sendProjectToReviewRollback');
const {
  projectStatuses,
  PROJECT_STEPS
} = require('../../../../rest/util/constants');

describe('Testing sendProjectToReviewRollback', () => {
  const updateProjectSpy = jest
    .spyOn(projectService, 'updateProject')
    .mockResolvedValue('projectHash');

  it('should call sendProjectToReviewRollback successfully', async () => {
    await sendProjectToReviewRollback({
      projectId: 'projectHash'
    });

    expect(updateProjectSpy).toHaveBeenCalledWith('projectHash', {
      status: projectStatuses.OPEN_REVIEW,
      step: PROJECT_STEPS.OPEN_REVIEW_PROJECT
    });
  });
});
