/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const activityDao = require('../../../../rest/dao/activityDao');
const sendActivityToReviewRollback = require('../../../../rest/services/transactions/fail-handler/sendActivityToReviewRollback');
const {
  ACTIVITY_STATUS,
  ACTIVITY_STEPS
} = require('../../../../rest/util/constants');

describe('Testing sendActivityToReviewRollback', () => {
  const updateActivitySpy = jest
    .spyOn(activityDao, 'updateActivity')
    .mockResolvedValue('projectHash');

  it('should call sendActivityToReviewRollback successfully', async () => {
    await sendActivityToReviewRollback({
      activityId: 1
    });

    expect(updateActivitySpy).toHaveBeenCalledWith(
      {
        status: ACTIVITY_STATUS.IN_PROGRESS,
        step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
      },
      1
    );
  });
});
