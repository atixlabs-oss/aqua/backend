/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const bcrypt = require('bcrypt');
const { buildGenericUserWithEmail } = require('../testHelper');
const { passRecovery, passRecoveryWithExpiredToken } = require('../mockModels');
const { injectMocks } = require('../../rest/util/injection');
const passRecoveryService = require('../../rest/services/passRecoveryService');
const errors = require('../../rest/errors/exporter/ErrorExporter');

const getUserByEmail = email =>
  email === 'notvalid@email.com' ? undefined : buildGenericUserWithEmail(email);

describe('Testing PassRecoveryService startPassRecoveryProcess', () => {
  let userDao;
  let passRecoveryDao;
  let mailService;
  let projectDao;
  const dbProject = [];
  const dbUserProject = [];
  let userProjectDao;
  const project1 = { id: 1, projectName: 'projectName' };
  const project2 = { id: 2, projectName: 'projectName2' };

  beforeAll(() => {
    dbProject.push(project1);
    dbProject.push(project2);
    dbUserProject.push({
      userId: buildGenericUserWithEmail('dummy@email.com').id,
      projectId: project1.id
    });
    userDao = {
      getUserByEmail
    };
    passRecoveryDao = {
      createRecovery: () => passRecovery
    };
    mailService = {
      sendEmailRecoveryPassword: () => ({ accepted: ['dummy@email.com'] })
    };
    projectDao = {
      findById: id => dbProject.find(p => p.id === id)
    };
    userProjectDao = {
      findUserProject: ({ user, project }) =>
        dbUserProject.find(up => up.userId === user && up.projectId === project)
    };
    injectMocks(passRecoveryService, {
      passRecoveryDao,
      userDao,
      mailService,
      projectDao,
      userProjectDao
    });
    bcrypt.compare = jest.fn();
  });

  it('should success when the given email is found', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    const response = await passRecoveryService.startPassRecoveryProcess(
      'dummy@email.com'
    );
    expect(response).toEqual('dummy@email.com');
  });
  it('should success when the given email and projectId are valids', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    const response = await passRecoveryService.startPassRecoveryProcess(
      'dummy@email.com',
      project1.id
    );
    expect(response).toEqual('dummy@email.com');
  });
  it('should fail with an error when the given email is not found', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    await expect(
      passRecoveryService.startPassRecoveryProcess('notvalid@email.com')
    ).rejects.toThrow(errors.user.EmailNotExists('notvalid@email.com'));
  });
  it('should throw when project is not found', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    const nonExistentProjectId = 99;
    await expect(
      passRecoveryService.startPassRecoveryProcess(
        'dummy@email.com',
        nonExistentProjectId
      )
    ).rejects.toThrow(
      errors.common.CantFindModelWithId('project', nonExistentProjectId)
    );
  });
  it('should throw when given user is not related to the project', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    await expect(
      passRecoveryService.startPassRecoveryProcess(
        'dummy@email.com',
        project2.id
      )
    ).rejects.toThrow(errors.user.UserNotRelatedToTheProject);
  });
});

describe('Testing PassRecoveryService updatePassword', () => {
  let passRecoveryDao;
  let userDao;
  let userWalletDao;
  const TOKEN_NOT_FOUND = 'Token not found';
  const EXPIRED_TOKEN = 'Expired token';

  beforeAll(() => {
    passRecoveryDao = {
      findRecoverBytoken: token => {
        if (token === TOKEN_NOT_FOUND) return undefined;
        if (token === EXPIRED_TOKEN) return passRecoveryWithExpiredToken;
        return passRecovery;
      },
      deleteRecoverByToken: () => {}
    };
    userDao = {
      updatePasswordByMail: true,
      getUserByEmail,
      updateUserByEmail: () => true
    };
    userWalletDao = {
      createUserWallet: (userWallet, _) => userWallet,
      updateWallet: () => false
    };
    injectMocks(passRecoveryService, {
      passRecoveryDao,
      userDao,
      userWalletDao
    });
    bcrypt.compare = jest.fn();
    bcrypt.hash = jest.fn();
  });
  it('should success when the token and password are valid', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    const response = await passRecoveryService.updatePassword({
      token: '1d362dd70c3288ea7db239d04b57eea767112b0c77c5548a00',
      password: 'newpassword'
    });
    expect(response).toBeTruthy();
  });

  it('should  fail with an error when the given token is not found on the database', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    await expect(
      passRecoveryService.updatePassword({
        token: TOKEN_NOT_FOUND,
        password: 'newpassword'
      })
    ).rejects.toThrow(errors.user.InvalidToken);
  });
});

describe('Testing PassRecoveryService updatePassword Errors', () => {
  let passRecoveryDao;
  let userDao;
  const TOKEN_NOT_FOUND = 'Token not found';
  const EXPIRED_TOKEN = 'Expired token';

  beforeAll(() => {
    passRecoveryDao = {
      findRecoverBytoken: token => {
        if (token === TOKEN_NOT_FOUND) return undefined;
        if (token === EXPIRED_TOKEN) return passRecoveryWithExpiredToken;
        return passRecovery;
      },
      deleteRecoverByToken: () => {}
    };
    userDao = { updatePasswordByMail: false };
    injectMocks(passRecoveryService, {
      passRecoveryDao,
      userDao
    });
    bcrypt.compare = jest.fn();
  });

  it.skip('should fail with an error when the user is false', async () => {
    bcrypt.compare.mockReturnValueOnce(true);
    await expect(
      passRecoveryService.updatePassword({
        token: TOKEN_NOT_FOUND,
        password: 'newpassword'
      })
    ).rejects.toThrow('updating password');
  });
});

describe('Testing getTokenStatus', () => {
  let passRecoveryDao;
  const dbPassRecovery = [];
  const expiredToken = {
    id: 1,
    token: 'token',
    email: 'test@test.com',
    createdAt: new Date(),
    expirationDate: new Date('2000-11-16T19:17:51.627Z')
  };
  const nonExpiredToken = {
    id: 2,
    token: 'token"',
    email: 'test@test.com',
    createdAt: new Date(),
    expirationDate: new Date('2040-11-16T19:17:51.627Z')
  };
  beforeAll(() => {
    dbPassRecovery.push(expiredToken, nonExpiredToken);
    passRecoveryDao = {
      findRecoverBytoken: token => {
        return dbPassRecovery.find(pr => pr.token === token);
      }
    };
    injectMocks(passRecoveryService, { passRecoveryDao });
    bcrypt.compare = jest.fn();
  });
  it('should successfully return the status when the token is not expired', async () => {
    await expect(
      passRecoveryService.getTokenStatus(nonExpiredToken.token)
    ).resolves.toEqual({ expired: false });
  });
  it('should successfully return the status when the token is expired', async () => {
    await expect(
      passRecoveryService.getTokenStatus(expiredToken.token)
    ).resolves.toEqual({ expired: true });
  });
  it('should throw when the token doesnt exist', async () => {
    const invalidToken = 'invalidToken';
    await expect(
      passRecoveryService.getTokenStatus(invalidToken)
    ).rejects.toThrow(errors.user.InvalidToken);
  });
});
