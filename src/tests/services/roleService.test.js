/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { injectMocks } = require('../../rest/util/injection');

jest.mock('../../rest/services/helpers/checkExistence.js');
const roleService = require('../../rest/services/roleService');
const { rolesTypes } = require('../../rest/util/constants');
const errors = require('../../rest/errors/exporter/ErrorExporter');

describe('Testing roleService', () => {
  const roleDao = {
    getRoleByDescription: jest.fn(),
    getRolesByDescriptionIn: jest.fn()
  };

  describe('Testing getRoleByDescription', () => {
    beforeAll(() => {
      injectMocks(roleService, { roleDao });
    });

    it('should get role by description successfully', async () => {
      const beneficiaryRole = { id: 1, description: rolesTypes.BENEFICIARY };
      const getRoleByDescriptionSpy = jest
        .spyOn(roleDao, 'getRoleByDescription')
        .mockResolvedValue(beneficiaryRole);

      const role = await roleService.getRoleByDescription(
        rolesTypes.BENEFICIARY
      );
      expect(role).toEqual(beneficiaryRole);
      expect(getRoleByDescriptionSpy).toHaveBeenCalledWith(
        rolesTypes.BENEFICIARY
      );
    });

    it('should throw error when role does not exist', async () => {
      const getRoleByDescriptionSpy = jest
        .spyOn(roleDao, 'getRoleByDescription')
        .mockResolvedValue();

      await expect(
        roleService.getRoleByDescription('inexistentRole')
      ).rejects.toThrow(errors.common.ErrorGetting('role'));

      expect(getRoleByDescriptionSpy).toHaveBeenCalledWith('inexistentRole');
    });
  });

  describe('Testing getRolesByDescriptionIn', () => {
    beforeAll(() => {
      injectMocks(roleService, { roleDao });
    });

    it('should get roles by description in array successfully', async () => {
      const beneficiaryRole = { id: 1, description: rolesTypes.BENEFICIARY };
      const funderRole = { id: 2, description: rolesTypes.INVESTOR };
      const roleArray = [beneficiaryRole, funderRole];
      const getRoleByDescriptionsInSpy = jest
        .spyOn(roleDao, 'getRolesByDescriptionIn')
        .mockResolvedValue(roleArray);

      const roles = await roleService.getRolesByDescriptionIn([
        rolesTypes.BENEFICIARY,
        rolesTypes.INVESTOR
      ]);
      expect(roles).toEqual(roleArray);
      expect(getRoleByDescriptionsInSpy).toHaveBeenCalledWith([
        rolesTypes.BENEFICIARY,
        rolesTypes.INVESTOR
      ]);
    });
  });
});
