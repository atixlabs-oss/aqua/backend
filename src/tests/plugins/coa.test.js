/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { run, coa, ethers, web3 } = require('hardhat');
const logger = require('../../rest/logger');
const { sha3 } = require('../../rest/util/hash');

const deployContracts = async () => {
  await run('deploy', { resetStates: true });
  return ethers.provider.send('evm_snapshot', []);
};
const revertSnapshot = snapshot => ethers.provider.send('evm_revert', [snapshot]);

const periodSeconds = 17280;
const moveForwardAPeriod = async () => {
  await ethers.provider.send('evm_increaseTime', [periodSeconds]);
  await ethers.provider.send('evm_mine', []);
};

const votingPeriodSeconds = 604800;
const moveForwardVotingPeriod = async () => {
  await ethers.provider.send('evm_increaseTime', [votingPeriodSeconds]);
  await ethers.provider.send('evm_mine', []);
};

describe('COA plugin tests', () => {
  const address = '0xEa51CfB26e6547725835b4138ba96C0b5de9E54A';
  const txHash =
    '0xee079ea15a894cc95cca919812f490fdf5bc494ec69781d05cecda841d3c11a2';
  let evmSnapshot;
  beforeAll(async () => {
    evmSnapshot = await deployContracts();
  });
  beforeEach(async () => {
    await revertSnapshot(evmSnapshot);
  });

  describe('Testing getUnsignedTransaction method', () => {
    it(
      'should return the unsigned transaction for ' +
        'the corresponding method and contract',
      async () => {
        const coaContract = await coa.getCOA();
        const response = await coa.getUnsignedTransaction(
          coaContract,
          'createProject(string,string)',
          ['42', 'project_ipfs']
        );
        expect(response).toHaveProperty('to', expect.any(String));
        expect(response).toHaveProperty('gasLimit', expect.any(Number));
        expect(response).toHaveProperty('gasPrice', expect.any(Number));
        expect(response).toHaveProperty('data', expect.any(String));
      }
    );
  });

  describe('Testing createProject method', () => {
    it('should send the project to the COA contract', async () => {
      const response = await coa.createProject({
        projectId: 1,
        metadataHash: 'TestProject'
      });
      expect(response).toHaveProperty('hash', expect.any(String));
      expect(response).toMatchObject({gasLimit: {_hex: "0x0927c0"}});
    });
  });

  describe('Testing getTransactionCount method', () => {
    it('should return the transaction count for the address', async () => {
      const signer = await coa.getSigner();
      const initialTxNonce = await coa.getTransactionCount(signer._address);
      await signer.sendTransaction({ to: address, value: 100 });
      const finalTxNonce = await coa.getTransactionCount(signer._address);
      expect(finalTxNonce).toEqual(initialTxNonce + 1);
    });
  });

  describe('Testing getTransactionResponse method', () => {
    it('should return the transaction response for the transaction', async () => {
      const signer = await coa.getSigner();
      const { hash } = await signer.sendTransaction({
        to: address,
        value: 100
      });
      const response = await coa.getTransactionResponse(hash);
      expect(response).toHaveProperty('hash', hash);
      expect(response).toHaveProperty('blockNumber', expect.any(Number));
    });
    it('should return null if the transaction does not exist', async () => {
      const response = await coa.getTransactionResponse(txHash);
      expect(response).toEqual(null);
    });
  });

  describe('Testing getTransactionReceipt method', () => {
    it('should return the transaction receipt for the transaction', async () => {
      const signer = await coa.getSigner();
      const { hash } = await signer.sendTransaction({
        to: address,
        value: 100
      });
      const response = await coa.getTransactionReceipt(hash);
      expect(response).toHaveProperty('transactionHash', hash);
      expect(response).toHaveProperty('blockNumber', expect.any(Number));
      expect(response).toHaveProperty('status', 1);
    });
    it('should return null if the transaction does not exist', async () => {
      const response = await coa.getTransactionReceipt(txHash);
      expect(response).toEqual(null);
    });
  });

  describe('Testing getBlock method', () => {
    it('should return the block of the transaction using the number as arg', async () => {
      const signer = await coa.getSigner();
      const { hash } = await signer.sendTransaction({
        to: address,
        value: 100
      });
      const receipt = await coa.getTransactionReceipt(hash);
      expect(receipt).toHaveProperty('blockNumber', expect.any(Number));
      const block = await coa.getBlock(receipt.blockNumber);
      expect(block).toHaveProperty('hash', expect.any(String));
      expect(block).toHaveProperty('number', receipt.blockNumber);
      expect(block).toHaveProperty('timestamp', expect.any(Number));
    });
    it('should return the block of the transaction using the hash as arg', async () => {
      const signer = await coa.getSigner();
      const { hash } = await signer.sendTransaction({
        to: address,
        value: 100
      });
      const receipt = await coa.getTransactionReceipt(hash);
      expect(receipt).toHaveProperty('blockHash', expect.any(String));
      const block = await coa.getBlock(receipt.blockHash);
      expect(block).toHaveProperty('hash', receipt.blockHash);
      expect(block).toHaveProperty('number', expect.any(Number));
      expect(block).toHaveProperty('timestamp', expect.any(Number));
    });
    it('should return null if the block does not exist', async () => {
      const response = await coa.getBlock(50000);
      expect(response).toEqual(null);
    });
  });

  describe('Testing getProjects method', () => {
    beforeAll(async () => {
      evmSnapshot = await deployContracts();
    });

    const mockProjects = [
      {
        id: '1',
        name: 'project1'
      }
    ];

    it('SHOULD return an empty array if no project was created', async () => {
      const coaProjects = await coa.getProjects();
      expect(coaProjects).toEqual([]);
    });
  });

  describe('Testing getProjectsLength method', () => {
    beforeAll(async () => {
      evmSnapshot = await deployContracts();
    });

    const mockProjects = [
      {
        id: '1',
        name: 'project1'
      }
    ];

    it('SHOULD return 0 if no project was created', async () => {
      const coaProjectsLength = await coa.getProjectsLength();
      expect(coaProjectsLength.toString()).toEqual('0');
    });

    it('SHOULD return 1 if only 1 project was created', async () => {
      const mockProject = mockProjects[0];
      await coa.createProject({
        projectId: mockProject.id,
        metadataHash: mockProject.name
      });
      const coaProjectsLength = await coa.getProjectsLength();
      expect(coaProjectsLength.toString()).toEqual('1');
    });
  });

  describe('Testing proposeClaim method', async () => {
    let registry;
    before(async() => {
      registry = await coa.getRegistry();
    });
    it('should send the propose claim to the ClaimsRegistry contract', async () => {
      const response = await registry.proposeClaim({
        projectId: 1,
        claimHash: 'TestClaimHash',
        proofHash: 'TestProofHash',
        activityId: 1,
        proposerEmail: 'test@email.com',
        authorizationSignature: 'signature'
      });
      expect(response).toHaveProperty('hash', expect.any(String));
    });
  });

  describe('Testing submitClaimAuditResult method', async () => {
    let registry;
    before(async() => {
      registry = await coa.getRegistry();
    });
    it('should send the submit claim audit result to the ClaimsRegistry contract', async () => {
      const response = await registry.submitClaimAuditResult({
        projectId: 1,
        claimHash: 'TestClaimHash',
        proofHash: 'TestProofHash',
        proposerAddress: 'TestAddress',
        auditorEmail: 'test@email.com',
        approved: true,
        authorizationSignature: 'signature'
      });
      expect(response).toHaveProperty('hash', expect.any(String));
    });
  });
});
