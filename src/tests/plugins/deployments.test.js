/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { describe, it, beforeAll, beforeEach, expect } = global;
const { artifacts } = require('hardhat');
const {
  getOrDeployContract,
  getOrDeployUpgradeableContract,
  buildGetOrDeployUpgradeableContract,
  getSigner,
  getContractFactory
} = require('../../plugins/deployments');

describe('Deployments tests', () => {
  let creator;

  beforeAll(async () => {
    creator = await getSigner(0);
  });


  const validContractName = 'ClaimsRegistry';
  const invalidContractName = 'NotContractName';

  describe('Static functions', () => {
    describe('with a contract deployed', () => {
      let contract1;

      beforeAll(async () => {
        contract1 = await getOrDeployContract(
          validContractName,
          [],
          creator,
          true
        );
      });

      it("getOrDeployContract should deploy if the contract doesn't exists", async () => {
        expect(contract1).toBeDefined();
      });

      it('getOrDeployContract should return the deployed contract if the contract was already deployed', async () => {
        const contract2 = await getOrDeployContract(
          validContractName,
          [],
          creator,
          false
        );
        expect(contract1.address).toEqual(contract2.address);
      });

      it('getOrDeployContract should throw an error if the contractName is invalid', () => {
        expect(
          getOrDeployContract(invalidContractName, [], false)
        ).rejects.toThrow();
      });
    });

    describe('with an upgradeable contract deployed', () => {
      let contract1;

      beforeEach(async () => {
        contract1 = await getOrDeployUpgradeableContract(
          validContractName,
          [],
          creator,
          { initializer: 'registryInitialize' },
          true
        );
      });

      it("getOrDeployUpgradeableContract should deploy if the contract doesn't exists", async () => {
        expect(contract1).toBeDefined();
      });

      it('getOrDeployUpgradeableContract should return the deployed contract if the contract was already deployed', async () => {
        const contract2 = await getOrDeployUpgradeableContract(
          validContractName,
          [],
          creator
        );
        expect(contract1.address).toEqual(contract2.address);
      });

      it('getOrDeployUpgradeableContract should throw an error if the contractName is invalid', () => {
        expect(
          getOrDeployUpgradeableContract(
            invalidContractName,
            [],
            creator,
            undefined,
            true
          )
        ).rejects.toThrow();
      });

      it('getOrDeployUpgradeableContract should throw an error if there is a new implementation', async () => {
        // Mocking readArtifactSync
        const mockedClaimsRegistryArtifactFun = (_) =>
          artifacts.readArtifactSync('MockClaimsRegistryV1');

        // Mocking getContractFactory
        const mockedClaimsRegistryFactFun = () =>
          getContractFactory('MockClaimsRegistryV1', creator);

        expect(
          buildGetOrDeployUpgradeableContract(
            mockedClaimsRegistryArtifactFun,
            mockedClaimsRegistryFactFun
          )(validContractName, [], creator)
        ).rejects.toThrow();
      });
    });
  });
});
