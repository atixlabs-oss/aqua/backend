/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { generateAPIKeyAndSecret } = require('../../rest/util/apiKeys');
const { validate } = require('uuid');
const { encryption } = require('../../rest/util/constants');

describe('Testing apiKey util', () => {
  describe('Testing generateAPIKeyAndSecret', () => {
    it('apiKey should be an uuid', () => {
      const response = generateAPIKeyAndSecret();
      expect(validate(response.apiKey)).toBe(true);
    });

    it('apiKey should be an uuid', () => {
      const response = generateAPIKeyAndSecret();
      expect(Buffer.from(response.apiSecret, 'base64').byteLength).toBe(
        encryption.apiSecretSize
      );
    });
  });
});
