/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { format } = require('../../rest/util/emailFormatter');

describe('Testing emailFormatter util', () => {
  describe('Testing format', () => {
    it('should return empty string if email is empty', () => {
      expect(format('')).toBe('');
    });

    it('should return empty string if email has no @', () => {
      expect(format('email')).toBe('');
    });

    it('should format email correctly', () => {
      expect(format('test@test.test')).toBe('te...@....test');
    });
  });
});
