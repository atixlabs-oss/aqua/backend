/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { describe, it, beforeEach } = global;
const {
  web3,
  deployments,
  ethers
} = require('hardhat');
const { assert } = require('chai');
const { testConfig } = require('config');
const chai = require('chai');
const { solidity } = require('ethereum-waffle');
const { redeployContracts } = require('./helpers/deployHelpers');
const { waitForEvent } = require('./helpers/eventHelpers');
const { commonErrors, getVmRevertExceptionWithMsg, throwsAsync } = require('./helpers/exceptionHelpers');
const { projectRegistryErrors } = require('./helpers/projectRegistryHelpers.js')

chai.use(solidity);

describe('ProjectsRegistry.sol - remainder flows (users and project creation)', () => {
  let projectRegistry;
  let creator, other;

  // WARNING: Don't use arrow functions here, this.timeout doesn't work
  beforeEach('deploy contracts', async function be() {
    this.timeout(testConfig.contractTestTimeoutMilliseconds);
    await redeployContracts(['ProjectsRegistry']);
    projectRegistry = await deployments.getLastDeployedContract('ProjectsRegistry');

    const signers = await ethers.getSigners();
    creator = await signers[0].getAddress();
    other = await signers[1].getAddress();
  });

  it('Deployment works', async () => {
    const projectsLength = await projectRegistry.getProjectsLength();
    assert.equal(projectsLength, 0);
  });

  describe('Create Project method', () => {
    const projectData = {
      id: '1',
      ipfsHash: 'an_ipfs_hash'
    };

    it('Should create a project', async () => {
      await projectRegistry.createProject(projectData.id, projectData.ipfsHash);

      // Verify the project description
      const projectDescription = await projectRegistry.projectsDescription(projectData.id);
      assert.equal(projectDescription.proposalIpfsHash, projectData.ipfsHash);
      assert.equal(projectDescription.auditIpfsHash, "");
      assert.equal(projectDescription.authorAddress, creator);
      assert.equal(projectDescription.authorEmail, "");

      // Project created event is emitted properly
      const [
        eventProject,
        eventIpfsHash,
      ] = await waitForEvent(projectRegistry, 'ProjectCreated');
      assert.equal(eventProject, projectData.id);
      assert.equal(eventIpfsHash, projectData.ipfsHash);
    });

    it('Should fail when trying to create a project if already created', async () => {
      // Create the project a first time
      await projectRegistry.createProject(projectData.id, projectData.ipfsHash);

      await throwsAsync(
        projectRegistry
          .createProject(projectData.id, projectData.ipfsHash),
        getVmRevertExceptionWithMsg(projectRegistryErrors.projectAlreadyCreated)
      );
    });

    it('Should fail when trying to create a project if not owner', async () => {
      const signers = await ethers.getSigners();
      await throwsAsync(
        projectRegistry
          .connect(signers[signers.length - 1])
          .createProject(projectData.id, projectData.ipfsHash),
        getVmRevertExceptionWithMsg(commonErrors.senderIsNotOwner)
      );
    });
  });

  describe('Transaction', () => {
    it('Should revert when sending a tx to the contract', async () => {
      await chai.expect(
        web3.eth.sendTransaction({
          from: other,
          to: projectRegistry.address,
          value: '0x10',
          gasPrice: '0x10'
        })
      ).to.be.reverted;
    });
  });
});
