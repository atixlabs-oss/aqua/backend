/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { utils } = require('ethers');
const { deployments, ethers } = require('hardhat');
const { assert } = require('chai');
const { randomInt } = require('crypto');
const { testConfig } = require('config');
const { getVmRevertExceptionWithMsg, throwsAsync } = require('./helpers/exceptionHelpers');
const { redeployContracts } = require('./helpers/deployHelpers');
const { assertEqualForEventIndexedParam, waitForEvent } = require('./helpers/eventHelpers');
const { claimRegistryErrors, proposeClaim, submitClaimAuditResult, getSubmitClaimAuditResultSignature, submitClaimAuditResultFromSignature } = require('./helpers/claimRegistryHelpers')

const billion = 1000000000;
const nonceRandom = () => randomInt(billion);

describe('ClaimsRegistry.sol - audit a claim', () => {
  let registry;
  const projectId = '666';
  let proposerSigner, proposerAddress;
  let auditorSigner, auditorAddress;
  let otherAuditorAddress;
  let claimProposal = {
    claim: 'a claim',
    proof: 'a proof'
  };
  let claimHash, proofHash;
  const auditIpfsHash = 'an audit hash';

  // WARNING: Don't use arrow functions here, this.timeout doesn't work
  beforeEach('deploy contracts and create proposal', async function be() {
    // Deploy contracts
    this.timeout(testConfig.contractTestTimeoutMilliseconds);
    await redeployContracts(['ClaimsRegistry']);
    registry = await deployments.getLastDeployedContract('ClaimsRegistry');

    // Create signers
    const signers = await ethers.getSigners();
    proposerSigner = signers[1];
    proposerAddress = await proposerSigner.getAddress();
    auditorSigner = signers[2];
    auditorAddress = await auditorSigner.getAddress();
    const otherAuditorSigner = signers[3];
    otherAuditorAddress = await otherAuditorSigner.getAddress();

    // Create proposal
    const claimProposalHashes = await proposeClaim(registry, projectId, proposerSigner, claimProposal, nonce = nonceRandom());
    claimHash = claimProposalHashes.claimHash;
    proofHash = claimProposalHashes.proofHash;
    claimProposal = await registry.registryProposedClaims(projectId, proposerAddress, claimHash);
  });

  it('Should allow an auditor to approve a claim', async () => {
    const approved = true;
    await submitClaimAuditResult(
      registry,
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      approved,
      auditorSigner,
      nonce = nonceRandom()
    );

    // Audit is stored properly
    const claimAudit = await registry.registryAuditedClaims(projectId, auditorAddress, claimHash);
    assert.equal(claimAudit.auditorAddress, auditorAddress);
    assert.equal(claimAudit.approved, approved);
    assert.isTrue(claimAudit.exists);
    assert.equal(claimAudit.ipfsHash, auditIpfsHash);

    // Claim audited event is emitted properly
    const [
      eventProject,
      eventAuditor,
      eventClaim,
      eventApproved,
      eventProof,
      ,
      ,
    ] = await waitForEvent(registry, 'ClaimAudited');
    assertEqualForEventIndexedParam(eventProject, projectId);
    assert.equal(eventAuditor, auditorAddress);
    assert.equal(eventClaim, claimHash);
    assert.equal(eventApproved, approved);
    assert.equal(eventProof, auditIpfsHash);
  });

  it('Should allow an auditor to reject a claim', async () => {
    const approved = false;
    await submitClaimAuditResult(
      registry,
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      approved,
      auditorSigner,
      nonce = nonceRandom()
    );

    // Audit is stored properly
    const claimAudit = await registry.registryAuditedClaims(projectId, auditorAddress, claimHash);
    assert.equal(claimAudit.auditorAddress, auditorAddress);
    assert.equal(claimAudit.approved, approved);
    assert.isTrue(claimAudit.exists);
    assert.equal(claimAudit.ipfsHash, auditIpfsHash);

    // Claim audited event is emitted properly
    const [
      eventProject,
      eventAuditor,
      eventClaim,
      eventApproved,
      eventProof,
      ,
      ,
    ] = await waitForEvent(registry, 'ClaimAudited');
    assertEqualForEventIndexedParam(eventProject, projectId);
    assert.equal(eventAuditor, auditorAddress);
    assert.equal(eventClaim, claimHash);
    assert.equal(eventApproved, approved);
    assert.equal(eventProof, auditIpfsHash);
  });

  it('Should not modify the proposal of an audit even if the original was edited', async () => {
    const approved = true;
    await submitClaimAuditResult(
      registry,
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      approved,
      auditorSigner,
      nonce = nonceRandom()
    );

    // Edit the original proposal
    const { claimHash : newClaimHash, proofHash : newProofHash } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      { claim: claimProposal.claim, proof: 'different proof' },
      nonce = nonceRandom()
    );
    assert.equal(claimHash, newClaimHash);
    assert.notEqual(proofHash, newProofHash);

    // The audit and proposal are not modified
    const claimAudit = await registry.registryAuditedClaims(projectId, auditorAddress, claimHash);
    assert.equal(claimAudit.proposal.proofHash, proofHash);
    assert.equal(claimAudit.auditorAddress, auditorAddress);
    assert.equal(claimAudit.approved, approved);
  });

  it('Should fail when auditing a non existing claim', async () => {
    const nonExistingClaimHash = utils.id('non_existing_claimhash');
    
    await throwsAsync(
      submitClaimAuditResult(
        registry,
        projectId,
        nonExistingClaimHash,
        proofHash,
        auditIpfsHash,
        proposerAddress,
        true,
        auditorSigner,
        nonce = nonceRandom()
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.proposalAuditedDoesNotExists)
    );
  });

  it('Should fail when auditing a claim with different proof hash', async () => {
    const invalidProofHash = utils.id('different_proofhash');
    
    await throwsAsync(
      submitClaimAuditResult(
        registry,
        projectId,
        claimHash,
        invalidProofHash,
        auditIpfsHash,
        proposerAddress,
        true,
        auditorSigner,
        nonce = nonceRandom()
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.auditWithInvalidProofHash)
    );
  });

  it('Should fail when auditing a claim twice', async () => {
    await submitClaimAuditResult(
      registry,
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      true,
      auditorSigner,
      nonce = nonceRandom()
    );
    
    await throwsAsync(
      submitClaimAuditResult(
        registry,
        projectId,
        claimHash,
        proofHash,
        auditIpfsHash,
        proposerAddress,
        true,
        auditorSigner,
        nonce = nonceRandom()
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.auditAlreadySubmitted)
    );
  });

  it('Should fail to audit a claim when the auditor address does not match the message authorization signer', async () => {
    const rejectionSignature = await getSubmitClaimAuditResultSignature(
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      false,
      auditorSigner,
      nonce = nonceRandom()
    );
    
    // Submit an approval request with an approval signature
    await throwsAsync(
      submitClaimAuditResultFromSignature(
        registry,
        projectId,
        claimHash,
        proofHash,
        auditIpfsHash,
        proposerAddress,
        true,
        rejectionSignature,
        otherAuditorAddress,
        nonce = nonceRandom()
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.auditInvalidSignature)
    );
  });

  it('Should fail to reject a claim with an approved signature', async () => {
    const approvalSignature = await getSubmitClaimAuditResultSignature(
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      true,
      auditorSigner,
      nonce = nonceRandom()
    );
    
    // Submit a rejection request with an approval signature
    await throwsAsync(
      submitClaimAuditResultFromSignature(
        registry,
        projectId,
        claimHash,
        proofHash,
        auditIpfsHash,
        proposerAddress,
        false,
        approvalSignature,
        auditorAddress,
        nonce = nonceRandom()
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.auditInvalidSignature)
    );
  });

  it('Should fail to approve a claim with a rejected signature', async () => {
    const rejectionSignature = await getSubmitClaimAuditResultSignature(
      projectId,
      claimHash,
      proofHash,
      auditIpfsHash,
      proposerAddress,
      false,
      auditorSigner,
      nonce = nonceRandom()
    );
    
    // Submit an approval request with an approval signature
    await throwsAsync(
      submitClaimAuditResultFromSignature(
        registry,
        projectId,
        claimHash,
        proofHash,
        auditIpfsHash,
        proposerAddress,
        true,
        rejectionSignature,
        auditorAddress,
        nonce = nonceRandom()
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.auditInvalidSignature)
    );
  });
});
