/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-destructuring */
const { it, beforeEach } = global;
const { deployments, ethers } = require('hardhat');
const { assert } = require('chai');
const { randomInt } = require('crypto');
const { testConfig } = require('config');
const {
  claimRegistryErrors,
  proposeClaim,
  getProposeClaimSignature,
  proposeClaimFromSignature
} = require('./helpers/claimRegistryHelpers');
const { redeployContracts } = require('./helpers/deployHelpers');
const {
  commonErrors,
  getVmRevertExceptionWithMsg,
  throwsAsync
} = require('./helpers/exceptionHelpers');
const {
  assertEqualForEventIndexedParam,
  waitForEvent
} = require('./helpers/eventHelpers');

const billion = 1000000000;
const nonceRandom = () => randomInt(billion);

describe('ClaimsRegistry.sol - propose a claim', () => {
  let registry;
  const projectId = '666';
  let proposerSigner;
  let proposerAddress;
  let otherProposerSigner;
  let otherProposerAddress;
  const proposal = {
    claim: 'this is a claim',
    proof: 'this is the proof',
    activityId: 42,
    proposerEmail: 'proposer@email.com'
  };

  // WARNING: Don't use arrow functions here, this.timeout doesn't work
  beforeEach('deploy contracts', async function be() {
    // Deploy contracts
    this.timeout(testConfig.contractTestTimeoutMilliseconds);
    await redeployContracts(['ClaimsRegistry']);
    registry = await deployments.getLastDeployedContract('ClaimsRegistry');

    // Create signers
    const signers = await ethers.getSigners();
    proposerSigner = signers[1];
    proposerAddress = await proposerSigner.getAddress();
    otherProposerSigner = signers[2];
    otherProposerAddress = await otherProposerSigner.getAddress();
  });

  it('Should allow a proposer to propose a claim', async () => {
    const { claimHash, proofHash } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      proposal,
      nonce = nonceRandom()
    );

    // Proposal is stored properly
    const claimProposal = await registry.registryProposedClaims(
      projectId,
      proposerAddress,
      claimHash
    );
    assert.equal(claimProposal.proofHash, proofHash);
    assert.equal(claimProposal.activityId, proposal.activityId);
    assert.equal(claimProposal.proposerAddress, proposerAddress);
    assert.equal(claimProposal.proposerEmail, proposal.proposerEmail);

    // Claim proposed event is emitted properly
    const [
      eventProject,
      eventProposer,
      eventClaim,
      eventProof,
      ,
      ,
    ] = await waitForEvent(registry, 'ClaimProposed');
    assertEqualForEventIndexedParam(eventProject, projectId);
    assert.equal(eventProposer, proposerAddress);
    assert.equal(eventClaim, claimHash);
    assert.equal(eventProof, proofHash);
  });

  it('Should allow a proposer to propose a claim with 34 bytes proof hash', async () => {
    const proofHash = 'QmR86wutAMSxuAcYPW9C6hqowWHbtQSiuJHuebXtn2zX7M';
    const { claimHash } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      {
        proofHash
      },
      nonce = nonceRandom()
    );

    // Proposal is stored properly
    const claimProposal = await registry.registryProposedClaims(
      projectId,
      proposerAddress,
      claimHash
    );
    assert.equal(claimProposal.proofHash, proofHash);
    assert.equal(claimProposal.activityId, proposal.activityId);
    assert.equal(claimProposal.proposerAddress, proposerAddress);
    assert.equal(claimProposal.proposerEmail, proposal.proposerEmail);
  });

  it('Should allow a proposer to override his claim', async () => {
    const { claimHash, proofHash1 } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      proposal,
      nonce = nonceRandom()
    );

    const newProposal = Object.assign({}, proposal);
    newProposal.proof = 'this is the new proof';
    const { proofHash: proofHash2 } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      newProposal,
      nonce = nonceRandom()
    );

    assert.notEqual(proofHash1, proofHash2);

    // Proposal is updated properly
    const updatedClaimProposal = await registry.registryProposedClaims(
      projectId,
      proposerAddress,
      claimHash
    );
    assert.equal(updatedClaimProposal.proofHash, proofHash2);
  });

  it('Should allow multiple proposals for the same claim but different sender to coexist', async () => {
    const { claimHash: claimHash1, proofHash: proofHash1 } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      {},
      nonce = nonceRandom()
    );

    const { claimHash: claimHash2, proofHash: proofHash2 } = await proposeClaim(
      registry,
      projectId,
      otherProposerSigner,
      {},
      nonce = nonceRandom()
    );

    assert.equal(claimHash1, claimHash2);
    assert.equal(proofHash1, proofHash2);

    // Both proposals exists
    const claimProposal1 = await registry.registryProposedClaims(
      projectId,
      proposerAddress,
      claimHash1
    );
    assert.equal(claimProposal1.proofHash, proofHash1);
    const claimProposal2 = await registry.registryProposedClaims(
      projectId,
      otherProposerAddress,
      claimHash1
    );
    assert.equal(claimProposal2.proofHash, proofHash1);
  });

  it('Should fail when sender is not the a owner', async () => {
    await throwsAsync(
      proposeClaim(
        registry,
        projectId,
        proposerSigner,
        proposal,
        nonce = nonceRandom(),
        proposerSigner,
      ),
      getVmRevertExceptionWithMsg(commonErrors.senderIsNotOwner)
    );
  });

  it('Should fail when the claim proposer does not match the message authorization signer', async () => {
    const proposeClaimSignature = await getProposeClaimSignature(
      projectId,
      proposerSigner,
      proposal,
      nonce = nonceRandom()
    );

    // Authorization from one proposer is submiter for another
    await throwsAsync(
      proposeClaimFromSignature(
        registry,
        projectId,
        proposeClaimSignature.authorizationMessage,
        nonce = nonceRandom(),
        {
          claimHash: proposeClaimSignature.claimHash,
          proofHash: proposeClaimSignature.proofHash,
          proposerAddress: otherProposerAddress
        }
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.proposalInvalidSignature)
    );
  });

  it('Should fail when a proposal authorization is submitted twice', async () => {
    const nonce = nonceRandom();
    const proposeClaimSignature = await getProposeClaimSignature(
      projectId,
      proposerSigner,
      proposal,
      nonce
    );

    // Authorization submitted for the first time
    await proposeClaimFromSignature(
      registry,
      projectId,
      proposeClaimSignature.authorizationMessage,
      nonce,
      {
        claimHash: proposeClaimSignature.claimHash,
        proofHash: proposeClaimSignature.proofHash,
        proposerAddress
      }
    );
    const claimProposal1 = await registry.registryProposedClaims(
      projectId,
      proposerAddress,
      proposeClaimSignature.claimHash
    );
    assert.equal(claimProposal1.proofHash, proposeClaimSignature.proofHash);

    // Proposer changes his submission
    const newProposal = Object.assign({}, proposal);
    newProposal.proof = 'this is the new proof';
    const { proofHash: proofHash2 } = await proposeClaim(
      registry,
      projectId,
      proposerSigner,
      newProposal,
      nonceRandom()
    );
    assert.notEqual(proposeClaimSignature.proofHash, proofHash2);
    const claimProposal2 = await registry.registryProposedClaims(
      projectId,
      proposerAddress,
      proposeClaimSignature.claimHash
    );
    assert.equal(claimProposal2.proofHash, proofHash2);

    // Authorization submitted for the second time
    await throwsAsync(
      proposeClaimFromSignature(
        registry,
        projectId,
        proposeClaimSignature.authorizationMessage,
        nonceRandom(),
        {
          claimHash: proposeClaimSignature.claimHash,
          proofHash: proposeClaimSignature.proofHash,
          proposerAddress
        }
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.proposalInvalidSignature)
    );
  });
});
