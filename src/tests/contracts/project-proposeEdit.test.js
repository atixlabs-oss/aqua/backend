/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { it, beforeEach } = global;
const { deployments, ethers } = require('hardhat');
const { assert } = require('chai');
const { testConfig } = require('config');
const { redeployContracts } = require('./helpers/deployHelpers');
const { waitForEvent } = require('./helpers/eventHelpers');
const { commonErrors, getVmRevertExceptionWithMsg, throwsAsync } = require('./helpers/exceptionHelpers');
const { projectRegistryErrors, proposeProjectEdit, getProposeProjectEditSignature, proposeProjectEditFromSignature } = require('./helpers/projectRegistryHelpers.js')

describe('ProjectsRegistry.sol - propose edits', () => {
  let projectRegistry;

  const projectData = {
    id: '1',
    ipfsHash: 'an_ipfs_hash'
  };
  const newIpfsHash = 'other_ipfs_hash';
  const otherIpfsHash = 'even_another_ipfs_hash';
  const proposerEmail = "example@gmail.com";
  let proposerSigner, proposerAddress;
  let otherProposerSigner, otherProposerAddress;


  // WARNING: Don't use arrow functions here, this.timeout doesn't work
  beforeEach('deploy contracts and setup', async function be() {
    // Deploy contracts
    this.timeout(testConfig.contractTestTimeoutMilliseconds);
    await redeployContracts(['ProjectsRegistry']);
    projectRegistry = await deployments.getLastDeployedContract('ProjectsRegistry');

    // Setup: create a project
    await projectRegistry.createProject(projectData.id, projectData.ipfsHash);

    const signers = await ethers.getSigners();
    proposerSigner = signers[1];
    proposerAddress = await proposerSigner.getAddress();

    otherProposerSigner = signers[2];
    otherProposerAddress = await otherProposerSigner.getAddress();
  });

  it('Should allow the owner to relay an edit proposal', async () => {
    await proposeProjectEdit(projectRegistry, projectData.id, newIpfsHash, proposerEmail, proposerSigner);
    
    // Verify the edit proposal was added correctly
    const proposedEditAdded = await projectRegistry.pendingEdits(projectData.id, proposerAddress);
    assert.equal(proposedEditAdded.proposalIpfsHash, newIpfsHash);
    assert.equal(proposedEditAdded.authorAddress, proposerAddress);
    assert.equal(proposedEditAdded.authorEmail, proposerEmail);

    // Project edit proposed event is emitted properly
    const [
      eventProject,
      eventProposer,
      eventIpfsHash,
    ] = await waitForEvent(projectRegistry, 'ProjectEditProposed');
    assert.equal(eventProject, projectData.id);
    assert.equal(eventProposer, proposerAddress);
    assert.equal(eventIpfsHash, newIpfsHash);
  });

  it('Should allow a proposer to override his proposal', async () => {
    // First proposal
    await proposeProjectEdit(projectRegistry, projectData.id, newIpfsHash, proposerEmail, proposerSigner);

    // Second proposal
    await proposeProjectEdit(projectRegistry, projectData.id, otherIpfsHash, proposerEmail, proposerSigner);
      
    // Verify the edit proposal was updated correctly
    const proposedEditAdded = await projectRegistry.pendingEdits(projectData.id, proposerAddress);
    assert.equal(proposedEditAdded.proposalIpfsHash, otherIpfsHash);
    assert.equal(proposedEditAdded.authorAddress, proposerAddress);
    assert.equal(proposedEditAdded.authorEmail, proposerEmail);
  });

  it('Should allow multiple proposal to exists for the same project', async () => {
    // Proposal by first proposer
    await proposeProjectEdit(projectRegistry, projectData.id, newIpfsHash, proposerEmail, proposerSigner);

    // Proposal by second proposer
    await proposeProjectEdit(projectRegistry, projectData.id, otherIpfsHash, proposerEmail, otherProposerSigner);
      
    // Verify the first edit proposal still exists
    const firstProposalEditAdded = await projectRegistry.pendingEdits(projectData.id, proposerAddress);
    assert.equal(firstProposalEditAdded.proposalIpfsHash, newIpfsHash);
    assert.equal(firstProposalEditAdded.authorAddress, proposerAddress);

    // Verify the second edit proposal also exists
    const secondProposalEditAdded = await projectRegistry.pendingEdits(projectData.id, otherProposerAddress);
    assert.equal(secondProposalEditAdded.proposalIpfsHash, otherIpfsHash);
    assert.equal(secondProposalEditAdded.authorAddress, otherProposerAddress);
  });

  it('Should fail when trying to relay an edit for a non existing project', async () => {
    const nonExistingProjectId = projectData.id + 1;
    await throwsAsync(
      proposeProjectEdit(projectRegistry, nonExistingProjectId, newIpfsHash, proposerEmail, proposerSigner),
      getVmRevertExceptionWithMsg(projectRegistryErrors.editingNonExistingProject)
    );
  });

  it('Should fail when trying to relay an edit and the sender is not the owner', async () => {
    await throwsAsync(
      proposeProjectEdit(projectRegistry, projectData.id, newIpfsHash, proposerEmail, proposerSigner, proposerSigner),
      getVmRevertExceptionWithMsg(commonErrors.senderIsNotOwner)
    );
  });

  it('Should fail when the proyect proposer does not match the message authorization signer', async () => {
    const nonce = 5;
    const proposalSignature = await getProposeProjectEditSignature(
      projectData.id, projectData.ipfsHash, proposerEmail, proposerSigner, nonce
    );

    // Submit the proposal of one user for another
    await throwsAsync(
      proposeProjectEditFromSignature(
        projectRegistry, projectData.id, projectData.ipfsHash, proposerEmail, otherProposerAddress, proposalSignature, nonce
      ),
      getVmRevertExceptionWithMsg(projectRegistryErrors.proposalInvalidSignature)
    );
  });

  it('Should fail when a proposal authorization is submitted twice', async () => {
    const nonce = 5;
    const proposalSignature = await getProposeProjectEditSignature(
      projectData.id, projectData.ipfsHash, proposerEmail, proposerSigner, nonce
    );

    // Submit the proposal for the first time
    await proposeProjectEditFromSignature(
      projectRegistry, projectData.id, projectData.ipfsHash, proposerEmail, proposerAddress, proposalSignature, nonce
    );
    const proposal1 = await projectRegistry.pendingEdits(projectData.id, proposerAddress);
    assert.equal(proposal1.proposalIpfsHash, projectData.ipfsHash);

    // Proposer changes his proposal
    await proposeProjectEdit(projectRegistry, projectData.id, otherIpfsHash, proposerEmail, proposerSigner);
    const proposal2 = await projectRegistry.pendingEdits(projectData.id, proposerAddress);
    assert.equal(proposal2.proposalIpfsHash, otherIpfsHash);

    // Authorization submitted for the second time
    await throwsAsync(
      proposeProjectEditFromSignature(
        projectRegistry, projectData.id, projectData.ipfsHash, proposerEmail, proposerAddress, proposalSignature, 6
      ),
      getVmRevertExceptionWithMsg(projectRegistryErrors.proposalInvalidSignature)
    );
  });

  it('Should fail when a proposal authorization is submitted with an used nonce', async () => {
    const nonce = 5;
    const proposalSignature = await getProposeProjectEditSignature(
      projectData.id, projectData.ipfsHash, proposerEmail, proposerSigner, nonce
    );

    // Submit the proposal for the first time
    await proposeProjectEditFromSignature(
      projectRegistry, projectData.id, projectData.ipfsHash, proposerEmail, proposerAddress, proposalSignature, nonce
    );
    const proposal1 = await projectRegistry.pendingEdits(projectData.id, proposerAddress);
    assert.equal(proposal1.proposalIpfsHash, projectData.ipfsHash);

    // Proposer changes his proposal
    const proposalSignature2 = await getProposeProjectEditSignature(
      projectData.id, otherIpfsHash, proposerEmail, proposerSigner, nonce
    );

    // Authorization submitted for the second time
    await throwsAsync(
      proposeProjectEditFromSignature(
        projectRegistry, projectData.id, otherIpfsHash, proposerEmail, proposerAddress, proposalSignature2, nonce
      ),
      getVmRevertExceptionWithMsg(projectRegistryErrors.nonceUsed)
    );
  });
});
