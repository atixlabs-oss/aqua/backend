/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { deployments, web3, ethers } = require('hardhat');
const { assert } = require('chai');
const { testConfig } = require('config');
const { randomInt } = require('crypto');
const chai = require('chai');
const { solidity } = require('ethereum-waffle');
const { claimRegistryErrors, proposeAndAuditClaim } = require('./helpers/claimRegistryHelpers');
const { getVmRevertExceptionWithMsg, throwsAsync } = require('./helpers/exceptionHelpers');
const { redeployContracts } = require('./helpers/deployHelpers');

chai.use(solidity);

const billion = 1000000000;
const nonceRandom = () => randomInt(billion);

describe('ClaimsRegistry.sol - remainder flows (queries)', () => {
  let registry;
  const projectId = '666';
  let txSender;
  let proposerSigner, proposerAddress;
  let auditorSigner, auditorAddress;

  // WARNING: Don't use arrow functions here, this.timeout doesn't work
  beforeEach('deploy contracts', async function () {
    this.timeout(testConfig.contractTestTimeoutMilliseconds);

    // Deploy contracts
    await redeployContracts(['ClaimsRegistry']);
    registry = await deployments.getLastDeployedContract('ClaimsRegistry');

    // Create signers
    const signers = await ethers.getSigners();
    txSender = await signers[0].getAddress();
    proposerSigner = signers[1];
    proposerAddress = await proposerSigner.getAddress();

    auditorSigner = signers[2];
    auditorAddress = await auditorSigner.getAddress();
  });

  it('It should return true when checking for only approved claims', async () => {
    const { claimHash: claim1Hash } = await proposeAndAuditClaim(registry, projectId, proposerSigner, auditorSigner, {
      claim: 'claim 1',
      approved: true
    }, nonce = nonceRandom());

    const { claimHash: claim2Hash } = await proposeAndAuditClaim(registry, projectId, proposerSigner, auditorSigner, {
      claim: 'claim 2',
      approved: true
    }, nonce = nonceRandom());

    // Check claim hashes are not equal
    assert.notEqual(claim1Hash, claim2Hash);

    // Check both are approved
    const approved = await registry.areApproved(
      projectId,
      [auditorAddress, auditorAddress],
      [claim1Hash, claim2Hash]
    );
    assert.equal(approved, true);
  });

  it('It should return false when checking if one of the claims is not approved', async () => {
    const { claimHash: claim1Hash } = await proposeAndAuditClaim(registry, projectId, proposerSigner, auditorSigner, {
      claim: 'claim 1',
      approved: true
    }, nonce = nonceRandom());

    const { claimHash: claim2Hash } = await proposeAndAuditClaim(registry, projectId, proposerSigner, auditorSigner, {
      claim: 'claim 2',
      approved: false
    }, nonce = nonceRandom());

    // Check claim hashes are not equal
    assert.notEqual(claim1Hash, claim2Hash);

    // Check that both aren't approved
    const approved = await registry.areApproved(
      projectId,
      [auditorAddress, auditorAddress],
      [claim1Hash, claim2Hash]
    );
    assert.equal(approved, false);
  });

  it('It should return fail when checking for approvals but with auditors and claim hashes of different size', async () => {
    await throwsAsync(
      registry.areApproved(
        projectId,
        [auditorAddress, auditorAddress],
        [ethers.utils.id("claim1Hash")]
      ),
      getVmRevertExceptionWithMsg(claimRegistryErrors.areApprovedWithArraysDifSize)
    );
  });

  // Note: this set was bigger, but it was reduced due to timeouts on the CI
  it('It should handle large set of claims to be checked', async () => {
    const claims = [];
    const validators = [];
    for (let i = 0; i < 5; i++) {
      // eslint-disable-next-line no-await-in-loop
      const { claimHash } = await proposeAndAuditClaim(registry, projectId, proposerSigner, auditorSigner, {
        claim: `claim ${i}`,
        approved: true
      }, nonce = nonceRandom());
      claims.push(claimHash);
      validators.push(auditorAddress);
    }
    const approved = await registry.areApproved(projectId, validators, claims);
    assert.equal(approved, true);
  });

  it('It should revert when sending a tx to the contract', async () => {
    await chai.expect(
      web3.eth.sendTransaction({
        from: txSender,
        to: registry.address,
        value: '0x10',
        gasPrice: '0x10'
      })
    ).to.be.reverted;
  });
});
