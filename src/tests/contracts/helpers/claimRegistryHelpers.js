/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { utils } = require('ethers');
const { signParameters } = require('./signatureHelpers.js');

const claimRegistryErrors = {
  proposalInvalidSignature: 'Invalid signature for claim proposal',
  proposalAuditedDoesNotExists: "Claim wasn't proposed",
  auditAlreadySubmitted: 'Auditor already audited this claim',
  auditWithInvalidProofHash:
    'Claim proposal has different proof hash than expected',
  areApprovedWithArraysDifSize: 'arrays must be equal size',
  auditInvalidSignature: 'Invalid signature for claim audit'
};

const proposeClaim = async (
  claimsRegistry,
  projectId,
  proposerSigner,
  {
    claim = 'this is a claim',
    proof = 'this is the proof',
    // Optional parameter with the hash of the proof
    // It overrides the proof parameter
    proofHash = null,
    activityId = 42,
    proposerEmail = 'proposer@email.com'
  } = {},
  nonce,
  senderSigner = null
) => {
  const proposeClaimSignature = await getProposeClaimSignature(
    projectId,
    proposerSigner,
    {
      claim,
      proof,
      proofHash,
      activityId,
      proposerEmail
    },
    nonce
  );

  await proposeClaimFromSignature(
    claimsRegistry,
    projectId,
    proposeClaimSignature.authorizationMessage,
    nonce,
    {
      claimHash: proposeClaimSignature.claimHash,
      proofHash: proposeClaimSignature.proofHash,
      activityId,
      proposerEmail,
      proposerAddress: proposerSigner.getAddress()
    },
    senderSigner
  );

  const toReturn = {
    claimHash: proposeClaimSignature.claimHash,
    proofHash: proposeClaimSignature.proofHash
  };
  return toReturn;
};

const getProposeClaimSignature = async (
  projectId,
  proposerSigner,
  {
    claim = 'this is a claim',
    proof = 'this is the proof',
    // Optional parameter with the hash of the proof
    // It overrides the proof parameter
    proofHash = null,
    activityId = 42,
    proposerEmail = 'proposer@email.com'
  } = {},
  proposerNonce
) => {
  const claimHashes = getClaimHashes(claim, proof);

  // Obtain the authorization message
  const authorizationMessage = await signParameters(
    ['uint256', 'string', 'bytes32', 'string', 'uint256', 'string'],
    [
      proposerNonce,
      projectId,
      claimHashes.claimHash,
      proofHash || claimHashes.proofHash,
      activityId,
      proposerEmail
    ],
    proposerSigner
  );

  return {
    authorizationMessage,
    claimHash: claimHashes.claimHash,
    proofHash: proofHash || claimHashes.proofHash
  };
};

const proposeClaimFromSignature = async (
  claimsRegistry,
  projectId,
  authorizationMessage,
  nonce,
  {
    claimHash,
    proofHash,
    activityId = 42,
    proposerEmail = 'proposer@email.com',
    proposerAddress
  } = {},
  senderSigner = null
) => {
  if (senderSigner) {
    await claimsRegistry
      .connect(senderSigner)
      .proposeClaim(
        projectId,
        claimHash,
        proofHash,
        activityId,
        proposerEmail,
        proposerAddress,
        authorizationMessage,
        nonce
      );
  } else {
    await claimsRegistry.proposeClaim(
      projectId,
      claimHash,
      proofHash,
      activityId,
      proposerEmail,
      proposerAddress,
      authorizationMessage,
      nonce
    );
  }
};

const submitClaimAuditResult = async (
  claimsRegistry,
  projectId,
  claimHash,
  proposalProofHash,
  auditIpfsHash,
  proposerAddress,
  approved,
  auditorSigner,
  nonce,
  auditorEmail = 'auditor@email.com'
) => {
  // Obtain the authorization message
  const authorizationMessage = await getSubmitClaimAuditResultSignature(
    projectId,
    claimHash,
    proposalProofHash,
    auditIpfsHash,
    proposerAddress,
    approved,
    auditorSigner,
    nonce,
    auditorEmail
  );

  await submitClaimAuditResultFromSignature(
    claimsRegistry,
    projectId,
    claimHash,
    proposalProofHash,
    auditIpfsHash,
    proposerAddress,
    approved,
    authorizationMessage,
    auditorSigner.getAddress(),
    nonce,
    auditorEmail
  );
};

const getSubmitClaimAuditResultSignature = async (
  projectId,
  claimHash,
  proposalProofHash,
  auditIpfsHash,
  proposerAddress,
  approved,
  auditorSigner,
  auditorNonce,
  auditorEmail = 'auditor@email.com'
) => {
  // Obtain the authorization message
  return signParameters(
    [
      'uint256',
      'string',
      'bytes32',
      'string',
      'address',
      'string',
      'string',
      'bool'
    ],
    [
      auditorNonce,
      projectId,
      claimHash,
      proposalProofHash,
      proposerAddress,
      auditorEmail,
      auditIpfsHash,
      approved
    ],
    auditorSigner
  );
};

const submitClaimAuditResultFromSignature = async (
  claimsRegistry,
  projectId,
  claimHash,
  proposalProofHash,
  auditIpfsHash,
  proposerAddress,
  approved,
  authorizationMessage,
  auditorAddress,
  nonce,
  auditorEmail = 'auditor@email.com'
) => {
  await claimsRegistry.submitClaimAudit(
    projectId,
    claimHash,
    proposalProofHash,
    proposerAddress,
    {
      auditorEmail,
      auditorAddress,
      ipfsHash: auditIpfsHash,
      approved
    },
    authorizationMessage,
    nonce
  );
};

const proposeAndAuditClaim = async (
  claimsRegistry,
  projectId,
  proposerSigner,
  auditorSigner,
  {
    claim = 'this is a claim',
    proof = 'this is the proof',
    auditIpfsHash = 'audit_ipfs_hash',
    activityId = 42,
    proposerEmail = 'proposer@email.com',
    auditorEmail = 'auditor@email.com',
    approved = true
  },
  nonce
) => {
  const proposerAddress = await proposerSigner.getAddress();

  const proposedClaim = { claim, proof, activityId, proposerEmail };
  const { claimHash, proofHash } = await proposeClaim(
    claimsRegistry,
    projectId,
    proposerSigner,
    proposedClaim,
    nonce
  );
  await submitClaimAuditResult(
    claimsRegistry,
    projectId,
    claimHash,
    proofHash,
    auditIpfsHash,
    proposerAddress,
    approved,
    auditorSigner,
    nonce++,
    auditorEmail
  );

  return { claimHash, proofHash };
};

const getClaimHashes = (claim, proof) => {
  const claimHash = utils.id(claim || 'this is a claim');
  const proofHash = utils.id(proof || 'this is the proof');
  return {
    claimHash,
    proofHash
  };
};

module.exports = {
  claimRegistryErrors,
  proposeClaim,
  getProposeClaimSignature,
  proposeClaimFromSignature,
  submitClaimAuditResult,
  getSubmitClaimAuditResultSignature,
  submitClaimAuditResultFromSignature,
  proposeAndAuditClaim
};
