/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { ethers, upgrades, run } = require('hardhat');

const redeployContracts = async (
  contractsToDeploy = null
) => {
  if (contractsToDeploy != null) {
    await run('deploy', { resetStates: true, contractsToDeploy: contractsToDeploy });
  } else {
    await run('deploy', { resetStates: true });
  }
};

/**
 * This function is no longer used, but is kept just in case, until the upgrade from buidler to hardhat is performed.
 * An example call is:
 *   claimsRegistryV2 = await upgradeContract(
 *     claimsRegistryContract.address,
 *     'ClaimsRegistryV2',
 *     {
 *       unsafeAllowCustomTypes: true,
 *       upgradeContractFunction: 'claimUpgradeToV1',
 *       upgradeContractFunctionParams: [
 *         creator,
 *         initialVariable
 *       ]
 *     }
 *   );
 * 
 * @param {Address of the previous version contract} contractAddress 
 * @param {Name of the new version of the contract} newImplementationName 
 * @param {Of the upgrade deployment} options 
 * @param {Called for performing the upgrade} upgradeFunction 
 * @returns 
 */
const upgradeContract = async (
  contractAddress,
  newImplementationName,
  options,
  upgradeFunction = upgrades.upgradeProxy
) => {
  // Deploy the upgraded version of the contract
  const newImplementationFactory = await ethers.getContractFactory(newImplementationName);
  options.contractName = newImplementationName;
  const upgradedContract = await upgradeFunction(contractAddress, newImplementationFactory, options);

  // Call the -UpgradeToV1 function from the contract's new version if necessary
  if (!!options.upgradeContractFunction) {
    await upgradedContract[options.upgradeContractFunction](...options.upgradeContractFunctionParams);
  }

  return upgradedContract;
}

module.exports = {
  upgradeContract,
  redeployContracts
}
