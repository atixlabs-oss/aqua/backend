/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { assert } = require('chai');

const commonErrors = {
    senderIsNotOwner: 'Ownable: caller is not the owner'
}

const getVmExceptionWithMsg = (exceptionMsg) => {
    return 'VM Exception while processing transaction: ' + exceptionMsg;
}

const getVmRevertExceptionWithMsg = (exceptionMsg) => {
    return getVmExceptionWithMsg(`reverted with reason string \'${exceptionMsg}\'`);
};

/**
 * Executes an async function and checks if an error is thrown.
 *
 * @param {Promise} Promise to be waited for
 * @param {String} errorMsg error message the exception should have
 * @returns {Boolean} true if exception was thrown with proper message, false otherwise
 */
 const throwsAsync = async (promise, errMsg) => {
  try {
    await promise;
  } catch (err) {
    const obtainedErrorMessage = err.message ? err.message : err.error;
    // Coverage has Error prefix while tests don't
    const withCorrectErrorMessage =
      obtainedErrorMessage == errMsg ||
      obtainedErrorMessage == "Error: " + errMsg
    assert.isTrue(
      withCorrectErrorMessage,
      `Expected exception ${errMsg} failed but got ${obtainedErrorMessage}`
    );
    return;
  }
  assert.fail(`Expected ${errMsg} to have been thrown`);
};

module.exports = {
    commonErrors,
    getVmExceptionWithMsg,
    getVmRevertExceptionWithMsg,
    throwsAsync
};
