/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { randomInt } = require('crypto');

const { signParameters } = require('./signatureHelpers.js');

const projectRegistryErrors = {
    projectAlreadyCreated: 'The project is already created',
    editingNonExistingProject: 'Project being edited doesn\'t exist',
    proposalInvalidSignature: 'Invalid signature for project proposal',
    auditForNonExistingProposal: 'The pending edit doesn\'t exists',
    auditWithInvalidIpfsHash: 'The pending edit doesn\'t have the ipfs hash selected',
    nonceUsed: 'Nonce used'
}

const billion = 1000000000;
const nonceRandom = () => randomInt(billion);

const proposeProjectEdit = async (
    registry,
    projectId,
    ipfsHash,
    proposerEmail,
    proposerSigner,
    senderSigner = null
) => {
    const proposerNonce = nonceRandom();

    const authorizationMessage = await getProposeProjectEditSignature(projectId, ipfsHash, proposerEmail, proposerSigner, proposerNonce);

    await proposeProjectEditFromSignature(registry, projectId, ipfsHash, proposerEmail, proposerSigner.getAddress(), authorizationMessage, proposerNonce, senderSigner);
}

const getProposeProjectEditSignature = async (
    projectId,
    ipfsHash,
    proposerEmail,
    proposerSigner,
    proposerNonce
) => {
    return signParameters(
        ["uint256", "string", "string", "string"],
        [proposerNonce, projectId, ipfsHash, proposerEmail],
        proposerSigner
    );
}

const proposeProjectEditFromSignature = async (
    registry,
    projectId,
    ipfsHash,
    proposerEmail,
    proposerAddress,
    authorizationMessage,
    proposerNonce,
    senderSigner = null
) => {
    if (!!senderSigner) {
        await registry
            .connect(senderSigner)
            .proposeProjectEdit(projectId, ipfsHash, proposerEmail, proposerAddress, authorizationMessage, proposerNonce);
    } else {
        // Use default sender
        await registry.proposeProjectEdit(projectId, ipfsHash, proposerEmail, proposerAddress, authorizationMessage, proposerNonce);
    }
}

module.exports = {
    projectRegistryErrors,
    proposeProjectEdit,
    getProposeProjectEditSignature,
    proposeProjectEditFromSignature
}
