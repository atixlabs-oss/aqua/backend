/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { assert } = require('chai');
const { ethers } = require('ethers');

/**
 * Waits for a solidity event to be emitted
 * @param {Contract} contract contract
 * @param {string} eventName event name or '*' to watch them all
 * @param {number} timeout max amount of ms to wait for the event to happen
 */
const waitForEvent = (contract, eventName, timeout = 20000) =>
  new Promise((resolve, reject) => {
    contract.on(eventName, function callback() {
      // eslint-disable-next-line prefer-rest-params
      const event = arguments[arguments.length - 1];
      event.removeListener();
      // eslint-disable-next-line prefer-rest-params
      resolve(arguments);
    });

    setTimeout(() => {
      reject(new Error(`Timeout when waiting for ${eventName}`));
    }, timeout);
  });

const assertEqualForEventIndexedParam = (
  obtainedIndexedParam,
  expectedParam
) => {
  const hashedExpectedParam = ethers.utils.id(expectedParam);
  assert.equal(
    hashedExpectedParam,
    obtainedIndexedParam.hash,
    `Expected hash(${expectedParam}) (${hashedExpectedParam}) but got ${obtainedIndexedParam}`
  );
};

module.exports = {
  waitForEvent,
  assertEqualForEventIndexedParam
};
