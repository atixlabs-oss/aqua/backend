// SPDX-License-Identifier: AGPL-3.0-only

/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.9;

/**
 * @title UpgradeableToV1
 *
 * @dev Helper contract to support upgrade functions. To use it, replace
 *      the constructor with a function that has the `initializer` modifier.
 * @dev Not in use right now (apart from testing) as a single version contract is provided.
 *      See the test contracts for an example on how to use it.
 * WARNING: Unlike constructors, initializer functions must be manually
 *          invoked. This applies both to deploying an UpgradeableToV1 contract, as well
 *          as extending an UpgradeableToV1 contract via inheritance.
 * WARNING: When used with inheritance, manual care must be taken to not invoke
 *          a parent initializer twice, or ensure that all initializers are idempotent,
 *          because this is not dealt with automatically as with constructors.
 */
contract UpgradeableToV1 {
    /**
     * @dev Indicates that the contract has been upgraded to v1.
     */
    bool private upgradedToV1;

    /**
     * @dev Indicates that the contract is in the process of being upgraded to v1.
     */
    bool private upgradingToV1;

    /**
     * @dev Modifier to use in the upgraded function of a contract.
     */
    modifier upgraderToV1() {
        require(upgradingToV1 || !upgradedToV1, "Contract instance has already been upgraded to v1");

        bool isTopLevelCall = !upgradingToV1;
        if (isTopLevelCall) {
            upgradingToV1 = true;
            upgradedToV1 = true;
        }

        _;

        if (isTopLevelCall) {
            upgradingToV1 = false;
        }
    }

    function version() public pure returns (uint16) {
        return 1;
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     *      variables without shifting down storage in contracts inheriting from this.
     *      See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     * @dev This array size plus the number of variables defined should always be 50
     */
    uint256[48] private ______gap;
}
