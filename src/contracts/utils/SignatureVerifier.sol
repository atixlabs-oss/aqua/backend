// SPDX-License-Identifier: AGPL-3.0-only

/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

/**
 * @dev Contract for verifying the signatures of the authorization message for performing actions on behalf of users
 *      Includes replay protection
 */
contract SignatureVerifier {
    bytes internal constant prefix = "\x19Ethereum Signed Message:\n32";

    // Nonces per user, used for replay protection
    mapping(address => mapping(uint256 => bool)) public userNoncesUsed;

    /**
     * @notice Verifies that the signature of the message hash was signed by a parameter address
     *         Verify the nonce of that signer for replay protection
     * @param signerAddress - the signer of the messageHash
     * @param messageHash - the hash of the message
     * @param signature - the signature of the message hash
     * @param nonce - the nonce used for the signature
     */
    function verifySignature(
        address signerAddress,
        bytes32 messageHash,
        bytes memory signature,
        uint256 nonce
    ) internal returns (bool) {
        // Verify the signer nonce
        require(!userNoncesUsed[signerAddress][nonce], "Nonce used");
        userNoncesUsed[signerAddress][nonce] = true;

        // Add message prefix to the message hash
        bytes32 prefixedMessageHash = keccak256(abi.encodePacked(prefix, messageHash));

        // Recover signer
        address signer = ECDSA.recover(prefixedMessageHash, signature);

        return signer == signerAddress;
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     *      variables without shifting down storage in contracts inheriting from this.
     *      See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     * @dev This array size plus the number of variables defined should always be 50
     */
    uint256[49] private _gap;
}
