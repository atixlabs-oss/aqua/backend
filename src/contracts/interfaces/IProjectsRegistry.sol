// SPDX-License-Identifier: AGPL-3.0-only

/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.9;

/// @title Stores projects related information
interface IProjectsRegistry {
    /// Emitted when a new Project is created
    event ProjectCreated(string id, string ipfsHash);
    /// Emitted when an edit is proposed
    event ProjectEditProposed(string id, address proposer, string proposedIpfsHash);
    /// Emmited when an edit is audited
    event ProjectEditAudited(string id, address proposer, string proposalIpfsHash, string auditIpfsHash, bool approved);

    /**
     * @notice Creates a Project, can only be run by the admin
     *         Submits the initial IPFS hash of a project
     * @dev Validations being performed:
     *       - The sender is the contract owner
     *       - There's no created project with the same id
     * @param _projectId - the id of the project created
     * @param _initialIpfsHash - the IPFS hash of the newly created project
     */
    function createProject(string calldata _projectId, string calldata _initialIpfsHash) external;

    /**
     * @notice proposes a project edit
     *         It can only be run by the owner of the contract, which acts as the relayer,
     *         by propagating a signature of the proposer
     * @dev Validations being performed:
     *       - The sender is the contract owner
     *       - The project edited exists
     *      Note: the identifier of a proposal is the projectId+proposerAddress,
     *            so there can be multiple proposals from different users
     *      Note: a user can override his proposal by sending a new one
     * @param _projectId - the id of the project that's proposed to be edited
     * @param _proposedIpfsHash - the new proposed project's IPFS hash
     * @param _proposerEmail - the email of the proposer
     * @param _proposerAddress - the address of the proposer
     * @param _authorizationSignature - the authorization signature by the edit proposer\
     * @param _proposerNonce - the nonce used for the signature
     */
    function proposeProjectEdit(
        string calldata _projectId,
        string calldata _proposedIpfsHash,
        string calldata _proposerEmail,
        address _proposerAddress,
        bytes calldata _authorizationSignature,
        uint256 _proposerNonce
    ) external;

    /**
     * @notice Approves/rejects a proposed project edit
     *         This can only be run by the admin
     * @dev Validations being performed:
     *       - The sender is the contract owner
     *       - The proposal exists, and has the parameter IPFS hash
     * @param _projectId - id of the project the edit belongs to
     * @param _proposalIpfsHash - the IPFS hash of the project edit being audited.
     *                            This is required as it's allowed for a user to override his proposal,
     *                            preventing this from the auditor approving a proposal he didn't intended.
     * @param _auditIpfsHash - the IPFS hash of the audit report
     * @param _authorAddress - the address of the author of the proposal
     * @param _approved - the audt result, whether the proposal was approved or not
     */
    function submitProjectEditAuditResult(
        string calldata _projectId,
        string calldata _proposalIpfsHash,
        string calldata _auditIpfsHash,
        address _authorAddress,
        bool _approved
    ) external;

    // Returns the number of projects created
    function getProjectsLength() external view returns (uint256);
}
