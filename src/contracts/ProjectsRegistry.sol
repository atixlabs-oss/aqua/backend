// SPDX-License-Identifier: AGPL-3.0-only

/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./utils/SignatureVerifier.sol";
import "./utils/StringUtils.sol";
import "./interfaces/IProjectsRegistry.sol";

/**
 * @title Stores projects related information
 */
contract ProjectsRegistry is IProjectsRegistry, Initializable, OwnableUpgradeable, SignatureVerifier {
    // The IPFS hashes can be bytes32 but IPFS hashes are 34 bytes long due to multihash.
    // We could strip the first two bytes but for now it seems unnecessary.
    // We are then representing ipfs hashes as strings
    struct ProjectDescription {
        // IPFS hash of the new project description proposal
        string proposalIpfsHash;
        // Address of the author of the proposal
        address authorAddress;
        // Email of the author of the proposal
        string authorEmail;
        // Used for determining whether this structure is initialized or not
        bool isCreated;
        // IPFS hash of the file for the audit
        // Is empty when a description wasn't yet reviewed
        string auditIpfsHash;
    }

    /// Project's ids list
    string[] public projectIds;
    // Pending project edits by
    // project id => proposer address => project description
    mapping(string => mapping(address => ProjectDescription)) public pendingEdits;
    // Project description by
    // project id => project description
    mapping(string => ProjectDescription) public projectsDescription;

    function registryInitialize() public initializer {
        OwnableUpgradeable.__Ownable_init();
    }

    function createProject(string calldata _projectId, string calldata _initialProposalIpfsHash)
        external
        override
        onlyOwner
    {
        // Perform validations
        require(!projectsDescription[_projectId].isCreated, "The project is already created");

        // Save the initial project description
        projectsDescription[_projectId] = ProjectDescription({
            proposalIpfsHash: _initialProposalIpfsHash,
            auditIpfsHash: "",
            authorAddress: msg.sender,
            authorEmail: "",
            isCreated: true
        });

        // Append the project to our list
        projectIds.push(_projectId);

        // Emit event
        emit ProjectCreated(_projectId, _initialProposalIpfsHash);
    }

    function proposeProjectEdit(
        string calldata _projectId,
        string calldata _proposedIpfsHash,
        string calldata _proposerEmail,
        address _proposerAddress,
        bytes calldata _authorizationSignature,
        uint256 _proposerNonce
    ) external override onlyOwner {
        // Perform validations
        require(projectsDescription[_projectId].isCreated, "Project being edited doesn't exist");

        // Get the proposer address
        bytes32 messageHash = hashProposedEdit(_projectId, _proposedIpfsHash, _proposerEmail, _proposerNonce);
        bool validSignature = verifySignature(_proposerAddress, messageHash, _authorizationSignature, _proposerNonce);
        require(validSignature, "Invalid signature for project proposal");

        // Add the proposal to the pending edits
        pendingEdits[_projectId][_proposerAddress] = ProjectDescription({
            proposalIpfsHash: _proposedIpfsHash,
            auditIpfsHash: "",
            authorAddress: _proposerAddress,
            authorEmail: _proposerEmail,
            isCreated: true
        });

        // Emit event
        emit ProjectEditProposed(_projectId, _proposerAddress, _proposedIpfsHash);
    }

    function submitProjectEditAuditResult(
        string calldata _projectId,
        string calldata _proposalIpfsHash,
        string calldata _auditIpfsHash,
        address _authorAddress,
        bool _approved
    ) external override onlyOwner {
        // Perform validations
        ProjectDescription storage proposedEdit = pendingEdits[_projectId][_authorAddress];
        require(proposedEdit.isCreated, "The pending edit doesn't exists");
        require(
            StringUtils.areEqual(proposedEdit.proposalIpfsHash, _proposalIpfsHash),
            "The pending edit doesn't have the ipfs hash selected"
        );

        // Update the project description if needed
        if (_approved) {
            proposedEdit.auditIpfsHash = _auditIpfsHash;
            projectsDescription[_projectId] = proposedEdit;
        }

        // Delete the pending edit that was audited
        delete pendingEdits[_projectId][_authorAddress];

        // Emit event
        emit ProjectEditAudited(_projectId, _authorAddress, _proposalIpfsHash, _auditIpfsHash, _approved);
    }

    function getProjectsLength() external view override returns (uint256) {
        return projectIds.length;
    }

    function hashProposedEdit(
        string memory _projectId,
        string memory _proposedIpfsHash,
        string memory _proposerEmail,
        uint256 _proposerNonce
    ) internal view returns (bytes32) {
        return keccak256(abi.encode(_proposerNonce, _projectId, _proposedIpfsHash, _proposerEmail));
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     *      variables without shifting down storage in contracts inheriting from this.
     *      See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     * @dev This array size plus the number of variables defined should always be 50
     */
    uint256[47] private _gap;
}
