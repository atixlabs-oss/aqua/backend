// SPDX-License-Identifier: AGPL-3.0-only

/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./interfaces/IClaimsRegistry.sol";
import "./utils/SignatureVerifier.sol";
import "./utils/StringUtils.sol";

/**
 * @title This contract holds information about claims made by COA members
 * @dev this was originally based on EIP780 Ethereum Claims Registry https://github.com/ethereum/EIPs/issues/780
 *      but with current version more based on EIP1812 Ethereum Verifiable Claims
 *      https://eips.ethereum.org/EIPS/eip-1812, on which the user is not the sender of the tx but only part of
 *      it through a signature
 */
contract ClaimsRegistry is IClaimsRegistry, Initializable, OwnableUpgradeable, SignatureVerifier {
    struct ClaimProposal {
        // The IPFS hash can be bytes32 but IPFS hashes are 34 bytes long due to multihash.
        // We could strip the first two bytes but for now it seems unnecessary.
        // We are then representing ipfs hashes as strings
        string proofHash;
        uint256 activityId;
        address proposerAddress;
        string proposerEmail;
        // Used for determining whether this structure is initialized or not
        bool exists;
    }

    struct ClaimAudit {
        // Included as the original proposal could be edited
        ClaimProposal proposal;
        address auditorAddress;
        string auditorEmail;
        bool approved;
        // Used for determining whether this structure is initialized or not
        bool exists;
        string ipfsHash;
    }

    // Proposed claim by project id => proposer address => claim's hash => proposed claim
    mapping(string => mapping(address => mapping(bytes32 => ClaimProposal))) public registryProposedClaims;
    // Claim by project id => auditor address => claim's hash => audit of claim.
    mapping(string => mapping(address => mapping(bytes32 => ClaimAudit))) public registryAuditedClaims;

    function registryInitialize() public initializer {
        OwnableUpgradeable.__Ownable_init();
    }

    function proposeClaim(
        string calldata _projectId,
        bytes32 _claimHash,
        string calldata _proofHash,
        uint256 _activityId,
        string calldata _proposerEmail,
        address _proposerAddress,
        bytes calldata _authorizationSignature,
        uint256 _nonce
    ) external override onlyOwner {
        // Get the signer of the authorization message
        bytes32 messageHash =
            hashProposedClaim(_projectId, _claimHash, _proofHash, _activityId, _proposerEmail, _nonce);
        bool validSignature = verifySignature(_proposerAddress, messageHash, _authorizationSignature, _nonce);
        require(validSignature, "Invalid signature for claim proposal");

        // Register proposed claim
        registryProposedClaims[_projectId][_proposerAddress][_claimHash] = ClaimProposal({
            proofHash: _proofHash,
            activityId: _activityId,
            proposerAddress: _proposerAddress,
            proposerEmail: _proposerEmail,
            exists: true
        });

        // Emit event for the claim proposed
        emit ClaimProposed(_projectId, _proposerAddress, _claimHash, _proofHash, block.timestamp, _activityId);
    }

    function submitClaimAudit(
        string calldata _projectId,
        bytes32 _claimHash,
        string calldata _proposalProofHash,
        address _proposerAddress,
        ClaimAuditReq calldata _audit,
        bytes calldata _authorizationSignature,
        uint256 _nonce
    ) external override onlyOwner {
        // Obtain the signer of the authorization msg
        bytes32 messageHash =
            hashClaimAuditResult(_projectId, _claimHash, _proposalProofHash, _proposerAddress, _audit, _nonce);
        bool validSignature = verifySignature(_audit.auditorAddress, messageHash, _authorizationSignature, _nonce);
        require(validSignature, "Invalid signature for claim audit");

        // Perform validations
        ClaimProposal storage proposedClaim = registryProposedClaims[_projectId][_proposerAddress][_claimHash];
        require(proposedClaim.exists, "Claim wasn't proposed");
        require(
            StringUtils.areEqual(proposedClaim.proofHash, _proposalProofHash),
            "Claim proposal has different proof hash than expected"
        );
        require(
            !registryAuditedClaims[_projectId][_audit.auditorAddress][_claimHash].exists,
            "Auditor already audited this claim"
        );

        // Register audited claim
        registryAuditedClaims[_projectId][_audit.auditorAddress][_claimHash] = ClaimAudit({
            proposal: proposedClaim,
            ipfsHash: _audit.ipfsHash,
            auditorAddress: _audit.auditorAddress,
            auditorEmail: _audit.auditorEmail,
            approved: _audit.approved,
            exists: true
        });

        // Emit event for the audited claim
        emit ClaimAudited(
            _projectId,
            _audit.auditorAddress,
            _claimHash,
            _audit.approved,
            _audit.ipfsHash,
            block.timestamp,
            proposedClaim.activityId
        );
    }

    function areApproved(
        string calldata _projectId,
        address[] calldata _auditors,
        bytes32[] calldata _claims
    ) external view override returns (bool) {
        require(_auditors.length == _claims.length, "arrays must be equal size");
        for (uint256 i = 0; i < _claims.length; i++) {
            ClaimAudit memory claim = registryAuditedClaims[_projectId][_auditors[i]][_claims[i]];
            // If claim.approved then the ClaimProposal exists,
            // as it's checked when the audit result is processed
            if (!claim.approved) return false;
        }
        return true;
    }

    function hashProposedClaim(
        string memory _projectId,
        bytes32 _claimHash,
        string memory _proofHash,
        uint256 _activityId,
        string memory _proposerEmail,
        uint256 _proposerNonce
    ) internal view returns (bytes32) {
        return keccak256(abi.encode(_proposerNonce, _projectId, _claimHash, _proofHash, _activityId, _proposerEmail));
    }

    // Receives all parameters for proposeClaim, only the submitClaimAudit isn't hashed, as it is derived
    // from the signature
    function hashClaimAuditResult(
        string memory _projectId,
        bytes32 _claimHash,
        string memory _proposalProofHash,
        address _proposerAddress,
        ClaimAuditReq memory _audit,
        uint256 _auditorNonce
    ) internal view returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    _auditorNonce,
                    _projectId,
                    _claimHash,
                    _proposalProofHash,
                    _proposerAddress,
                    _audit.auditorEmail,
                    _audit.ipfsHash,
                    _audit.approved
                )
            );
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     *      variables without shifting down storage in contracts inheriting from this.
     *      See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     * @dev This array size plus the number of variables defined should always be 50
     */
    uint256[48] private _gap;
}
