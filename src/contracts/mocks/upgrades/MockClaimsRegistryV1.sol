// SPDX-License-Identifier: AGPL-3.0-only

/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.9;

import "../../ClaimsRegistry.sol";
import "../../utils/UpgradeableToV1.sol";
import "./VariableStorage.sol";

/**
 * @title V2 of the ClaimsRegistry contract, extending it's behavior with the VariableStorage
 *        Used only for testing purposes
 */
contract MockClaimsRegistryV1 is ClaimsRegistry, VariableStorage, UpgradeableToV1 {
    function claimUpgradeToV1(address _owner, string calldata _initialVariable) external upgraderToV1 {
        OwnableUpgradeable.transferOwnership(_owner);
        setVariable(_initialVariable);
    }

    uint256[49] private _gap;
}
