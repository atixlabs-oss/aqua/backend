/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  UpdateWithInvalidProjectStatus: status => ({
    message: `Activity of project with status ${status} can't be updated`,
    statusCode: 403
  }),
  UserIsNotAuditorInProject: (userId, projectId) => ({
    message: `User with id ${userId} can't be assigned as an auditor because they doesn't have the role in the project with id ${projectId}`,
    statusCode: 403
  }),
  ProjectNotFound: taskId => ({
    message: `Project of task id ${taskId} not found`,
    statusCode: 400
  }),
  MilestoneNotFound: activityId => ({
    message: `Milestone of activity id ${activityId} not found`,
    statusCode: 400
  }),
  DeleteWithInvalidProjectStatus: status => ({
    message: `Task of project with status ${status} can't be deleted`,
    statusCode: 403
  }),
  CreateWithInvalidProjectStatus: status => ({
    message: `Can't create new activity in project with status ${status}`,
    statusCode: 403
  }),
  OracleAddressNotFound: taskId => ({
    message: `Address of oracle assigned for task ${taskId} not found`,
    statusCode: 400
  }),
  EvidenceStatusNotValid: status => ({
    message: `Evidence status '${status}' is not a valid value`,
    statusCode: 400
  }),
  EvidenceStatusCannotChange: status => ({
    message: `Status ${status} of an evidence cannot be updated`,
    statusCode: 400
  }),
  UserCanNotAddEvidenceToProject: ({ userId, activityId, projectId }) => ({
    message: `User ${userId} can't add evidence to activity ${activityId} because it is not the beneficiary or investor of project ${projectId}`,
    statusCode: 403
  }),
  InvalidEvidenceType: type => ({
    message: `The evidence type ${type} is invalid`,
    statusCode: 400
  }),
  UserCantUpdateEvidence: {
    message: 'User does not have the rights to update the evidence',
    statusCode: 400
  },
  UserCantDeleteEvidence: (userId, evidenceId) => ({
    message: `User ${userId} does not have the rights to delete the evidence ${evidenceId}`,
    statusCode: 400
  }),
  EvidenceUpdateError: {
    message: 'Evidence couldnt be updated',
    statusCode: 500
  },
  ActivityStatusCantBeUpdated: {
    message: 'There was an error updating activity status',
    statusCode: 500
  },
  InvalidStatusTransition: {
    message: 'Status transition is invalid',
    statusCode: 400
  },
  InvalidStatus: status => ({
    message: `Status ${status} is invalid`,
    statusCode: 400
  }),
  InvalidRequiredStatus: {
    message: 'Required activity status was not meet',
    statusCode: 400
  },
  UserIsNotActivityAuditor: {
    message: 'User is not auditor of this activity',
    statusCode: 400
  },
  TaskNotReady: {
    message: 'All of the evidences need to be rejected or approved',
    statusCode: 400
  },
  ActivityIsApprovedOrInProgress: status => ({
    message: `Cant add evidence to an activity with ${status} status`,
    statusCode: 400
  }),
  TransactionIsNotRelatedToProjectAddress: {
    message: 'Transaction is not related with project address',
    statusCode: 400
  },
  CantDeleteTaskWithStatus: status => ({
    message: `Cant delete activity with status ${status}`,
    statusCode: 500
  }),
  CantUpdateTaskWithStatus: status => ({
    message: `Cant update activity with status ${status}`,
    statusCode: 500
  }),
  OnlyProposerCanSendProposeClaimTransaction: {
    message:
      'Can not send the transaction because the user is not the proposer of the activity',
    statusCode: 400
  },
  OnlyAuditorCanSendubmitClaimAuditResultTransaction: {
    message:
      'Can not send the transaction because the user is not the auditor of the activity',
    statusCode: 400
  },
  InvalidStatusToSendTransaction: {
    message:
      'Can not send the transaction because activity does not have status valid',
    statusCode: 400
  },
  InvalidStep: {
    message:
      'The action you are trying to perform does not follow the appropriate steps',
    statusCode: 400
  },
  InvalidActivityType: {
    message: 'Invalid activity type',
    statusCode: 400
  },
  InvalidActivityTypeInProjectType: type => ({
    message: `Invalid ${type} activity type in grant project type`,
    statusCode: 400
  }),
  OnlyAuditorIsUpdaptable: status => ({
    message: `Only auditor is updaptable in activity with status ${status}`,
    statusCode: 400
  }),
  TaskTypeIsNotUpdaptable: status => ({
    message: `Type is not updaptable in activity with status ${status}`,
    statusCode: 400
  })
};
