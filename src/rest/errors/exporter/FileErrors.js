/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  ImgFileTyPeNotValid: {
    message: 'The image file type is not a valid one',
    statusCode: 400
  },
  MilestoneFileTypeNotValid: {
    message: 'The milestone file type is not a valid one',
    statusCode: 400
  },
  DocFileTypeNotValid: {
    message: 'The document file type is not a valid one',
    statusCode: 400
  },
  EvidenceFileTypeNotValid: {
    message: 'The evidence file type is not a valid one',
    statusCode: 400
  },
  ImgSizeBiggerThanAllowed: {
    // TODO: change name to FileSizeBiggerThanAllowed
    message: 'The file size is bigger than allowed',
    statusCode: 400
  },
  MilestoneTemplateNotExist: {
    message: "Milestone template doesn't exist",
    statusCode: 500
  },
  ErrorReadingMilestoneTemplate: {
    message: 'Error reading milestones template file',
    statusCode: 500
  }
};
