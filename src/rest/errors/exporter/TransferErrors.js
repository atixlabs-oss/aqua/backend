/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  ProjectCantReceiveTransfers: status => ({
    message: `Project with status ${status} can't receive transfers`,
    statusCode: 403
  }),
  TransferIdAlreadyExists: transferId => ({
    message: `There is another PENDING or VERIFIED transfer with the same transferId ${transferId}`,
    statusCode: 403
  }),
  TransferStatusNotValid: status => ({
    message: `Transfer status '${status}' is not a valid value`,
    statusCode: 403
  }),
  TransferStatusCannotChange: status => ({
    message: `A transfer of status '${status}' can't be updated`,
    statusCode: 403
  }),
  InvalidTransferTransition: {
    message: 'Transfer status transition is not valid',
    statusCode: 400
  },
  CantCreateTransfer: {
    message: "Couldn't create transfer"
  },
  BlockchainInfoNotFound: tansferId => ({
    message: `Fund transfer ${tansferId} doesn't have blockchain information`,
    statusCode: 400
  })
};
