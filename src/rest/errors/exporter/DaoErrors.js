/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  ErrorSubmittingProposal: daoId => ({
    message: `An error has occurred while submitting the proposal to DAO ${daoId}`
  }),
  ErrorVotingProposal: (proposalId, daoId) => ({
    message: `An error has occurred while voting the proposal ${proposalId} of DAO ${daoId}`
  }),
  ErrorProcessingProposal: (proposalId, daoId) => ({
    message: `An error has occurred while processing the proposal ${proposalId} of DAO ${daoId}`
  }),
  ErrorGettingProposals: daoId => ({
    message: `An error has occurred while getting the proposals of ${daoId}`
  }),
  ErrorGettingDaos: () => ({
    message: 'An error has occurred while getting the Daos'
  }),
  ErrorGettingDaoUsers: daoId => ({
    message: `An error has occurred while getting the Users of DAO ${daoId}`
  }),
  ErrorGettingMember: (address, daoId) => ({
    message: `An error has occurred while getting the member of address ${address} in DAO ${daoId}`
  }),
  MemberNotFound: (address, daoId) => ({
    message: `Member of address ${address} in DAO ${daoId} not found`,
    statusCode: 403
  }),
  InvalidProposalType: () => ({
    message: 'Proposal type is not valid',
    statusCode: 403
  }),
  ProposalStatusNotValid: status => ({
    message: `Proposal status '${status}' is not a valid value`,
    statusCode: 400
  }),
  ProposalStatusCannotChange: status => ({
    message: `Status ${status} of a proposal cannot be updated`,
    statusCode: 400
  }),
  VoteStatusNotValid: status => ({
    message: `Vote status '${status}' is not a valid value`,
    statusCode: 400
  }),
  VoteStatusCannotChange: status => ({
    message: `Status ${status} of a vote cannot be updated`,
    statusCode: 400
  })
};
