/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const common = require('./CommonErrors');
const file = require('./FileErrors');
const task = require('./TaskErrors');
const milestone = require('./MilestoneErrors');
const project = require('./ProjectErrors');
const transfer = require('./TransferErrors');
const user = require('./UserErrors');
const dao = require('./DaoErrors');
const server = require('./ServerErrors');
const mail = require('./MailErrors');
const transaction = require('./TransactionErrors');
const userWallet = require('./UserWalletErrors');
const token = require('./TokenErrors');
const userProject = require('./UserProjectErrors');
const changelog = require('./ChangelogErrors');
const middleware = require('./MiddlewareErrors');

module.exports = {
  common,
  file,
  task,
  milestone,
  project,
  transfer,
  user,
  dao,
  server,
  mail,
  transaction,
  userWallet,
  token,
  userProject,
  changelog,
  middleware
};
