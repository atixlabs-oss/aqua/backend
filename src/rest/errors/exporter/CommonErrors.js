/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  RequiredParamsMissing: method => ({
    message: `Required params are missing for method ${method}`,
    statusCode: 400
  }),
  CantFindModelWithId: (model, id) => ({
    message: `Cant find ${model} with id ${id}`,
    statusCode: 400
  }),
  ErrorGetting: model => ({
    message: `Error getting ${model}`,
    statusCode: 500
  }),
  UserNotAuthorized: userId => ({
    message: `User ${userId} not authorized for this action`,
    statusCode: 401
  }),
  InvalidStatus: (model, status) => ({
    message: `Can't make this action when the ${model} is in ${status} status`,
    statusCode: 400
  }),
  CantFindModelWithAddress: (model, address) => ({
    message: `Can't find ${model} with address ${address}`,
    statusCode: 400
  }),
  CantFindModelWithTxHash: (model, txHash) => ({
    message: `Can't find ${model} with txHash ${txHash}`,
    statusCode: 400
  }),
  ErrorCreating: model => ({
    message: `There was an error creating ${model}`,
    statusCode: 500
  }),
  ErrorDeleting: model => ({
    message: `There was an error deleting ${model}`,
    statusCode: 500
  }),
  InvalidStep: {
    message:
      'The action you are trying to perform does not follow the appropriate steps',
    statusCode: 400
  }
};
