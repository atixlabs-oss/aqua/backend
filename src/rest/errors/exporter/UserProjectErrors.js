/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  RolesUserError: user => ({
    message: `Error getting roles of user: ${user}`,
    statusCode: 500
  }),
  CantRemoveAuditorFromProject: projectId => ({
    message: `Cant remove auditor from project ${projectId} since it has assigned activities`,
    statusCode: 400
  }),
  CantEditUserProject: (roleDescription, projectStatus) => ({
    message: `Cant edit user with role ${roleDescription} in a project with status ${projectStatus}`,
    statusCode: 400
  })
};
