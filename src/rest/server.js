/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable no-unused-vars */
const { network, run, ethers } = require('hardhat');
const COAError = require('./errors/COAError');
const errors = require('./errors/exporter/ErrorExporter');
const { ethInit } = require('../rest/services/eth/ethInit');
const userService = require('./services/userService');
const cronJobService = require('./services/cronjob/cronjobService');

/**
 * @method start asynchronous start server -> initialice fastify, with database, plugins and routes
 * @param db instance of database creator.
 * @param logger instance of a logger that contains the pino interface
 * @param serverConfigs server configs for the connection. I.e -> {host: 'localhost', port: 3000}
 */

// TODO : this should handle the txs that reverted.
// TODO : move this to another file.
process.on('unhandledRejection', (reason, p) => {
  const { data, message, code } = reason;
  if (message === 'VM Exception while processing transaction: revert') {
    const txs = Object.keys(data).filter(k => !['stack', 'name'].includes(k));
  }
});

global.secondsSinceLastBlockRead = 0;

module.exports.start = async ({ db, logger, configs }) => {
  try {
    const swaggerConfigs = configs.swagger;
    const fastify = require('fastify')({ logger });
    fastify.register(require('fastify-cors'), {
      credentials: true,
      allowedHeaders: [
        'content-type',
        'authorization',
        'access-control-allow-origin'
      ],
      origin: true,
      exposedHeaders: ['authorization']
    });

    // fastify.register(require('fastify-cookie'));
    fastify.configs = configs;
    fastify.register(require('fastify-file-upload'));
    initJWT(fastify);
    // Init DB
    try {
      await db.register(fastify); // fastify.models works after run fastify.listen(...,...)
    } catch (e) {
      fastify.log.error('Cant connect to DB');
    }

    // Load Swagger
    fastify.register(require('fastify-swagger'), swaggerConfigs);

    const currentWorkDirectory = process.cwd();
    fastify.register(require('fastify-static'), {
      root: `${currentWorkDirectory}/projects`
    });

    fastify.setErrorHandler((error, request, reply) => {
      if (error instanceof COAError) {
        logger.error('Error handler catch COAError:', error);
        reply.status(error.statusCode).send(error.message);
      } else {
        logger.error('Error handler catch generic error:', error);
        reply.status(500).send('Internal Server Error');
      }
    });
    loadRoutes(fastify);

    await fastify.listen(configs.server);
    // start service initialization, load and inject dependencies
    if (network.name === 'hardhat') {
      try {
        logger.info('Deploying contracts');
        await run('deploy');
        logger.info('Contracts deployed');
      } catch (error) {
        logger.error('Error deploying contracts', error);
      }
    }
    require('./ioc')(fastify);
    cronJobService.cronInit(); // TODO is this doing anything useful?

    module.exports.fastify = fastify;
  } catch (err) {
    logger.error('Error initializing server', err);
    process.exit(1);
  }
};

/**
 * remove the server and run it all in one node process.
 * fastify was used temporarily because service injection,
 * data and database issues require more working time.
 */
module.exports.startCheckTransaction = async ({ db, logger, configs }) => {
  try {
    const fastify = require('fastify')({ logger });

    fastify.configs = configs;

    // Init DB
    try {
      await db.register(fastify); // fastify.models works after run fastify.listen(...,...)
    } catch (e) {
      fastify.log.error('Cant connect to DB');
    }

    await fastify.listen({ port: 3002 });
    require('./ioc')(fastify);
    ethInit();

    const SECONDS_CHECK_PROVIDER = 120;
    setInterval(() => {
      const currentTime = new Date().getTime();
      if (
        currentTime - global.secondsSinceLastBlockRead >
        1000 * SECONDS_CHECK_PROVIDER
      ) {
        logger.info('Provider listener is down, re-initializing...');
        ethInit();
      }

      if (ethers.provider._events.length === 0) {
        logger.info('Provider listener is down, re-initializing...');
        ethInit();
      }
    }, 1000 * SECONDS_CHECK_PROVIDER);

    module.exports.fastify = fastify;
  } catch (err) {
    logger.error('Error initializing server', err);
    process.exit(1);
  }
};

const loadRoutes = fastify => {
  const fs = require('fs');
  const routesDir = `${__dirname}/routes`;
  const dirents = fs.readdirSync(routesDir, { withFileTypes: true });
  const routeNames = dirents
    .filter(dirent => !dirent.isDirectory())
    .map(dirent => dirent.name);
  const routes = routeNames.map(route => require(`${routesDir}/${route}`));

  routes.forEach(route =>
    Object.values(route).forEach(async ({ method, path, options, handler }) => {
      fastify.register(async () => {
        const routeOptions = { ...options };
        if (options.beforeHandler) {
          const decorators = options.beforeHandler.map(
            decorator => fastify[decorator]
          );
          routeOptions.beforeHandler = decorators;
        }

        fastify.route({
          method: method.toUpperCase(),
          url: path,
          ...routeOptions,
          handler: handler(fastify)
        });
      });
    })
  );
};

const initJWT = fastify => {
  const fp = require('fastify-plugin');
  const jwtPlugin = fp(async () => {
    fastify.register(require('fastify-jwt'), {
      secret: fastify.configs.jwt.secret
    });

    const getToken = request => {
      // const cookieToken = request.cookies.userAuth;
      const authHeader = request.headers.authorization;
      let token;

      if (authHeader) {
        let _; // unused
        if (!authHeader.startsWith('Bearer '))
          throw new Error('Invalid token format');
        [_, token] = authHeader.split(' ');
      }

      // if (!token && !cookieToken) {
      if (!token) {
        fastify.log.error('[Server] :: No token received for authentication');
        throw new COAError(errors.server.NotRegisteredUser);
      }
      // return token || cookieToken;
      return token;
    };

    // TODO : this should be somewhere else.
    const validateUser = async (token, isAdmin) => {
      const user = await fastify.jwt.verify(token);
      const validUser = await userService.validUser(user, isAdmin);
      if (!validUser) {
        fastify.log.error('[Server] :: Unathorized access for user:', user);
        throw new COAError(errors.server.UnauthorizedUser);
      }
      return user;
    };

    const getUserWallet = async userId => {
      const wallet = await userService.getUserWallet(userId);
      return wallet
        ? { encryptedWallet: wallet.encryptedWallet, address: wallet.address }
        : wallet;
    };

    fastify.decorate('generalAuth', async (request, reply) => {
      try {
        const token = getToken(request, reply);
        fastify.log.info('[Server] :: General JWT Authentication');
        if (token) await validateUser(token);
      } catch (err) {
        if (err.statusCode === errors.server.UnauthorizedUser.statusCode) {
          fastify.log.error('[Server] :: Unathorized access for user');
          throw new COAError(errors.server.UnauthorizedUser);
        }
        fastify.log.error('[Server] :: There was an error authenticating', err);
        throw new COAError(errors.server.AuthenticationFailed);
      }
    });
    fastify.decorate('adminAuth', async (request, reply) => {
      try {
        const token = getToken(request, reply);
        fastify.log.info('[Server] :: Admin JWT Authentication');
        if (token) await validateUser(token, true);
      } catch (error) {
        if (error.statusCode === errors.server.UnauthorizedUser.statusCode) {
          fastify.log.error('[Server] :: Unathorized access for user');
          throw new COAError(errors.server.UnauthorizedUser);
        }
        fastify.log.error(
          '[Server] :: There was an error authenticating',
          error
        );
        throw new COAError(errors.server.AuthenticationFailed);
      }
    });
    fastify.decorate('withUser', async request => {
      try {
        const token = getToken(request);
        if (token) request.user = await validateUser(token);
        request.user.wallet = await getUserWallet(request.user.id);
      } catch (error) {
        if (error.statusCode === errors.server.UnauthorizedUser.statusCode) {
          fastify.log.error('[Server] :: Unathorized access for user');
          throw new COAError(errors.server.UnauthorizedUser);
        }
        fastify.log.error(
          '[Server] :: There was an error authenticating',
          error
        );
        throw new COAError(errors.server.AuthenticationFailed);
      }
    });
  });
  fastify.register(jwtPlugin);
};
