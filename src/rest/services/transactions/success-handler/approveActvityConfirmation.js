/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { ACTIVITY_STATUS } = require('../../../util/constants');
const activityDao = require('../../../dao/activityDao');
const projectService = require('../../projectService');
const milestoneService = require('../../milestoneService');

const approveActivityConfirmation = async ({ activityId }) => {
  await activityDao.updateActivity(
    {
      status: ACTIVITY_STATUS.APPROVED
    },
    activityId
  );

  const activity = await activityDao.getActivityByIdWithMilestone(activityId);

  await milestoneService.updateStatusIfMilestoneIsComplete(
    activity.milestone.id
  );

  await projectService.updateStatusIfProjectIsComplete(
    activity.milestone.project
  );
};

module.exports = {
  approveActivityConfirmation
};
