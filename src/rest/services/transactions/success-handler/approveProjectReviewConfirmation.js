/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { ACTION_TYPE, projectStatuses } = require('../../../util/constants');
const mailService = require('../../mailService');
const projectService = require('../../projectService');
const userProjectService = require('../../userProjectService');
const milestoneService = require('../../milestoneService');
const logger = require('../../../logger');

const cancelProject = async (project, users) => {
  await projectService.updateProject(project.id, {
    status: projectStatuses.CANCELLED,
    isCancelRequested: false
  });

  await mailService.sendEmails({
    project,
    action: ACTION_TYPE.APPROVE_CANCELLATION,
    users
  });
}

const updateProject = async (project, users) => {
  const status = await projectService.getLastRevisionStatus(project.parent);
  await projectService.updateProject(project.id, {
    status
  });

  // Since the review approved can be a remove activity operation we have to
  // check if the project now is completed
  const milestones = await milestoneService.getAllMilestonesByProject(project.id)
  
  for (const milestone of milestones) {
    await milestoneService.updateStatusIfMilestoneIsComplete(milestone.id);
  }

  await projectService.updateStatusIfProjectIsComplete(
    project.id
  );

  await mailService.sendEmails({
    project,
    action: ACTION_TYPE.APPROVE_REVIEW,
    users
  });

  await userProjectService.setConfirmedUsersByProjectId({ projectId: project.id, confirmed: true });
}

module.exports = async ({ projectId }) => {
  const project = await projectService.getProjectById(projectId);
  logger.info(`[approveProjectReviewConfirmation] :: Project ${projectId} will be ${project.isCancelRequested ? 'cancelled' : 'updated'}`);

  const users = await userProjectService.getUsersOfProject(project.id);

  project.isCancelRequested ? await cancelProject(project, users) : await updateProject(project, users)
};
