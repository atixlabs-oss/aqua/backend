/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { ACTION_TYPE } = require('../../util/constants');
const publishProjectConfirmation = require('./success-handler/publishProjectConfirmation');
const sendActivityToReviewConfirmation = require('./success-handler/sendActivityToReviewConfirmation');
const sendProjectToReviewConfirmation = require('./success-handler/sendProjectToReviewConfirmation');
const {
  approveActivityConfirmation
} = require('./success-handler/approveActvityConfirmation');
const rejectActvityConfirmation = require('./success-handler/rejectActivityConfirmation');
const approveProjectReviewConfirmation = require('./success-handler/approveProjectReviewConfirmation');
const rejectProjectReviewConfirmation = require('./success-handler/rejectProjectReviewConfirmation');

module.exports = {
  [ACTION_TYPE.PUBLISH_PROJECT]: publishProjectConfirmation,
  [ACTION_TYPE.SEND_PROJECT_TO_REVIEW]: sendProjectToReviewConfirmation,
  [ACTION_TYPE.SEND_ACTIVITY_TO_REVIEW]: sendActivityToReviewConfirmation,
  [ACTION_TYPE.APPROVE_ACTIVITY]: approveActivityConfirmation,
  [ACTION_TYPE.REJECT_ACTIVITY]: rejectActvityConfirmation,
  [ACTION_TYPE.APPROVE_REVIEW]: approveProjectReviewConfirmation,
  [ACTION_TYPE.CANCEL_REVIEW]: rejectProjectReviewConfirmation
};
