/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { coa, web3 } = require('hardhat');

const config = require('config');
const logger = require('../../logger');
const { EVERY_DAY_AT_MIDNIGHT, EVERY_HOUR } = require('./cronExpressions');

module.exports = {
  transitionProjectStatusJob: {
    cronTime:
      config.crons.transitionProjectStatusJob.cronTime || EVERY_DAY_AT_MIDNIGHT,
    async onTick() {
      logger.info('[CronJobService] :: Executing transitionProjectStatusJob');
    },
    onComplete() {
      logger.info('[CronJobService] :: transitionProjectStatusJob has stopped');
    },
    timezone: config.crons.transitionProjectStatusJob.timezone || undefined,
    runOnInit: config.crons.transitionProjectStatusJob.runOnInit || false,
    disabled: config.crons.transitionProjectStatusJob.disabled || false
  },
  checkFailedTransactionsJob: {
    cronTime: config.crons.checkFailedTransactionsJob.cronTime || EVERY_HOUR,
    async onTick() {
      logger.info('[CronJobService] :: Executing checkFailedTransactionsJob');
      await this.activityService.updateFailedEvidenceTransactions();
    },
    onComplete() {
      logger.info('[CronJobService] :: checkFailedTransactionsJob has stopped');
    },
    timezone: config.crons.checkFailedTransactionsJob.timezone || undefined,
    runOnInit: config.crons.checkFailedTransactionsJob.runOnInit || false,
    disabled: config.crons.checkFailedTransactionsJob.disabled || false
  }
};
