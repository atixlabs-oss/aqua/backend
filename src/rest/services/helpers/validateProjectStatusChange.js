/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const errors = require('../../errors/exporter/ErrorExporter');
const COAError = require('../../errors/COAError');
const validateRequiredParams = require('../helpers/validateRequiredParams');

const logger = require('../../logger');

const { projectStatuses } = require('../../util/constants');

const {
  PUBLISHED,
  CANCELLED,
  DRAFT,
  OPEN_REVIEW,
  IN_PROGRESS,
  IN_REVIEW,
  COMPLETED,
  CANCELLED_REVIEW,
  PENDING_PUBLISHED,
  PENDING_REVIEW
} = projectStatuses;

const allowedTransitions = {
  [DRAFT]: [
    {
      validator: () => {},
      nextSteps: [PUBLISHED]
    }
  ],
  [PUBLISHED]: [
    {
      validator: () => {},
      nextSteps: [IN_PROGRESS, CANCELLED, COMPLETED]
    }
  ],
  [IN_PROGRESS]: [
    {
      validator: () => {},
      nextSteps: [CANCELLED, COMPLETED]
    }
  ],
  [OPEN_REVIEW]: [
    {
      validator: () => {},
      nextSteps: [PENDING_REVIEW, CANCELLED_REVIEW]
    }
  ],
  [IN_REVIEW]: [
    {
      validator: () => {},
      nextSteps: [PUBLISHED, IN_PROGRESS]
    }
  ],
  [CANCELLED_REVIEW]: [
    {
      validator: () => {},
      nextSteps: []
    }
  ],
  [COMPLETED]: [
    {
      validator: () => {},
      nextSteps: []
    }
  ],
  [CANCELLED]: [
    {
      validator: () => {},
      nextSteps: []
    }
  ],
  [PENDING_PUBLISHED]: [{ validator: () => {}, nextSteps: [] }],
  [PENDING_REVIEW]: [{ validator: () => {}, nextSteps: [IN_REVIEW] }]
};

/**
 * Validates if a project can be changed to the provided status.
 * Returns the new status.
 *
 * @param {*} user user requesting the change
 * @param {number} projectId project to update
 * @param {string} newStatus new project status
 */
module.exports = async ({ user, newStatus, project }) => {
  validateRequiredParams({
    method: 'validateProjectStatusChange',
    params: { user, newStatus, project }
  });
  const [transition] = allowedTransitions[project.status].filter(
    ({ nextSteps }) => nextSteps.includes(newStatus)
  );
  if (!transition || !transition.validator) {
    logger.error(
      `[Project Service] :: Project status transition from ${
        project.status
      } to ${newStatus} is not valid`
    );
    throw new COAError(errors.project.InvalidProjectTransition);
  }
  await transition.validator({ user, newStatus, project });
  return newStatus;
};
