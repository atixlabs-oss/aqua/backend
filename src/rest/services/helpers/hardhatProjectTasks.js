/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { task, types } = require('hardhat/config');
const { getSigner, signParameters } = require('./hardhatTaskHelpers');

const getProjectRegistryContract = async env => {
  const registry = env.deployments.getLastDeployedContract('ProjectsRegistry');
  if (registry === undefined) {
    console.error('ProjectRegistry contract not deployed');
    throw 'ProjectRegistry contract not deployed';
  }
  return registry;
}

task('create-project', 'Create Project')
  .addOptionalParam('id', 'Project id', 1, types.string)
  .addOptionalParam('ipfsHash', 'Project IPFS hash', 'ipfsagreementhash')
  .setAction(async ({ id, ipfsHash }, env) => {
    const projectRegistry = await getProjectRegistryContract(env);
    if (projectRegistry === undefined) {
      console.error('ProjectRegistry contract not deployed');
      return;
    }

    await projectRegistry.createProject(id || '44', ipfsHash);
    console.log(
      `New project created with: ${id} and ipfs hash: ${ipfsHash}`
    );
  });

task('propose-project-edit', 'Propose an edit to a project')
  .addParam('id', 'Project id')
  .addOptionalParam('ipfsHash', 'Proposed new IPFS hash', 'ipfsagreementhash')
  .addOptionalParam('proposerEmail', 'Proposer email', 'proposer@email.com')
  .addOptionalParam('proposerAddress', 'ProposerAddress', '', types.string)
  .setAction(async ({ id, ipfsHash, proposerEmail, proposerAddress }, env) => {
    const signer = await getSigner(env);
    const projectRegistry = await getProjectRegistryContract(env);

    if (proposerAddress == '') {
      proposerAddress = await signer.getAddress();
    }

    const proposerNonce = await projectRegistry.userNonces(proposerAddress)

    const authorizationSignature = await signParameters(
      ["uint256", "string", "string", "string"],
      [proposerNonce, id, ipfsHash, proposerEmail],
      signer
    );

    await projectRegistry.proposeProjectEdit(
      id,
      ipfsHash,
      proposerEmail,
      proposerAddress,
      authorizationSignature
    );
    console.log(
      `New project edit created by ${proposerAddress} with: ${id} and ipfs hash: ${ipfsHash}`
    );
  });

task('audit-project-edit-proposal', 'Audit a project edit proposal')
  .addParam('id', 'Project id')
  .addOptionalParam('proposalIpfsHash', 'Proposed new IPFS hash', 'ipfsagreementhash')
  .addOptionalParam('auditIpfsHash', 'Proposed new IPFS hash', 'ipfsagreementhash')
  .addOptionalParam('proposerAddress', 'Proposer address', 'proposer@email.com')
  .addOptionalParam('isApproved', 'Audit result', true, types.boolean)
  .setAction(async ({ id, proposalIpfsHash, auditIpfsHash, proposerAddress, isApproved }, env) => {
    const projectRegistry = await getProjectRegistryContract(env);

    await projectRegistry.submitProjectEditAuditResult(
      id,
      proposalIpfsHash,
      auditIpfsHash,
      proposerAddress,
      isApproved
    );
    console.log(
      `Project ${id} edit audited created with result ${isApproved}`
    );
  });

task('get-project-description', 'Get project description')
  .addParam('id', 'Project id')
  .setAction(async ({ id }, env) => {
    const projectRegistry = await getProjectRegistryContract(env);

    const description = await projectRegistry.projectsDescription(id);
    if (description.isCreated) {
      console.log(
        `Project ${id} has description: ${JSON.stringify(Object.entries(description).slice(5))}`
      );
    } else {
      console.log(`Queried project ${id} doesn't exist`);
    }
  });

task('get-project-proposed-edit', 'Get project description')
  .addParam('id', 'Project id')
  .addParam('proposerAddress', 'Address of the proposal creator')
  .setAction(async ({ id, proposerAddress }, env) => {
    const projectRegistry = await getProjectRegistryContract(env);

    const proposalDescription = await projectRegistry.pendingEdits(id, proposerAddress);
    if (proposalDescription.isCreated) {
      console.log(
        `Project ${id} has proposal with description: ${JSON.stringify(proposalDescription)}`
      );
    } else {
      console.log(`Queried project proposal ${id} from ${proposerAddress} doesn't exist`);
    }
  });
