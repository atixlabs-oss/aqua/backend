/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const path = require('path');
const fs = require('fs');
const { promisify } = require('util');
const COAError = require('../../errors/COAError');
const errors = require('../../errors/exporter/ErrorExporter');
const logger = require('../../logger');

const readFile = promisify(fs.readFile);

const templateNames = {
  GENERIC: 'generic',
  EMAIL_CONFIRMATION: 'confirmEmail',
  RECOVERY_PASSWORD: 'resetPassword',
  WELCOME: 'welcome',
  WELCOME_USER: 'welcomeUser',
  ALERT: 'alert',
  PUBLISH_PROJECT: 'publishProject',
  CLONE_IN_REVIEW: 'cloneInReview',
  REVIEW_REJECTED: 'cloneRejected',
  REVIEW_APPROVED: 'cloneApproved',
  TX_FAILED: 'errorTransaction',
  DIGITAL_SIGNATURE_REQUEST_STATUS: 'digitalSignatureRequestStatus',
  REQUEST_NEW_PIN: 'requestNewPin',
  REQUEST_NEW_PIN_USER: 'requestNewPinUser'
};

const templatePaths = {
  [templateNames.GENERIC]: '../../../../assets/templates/email/generic.html',
  [templateNames.SIGNUP]: '../../../../assets/templates/email/signup.html',
  [templateNames.EMAIL_CONFIRMATION]:
    '../../../../assets/templates/email/confirmEmail.html',
  [templateNames.RECOVERY_PASSWORD]:
    '../../../../assets/templates/email/recoveryPassword.html',
  [templateNames.WELCOME]: '../../../../assets/templates/email/welcome.html',
  [templateNames.PUBLISH_PROJECT]:
    '../../../../assets/templates/email/publishProject.html'
};

const getTemplatePath = template =>
  path.join(__dirname, templatePaths[template]);

const loadTemplate = async (template, read = readFile) => {
  if (!Object.keys(templatePaths).includes(template)) {
    logger.error(`[TemplateLoader] :: Email template ${template} not found`);
    throw new COAError(errors.mail.TemplateNotFound);
  }
  const filePath = getTemplatePath(template);
  const file = read(filePath);
  return file;
};

module.exports = {
  loadTemplate,
  getTemplatePath,
  templateNames
};
