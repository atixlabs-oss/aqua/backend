/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const userProjectService = require('../userProjectService');
const COAError = require('../../errors/COAError');
const { projectStatuses, rolesTypes } = require('../../util/constants');

const logger = require('../../logger');

module.exports = async ({ project, user, error }) => {
  const userId = user.id;
  const projectId = project.id;
  const { status } = project;
  logger.info('Entering validateUserCanEditProject method');
  if (user.isAdmin) {
    if (project.status !== projectStatuses.DRAFT) {
      throw new COAError(error(status));
    }
  } else {
    if (project.status !== projectStatuses.OPEN_REVIEW) {
      throw new COAError(error(status));
    }
    await userProjectService.getUserProjectFromRoleDescription({
      userId,
      projectId,
      roleDescriptions: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR]
    });
  }
};
