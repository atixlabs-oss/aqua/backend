/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const config = require('config');
const errors = require('../../errors/exporter/ErrorExporter');
const COAError = require('../../errors/COAError');

const logger = require('../../logger');

const MAX_PHOTO_SIZE = config.fileServer.maxFileSize;

/**
 * Validates that the file size is not larger than the max allowed
 * @param {File} file - File to validate its size
 */
module.exports = (file, maxSize = MAX_PHOTO_SIZE) => {
  // TODO: change file name to validateFileSize
  logger.info('[ValidatePhotoSize] :: Entering validatePhotoSize method');
  if (file.size > maxSize) {
    logger.error(
      '[ValidatePhotoSize] :: File size is bigger than the size allowed'
    );
    throw new COAError(errors.file.ImgSizeBiggerThanAllowed);
  }
};
