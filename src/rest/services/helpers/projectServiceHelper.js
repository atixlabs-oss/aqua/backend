/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mime = require('mime');
const errors = require('../../errors/exporter/ErrorExporter');
const COAError = require('../../errors/COAError');
const validateOwnership = require('./validateOwnership');

const logger = require('../../logger');

const MAX_PHOTO_SIZE = 500000;

// TODO: this file should eventually be deleted

const imgValidator = file => {
  logger.info('[ProjectServiceHelper] :: Entering imgValidator method');
  logger.info(
    `[ProjectServiceHelper] :: Looking for fileType of file ${file.name}`
  );
  const fileType = mime.lookup(file.name);
  if (!fileType.includes('image/')) {
    logger.error('[ProjectServiceHelper] :: File type is not a valid img type');
    throw new COAError(errors.file.ImgFileTyPeNotValid);
  }
};

const xslValidator = file => {
  logger.info('[ProjectServiceHelper] :: Entering xsl method');
  logger.info(
    `[ProjectServiceHelper] :: Looking for fileType of file ${file.name}`
  );
  const fileType = mime.lookup(file.name);
  if (
    !(
      fileType === 'application/vnd.ms-excel' ||
      fileType ===
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
  ) {
    logger.error(
      '[ProjectServiceHelper] :: File type is not a valid excel type'
    );
    throw new COAError(errors.file.MilestoneFileTypeNotValid);
  }
};

const mtypesValidator = {
  coverPhoto: imgValidator,
  thumbnail: imgValidator,
  milestones: xslValidator,
  experiencePhoto: imgValidator
};

const validateMtype = type => file => mtypesValidator[type](file);

const validatePhotoSize = file => {
  logger.info('[ProjectServiceHelper] :: Entering validatePhotosSize method');
  if (file.size > MAX_PHOTO_SIZE) {
    logger.error(
      '[ProjectServiceHelper] :: File size is bigger than the size allowed'
    );
    throw new COAError(errors.file.ImgSizeBiggerThanAllowed);
  }
};

module.exports = {
  validateMtype,
  validatePhotoSize,
  validateOwnership,
  xslValidator,
  imgValidator
};
