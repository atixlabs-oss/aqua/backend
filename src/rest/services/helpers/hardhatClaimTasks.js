/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { task, types } = require('hardhat/config');
const { getSigner, signParameters } = require('./hardhatTaskHelpers');

const getClaimRegistryContract = async env => {
  const registry = env.deployments.getLastDeployedContract('ClaimsRegistry');
  if (registry === undefined) {
    console.error('ClaimRegistry contract not deployed');
    throw 'ClaimRegistry contract not deployed';
  }
  return registry;
}

function mappingToJSON(mapping, startingIndex = 0) {
  if (!!mapping) {
    return JSON.stringify(Object.entries(mapping).slice(startingIndex));
  } else {
    return "[]";
  }
}

task('propose-claim', 'Propose an edit to a project')
  .addParam('id', 'Project id')
  .addOptionalParam('claimHash', 'The hash of the claim', '0x0000000000000000000000000000000000000000000000000000000000001234')
  .addOptionalParam('proofHash', 'The hash of the proof', 'QmR86wutAMSxuAcYPW9C6hqowWHbtQSiuJHuebXtn2zX7M')
  .addOptionalParam('activityId', 'The id of the activity the claim belongs to', 10, types.int)
  .addOptionalParam('proposerEmail', 'The email of the proposer', "proposer@email.com")
  .addOptionalParam('proposerAddress', 'The address of the proposer', "")
  .setAction(async ({ id, claimHash, proofHash, activityId, proposerEmail, proposerAddress }, env) => {
    const signer = await getSigner(env);
    const claimRegistry = await getClaimRegistryContract(env);

    if (proposerAddress == '') {
      proposerAddress = await signer.getAddress();
    }
    const proposerNonce = await claimRegistry.userNonces(proposerAddress)

    const authorizationSignature = await signParameters(
      ['uint256', 'string', 'bytes32', 'string', 'uint256', 'string'],
      [proposerNonce, id, claimHash, proofHash, activityId, proposerEmail],
      signer
    );

    await claimRegistry.proposeClaim(
      id,
      claimHash,
      proofHash,
      activityId,
      proposerEmail,
      proposerAddress,
      authorizationSignature
    );
    console.log(
      `New claim created with: ${id}, claim hash: ${claimHash} and proof hash ${proofHash}`
    );
  });

task('audit-claim', 'Audit a project edit proposal')
  .addParam('id', 'Project id')
  .addOptionalParam('claimHash', 'The hash of the claim', '0x0000000000000000000000000000000000000000000000000000000000001234')
  .addOptionalParam('proposalProofHash', 'The hash of the proof', 'QmR86wutAMSxuAcYPW9C6hqowWHbtQSiuJHuebXtn2zX7M')
  .addOptionalParam('auditIpfsHash', 'The hash of the proof', 'QmVSDY2wZi8sfuCb6guL3bKieHXbHpgzogfwmNWXk44eEi')
  .addParam('proposerAddress', 'The address of the proposer')
  .addOptionalParam('auditorEmail', 'The email of the auditor', "proposer@email.com")
  .addOptionalParam('auditorAddress', 'The address of the auditor', "")
  .addOptionalParam('isApproved', 'The audit result', true, types.boolean)
  .setAction(async ({ id, claimHash, proposalProofHash, auditIpfsHash, proposerAddress, auditorEmail, auditorAddress, isApproved }, env) => {
    const signer = await getSigner(env);
    const claimRegistry = await getClaimRegistryContract(env);

    if (auditorAddress == '') {
      auditorAddress = await signer.getAddress();
    }
    const auditorNonce = await claimRegistry.userNonces(auditorAddress)

    const authorizationSignature = await signParameters(
      ['uint256', 'string', 'bytes32', 'string', 'address', 'string', 'string', 'bool'],
      [auditorNonce, id, claimHash, proposalProofHash, proposerAddress, auditorEmail, auditIpfsHash, isApproved],
      signer
    );

    await claimRegistry.submitClaimAudit(
      id,
      claimHash,
      proposalProofHash,
      proposerAddress,
      {
        auditorEmail: auditorEmail,
        auditorAddress: auditorAddress,
        ipfsHash: auditIpfsHash,
        approved: isApproved
      },
      authorizationSignature
    );
    console.log(
      `Audited (with result ${isApproved}) claim of project ${id}, claim hash: ${claimHash} proof hash ${proposalProofHash} and author ${proposerAddress}`
    );
});

task('get-claim', 'Get project description')
  .addParam('id', 'Project id')
  .addOptionalParam('proposerAddress', 'Proposer of the claim')
  .addOptionalParam('auditorAddress', 'Auditor of the proposal of the claim')
  .addOptionalParam('claimHash', 'The hash of the claim', '0x0000000000000000000000000000000000000000000000000000000000001234')
  .setAction(async ({ id, proposerAddress, auditorAddress, claimHash }, env) => {
    const claimRegistry = await getClaimRegistryContract(env);
    if (claimRegistry === undefined) {
      console.error('ClaimRegistry contract not deployed');
      return;
    }

    if (!!auditorAddress) {
        const audit = await claimRegistry.registryAuditedClaims(id, auditorAddress, claimHash);
        if (audit.exists) {
          console.log(
            `Claim ${claimHash} from project ${id} has audit:
              ${mappingToJSON(audit, 0)}`
          )
          return;
        }
    }

    if(!!proposerAddress) {
      const proposal = await claimRegistry.registryProposedClaims(id, proposerAddress, claimHash);
      if (proposal.exists) {
        console.log(
          `Claim ${claimHash} from project ${id} has proposal (with no audit):
            ${mappingToJSON(proposal, 5)}`
        );
        return;
      }
    }
  });
