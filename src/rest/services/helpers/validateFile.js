/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { common } = require('../../errors/exporter/ErrorExporter');
const COAError = require('../../errors/COAError');
const validateMtype = require('./validateMtype');
const validatePhotoSize = require('./validatePhotoSize');

const logger = require('../../logger');

module.exports = ({ filePathOrHash, fileParam, paramName, method, type }) => {
  logger.info('[validateFile] :: Entering validateFileInFirstUpdate method');
  if (!filePathOrHash && !fileParam) {
    logger.info(
      `[validateFile] :: In the first update the ${paramName} field is required`
    );
    throw new COAError(common.RequiredParamsMissing(method));
  }
  if (fileParam) {
    validateMtype(type, fileParam);
    validatePhotoSize(fileParam);
  }
};
