/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mustache = require('mustache');

const { loadTemplate } = require('./templateLoader');
const logger = require('../../logger');

/**
 * Completes the template with the given data
 *
 * @param {object} dataToComplete data to complete template with
 * @param {string} template template name
 * @param {Promise} loader function to load the template file
 * @returns {Promise<string>} string representation of the completed template
 */
exports.completeTemplate = async (
  dataToComplete,
  template,
  loader = loadTemplate
) => {
  logger.info(
    `[TemplateParser] :: Entering completeTemplate method for ${template} template`
  );
  const templateFile = await loader(template);
  const completedTemplate = mustache.render(
    templateFile.toString(),
    dataToComplete
  );
  return completedTemplate;
};
