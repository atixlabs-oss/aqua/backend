/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { isEmpty } = require('lodash');
const { common } = require('../../errors/exporter/ErrorExporter');
const COAError = require('../../errors/COAError');

const logger = require('../../logger');

/**
 * Looks up a record in database to check its existence.
 * Returns the object found or throws an error if not exists.
 * @param {Object} dao - Corresponding DAO for the record
 * @param {number} id - Id of the record to look up
 * @param {string} model - Corresponding model name for the record
 */
module.exports = async (dao, id, model, method) => {
  logger.info('[CheckExistence] :: Entering checkExistence method');
  logger.info(
    `[CheckExistence] :: About to check if ${model} with id ${id} exists`
  );
  const returnedObject = method ? await method : await dao.findById(id);
  if (isEmpty(returnedObject)) {
    logger.error(`[CheckExistence] :: ${model} with id ${id} not found`);
    throw new COAError(common.CantFindModelWithId(model, id));
  }
  logger.info(`[CheckExistence] :: ${model} found`);
  return returnedObject;
};
