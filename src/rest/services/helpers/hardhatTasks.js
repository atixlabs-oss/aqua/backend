/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { Wallet } = require('ethers');
const { task, types } = require('hardhat/config');

const testSetup = require('../../../../scripts/jestGlobalSetup');
const testTeardown = require('../../../../scripts/jestGlobalTearDown');

const { getSigner, convertEmptyToNullableList } = require('./hardhatTaskHelpers');
const Logger = require('../../logger');

task('test-contracts', 'Runs setup, tests and teardown').setAction(async () => {
  await global.run('test-contracts:testSetup');
  await global.run('test');
  await global.run('test-contracts:testTeardown');
});

task('test-contracts:testSetup', 'Runs the test setup').setAction(async () => {
  await testSetup();
});

task('test-contracts:testTeardown', 'Runs the test teardown').setAction(
  async () => {
    await testTeardown();
  }
);

async function getDeploymentSigner(env) {
  const { provider } = env.ethers;
  const accounts = await provider.listAccounts();

  return provider.getSigner(accounts[0]);
}

async function prepareDeployAndDo(
  env,
  resetStates,
  resetAllContracts,
  doFunction
) {
  if (resetStates || resetAllContracts) env.coa.clearContracts();
  const signer = await getDeploymentSigner(env);

  await doFunction(signer);

}

task('deploy', 'Deploys COA contracts')
  .addOptionalParam(
    'resetStates',
    'redeploy all proxies in order to reset all contract states',
    false,
    types.boolean
  )
  .addOptionalParam(
    'resetAllContracts',
    'force deploy of all contracts',
    false,
    types.boolean
  )
  .addVariadicPositionalParam(
    'contractsToDeploy',
    'List of contract names that should be deployed',
    []
  )
  .setAction(async ({ resetStates, resetAllContracts, contractsToDeploy }, env) => {
    // Set contracts to deploy to null if the list is empty
    const _contractsToDeploy = convertEmptyToNullableList(contractsToDeploy);

    // Deploy contracts
    await prepareDeployAndDo(
      env,
      resetStates,
      resetAllContracts,
      async signer =>
        env.deployments.deployContracts(signer, resetStates, resetAllContracts, _contractsToDeploy)
    );
  });

task(
  'upgradeToCurrentImpl',
  'Upgrades deployed contracts to their implemented version.' +
  'V1 versions of contracts are only for testing purposes'
)
  .addVariadicPositionalParam(
    'contractsToUpgrade',
    'List of contract names to update to',
    []
  )
  .setAction(async ({ contractsToUpgrade }, env) => {
    // Set contracts to upgrade to null if the list is empty
    const _contractsToUpgrade = convertEmptyToNullableList(contractsToUpgrade);

    // Upgrade contracts to v1
    await prepareDeployAndDo(
      env,
      true,
      true,
      async signer =>
        env.deployments.upgradeToCurrentImpl(signer, _contractsToUpgrade)
    );
  });

task(
  'upgradeContractsToV1',
  'Deploys and Upgrades COA contracts to v1.' +
  'V1 versions of contracts are only for testing purposes'
)
  .addOptionalParam(
    'resetStates',
    'redeploy all proxies in order to reset all contract states',
    false,
    types.boolean
  )
  .addOptionalParam(
    'resetAllContracts',
    'force deploy of all contracts',
    false,
    types.boolean
  )
  .addVariadicPositionalParam(
    'contractsToUpgrade',
    'List of contract names to update to'
  )
  .setAction(async ({ resetStates, resetAllContracts, contractsToUpgrade }, env) => {
    // Set contracts to upgrade to null if the list is empty
    const _contractsToUpgrade = convertEmptyToNullableList(contractsToUpgrade);

    // Upgrade contracts to v1
    await prepareDeployAndDo(
      env,
      resetStates,
      resetAllContracts,
      async signer =>
        env.deployments.upgradeToV1(signer, resetStates, resetAllContracts, _contractsToUpgrade)
    );
  });

task('get-signer-zero', 'Gets signer zero address').setAction(
  async (_args, env) => {
    const signer = await getSigner(env);
    console.log('Signer:', signer._address);
    return signer._address;
  }
);

task('encrypt-wallet')
  .addParam('pk')
  .addParam('password')
  .setAction(async ({ pk, password }, env) => {
    const wallet = new Wallet(pk, env.ethers.provider);
    const encryptedJson = JSON.stringify(await wallet.encrypt(password));
    console.log(encryptedJson);
    return encryptedJson;
  });
