/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { utils } = require('ethers');
const { randomInt } = require('crypto');

const getSigner = async (env, account) => env.deployments.getSigner(account);

const abiCoder = utils.defaultAbiCoder;

function getMessageHash(parameterTypes, parameterValues) {
  const encodedParams = abiCoder.encode(parameterTypes, parameterValues);
  return utils.keccak256(encodedParams);
}

async function signParameters(parameterTypes, parameterValues, signer) {
  const encodedParams = abiCoder.encode(parameterTypes, parameterValues);

  // Message hash is converted to bytes so that signMessage doesn't change it's encoding
  const messageHash = utils.arrayify(utils.keccak256(encodedParams));
  const signature = await signer.signMessage(messageHash);

  return signature;
}

function convertEmptyToNullableList(list) {
  let convertedList = null;
  if (list.length !== 0) {
    convertedList = list;
  }
  return convertedList;
}

const billion = 1000000000;
function generateNonceRandom() {
  return randomInt(billion);
} 

module.exports = {
  signParameters,
  getSigner,
  getMessageHash,
  convertEmptyToNullableList,
  generateNonceRandom
};
