/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { common } = require('../../errors/exporter/ErrorExporter');
const COAError = require('../../errors/COAError');

const logger = require('../../logger');

/**
 * Validate that all required params are defined.
 * Throws an error if any is `undefined`
 * @param {Object} args - Method's name and required params to validate
 * @param {string} args.method - method's name
 * @param {Object} args.params - method's required parameters
 */
module.exports = ({ method, params }) => {
  logger.info(
    '[ValidateRequiredParams] :: Entering validateRequiredParams method'
  );
  const undefinedParams = Object.keys(params).filter(
    key => params[key] === undefined || params[key] === null
  );
  if (undefinedParams.length > 0) {
    logger.error(
      `[ValidateRequiredParams] :: There are one or more params that are undefined for ${method}: ${undefinedParams}`
    );
    throw new COAError(common.RequiredParamsMissing(method));
  }
};
