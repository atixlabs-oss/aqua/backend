/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../../logger');

const BINARY_BASE = 2;
const BINARY_PLACES = 4;

const updateDataComplete = ({ dataComplete, step, value }) => {
  logger.info('[updateDataComplete] :: Entering updateDataComplete method');
  const dataCompletBinary = dataComplete.toString(BINARY_BASE);
  const dataCompleteArray = dataCompletBinary
    .padStart(BINARY_PLACES, '0')
    .split('');
  const stepIndex = dataCompleteArray.length - step;
  const dataCompleteArrayUpdated = dataCompleteArray.map((item, index) =>
    index !== stepIndex ? item : value
  );
  const dataCompleteBinaryUpdated = dataCompleteArrayUpdated.join('');
  return parseInt(dataCompleteBinaryUpdated, BINARY_BASE);
};

module.exports = {
  completeStep: ({ dataComplete, step }) =>
    updateDataComplete({ dataComplete, step, value: '1' }),
  removeStep: ({ dataComplete, step }) =>
    updateDataComplete({ dataComplete, step, value: '0' })
};
