/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const activityDaoBuilder = require('../dao/activityDao');
const activityFileDaoBuilder = require('../dao/activityFileDao');
const configsDaoBuilder = require('../dao/configsDao');
const fileDaoBuilder = require('../dao/fileDao');
const milestoneBudgetStatusDaoBuilder = require('../dao/milestoneBudgetStatusDao');
const milestoneDaoBuilder = require('../dao/milestoneDao');
const projectDaoBuilder = require('../dao/projectDao');
const projectStatusDaoBuilder = require('../dao/projectStatusDao');
const userDaoBuilder = require('../dao/userDao');
const userProjectDaoBuilder = require('../dao/userProjectDao');
const userSocialEntrepreneurDaoBuilder = require('../dao/userSocialEntrepreneurDao')
const blockchainBlockDaoBuilder = require('../dao/blockchainBlockDao');
const transactionDaoBuilder = require('../dao/transactionDao');

const helperBuilder = async fastify => {
  const { models } = fastify;
  const blockchainBlockDao = blockchainBlockDaoBuilder(models.blockchain_block);
  const configsDao = configsDaoBuilder({ configsModel: models.configs });
  const fileDao = fileDaoBuilder(models.file);
  const userDao = userDaoBuilder({ userModel: models.user });
  const activityDao = activityDaoBuilder(models.activity);
  const activityFileDao = activityFileDaoBuilder(models.activity_file);
  const userSocialEntrepreneurDao = userSocialEntrepreneurDaoBuilder(
    models.user_social_entrepreneur
  );
  const milestoneDao = milestoneDaoBuilder(models.milestone);
  const milestoneBudgetStatusDao = milestoneBudgetStatusDaoBuilder(
    models.milestone_budget_status
  );
  const projectDao = projectDaoBuilder({
    projectModel: models.project,
    userDao
  });
  const projectStatusDao = projectStatusDaoBuilder({
    projectStatusModel: models.project_status
  });
  const userProjectDao = userProjectDaoBuilder(models.user_project);

  const transactionDao = transactionDaoBuilder(fastify.models.transaction);

  exports.helper = {
    services: {
      projectService: undefined,
      questionnaireService: undefined
    },
    daos: {
      activityDao,
      activityFileDao,
      configsDao,
      fileDao,
      milestoneBudgetStatusDao,
      milestoneDao,
      projectDao,
      projectStatusDao,
      userDao,
      userProjectDao,
      userSocialEntrepreneurDao,
      blockchainBlockDao,
      transactionDao
    }
  };
};

exports.helperBuilder = helperBuilder;
