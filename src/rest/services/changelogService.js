/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../logger');
const COAError = require('../errors/COAError');
const errors = require('../errors/exporter/ErrorExporter');

module.exports = {
  changelogFilter(changelog, paramObj) {
    if (paramObj.milestoneId) {
      if (!changelog.milestone) {
        return false;
      }

      if (paramObj.parentMilestoneId) {
        if (
          !(
            changelog.milestone.id === paramObj.milestoneId ||
            changelog.milestone.id === paramObj.parentMilestoneId ||
            changelog.milestone.parent === paramObj.parentMilestoneId
          )
        ) {
          return false;
        }
      } else if (changelog.milestone.id !== paramObj.milestoneId) {
        return false;
      }
    }

    if (paramObj.activityId) {
      if (!changelog.activity) {
        return false;
      }

      if (paramObj.parentActivityId) {
        if (
          !(
            changelog.activity.id === paramObj.activityId ||
            changelog.activity.id === paramObj.parentActivityId ||
            changelog.activity.parent === paramObj.parentActivityId
          )
        ) {
          return false;
        }
      } else if (changelog.activity.id !== paramObj.activityId) {
        return false;
      }
    }

    if (paramObj.evidenceId) {
      if (!changelog.evidenceData) {
        return false;
      }

      if (paramObj.parentEvidenceId) {
        if (
          !(
            changelog.evidenceData.id === paramObj.evidenceId ||
            changelog.evidenceData.id === paramObj.parentEvidenceId ||
            changelog.evidenceData.parent === paramObj.parentEvidenceId
          )
        ) {
          return false;
        }
      } else if (changelog.evidenceData.id !== paramObj.evidenceId) {
        return false;
      }
    }

    return true;
  },

  async getChangelog(mainProjectId, childrenProjectIds, params) {
    logger.info('[ChangelogService] :: Entering getChangelog method');

    const where = { or: [{ project: mainProjectId }] };
    childrenProjectIds.forEach(id => {
      where.or.push({ project: id });
    });

    let changelogs = await this.changelogDao.getChangelogBy(where);

    changelogs = changelogs.filter(changelog =>
      this.changelogFilter(changelog, params)
    );

    logger.info('[ChangelogService] :: Fill activity auditors & users');
    changelogs.map(changelog => {
      changelog.user = changelog.userData
      changelog.project = changelog.projectData

      if (changelog.activity || changelog.activityData) {
        changelog.activity = changelog.activityData

        if (changelog.evidence || changelog.evidenceData) {
          changelog.evidence = changelog.evidenceData
        }
      }

      if (changelog.milestone || changelog.milestoneData) {
        changelog.milestone = changelog.milestoneData
      }
    })
    return changelogs
  },

  async createChangelog({
    project,
    revision,
    milestone,
    activity,
    evidence,
    user,
    transaction,
    description,
    action,
    extraData,
    status,
    milestoneData,
    evidenceData
  }) {
    const newChangelog = {
      project,
      revision,
      milestone,
      activity,
      evidence,
      user,
      transaction,
      description,
      action,
      extraData,
      status,
      milestoneData,
      evidenceData
    };
    try {
      logger.info(
        `[ChangelogService] :: Generating static data for the changelog`
      );
      await this.populateStaticData(newChangelog);

      logger.info(
        `[ChangelogService] :: About to insert changelog with ${JSON.stringify(
          newChangelog
        )}`
      );
      return this.changelogDao.createChangelog(newChangelog);
    } catch (error) {
      logger.error(
        '[ChangelogService] :: There was an error trying to insert changelog ',
        error
      );
    }
  },

  async deleteProjectChangelogs(projectId) {
    logger.info(
      '[ChangelogService] :: Entering deleteProjectChangelogs method'
    );
    return this.changelogDao.deleteProjectChangelogs(projectId);
  },

  async updateChangelog({ changelogId, toUpdate }) {
    logger.info('[ChangelogService] :: Entering updateChangelog method.');

    logger.info(
      `[ChangelogService] :: Updating changelog id ${changelogId}`,
      toUpdate
    );
    const updatedChangelog = await this.changelogDao.updateChangelog({
      id: changelogId,
      toUpdate
    });
    if (!updatedChangelog) {
      logger.error('[ChangelogService] :: Error updating changelog in DB');
      throw new COAError(errors.changelog.CantUpdateChangelog(changelogId));
    }
    return updatedChangelog.id;
  },

  async populateStaticData(newChangelog) {
    if (newChangelog.milestone && !newChangelog.milestoneData) {
      const { title } = await this.milestoneService.getMilestoneById(newChangelog.milestone);
      newChangelog.milestoneData = {
        id: Number(newChangelog.milestone),
        title
      }
    }

    if (newChangelog.activity) {
      const { title, auditor } = await this.activityService.getActivityById(newChangelog.activity);
      const { firstName, lastName } = await this.userService.getUserById(auditor);
      newChangelog.activityData = {
        id: Number(newChangelog.activity),
        title,
        auditor: {
          id: auditor,
          firstName,
          lastName
        }
      }

      if (newChangelog.evidence && !newChangelog.evidenceData) {
          const { title } = await this.activityService.getEvidenceById(newChangelog.evidence);
          newChangelog.evidenceData = {
            id: Number(newChangelog.evidence),
            title
          }
      }
    }

    const { projectName } = await this.projectService.getProjectById(newChangelog.project);
    newChangelog.projectData = {
      id: newChangelog.project,
      projectName
    };

    const { firstName, lastName, isAdmin } = await this.userService.getUserById(newChangelog.user);
    

    const role = isAdmin ? '' : (await this.userProjectService.getRolesOfUser({user: newChangelog.user, project: newChangelog.project}))[0].description
    newChangelog.userData = {
      id: newChangelog.user,
      firstName,
      lastName,
      isAdmin,
      role
    }
  },

  async findDeletedEvidenceInChangelog(evidence) {
    return this.changelogDao.findDeletedEvidenceInChangelog(evidence);
  }
};
