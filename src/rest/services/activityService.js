/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { createHash } = require('crypto');
const { BigNumber } = require('bignumber.js');
const { coa } = require('hardhat');
const fs = require('fs');
const { promisify } = require('util');
const { utils } = require('ethers');
const fileUtils = require('../util/files');
const {
  ACTIVITY_STATUS,
  ACTIVITY_STATUS_TRANSITION,
  projectSections,
  projectStatuses,
  rolesTypes,
  currencyTypes,
  evidenceTypes,
  evidenceStatus,
  validStatusToChange,
  lastEvidenceStatus,
  MILESTONE_STATUS,
  ACTION_TYPE,
  EDITABLE_ACTIVITY_STATUS,
  ACTIVITY_STEPS,
  ACTIVITY_TYPES,
  PROJECT_TYPES,
  TX_STATUS,
  pendingProjectStatus,
  pendingActivityStatus,
  pinStatus,
  MOVEMENT_TYPES
} = require('../util/constants');
const utilFiles = require('../util/files');

const filesUtil = require('../util/files');
const checkExistence = require('./helpers/checkExistence');
const validateRequiredParams = require('./helpers/validateRequiredParams');
const validateMtype = require('./helpers/validateMtype');
const validateFileSize = require('./helpers/validatePhotoSize');
const { completeStep, removeStep } = require('./helpers/dataCompleteUtil');
const COAError = require('../errors/COAError');
const errors = require('../errors/exporter/ErrorExporter');
const logger = require('../logger');
const mapFieldAndSum = require('./helpers/mapFieldAndSum');
const validateUserCanEditProject = require('./helpers/validateUserCanEditProject');
const { getMessageHash, generateNonceRandom } = require('./helpers/hardhatTaskHelpers');
const Joi = require('joi');
const configs = require('config');

const EVIDENCE_TYPE = 'evidence';
const INCOMING_MOVEMENT = 0;
const OUTGOING_MOVEMENT = 1;

module.exports = {
  readFile: promisify(fs.readFile),
  /**
   * Updates an existing activity.
   * Returns an object with the id of the updated activity
   *
   * @param { { activityId: number,
   *            title: string,
   *            description: string,
   *            acceptanceCriteria: string,
   *            budget: string,
   *            auditor: string }: object } activityData activity data
   * @returns { { activityId: number } } id of updated activity
   */
  async updateActivity({
    activityId,
    title,
    description,
    acceptanceCriteria,
    budget,
    auditor,
    user,
    type
  }) {
    logger.info('[ActivityService] :: Entering updateActivity method');

    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity'
    );
    logger.info(
      `[ActivityService] :: Found activity ${activity.id} of milestone ${activity.milestone
      }`
    );

    const project = await this.milestoneService.getProjectFromMilestone(
      activity.milestone
    );
    if (project.status === projectStatuses.PENDING_PUBLISHED) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    await validateUserCanEditProject({
      user,
      project,
      error: errors.task.UpdateWithInvalidProjectStatus
    });

    if (!EDITABLE_ACTIVITY_STATUS.includes(activity.status)) {
      throw new COAError(errors.task.CantUpdateTaskWithStatus(activity.status));
    }

    let toUpdate = {
      title,
      description,
      acceptanceCriteria,
      budget,
      auditor,
      type
    };

    if (activity.status === ACTIVITY_STATUS.IN_PROGRESS) {
      if (type) {
        logger.error(
          '[ActivityService] :: Type cannot be updated in an activity with status ',
          activity.status
        );
        throw new COAError(
          errors.task.TaskTypeIsNotUpdaptable(activity.status)
        );
      }
      toUpdate = {
        title,
        description,
        acceptanceCriteria,
        budget,
        auditor
      };
    }

    if (activity.status === ACTIVITY_STATUS.IN_REVIEW) {
      const isAuditorTheOnlyField = [
        title,
        description,
        acceptanceCriteria,
        budget,
        type
      ].every(field => field === undefined);

      if (!isAuditorTheOnlyField)
        throw new COAError(
          errors.task.OnlyAuditorIsUpdaptable(activity.status)
        );

      toUpdate = {
        auditor
      };
    }

    if (type) this.validateActivityType(type);

    if (auditor)
      await this.validateAuditorIsInProject({ project: project.id, auditor });

    logger.info(`[ActivityService] :: Updating task of id ${activityId}`);
    const updatedActivity = await this.activityDao.updateActivity(
      toUpdate,
      activityId
    );

    await this.updateProjectBudget({
      project,
      previousType: activity.type,
      newType: type,
      previousActivityBudget: activity.budget,
      newActivityBudget: budget
    });

    logger.info(
      `[ActivityService] :: Actvity of id ${updatedActivity.id} updated`
    );

    logger.info('[ProjectService] :: About to create changelog');
    await this.changelogService.createChangelog({
      project: project.parent || project.id,
      revision: project.revision,
      milestone: activity.milestone,
      activity: activityId,
      action: ACTION_TYPE.UPDATE_ACTIVITY,
      user: user.id
    });

    const activityUpdatedResponse = { activityId: updatedActivity.id };

    return activityUpdatedResponse;
  },
  /**
   * Deletes an existing activity.
   * Returns an object with the id of the deleted activity
   *
   * @param {number} activityId task identifier
   * @returns { {activityId: number} } id of deleted activity
   */
  async deleteActivity(activityId, user) {
    logger.info('[ActivityService] :: Entering deleteActivity method');
    validateRequiredParams({
      method: 'deleteActivity',
      params: { activityId }
    });

    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity'
    );
    logger.info(
      `[ActivityService] :: Found task ${activity.id} of milestone ${activity.milestone
      }`
    );
    if (activity.status !== ACTIVITY_STATUS.NEW) {
      throw new COAError(errors.task.CantDeleteTaskWithStatus(activity.status));
    }
    const project = await this.milestoneService.getProjectFromMilestone(
      activity.milestone
    );
    if (!project) {
      logger.info(
        `[ActivityService] :: No project found for milestone ${activity.milestone
        }`
      );
      throw new COAError(errors.task.ProjectNotFound(activityId));
    }
    if (project.status === projectStatuses.PENDING_PUBLISHED) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    await validateUserCanEditProject({
      user,
      project,
      error: errors.task.DeleteWithInvalidProjectStatus
    });

    logger.info(`[ActivityService] :: Deleting activity with id ${activityId}`);
    const deletedActivity = await this.activityDao.deleteActivity(activityId);
    logger.info(
      `[ActivityService] :: Activity with id ${deletedActivity.id} deleted`
    );
    if (!deletedActivity) {
      logger.error(
        '[ActivityService] :: There was an error trying to delete activity'
      );
      throw new COAError(errors.milestone.CantDeleteActivity);
    }
    const milestones = await this.milestoneService.getAllMilestonesByProject(
      project.id
    );
    const milestoneHasTasksLeft = milestones.some(
      milestone => milestone.tasks.length > 0
    );

    if (activity.type === ACTIVITY_TYPES.FUNDING) {
      const newGoalAmount = BigNumber(project.goalAmount)
        .minus(activity.budget)
        .toString();

      await this.projectService.updateProject(project.id, {
        goalAmount: newGoalAmount
      });
    }

    if (!milestoneHasTasksLeft) {
      const dataComplete = removeStep({
        dataComplete: project.dataComplete,
        step: projectSections.MILESTONES
      });

      await this.projectService.updateProject(project.id, { dataComplete });
    }

    logger.info('[ProjectService] :: About to create changelog');
    await this.changelogService.createChangelog({
      project: project.parent || project.id,
      revision: project.revision,
      action: ACTION_TYPE.REMOVE_ACTIVITY,
      user: user.id,
      extraData: { activity }
    });

    return { taskId: deletedActivity.id };
  },
  /**
   * Creates an task for an existing Milestone.
   * Returns an object with the id of the new task
   *
   * @param { { milestoneId: number,
   *            title: string,
   *            description: string,
   *            acceptanceCriteria: string,
   *            budget: string,
   *            auditor: string }: object } activityData activity data
   * @returns { {activityId: number} } id of created task
   */
  async createActivity({
    milestoneId,
    title,
    description,
    acceptanceCriteria,
    budget,
    auditor,
    user,
    type
  }) {
    logger.info('[ActivityService] :: Entering createActivity method');
    validateRequiredParams({
      method: 'createActivity',
      params: {
        milestoneId,
        title,
        description,
        acceptanceCriteria,
        budget,
        auditor,
        type
      }
    });

    this.validateActivityType(type);

    logger.info(
      `[ActivityService] :: checking if milestone with id ${milestoneId} exists`
    );
    const milestone = await checkExistence(
      this.milestoneDao,
      milestoneId,
      'milestone'
    );

    logger.info(
      `[ActivityService] :: Getting project of milestone ${milestoneId}`
    );
    const project = await this.milestoneService.getProjectFromMilestone(
      milestoneId
    );

    this.validateActivityTypeByProject({ project, activityType: type });

    await validateUserCanEditProject({
      project,
      user,
      error: errors.task.CreateWithInvalidProjectStatus
    });

    if (milestone.status === MILESTONE_STATUS.APPROVED) {
      logger.info(
        `[ActivityService] :: Can't add activities to a milestone with status ${milestone.status
        }`
      );
      throw new COAError(errors.milestone.MilestoneIsApproved);
    }

    await this.validateAuditorIsInProject({ project: project.id, auditor });

    logger.info(
      `[ActivityService] :: Creating new activity in project ${project.id
      }, milestone ${milestoneId}`
    );
    const createdActivity = await this.activityDao.saveActivity(
      {
        title,
        description,
        acceptanceCriteria,
        budget,
        auditor,
        type
      },
      milestoneId
    );
    logger.info(
      `[ActivityService] :: New task with id ${createdActivity.id} created`
    );

    if (type === ACTIVITY_TYPES.FUNDING) {
      const newGoalAmount = BigNumber(project.goalAmount).plus(budget);
      logger.info(
        `[ActivityService] :: Updating project ${project.id
        } goalAmount to ${newGoalAmount}`
      );

      await this.projectService.updateProject(project.id, {
        goalAmount: newGoalAmount.toString()
      });
    }

    await this.projectService.updateProject(project.id, {
      dataComplete: completeStep({
        dataComplete: project.dataComplete,
        step: projectSections.MILESTONES
      })
    });

    logger.info('[ProjectService] :: About to create changelog');
    await this.changelogService.createChangelog({
      project: project.parent || project.id,
      revision: project.revision,
      milestone: milestoneId,
      activity: createdActivity.id,
      action: ACTION_TYPE.ADD_ACTIVITY,
      user: user.id
    });

    return { activityId: createdActivity.id };
  },
  async validateAuditorIsInProject({ project, auditor }) {
    logger.info(
      '[ActivityService] :: Entering validateAuditorIsInProject method'
    );

    const auditorRole = await this.roleDao.getRoleByDescription(
      rolesTypes.AUDITOR
    );
    if (!auditorRole) throw COAError(errors.common.ErrorGetting('role'));

    const result = await this.userProjectDao.findUserProject({
      user: auditor,
      project,
      role: auditorRole.id
    });

    if (!result)
      throw new COAError(
        errors.task.UserIsNotAuditorInProject(auditor, project)
      );
  },

  /**
   * Sends the signed transaction to the blockchain
   * and saves the evidence in the database
   *
   * @param {Number} taskId
   * @param {Number} userId
   * @param {File} file
   * @param {String} description
   * @param {Boolean} approved
   * @param {Transaction} signedTransaction
   */
  async addEvidence({
    activityId,
    userId,
    title,
    description,
    type,
    movementId,
    files
  }) {
    logger.info('[ActivityService] :: Entering addEvidence method');

    // Format validation
    const movementValidationSchema = Joi.object({
      activityId: Joi.number().positive().required(),
      userId: Joi.string().required(),
      title: Joi.string().required(),
      description: Joi.string().required(),
      type: Joi.string().required(),
      movementId: Joi.number().positive()
    });
    const { error } = movementValidationSchema.validate({
      activityId,
      userId,
      title,
      description,
      type,
      movementId
    });
    if (error) throw new Error(`[ActivityService] :: There was an error: ${error}`);

    const method = 'addEvidence';

    let additionalInfo = {
      amount: '0'
    };

    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity'
    );

    const invalidStatusToAddEvidence = [
      ACTIVITY_STATUS.APPROVED,
      ACTIVITY_STATUS.IN_REVIEW,
      ACTIVITY_STATUS.PENDING_TO_REVIEW,
      ACTIVITY_STATUS.PENDING_TO_APPROVE,
      ACTIVITY_STATUS.PENDING_TO_REJECT
    ];

    if (invalidStatusToAddEvidence.includes(activity.status)) {
      logger.info(
        `[ActivityService] :: Can't add evidences to an activity with status ${invalidStatusToAddEvidence}`
      );
      throw new COAError(
        errors.task.ActivityIsApprovedOrInProgress(activity.status)
      );
    }

    const evidenceType = type.toLowerCase();

    this.validateEvidenceType(evidenceType);

    const milestone = await this.getMilestoneFromActivityId(activityId);

    const project = await this.projectService.getProjectById(milestone.project);

    if (project.status === projectStatuses.PENDING_PUBLISHED) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    const currencyType = project.currencyType.toLowerCase();

    if (movementId) {
      // validate movement unicity
      const evidencesLinkedToMovement = await this.activityEvidenceDao.getEvidencesByMovementId(
        movementId,
        {
          status: { '!=': evidenceStatus.REJECTED }
        }
      );
      if (evidencesLinkedToMovement.length > 0) 
        throw new Error(`[ActivityService] :: Movement is already linked to other evidences`);

      // validate movement type against activity type
      const movement = await checkExistence(
        this.movementDao,
        movementId,
        'movement'
      );
      if (
        (activity.type === ACTIVITY_TYPES.FUNDING && movement.type === MOVEMENT_TYPES[OUTGOING_MOVEMENT]) ||
        (activity.type != ACTIVITY_TYPES.FUNDING && movement.type === MOVEMENT_TYPES[INCOMING_MOVEMENT])
      ) {
        throw new Error(`[ActivityService] :: ${movement.type} movement is incompatible with ${activity.type} activity`);
      }

      // validate movement belongs to correct projectId
      const projectAccounts = await this.accountService.getAccountsByProject(project.id);
      if (!projectAccounts.map(account => account.id).includes(movement.accountId)) {
        throw new Error(`[ActivityService] :: Movement does not belong to project`);
      }

      additionalInfo = {
        amount: movement.amount,
        destinationAccount: movement.type === MOVEMENT_TYPES[INCOMING_MOVEMENT] ? movement.accountId : movement.otherAccount,
        transferTxHash: movement.externalId
      }
    }
    this.validateStatusToUploadEvidence({ status: project.status });

    const isEvidenceImpact = evidenceType === evidenceTypes.IMPACT;
    if (isEvidenceImpact || currencyType === currencyTypes.FIAT) {
      validateRequiredParams({
        method,
        params: {
          files
        }
      });

      this.validateFiles(files);
    }

    const roleDescription =
      activity.type === ACTIVITY_TYPES.FUNDING
        ? rolesTypes.INVESTOR
        : rolesTypes.BENEFICIARY;

    await this.validateUserCanAddEvidence({
      userId,
      activityId: activity.id,
      projectId: project.id,
      roleDescription
    });

    try {
      let savedFiles = [];
      if (
        files &&
        !(
          evidenceType === evidenceTypes.TRANSFER &&
          currencyType === currencyTypes.CRYPTO
        )
      ) {
        savedFiles = await Promise.all(
          Object.values(files).map(async file => {
            file.sha256 = createHash('sha256').update(file.data).digest('hex');
            file.projectId = milestone.project;
            const path = await fileUtils.saveFile(EVIDENCE_TYPE, file);
            return this.fileService.saveFile({
              path,
              name: file.name,
              size: file.size,
              hash: file.sha256
            });
          })
        );
      }

      if (movementId) {
        additionalInfo = {
          ...additionalInfo,
          movement: movementId
        };
      }

      const evidence = {
        title,
        description,
        activity: activityId,
        type: evidenceType,
        user: userId,
        roleDescription,
        ...additionalInfo
      };

      logger.info('[ActivityService] :: Saving evidence in database', evidence);

      const activityEvidences = await this.activityEvidenceDao.getEvidencesByTaskId(
        activity.id
      );

      if (
        activityEvidences.length === 0 ||
        activity.status === ACTIVITY_STATUS.REJECTED
      ) {
        logger.info(
          '[ActivityService] :: Setting activity status to ',
          ACTIVITY_STATUS.IN_PROGRESS
        );
        const milestoneActivities = await this.activityDao.getTasksByMilestone(
          milestone.id
        );
        if (
          milestoneActivities.every(
            _activity => _activity.status === ACTIVITY_STATUS.NEW
          )
        ) {
          logger.info(
            `[ActivityService] :: About to update milestone status to ${MILESTONE_STATUS.IN_PROGRESS
            }`
          );
          await this.milestoneDao.updateMilestone(
            { status: MILESTONE_STATUS.IN_PROGRESS },
            milestone.id
          );
        }
        await this.activityDao.updateActivity(
          { status: ACTIVITY_STATUS.IN_PROGRESS },
          activity.id
        );
      }
      const evidenceCreated = await this.activityEvidenceDao.saveActivityEvidence(
        evidence
      );

      await Promise.all(
        savedFiles.map(async file =>
          this.evidenceFileService.saveEvidenceFile({
            evidence: evidenceCreated.id,
            file: file.id
          })
        )
      );

      await this.projectService.updateProject(project.id, {
        status: projectStatuses.IN_PROGRESS
      });

      logger.info('[ProjectService] :: About to create changelog');
      await this.changelogService.createChangelog({
        project: project.parent || project.id,
        revision: project.revision,
        milestone: milestone.id,
        activity: activity.id,
        evidence: evidenceCreated.id,
        action: ACTION_TYPE.ADD_EVIDENCE,
        user: userId
      });

      const response = { evidenceId: evidenceCreated.id };

      return response;
    } catch (error) {
      logger.info(
        `[ActivityService] :: Occurs an error trying to save evidence :: ${error}`
      );
      throw new COAError(error);
    }
  },

  validateEvidenceType(type) {
    if (!Object.values(evidenceTypes).includes(type)) {
      logger.error('[ActivityService] :: Invalid evidence type');
      throw new COAError(errors.task.InvalidEvidenceType(type));
    }
  },

  validateFiles(files) {
    Object.values(files).forEach(file => {
      validateMtype(EVIDENCE_TYPE, file);
      validateFileSize(file);
    });
  },

  validateStatusToUploadEvidence({ status }) {
    if (
      status !== projectStatuses.PUBLISHED &&
      status !== projectStatuses.IN_PROGRESS
    ) {
      logger.error(
        `[ActivityService] :: Can't upload evidence when project is in ${status} status`
      );
      throw new COAError(errors.project.InvalidStatusForEvidenceUpload(status));
    }
  },

  async getTransactionInfo({ currency, address, txHash }) {
    logger.info(
      `[ActivityService] :: Entering getTransactionInfo method with currency ${currency}, address ${address} and txHash ${txHash}`
    );
    const isNativeToken = await this.blockchainService.isNativeToken(currency);
    const transaction = isNativeToken
      ? await this.blockchainService.getTransaction({
        currency,
        address,
        txHash
      })
      : (await this.blockchainService.getTransactions({
        currency,
        address,
        txHash
      })).transactions.find(_transaction => _transaction.txHash === txHash);

    logger.info(
      `[ActivityService] :: Transaction info ${JSON.stringify(transaction)}`
    );

    this.validateTransaction({ address: address.toLowerCase(), transaction });

    return {
      amount: transaction.value,
      destinationAccount: transaction.to
    };
  },

  validateTransaction({ address, transaction }) {
    if (
      !transaction ||
      (transaction.to.toLowerCase() !== address &&
        transaction.from.toLowerCase() !== address)
    ) {
      throw new COAError(errors.task.TransactionIsNotRelatedToProjectAddress);
    }
  },

  /**
   * Returns the milestone that the activity belongs to or `undefined`
   *
   * Throws an error if the activity does not exist
   *
   * @param {number} id
   * @returns milestone
   */
  async getMilestoneFromActivityId(activityId) {
    logger.info(
      '[ActivityService] :: Entering getMilestoneFromActivityId method'
    );
    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity'
    );
    logger.info(
      `[ActivityService] :: Found activity ${activity.id} of milestone ${activity.milestone
      }`
    );

    const milestone = await this.milestoneService.getMilestoneById(
      activity.milestone
    );
    if (!milestone) {
      logger.info(
        `[ActivityService] :: No milestone found for activity ${activityId}`
      );
      throw new COAError(errors.task.MilestoneNotFound(activityId));
    }

    return milestone;
  },

  async updateEvidenceStatus({ evidenceId, newStatus, userId, reason }) {
    logger.info('[ActivityService] :: Entering updateEvidenceStatus method');
    const evidence = await checkExistence(
      this.activityEvidenceDao,
      evidenceId,
      'evidence'
    );
    if (!validStatusToChange.includes(newStatus)) {
      logger.info(`[ActivityService] :: given status is invalid ${newStatus}`);
      throw new COAError(errors.task.EvidenceStatusNotValid(newStatus));
    }
    if (evidence.status !== evidenceStatus.NEW) {
      logger.info(
        `[ActivityService] :: Evidence with status ${evidence.status
        } can not be updated`
      );
      throw new COAError(
        errors.task.EvidenceStatusCannotChange(evidence.status)
      );
    }
    logger.info(
      '[ActivityService] :: About to get activity by id ',
      evidence.activity
    );
    const activity = await this.activityDao.getActivityByIdWithMilestone(
      evidence.activity.id
    );
    const evidenceProjectId = activity.milestone.project;
    logger.info(
      `[ActivityService] :: About to get role by description ${rolesTypes.AUDITOR
      }`
    );

    const project = await this.projectService.getProjectById(evidenceProjectId);
    if (project.status === projectStatuses.PENDING_PUBLISHED) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    const auditorRole = await this.roleDao.getRoleByDescription(
      rolesTypes.AUDITOR
    );
    if (!auditorRole) {
      logger.error(
        '[ActivityService] :: there was an error getting role ',
        rolesTypes.AUDITOR
      );
      throw new COAError(errors.common.ErrorGetting('role'));
    }
    logger.info('[ActivityService] :: About to get user project ', {
      role: auditorRole.id,
      user: userId,
      project: evidenceProjectId
    });
    const userProject = await this.userProjectDao.findUserProject({
      role: auditorRole.id,
      user: userId,
      project: evidenceProjectId
    });
    if (!userProject) {
      logger.info(
        '[ActivityService] :: User does not have an auditor role for this activity'
      );
      throw new COAError(errors.task.UserCantUpdateEvidence);
    }
    if (activity.auditor !== userId) {
      logger.info(
        '[ActivityService] :: User is not an auditor of this activity'
      );
      throw new COAError(errors.task.UserIsNotActivityAuditor);
    }
    let toUpdate = { status: newStatus, auditor: userId, auditedAt: new Date().toISOString() };
    if (newStatus === ACTIVITY_STATUS.REJECTED && reason)
      toUpdate = { ...toUpdate, reason };
    logger.info(
      `[ActivityService] :: About to update evidence with ${JSON.stringify(
        toUpdate
      )}`
    );
    const updated = await this.activityEvidenceDao.updateTaskEvidence(
      evidenceId,
      toUpdate
    );
    if (!updated) {
      logger.info('[ActivityService] :: Task evidence could not be updated');
      throw new COAError(errors.task.EvidenceUpdateError);
    }
    if (newStatus === evidenceStatus.APPROVED) {
      const newActivityCurrent = BigNumber(activity.current)
        .plus(BigNumber(evidence.amount))
        .toString();
      logger.info(
        '[ActivityService] :: Updating task with current value ',
        newActivityCurrent
      );
      await this.activityDao.updateActivity(
        { current: newActivityCurrent },
        activity.id
      );
    }

    const action =
      newStatus === evidenceStatus.APPROVED
        ? { action: ACTION_TYPE.APPROVE_EVIDENCE }
        : { action: ACTION_TYPE.REJECT_EVIDENCE, extraData: { reason } };

    logger.info('[ActivityService] :: About to insert changelog');
    await this.changelogService.createChangelog({
      project: project.parent ? project.parent : project.id,
      revision: project.revision,
      milestone: evidence.activity.milestone,
      activity: evidence.activity.id,
      evidence: evidenceId,
      user: userId,
      ...action
    });

    const toReturn = { success: !!updated };
    return toReturn;
  },

  async createActivityFile({ taskId, userId }) {
    logger.info(
      '[ActivityService] :: About to create activity file for activity with id ',
      taskId
    );
    const activity = await checkExistence(
      this.activityDao,
      taskId,
      'activity',
      this.activityDao.getActivityByIdWithMilestone(taskId)
    );

    const project = await this.projectDao.findById(activity.milestone.project);
    if (project.status === projectStatuses.PENDING_PUBLISHED) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    await this.userProjectService.getUserProjectFromRoleDescription({
      projectId: activity.milestone.project,
      roleDescriptions: [rolesTypes.BENEFICIARY],
      userId
    });
    if (activity.status !== ACTIVITY_STATUS.NEW) {
      logger.error(
        '[ActivityService] :: Current activity status is not ',
        ACTIVITY_STATUS.NEW
      );
      throw new COAError(errors.task.InvalidRequiredStatus);
    }
    logger.info('[ActivityService] :: About to obtain evidences');
    const evidences = await this.activityEvidenceDao.getEvidencesByTaskId(
      taskId
    );
    logger.info('[ActivityService] :: About to create activity file');
    const activityFile = { ...activity, evidences };
    await filesUtil.saveActivityFile({
      taskId,
      data: { ...activityFile }
    });
  },

  async updateActivityStatus({ activityId, user, status, reason }) {
    logger.info('[ActivityService] :: About to update activity status');

    const userData = await this.userDao.findById(user.id);
    const currentPinStatus = userData.pinStatus;
    if (currentPinStatus === pinStatus.APPROVED || currentPinStatus === pinStatus.INACTIVE) {
      logger.error(`[ProjectService] Current pin status doesn't match expected status`);
      throw new COAError(errors.user.InvalidPinStatus);
    }

    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity',
      this.activityDao.getActivityByIdWithMilestone(activityId)
    );
    this.validateActivityStep({
      activity,
      step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
    });

    const project = await this.projectDao.findById(activity.milestone.project);
    if (pendingProjectStatus.includes(project.status)) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    if (!Object.values(ACTIVITY_STATUS).includes(status)) {
      logger.error('[ActivityService] :: Given status is invalid ', status);
      throw new COAError(errors.task.InvalidStatus(status));
    }
    if (!ACTIVITY_STATUS_TRANSITION[activity.status].includes(status)) {
      logger.error('[ActivityService] :: Status transition is not valid');
      throw new COAError(errors.task.InvalidStatusTransition);
    }
    if (status === ACTIVITY_STATUS.IN_REVIEW) {
      await this.userProjectService.getUserProjectFromRoleDescription({
        projectId: activity.milestone.project,
        roleDescriptions: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR],
        userId: user.id
      });
    }
    if ([ACTIVITY_STATUS.APPROVED, ACTIVITY_STATUS.REJECTED].includes(status)) {
      await this.userProjectService.getUserProjectFromRoleDescription({
        projectId: activity.milestone.project,
        roleDescriptions: [rolesTypes.AUDITOR],
        userId: user.id
      });
      const evidences = await this.activityEvidenceDao.getEvidencesByTaskId(
        activity.id
      );
      if (
        !evidences.length ||
        !evidences.every(evidence =>
          lastEvidenceStatus.includes(evidence.status)
        )
      ) {
        logger.error(
          '[ActivityService] :: Not all of the evidences are rejected or approved'
        );
        throw new COAError(errors.task.TaskNotReady);
      }
    }

    let toUpdate = {
      status: this.mapToPendingStatus(status),
      step: ACTIVITY_STEPS.WAITING_SIGNATURE_AUTHORIZATION
    };
    if (status === ACTIVITY_STATUS.REJECTED && reason)
      toUpdate = { ...toUpdate, reason };

    if (toUpdate.status === ACTIVITY_STATUS.PENDING_TO_REVIEW) {
      toUpdate.proposer = user.id;
      toUpdate.revisionDate = new Date().toISOString();
    }
  
    if (toUpdate.status === ACTIVITY_STATUS.PENDING_TO_APPROVE || toUpdate.status === ACTIVITY_STATUS.PENDING_TO_REJECT ) {
      toUpdate.approvalDate = new Date().toISOString();
      toUpdate.auditorId = user.id;
    }

    logger.info(
      `[ActivityService] :: About to update activity with ${JSON.stringify(
        toUpdate
      )}`
    );
    const updated = await this.activityDao.updateActivity(
      toUpdate,
      activity.id
    );

    if (!updated) {
      logger.error(
        '[ActivityService] :: Activity couldnt be updated successfully'
      );
      throw new COAError(errors.task.ActivityStatusCantBeUpdated);
    }

    logger.info(
      '[ActivityService] :: Getting project with id ',
      activity.milestone.project
    );
    const projectId = project.parent || project.id;

    logger.info('[ActivityService] :: About to create activity file');
    utilFiles.saveActivityFile({ data: updated, taskId: activity.id });


    // NOTE: The notarization should be generated after update status in DB.
    const projectData = await this.projectService.mapProjectToPublish(project);

    let proofHash
    try {
      const { metadataHash } = await this.projectService.uploadProjectMetadataToIPFS({ project: projectData }, user.id, activityId)
      proofHash = metadataHash;
    } catch (error) {
      const statusRollback = status === ACTIVITY_STATUS.IN_REVIEW ? ACTIVITY_STATUS.IN_PROGRESS : ACTIVITY_STATUS.IN_REVIEW;
      let toUpdate = {
        status: statusRollback,
        step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
      };

      const updated = await this.activityDao.updateActivity(
        toUpdate,
        activity.id
      );

      if (!updated) {
        logger.error(
          '[ActivityService] :: Activity couldnt be updated successfully'
        );
        throw new COAError(errors.task.ActivityStatusCantBeUpdated);
      }

      throw error;
    }

    const claimHash = utils.keccak256(
      utils.toUtf8Bytes(
        JSON.stringify({
          projectId,
          activityId: activity.parent || activity.id,
          claimCounter: activity.claimCounter
        })
      )
    );

    let toSign;
    const userAddress = (await this.userService.getUserById(user.id)).address;
    const nonce = generateNonceRandom();
    if (status === ACTIVITY_STATUS.IN_REVIEW) {

      const proposerEmail = user.email;

      toSign = {
        projectId,
        claimHash,
        proofHash,
        activityId,
        proposerEmail,
        proposerAddress: userAddress,
        nonce,
        messageHash: getMessageHash(
          ['uint256', 'string', 'bytes32', 'string', 'uint256', 'string'],
          [
            nonce,
            projectId,
            claimHash,
            proofHash,
            activityId,
            proposerEmail
          ]
        )
      };
    } else {
      const proposerAddress = (await this.userService.getUserById(
        activity.proposer
      )).address;

      const auditResultBool = status === ACTIVITY_STATUS.APPROVED;
      const auditorEmail = user.email;
      const proposalProofHash = activity.activityHash;
      toSign = {
        projectId,
        claimHash,
        proposalProofHash,
        auditProofHash: proofHash,
        proposerAddress,
        auditorEmail,
        auditorAddress: userAddress,
        nonce,
        messageHash: getMessageHash(
          [
            'uint256',
            'string',
            'bytes32',
            'string',
            'address',
            'string',
            'string',
            'bool'
          ],
          [
            nonce,
            projectId,
            claimHash,
            proposalProofHash,
            proposerAddress,
            auditorEmail,
            proofHash,
            auditResultBool
          ]
        )
      };
    }

    logger.info('[ActivityService] :: About to information to sign', toSign);

    await this.activityDao.updateActivity(
      { activityHash: proofHash, toSign },
      activity.id
    );

    const toReturn = {
      success: !!updated,
      toSign: toSign.messageHash
    };
    return toReturn;
  },

  mapToPendingStatus(status) {
    const map = {
      [ACTIVITY_STATUS.IN_REVIEW]: ACTIVITY_STATUS.PENDING_TO_REVIEW,
      [ACTIVITY_STATUS.APPROVED]: ACTIVITY_STATUS.PENDING_TO_APPROVE,
      [ACTIVITY_STATUS.REJECTED]: ACTIVITY_STATUS.PENDING_TO_REJECT
    };
    return map[status];
  },

  async getActivityEvidences({ activityId, user }) {
    logger.info('[ActivityService] :: Entering getActivityEvidences method');
    validateRequiredParams({
      method: 'getActivityEvidences',
      params: { activityId }
    });

    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity'
    );

    logger.info(
      `[ActivityService] :: Getting milestone ${activity.milestone
      } of activity`,
      activityId
    );

    const milestone = await this.milestoneService.getMilestoneById(
      activity.milestone
    );

    logger.info(
      '[ActivityService] :: Getting evidences for activity',
      activityId
    );

    const evidences = await this.activityEvidenceDao.getEvidencesByActivityId(
      activityId
    );
    logger.info(
      `[ActivityService] :: Found ${evidences.length
      } evidences for activity ${activityId}`
    );

    logger.info('[ActivityService] :: Getting files of evidences');
    const evidencesWithFiles = await Promise.all(
      evidences.map(async evidence => ({
        ...evidence,
        files: await Promise.all(
          evidence.files.map(evidenceFile =>
            this.fileService.getFileById(evidenceFile.file)
          )
        )
      }))
    );

    const activityEvidences = {
      milestone,
      activity: activity.toSign
        ? { ...activity, toSign: activity.toSign.messageHash }
        : activity,
      evidences: evidencesWithFiles
    };
    return user
      ? activityEvidences
      : {
        ...activityEvidences,
        evidences: activityEvidences.evidences.filter(
          evidence => evidence.status === evidenceStatus.APPROVED
        )
      };
  },
  async getEvidenceById(evidenceId) {
    logger.info('[ActivityService] :: Entering getEvidenceById method');
    const evidence = await checkExistence(
      this.activityEvidenceDao,
      evidenceId,
      'evidence'
    );
    return evidence;
  },

  async getEvidence(evidenceId) {
    logger.info('[ActivityService] :: Entering getEvidence method');
    let evidence;
    try {
      evidence = await this.getEvidenceById(evidenceId);
    } catch (error) {
      evidence = (await this.changelogService.findDeletedEvidenceInChangelog(evidenceId))[0];

      if (!evidence) {
        throw error
      }

      // If the evidence was deleted and it was found in changelog
      return {
        id: evidenceId,
        evidenceTitle: evidence.evidenceData.title,
        activityTitle: evidence.activityData.title,
        milestoneTitle: evidence.milestoneData.title,
        deleted: true
      }
    }

    const userWallet = await this.userWalletDao.findActiveByUserId(
      evidence.user.id
    );

    const milestone = await this.milestoneService.getMilestoneById(
      evidence.activity.milestone
    );

    const project = await this.projectService.getProjectById(milestone.project);

    const files = await Promise.all(
      evidence.files.map(evidenceFile =>
        this.fileService.getFileById(evidenceFile.file)
      )
    );

    const activityAuditor = await this.userService.getUserById(
      evidence.activity.auditor
    );

    let auditor;
    if (evidence.auditor) {
      const userWalletAuditor = await this.userWalletDao.findActiveByUserId(
        evidence.auditor.id
      );
      auditor = {
        id: evidence.auditor.id,
        firstName: evidence.auditor.firstName,
        lastName: evidence.auditor.lastName,
        address: userWalletAuditor ? userWalletAuditor.address : undefined
      };
    }

    const userWalletActivityAuditor = await this.userWalletDao.findActiveByUserId(
      activityAuditor.id
    );

    return {
      ...evidence,
      user: {
        ...evidence.user,
        address: userWallet ? userWallet.address : undefined
      },
      currency: project.currency,
      activity: {
        status: evidence.activity.status,
        id: evidence.activity.id,
        title: evidence.activity.title,
        auditor: {
          id: activityAuditor.id,
          firstName: activityAuditor.firstName,
          lastName: activityAuditor.lastName,
          address: userWalletActivityAuditor
            ? userWalletActivityAuditor.address
            : undefined
        },
        type: evidence.activity.type
      },
      milestone: {
        id: milestone.id,
        title: milestone.title
      },
      auditor,
      files
    };
  },

  getActionFromActivityStatus(status) {
    switch (status) {
      case ACTIVITY_STATUS.PENDING_TO_APPROVE:
        return ACTION_TYPE.APPROVE_ACTIVITY;
      case ACTIVITY_STATUS.PENDING_TO_REJECT:
        return ACTION_TYPE.REJECT_ACTIVITY;
      default:
        return ACTION_TYPE.SEND_ACTIVITY_TO_REVIEW;
    }
  },

  async sendActivityTransaction({ user, activityId, authorizationSignature }) {
    let transaction;
    logger.info('[ActivityService] :: Entering sendActivityTransaction method');

    const userData = await this.userDao.findById(user.id);
    const currentPinStatus = userData.pinStatus;
    if (currentPinStatus === pinStatus.APPROVED || currentPinStatus === pinStatus.INACTIVE) {
      logger.error(`[ProjectService] Current pin status doesn't match expected status`);
      throw new COAError(errors.user.InvalidPinStatus);
    }

    const activity = await checkExistence(
      this.activityDao,
      activityId,
      'activity',
      this.activityDao.getActivityByIdWithMilestone(activityId)
    );

    this.validateActivityStep({
      activity,
      step: ACTIVITY_STEPS.WAITING_SIGNATURE_AUTHORIZATION
    });

    const activityStatus = activity.status;

    const project = await this.projectDao.findById(activity.milestone.project);
    if (project.status === projectStatuses.PENDING_PUBLISHED) {
      throw new COAError(errors.project.ProjectInPendingStatus);
    }

    const { messageHash, ...paramsWithoutSignature } = activity.toSign;

    const activityIsApproved =
      activityStatus === ACTIVITY_STATUS.PENDING_TO_APPROVE;

    if (activityStatus === ACTIVITY_STATUS.PENDING_TO_REVIEW) {
      this.validateUsersAreEqualsOrThrowError({
        firstUserId: user.id,
        secondUserId: activity.proposer,
        error: errors.task.OnlyProposerCanSendProposeClaimTransaction
      });

      const proposeClaimParams = {
        ...paramsWithoutSignature,
        authorizationSignature
      };
      logger.info(
        '[ActivityService] :: Call proposeClaim method with following params',
        proposeClaimParams
      );
      try {
        transaction = await coa.proposeClaim(proposeClaimParams);
        
      } catch (error) {
        await activityDao.updateActivity(
          {
            status: ACTIVITY_STATUS.IN_PROGRESS,
            step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
          },
          activityId
        );
        logger.error(
          `[ActivityService] :: Error: ${error.message}`
        );
        throw new COAError(error.message)
      }
    } else if (
      activityStatus === ACTIVITY_STATUS.PENDING_TO_APPROVE ||
      activityStatus === ACTIVITY_STATUS.PENDING_TO_REJECT
    ) {
      this.validateUsersAreEqualsOrThrowError({
        firstUserId: user.id,
        secondUserId: activity.auditor,
        error: errors.task.OnlyAuditorCanSendubmitClaimAuditResultTransaction
      });

      const submitClaimAuditResultParams = {
        ...paramsWithoutSignature,
        isApproved: activityIsApproved,
        authorizationSignature
      };

      logger.info(
        `[ActivityService] :: Call ${activityIsApproved ? 'submitClaimApproval' : 'submitClaimRejection'
        } method with the following params`,
        submitClaimAuditResultParams
      );

      try {
        transaction = await coa.submitClaimAudit(submitClaimAuditResultParams);
      } catch (error) {
        await activityDao.updateActivity(
          {
            status: ACTIVITY_STATUS.IN_REVIEW,
            step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
          },
          activityId
        );
        logger.error(
          `[ActivityService] :: Error: ${error.message}`
        );
        throw new COAError(error.message)
      }
    } else {
      throw new COAError(errors.task.InvalidStatusToSendTransaction);
    }

    logger.info(
      '[ActivityService] :: Infomration about the transaction sent',
      transaction
    );

    const step = ACTIVITY_STEPS.SIGNATURE_AUTHORIZATION_SENT;
    logger.info('[ActivityService] :: Update activity step to', step);
    await this.activityDao.updateActivity(
      {
        step
      },
      activity.id
    );

    const action = this.getActionFromActivityStatus(activityStatus);

    logger.info('[ActivityService] :: About to insert changelog');
    const changelog = await this.changelogService.createChangelog({
      project: project.parent || project.id,
      revision: project.revision,
      milestone: activity.milestone.id,
      activity: activityId,
      user: user.id,
      action,
      transaction: transaction.hash,
      status: TX_STATUS.PENDING
    });

    logger.info('[ProjectService] :: About to create tx');
    await this.txService.createTransaction({
      hash: transaction.hash,
      action,
      status: TX_STATUS.PENDING,
      data: { activityId, changelogId: changelog.id, userEmail: user.email }
    });

    const toReturn = { txHash: transaction.hash };

    return toReturn;
  },

  validateUsersAreEqualsOrThrowError({ firstUserId, secondUserId, error }) {
    if (firstUserId !== secondUserId) {
      throw new COAError(error);
    }
  },

  validateActivityStep({ activity, step }) {
    logger.info('[ActivityService] :: Entering validateActivityStep method');
    logger.info('[ActivityService] :: Activity step is:', activity.step);
    if (activity.step !== step) {
      throw new COAError(errors.common.InvalidStep);
    }
  },

  validateActivityType(type) {
    logger.info('[ActivityService] :: Validate activity type', type);
    if (!Object.values(ACTIVITY_TYPES).includes(type)) {
      throw new COAError(errors.task.InvalidActivityType);
    }
  },

  async updateProjectBudget({
    project,
    previousType,
    previousActivityBudget,
    newType,
    newActivityBudget
  }) {
    let newGoalAmount = project.goalAmount;

    const previousTypeIsFunding = previousType === ACTIVITY_TYPES.FUNDING;
    const newTypeIsFunding = newType === ACTIVITY_TYPES.FUNDING;

    if (previousTypeIsFunding && newTypeIsFunding) {
      const difference = BigNumber(newActivityBudget).minus(
        previousActivityBudget
      );
      if (!difference.isEqualTo(0)) {
        newGoalAmount = BigNumber(project.goalAmount).plus(difference);
      }
    }

    if (!previousTypeIsFunding && newTypeIsFunding) {
      newGoalAmount = BigNumber(project.goalAmount).plus(newActivityBudget);
    }

    if (previousTypeIsFunding && !newTypeIsFunding) {
      newGoalAmount = BigNumber(project.goalAmount).minus(
        previousActivityBudget
      );
    }

    logger.info(
      `[ActivityService] :: Updating project ${project.id
      } goalAmount to ${newGoalAmount}`
    );

    await this.projectService.updateProject(project.id, {
      goalAmount: newGoalAmount.toString()
    });
  },

  async getActivitiesByProject(projectId) {
    logger.info('[ActivityService] :: Entering getActivitiesByProject method');
    const milestones = await this.milestoneService.getAllMilestonesByProject(
      projectId
    );
    const milestoneIds = milestones.map(({ id }) => id);
    return this.activityDao.getActivitiesByMilestones(milestoneIds);
  },

  getActivitiesBudget({ activities, type }) {
    const activitiesFiltered = activities.filter(
      activity => activity.type === type
    );

    return mapFieldAndSum({ array: activitiesFiltered, field: 'budget' });
  },

  getActivitiesBudgetAndCurrentByType({ activities, type }) {
    const activitiesFiltered = activities.filter(
      activity => activity.type === type
    );

    return {
      budget: mapFieldAndSum({ array: activitiesFiltered, field: 'budget' }),
      current: mapFieldAndSum({ array: activitiesFiltered, field: 'current' })
    };
  },

  validateActivityTypeByProject({ project, activityType }) {
    const isGrantProject = project.type === PROJECT_TYPES.GRANT;
    const validActivityTypes = [
      ACTIVITY_TYPES.FUNDING,
      ACTIVITY_TYPES.SPENDING
    ];
    if (isGrantProject && !validActivityTypes.includes(activityType)) {
      throw new COAError(
        errors.task.InvalidActivityTypeInProjectType(activityType)
      );
    }
  },

  async getApprovedEvidencesByProject({ projectId, limit }) {
    logger.info(
      '[ActivityService] :: Entering getApprovedEvidencesByProject method'
    );
    const milestones = await this.milestoneDao.getMilestonesByProjectId(
      projectId
    );
    const approvedTasksIds = milestones
      .flatMap(milestone => milestone.tasks)
      .filter(_task => _task.status === ACTIVITY_STATUS.APPROVED)
      .map(_task => _task.id);
    const evidences = await this.activityEvidenceDao.getApprovedEvidences({
      tasksIds: approvedTasksIds,
      limit
    });
    return evidences;
  },

  async validateUserCanAddEvidence({
    userId,
    roleDescription,
    activityId,
    projectId
  }) {
    await this.userProjectService.validateUserWithRoleInProject({
      user: userId,
      descriptionRoles: [roleDescription],
      project: projectId,
      error: errors.task.UserCanNotAddEvidenceToProject({
        userId,
        activityId,
        projectId
      })
    });
  },
  async getActivitiesByMilestone(milestoneId) {
    logger.info('[ActivityService] :: Entering getActvitiesByMilestone method');
    return this.activityDao.getTasksByMilestone(milestoneId);
  },
  async thereArePendingActivityTransactions(projectId) {
    logger.info(
      '[ActivityService] :: Entering thereArePendingActivityTransactions method'
    );

    const activities = await this.getActivitiesByProject(projectId);

    return activities.some(_activity =>
      pendingActivityStatus.includes(_activity.status)
    );
  },

  async deleteEvidence(activityId, evidenceId, userId) {
    try {
      logger.info('[ActivityService] :: Entering deleteEvidence method');

      validateRequiredParams({
        method: 'deleteEvidence',
        params: {
          activityId,
          evidenceId,
          userId
        }
      });

      const activity = await checkExistence(
        this.activityDao,
        activityId,
        'activity'
      );

      const validActivityStatusToDeleteEvidence = [
        ACTIVITY_STATUS.IN_PROGRESS
      ];

      if (!validActivityStatusToDeleteEvidence.includes(activity.status)) {
        logger.info(
          `[ActivityService] :: Can't delete evidence to an activity with status ${activity.status}`
        );
        throw new COAError(
          errors.task.InvalidRequiredStatus(activity.status)
        );
      }

      const evidence = await checkExistence(
        this.activityEvidenceDao,
        evidenceId,
        'evidence'
      );

      if (evidence.user.id !== userId) {
        logger.info(
          `[ActivityService] :: Can't delete evidence created by another user`
        );
        throw new COAError(
          errors.task.UserCantDeleteEvidence(userId, evidenceId)
        );
      }

      const validEvidenceStatusToDeleteEvidence = [
        evidenceStatus.NEW
      ];

      if (!validEvidenceStatusToDeleteEvidence.includes(evidence.status)) {
        logger.info(
          `[ActivityService] :: Can't delete evidence with status ${evidence.status}`
        );
        throw new COAError(
          errors.task.InvalidRequiredStatus(evidence.status)
        );
      }

      await this.evidenceFileService.deleteEvidenceFileByEvidenceId(evidenceId);

      await Promise.all(
        evidence.files.map(async file => {
          return this.fileService.deleteFile(file.file)
        })
      )

      logger.info('[ActivityService] :: Removing evidence in database', evidence);
      await this.activityEvidenceDao.deleteEvidence(evidence.id)

      logger.info('[ProjectService] :: About to create changelog');

      const milestone = await this.getMilestoneFromActivityId(activityId);
      const project = await this.projectService.getProjectById(milestone.project);

      const evidences = await this.activityEvidenceDao.getEvidencesByTaskId(activityId);
      if (evidences.length === 0) {
        const toUpdate = {
          status: ACTIVITY_STATUS.NEW,
          step: ACTIVITY_STEPS.CAN_UPDATE_ACTIVITY_STATUS
        };

        logger.info(
          `[ActivityService] :: About to update activity with ${JSON.stringify(
            toUpdate
          )}`
        );
        await this.activityDao.updateActivity(
          toUpdate,
          activity.id
        );

        const activities = await this.milestoneDao.getMilestoneTasks(milestone.id)
        const inProgressActivities = activities.filter(activity => activity.status !== ACTIVITY_STATUS.NEW);


        if (inProgressActivities.length === 0) {
          logger.info(
            `[ActivityService] :: About to update milestone status to ${MILESTONE_STATUS.NOT_STARTED
            }`
          );
          await this.milestoneDao.updateMilestone(
            { status: MILESTONE_STATUS.NOT_STARTED },
            milestone.id
          );
        }
      }

      await this.changelogService.createChangelog({
        project: project.parent || project.id,
        revision: project.revision,
        milestone: milestone.id,
        activity: activityId,
        evidence: evidenceId,
        action: ACTION_TYPE.DELETE_EVIDENCE,
        user: userId,
        evidenceData: {
          id: Number(evidenceId),
          title: evidence.title
        }
      });
    } catch (error) {
      logger.info(
        `[ActivityService] :: Occurs an error trying to delete evidence :: ${error}`
      );
      throw new COAError(error);
    }
  },

  async getActivityById(activityId) {
    logger.info('[ActivityService] :: Entering getActivityById method');
    activity = await this.activityDao.findById(activityId);
    logger.info(`[ActivityService] :: Activity id ${activity.id} found`);
    return activity;
  },
};
