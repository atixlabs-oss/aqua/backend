/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { ethers } = require('hardhat');
const logger = require('../../logger');
const txService = require('../../services/txService');

const ethProvider = () =>
  ethers.provider.on('block', async blockNumber => {
    global.secondsSinceLastBlockRead = new Date().getTime();
    logger.info(`[ethProvider] :: block ${blockNumber}, secondsSinceLastBlockRead: ${global.secondsSinceLastBlockRead}`);
    await txService.processPendingTransactions(blockNumber);
  });

module.exports = {
  ethProvider
};
