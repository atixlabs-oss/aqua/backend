/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { coa } = require('hardhat');
const { registerEvents } = require('../../util/events');
const logger = require('../../logger');
const { ethProvider } = require('./ethProvider');

const ethInit = async () => {
  logger.info('ethInit :: initializing eth');
  ethProvider();
  const contract = await coa.getCOA();
  const registry = await coa.getRegistry();
  const daos = await coa.getDaos();
  /* eslint-disable no-await-in-loop */
  for (let i = 0; i < daos.length; i++) {
    const currentAddress = await daos[i].address;
    const dao = await coa.getDaoContract(currentAddress);
    await registerEvents(dao, 'DAO');
  }
  await registerEvents(contract, 'ProjectsRegistry');
  await registerEvents(registry, 'ClaimsRegistry');
};

module.exports = {
  ethInit
};
