/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const ethServicesMock = () => ({
  createProject: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a',
  isTransactionConfirmed: creationTransactionHash => !!creationTransactionHash,
  startProject: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a',
  createMilestone: () => true,
  createActivity: () => true,
  validateActivity: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a',
  claimMilestone: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a',
  setMilestoneFunded: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a',
  uploadHashEvidenceToActivity: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a',
  createAccount: () => ({
    address: '0xdf08f82de32b8d460adbe8d72043e3a7e25a3b39',
    privateKey:
      '0x2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501200'
  }),
  transferInitialFundsToAccount: () =>
    '0x0d8cd6fd460d607b2590fb171a3dff04e33285830add91a2f9a4e43ced1ed01a'
});

module.exports = ethServicesMock;
