/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../../../logger');
const projectService = require('../../../services/projectService');
const { projectStatuses } = require('../../../util/constants');

module.exports = {
  ProjectCreated: async (id, address) => {
    const projectId = id.toNumber();
    logger.info('[COA] :: Incoming event ProjectCreated - address:', address);
    await projectService.updateProject(projectId, {
      status: projectStatuses.FUNDING,
      address
    });

    logger.info(
      `[COA] :: Project ${projectId} status updated to ${
        projectStatuses.FUNDING
      }`
    );
  }
};
