/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const { isEmpty } = require('lodash');
const { support } = require('config');

const checkExistence = require('./helpers/checkExistence');

const COAError = require('../errors/COAError');
const errors = require('../errors/exporter/ErrorExporter');
const logger = require('../logger');
const { addHours } = require('../util/date');

module.exports = {
  async startPassRecoveryProcess(email, projectId) {
    logger.info(
      '[Pass Recovery Service] :: Starting pass recovery for email:',
      email
    );
    const user = await this.userDao.getUserByEmail(email);
    if (!user) {
      logger.info(
        '[PassRecovery Service] :: There is no user associated with that email',
        email
      );
      throw new COAError(errors.user.EmailNotExists(email));
    }
    const { id } = user;
    if (projectId) {
      await checkExistence(this.projectDao, projectId, 'project');
      const userProject = await this.userProjectDao.findUserProject({
        user: user.id,
        project: projectId
      });
      if (!userProject) {
        logger.error(
          `[PassRecovery Service] User with id ${id} is not related to project with id ${projectId}`
        );
        throw new COAError(errors.user.UserNotRelatedToTheProject);
      }
    }
    const hash = await crypto.randomBytes(25);
    const token = hash.toString('hex');
    const expirationDate = addHours(support.recoveryTime, new Date());
    const recovery = await this.passRecoveryDao.createRecovery(
      email,
      token,
      expirationDate
    );

    if (!recovery) {
      logger.info(
        '[PassRecovery Service]:: Can not create recovery with email',
        email
      );
      return email;
    }

    const info = await this.mailService.sendEmailRecoveryPassword({
      to: email,
      bodyContent: {
        token,
        projectId
      }
    });

    if (!info || !isEmpty(info.rejected)) {
      logger.info('[PassRecovery Service] :: Invalid email', email);
    }

    return email;
  },

  async updatePassword({ token, password }) {
    try {
      const recovery = await this.passRecoveryDao.findRecoverBytoken(token);
      if (!recovery || !recovery.email) {
        logger.error('[Pass Recovery Service] :: Token not found: ', token);
        throw new COAError(errors.user.InvalidToken);
      }
      const { email } = recovery;
      const user = await this.userDao.getUserByEmail(email);
      if (!user) {
        logger.error(
          '[Pass Recovery Service] :: There is no user associated with that email',
          email
        );
        throw new COAError(errors.user.InvalidEmail);
      }

      const hashedPwd = await bcrypt.hash(password, 10);
      const changeFields = {
        password: hashedPwd,
        forcePasswordChange: false,
        emailConfirmation: true
      };
      if (user.first) changeFields.first = false;
      const updated = await this.userDao.updateUserByEmail(email, changeFields);

      if (!updated) {
        logger.error(
          '[Pass Recovery Service] :: Error updating password in database for user: ',
          email
        );
        throw new COAError(errors.user.UserUpdateError);
      }

      await this.passRecoveryDao.deleteRecoverByToken(token);
      return { first: user.first };
    } catch (error) {
      if (error instanceof COAError) throw error;
      logger.error('[Pass Recovery Service] :: Error updating password');
      throw Error('Error updating password');
    }
  },

  async getTokenStatus(token) {
    const recover = await this.passRecoveryDao.findRecoverBytoken(token);
    let expired = false;
    if (!recover) {
      logger.error('[Pass Recovery Service] :: Token not found: ', token);
      throw new COAError(errors.user.InvalidToken);
    }
    if (new Date() > new Date(recover.expirationDate)) {
      logger.error('[Pass Recovery Service] :: Token has expired: ', token);
      expired = true;
    }
    const toReturn = { expired };
    return toReturn;
  }
};
