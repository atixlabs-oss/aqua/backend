/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../logger');
const errors = require('../errors/exporter/ErrorExporter');
const COAError = require('../errors/COAError');

module.exports = {
  async getRoleByDescription(description) {
    logger.info('[RoleService] :: Entering getRoleByDescription method');
    const role = await this.roleDao.getRoleByDescription(description);
    if (!role) throw new COAError(errors.common.ErrorGetting('role'));
    return role;
  },
  async getRolesByDescriptionIn(descriptions) {
    logger.info('[RoleService] :: Entering getRolesByDescriptionIn method');
    return this.roleDao.getRolesByDescriptionIn(descriptions);
  }
};
