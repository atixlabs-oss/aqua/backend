/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { coa } = require('hardhat');
const logger = require('../logger');
const validateRequiredParams = require('./helpers/validateRequiredParams');
const rollbackTransactionByAction = require('./transactions/rollbackTransactionByAction');
const confirmTransactionByAction = require('./transactions/confirmTransactionByAction');
const { TX_STATUS } = require('../util/constants');

module.exports = {
  async findAllPendingTransactions() {
    logger.info('[TxService] :: Entering findAllPendingTransactions method');
    return this.txDao.findAllPendingTransactions();
  },
  async hasFailed(txHash) {
    logger.info('[TxService] :: Entering hasFailed method');
    validateRequiredParams({
      method: 'hasFailed',
      params: { txHash }
    });
    logger.info(`[TxService] :: Checking if ${txHash} has failed`);
    const txReceipt = await coa.getTransactionReceipt(txHash);
    return !txReceipt || txReceipt.status === 0;
  },
  async hasVerified(blockNumber, currentBlockNumber) {
    logger.info('[TxService] :: Entering hasVerified method');
    validateRequiredParams({
      method: 'hasVerified',
      params: { blockNumber, currentBlockNumber }
    });
    logger.info(
      `[TxService] :: Checking if block with block number ${blockNumber} is still verified`
    );
    return currentBlockNumber - blockNumber > process.env.BLOCKS_NUMBER_TO_WAIT;
  },
  async processPendingTransactions(currentBlockNumber) {
    logger.info('[TxService] :: Entering processPendingTransactions method');
    const transactions = await this.findAllPendingTransactions();
    logger.info(
      `[TxService] :: Found ${transactions.length} pending transactions`
    );
    await Promise.all(
      transactions.map(async ({ id, hash, action, data }) => {
        const { blockNumber } = await coa.getTransactionResponse(hash);
        const hasVerified = await this.hasVerified(
          blockNumber,
          currentBlockNumber
        );
        if (hasVerified) {
          const hasFailed = await this.hasFailed(hash);
          if (hasFailed) {
            logger.error(
              `[TxService] :: Transaction with hash ${hash} failed - Operation type ${action}`
            );
            const rollbackTransaction = rollbackTransactionByAction[action];
            await rollbackTransaction(data);
            await this.changelogService.updateChangelog({
              changelogId: data.changelogId,
              toUpdate: {
                status: TX_STATUS.FAILED
              }
            });
            await this.updateTransaction({
              id,
              toUpdate: {
                status: TX_STATUS.FAILED
              }
            });
            return this.mailService.sendTxFailedMail({
              bodyContent: { txHash: hash },
              to: data.userEmail
            });
          }
          logger.info(
            `[TxService] :: Confirming transaction with hash ${hash} - Operation type ${action}`
          );
          const confirmTransaction = confirmTransactionByAction[action];
          await confirmTransaction(data);
          await this.changelogService.updateChangelog({
            changelogId: data.changelogId,
            toUpdate: {
              status: TX_STATUS.CONFIRMED
            }
          });
          await this.updateTransaction({
            id,
            toUpdate: {
              status: TX_STATUS.CONFIRMED
            }
          });
        }
      })
    );
  },
  async updateTransaction({ id, toUpdate }) {
    logger.info('[TxService] :: Entering updateTransaction method');
    return this.txDao.updateTransaction({ id, toUpdate });
  },
  async createTransaction({ hash, action, status, data }) {
    logger.info('[TxService] :: Entering createTransaction method');
    return this.txDao.createTx({
      hash,
      action,
      status,
      data
    });
  }
};
