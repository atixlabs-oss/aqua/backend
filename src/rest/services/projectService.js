/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { isEmpty, pick, omit } = require('lodash');
const { coa } = require('hardhat');
const {
  projectStatuses,
  projectSensitiveDataFields,
  projectPublicFields,
  rolesTypes,
  currencyTypes,
  evidenceStatus,
  ACTION_TYPE,
  TIMEFRAME_DECIMALS,
  projectStatusToClone,
  MILESTONE_STATUS,
  PROJECT_STEPS,
  PROJECT_TYPES,
  ACTIVITY_TYPES,
  TX_STATUS,
  pinStatus,
  PROJECT_VISIBILITY_TYPES,
  ACTIVITY_STATUS,
} = require('../util/constants');
const files = require('../util/files');
const checkExistence = require('./helpers/checkExistence');
const { completeStep } = require('./helpers/dataCompleteUtil');
const { getMessageHash, generateNonceRandom } = require('./helpers/hardhatTaskHelpers');
const validateRequiredParams = require('./helpers/validateRequiredParams');
const validateStatusToUpdate = require('./helpers/validateStatusToUpdate');
const validateUserCanEditProject = require('./helpers/validateUserCanEditProject');
const validateFile = require('./helpers/validateFile');
const validateTimeframe = require('./helpers/validateTimeframe');
const validateUsersAreEqualsOrThrowError = require('./helpers/validateUsersAreEqualsOrThrowError');
const validateProjectStatusChange = require('./helpers/validateProjectStatusChange');
const COAError = require('../errors/COAError');
const errors = require('../errors/exporter/ErrorExporter');
const logger = require('../logger');
const { format } = require('../util/emailFormatter');
const sortByCreatedAt = require('./helpers/sortProjectsByCreatedAt');
const countryService = require('./countryService');

const thumbnailType = files.TYPES.thumbnail;
const legalAgreementFileType = files.TYPES.agreementFile;
const projectProposalFileType = files.TYPES.agreementFile;

const STEPS_COMPLETED = 11;
const { currentWorkingDir } = files;

module.exports = {
  async getProjectById(id) {
    logger.info('[ProjectService] :: Entering getProjectById method');
    const project = await checkExistence(this.projectDao, id, 'project');
    logger.info(`[ProjectService] :: Project id ${project.id} found`);
    return project;
  },

  async updateProject(projectId, fields) {
    logger.info('[ProjectService] :: Entering updateProject method.');
    let toUpdate = { ...fields };
    if (fields.status) {
      toUpdate = { ...fields, lastUpdatedStatusAt: new Date() };
    }
    logger.info(
      `[ProjectService] :: Updating project id ${projectId}`,
      toUpdate
    );
    const updatedProject = await this.projectDao.updateProject(
      toUpdate,
      projectId
    );
    if (!updatedProject) {
      logger.error('[ProjectService] :: Error updating project in DB');
      throw new COAError(errors.project.CantUpdateProject(projectId));
    }
    return updatedProject.id;
  },

  async saveProject(project) {
    const savedProject = await this.projectDao.saveProject(project);
    if (!savedProject) {
      logger.error('[ProjectService] :: Error saving project in DB');
      throw new COAError(errors.project.CantSaveProject);
    }
    return savedProject;
  },

  async createProject({ ownerId }) {
    logger.info('[ProjectService] :: Entering createProject method');

    const user = await this.userService.getUserById(ownerId);

    if (!isEmpty(user)) {
      logger.info('[ProjectService] :: Saving new project');
      const project = await this.saveProject({
        projectName: 'Untitled',
        owner: ownerId
      });

      const projectId = project.id;

      logger.info(
        `[ProjectService] :: New project created with id ${projectId}`
      );

      logger.info('[ProjectService] :: About to create changelog');
      await this.changelogService.createChangelog({
        project: project.parent ? project.parent : projectId,
        revision: project.revision,
        action: ACTION_TYPE.CREATE_PROJECT,
        user: ownerId
      });
      return { projectId };
    }
    logger.error(
      `[ProjectService] :: Undefined user for provided ownerId: ${ownerId}`
    );
    throw new COAError(errors.user.UndefinedUserForOwnerId(ownerId));
  },

  async cloneProject({ user, projectId }) {
    const project = await checkExistence(this.projectDao, projectId, 'project');
    if (project.parent) {
      logger.error('[ProjectService] :: Project is not genesis');
      throw new COAError(errors.project.ProjectNotGenesis);
    }

    logger.info('[ProjectService] :: Getting last review with valid status');
    const {
      id: lastProjectId,
      createdAt,
      ...lastProject
    } = await this.projectDao.getLastProjectWithValidStatus(projectId);
    const projectToClone = {
      ...lastProject,
      revision: lastProject.revision + 1,
      parent: projectId,
      status: projectStatuses.OPEN_REVIEW,
      step: 0,
      proposer: user.id
    };

    if (!user.isAdmin) {
      await this.userProjectService.getUserProjectFromRoleDescription({
        projectId: lastProjectId,
        roleDescriptions: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR],
        userId: user.id
      });
    }

    if (!projectStatusToClone.includes(lastProject.status)) {
      logger.error(
        `[ProjectService] :: Project with status ${
          project.status
        } is not available to get cloned`
      );
      throw new COAError(errors.project.ProjectInvalidStatus(projectId));
    }

    const activeClone = await this.projectDao.findActiveProjectClone(projectId);
    if (activeClone) {
      throw new COAError(errors.project.CloneAlreadyExists(projectId));
    }

    await this.validateThereAreNoPendingActivityTransactions(lastProjectId);

    logger.info('[ProjectService] :: About to clone project');
    const clonedProject = await this.projectDao.saveProject(projectToClone);
    logger.info('[ProjectService] :: Getting milestones');
    const milestonesFromLastProject = await this.milestoneDao.getMilestonesByProjectId(
      lastProjectId
    );
    logger.info(
      '[ProjectService] :: About to clone milestones, activities and evidences'
    );
    await this.cloneMilestoneActivitiesAndEvidences({
      projectId: clonedProject.id,
      milestones: milestonesFromLastProject
    });

    logger.info('[ProjectService] :: Getting user projects');
    const userProjects = await this.userProjectDao.getUserProject({
      project: lastProjectId
    });

    logger.info('[ProjectService] :: About to clone user projects');
    await Promise.all(
      userProjects.map(({ id: userProjectId, ...userProject }) =>
        this.userProjectDao.createUserProject({
          ...userProject,
          project: clonedProject.id
        })
      )
    );

    logger.info('[ProjectService] :: About to insert changelog');
    await this.changelogService.createChangelog({
      project: project.parent ? project.parent : project.id,
      user: user.id,
      revision: lastProject.revision + 1,
      action: ACTION_TYPE.PROJECT_CLONE
    });

    const toReturn = { projectId: clonedProject.id };
    return toReturn;
  },

  async cloneMilestoneActivitiesAndEvidences({ projectId, milestones }) {
    return Promise.all(
      milestones.map(async ({ id: milestoneId, tasks, ...milestone }) => {
        const newMilestone = await this.milestoneDao.createMilestone({
          ...milestone,
          project: projectId,
          parent: milestone.parent || milestoneId
        });
        await tasks.reduce(async (previousPromise, { id: taskId, ...task }) => {
          await previousPromise;
          const newTask = await this.activityDao.createActivity({
            ...task,
            milestone: newMilestone.id,
            parent: task.parent || taskId
          });
          const evidences = await this.activityEvidenceDao.getEvidencesByTaskId(
            taskId
          );
          return Promise.all(
            evidences.map(async ({ id: evidenceId, ...evidence }) => {
              const newEvidence = await this.activityEvidenceDao.addActivityEvidence(
                {
                  ...evidence,
                  activity: newTask.id,
                  parent: evidence.parent || evidenceId
                }
              );
              const evidenceFiles = await this.evidenceFileDao.getEvidenceFilesByEvidenceId(
                evidenceId
              );
              await Promise.all(
                evidenceFiles.map(({ id: evidenceFileId, ...evidenceFile }) =>
                  this.evidenceFileDao.saveEvidenceFile({
                    ...evidenceFile,
                    evidence: newEvidence.id
                  })
                )
              );
            })
          );
        }, Promise.resolve());
      })
    );
  },

  async updateBasicProjectInformation({
    projectId,
    projectName,
    location,
    timeframe,
    timeframeUnit,
    file,
    user
  }) {
    const userId = user.id;
    logger.info(
      '[ProjectService] :: Entering updateBasicProjectInformation method'
    );
    validateRequiredParams({
      method: 'updateBasicProjectInformation',
      params: {
        projectId,
        projectName,
        location,
        timeframe,
        timeframeUnit
      }
    });

    const project = await checkExistence(this.projectDao, projectId, 'project');

    await validateUserCanEditProject({
      user,
      project,
      error: errors.project.ProjectCantBeUpdated
    });

    validateTimeframe(timeframe);

    validateFile({
      filePathOrHash: project.cardPhotoPath,
      fileParam: file,
      paramName: 'thumbnailPhoto',
      method: 'updateBasicProjectInformation',
      type: thumbnailType
    });

    let { cardPhotoPath } = project;

    if (file) {
      logger.info(`[ProjectService] :: Saving file of type '${thumbnailType}'`);
      cardPhotoPath = await files.saveFile(thumbnailType, file);
      logger.info(`[ProjectService] :: File saved to: ${cardPhotoPath}`);
    }

    logger.info('[ProjectService] :: Generating the new dataComplete value');

    const dataCompleteUpdated = completeStep({
      dataComplete: project.dataComplete,
      step: 1
    });

    logger.info(`[ProjectService] :: Updating project of id ${projectId}`);

    const toUpdate =
      project.status === projectStatuses.DRAFT
        ? {
            projectName,
            location,
            timeframe,
            timeframeUnit,
            dataComplete: dataCompleteUpdated,
            cardPhotoPath
          }
        : {
            projectName,
            location,
            timeframe,
            cardPhotoPath
          };

    const updatedProjectId = await this.updateProject(projectId, toUpdate);
    logger.info(`[ProjectService] :: Project of id ${projectId} updated`);

    const fields = {
      projectName,
      location,
      timeframe: Number(timeframe).toFixed(TIMEFRAME_DECIMALS),
      timeframeUnit,
      dataComplete: dataCompleteUpdated
    };

    logger.info('[ProjectService] :: About to insert changelog');
    if (file) {
      await this.changelogService.createChangelog({
        project: projectId,
        revision: project.revision,
        user: userId,
        action: ACTION_TYPE.EDIT_PROJECT_BASIC_INFORMATION,
        extraData: { fieldName: 'thumbnailPhotoFile' }
      });
    }
    await this.compareFieldsAndCreateChangelog({
      project,
      fields,
      action: ACTION_TYPE.EDIT_PROJECT_BASIC_INFORMATION,
      user: userId
    });

    return { projectId: updatedProjectId };
  },

  async updateProjectDetails({
    projectId,
    mission,
    problemAddressed,
    currencyType,
    currency,
    additionalCurrencyInformation,
    legalAgreementFile,
    projectProposalFile,
    user,
    type
  }) {
    const userId = user.id;
    logger.info('[ProjectService] :: Entering updateProjectDetails method');
    validateRequiredParams({
      method: 'updateProjectDetails',
      params: {
        mission,
        problemAddressed,
        currencyType,
        currency,
        additionalCurrencyInformation,
        type
      }
    });

    const project = await checkExistence(this.projectDao, projectId, 'project');

    let { agreementFilePath, proposalFilePath } = project;

    await validateUserCanEditProject({
      user,
      project,
      error: errors.project.ProjectCantBeUpdated
    });

    this.validateProjectType(type);

    validateFile({
      filePathOrHash: agreementFilePath,
      fileParam: legalAgreementFile,
      paramName: 'legalAgreementFile',
      method: 'updateProjectDetails',
      type: legalAgreementFileType
    });
    validateFile({
      filePathOrHash: proposalFilePath,
      fileParam: projectProposalFile,
      paramName: 'projectProposalFile',
      method: 'updateProjectDetails',
      type: projectProposalFileType
    });

    if (legalAgreementFile) {
      logger.info('[ProjectService] :: Updating legal agreement file');
      agreementFilePath = await files.validateAndSaveFile(
        files.TYPES.agreementFile,
        legalAgreementFile
      );
    }
    if (projectProposalFile) {
      logger.info('[ProjectService] :: Updating project proposal file');
      proposalFilePath = await files.validateAndSaveFile(
        files.TYPES.proposalFile,
        projectProposalFile
      );
    }

    logger.info('[ProjectService] :: Generating the new dataComplete value');

    const dataCompleteUpdated = completeStep({
      dataComplete: project.dataComplete,
      step: 2
    });

    logger.info(`[ProjectService] :: Updating project of id ${projectId}`);

    const toUpdate =
      project.status === projectStatuses.OPEN_REVIEW
        ? {
            mission,
            problemAddressed,
            agreementFilePath,
            proposalFilePath
          }
        : {
            mission,
            problemAddressed,
            currencyType,
            currency,
            additionalCurrencyInformation,
            agreementFilePath,
            proposalFilePath,
            dataComplete: dataCompleteUpdated,
            type
          };

    const updatedProjectId = await this.updateProject(projectId, toUpdate);
    logger.info(`[ProjectService] :: Project of id ${projectId} updated`);

    logger.info('[ProjectService] :: About to insert changelog');
    await this.compareProjectDetailsFieldsAndCreateChangelog({
      project,
      mission,
      problemAddressed,
      currencyType,
      currency,
      additionalCurrencyInformation,
      legalAgreementFile,
      projectProposalFile,
      user: userId
    });

    return { projectId: updatedProjectId };
  },

  async compareProjectDetailsFieldsAndCreateChangelog({
    project,
    mission,
    problemAddressed,
    currencyType,
    currency,
    additionalCurrencyInformation,
    legalAgreementFile,
    projectProposalFile,
    user
  }) {
    logger.info(
      '[ProjectService] :: Entering compareProjectDetailsFieldsAndInsertChangelog method'
    );
    const projectId = project.parent ? project.parent : project.id;

    const fields = {
      mission,
      problemAddressed,
      currencyType,
      currency,
      additionalCurrencyInformation
    };

    await this.compareFieldsAndCreateChangelog({
      project,
      fields,
      action: ACTION_TYPE.EDIT_PROJECT_DETAILS,
      user
    });

    if (projectProposalFile) {
      await this.changelogService.createChangelog({
        project: projectId,
        revision: project.revision,
        user,
        action: ACTION_TYPE.EDIT_PROJECT_DETAILS,
        extraData: { fieldName: 'projectProposalFile' }
      });
    }
    if (legalAgreementFile) {
      await this.changelogService.createChangelog({
        project: projectId,
        revision: project.revision,
        user,
        action: ACTION_TYPE.EDIT_PROJECT_DETAILS,
        extraData: { fieldName: 'legalAgreementFile' }
      });
    }
  },

  async compareFieldsAndCreateChangelog({ project, fields, action, user }) {
    const projectId = project.parent ? project.parent : project.id;
    const fieldsChanged = Object.keys(fields).filter(
      key => project[key] !== fields[key]
    );

    await Promise.all(
      fieldsChanged.map(field =>
        this.changelogService.createChangelog({
          project: projectId,
          revision: project.revision,
          user,
          action,
          extraData: { fieldName: field }
        })
      )
    );
  },

  async getProjectMilestones(projectId) {
    logger.info('[ProjectService] :: Entering getProjectMilestones method');
    validateRequiredParams({
      method: 'getProjectMilestones',
      params: { projectId }
    });
    await checkExistence(this.projectDao, projectId, 'project');
    return this.milestoneService.getAllMilestonesByProject(projectId);
  },

  async publishProject({ projectId, userId, userEmail, previousStatus }) {
    logger.info('[ProjectService] :: Entering publishProject method');
    validateRequiredParams({
      method: 'publishProject',
      params: { projectId }
    });
    const project = await checkExistence(this.projectDao, projectId, 'project');
    logger.info(`[Project Service] :: Publish project ${projectId}`);

    if (!previousStatus) {
      validateStatusToUpdate({
        status: project.status,
        error: errors.project.ProjectIsNotPublishable
      });
    }

    this.validateDataComplete({ dataComplete: project.dataComplete });

    const users = await this.getUsersByProjectId({ projectId });

    this.validateProjectUsersAreVerified({ users });

    await this.validateProjectBudget(projectId);

    logger.info('[ProjectService] :: Reading agreement file');
    const agreementFile = files.getFileFromPath(
      `${currentWorkingDir}${project.agreementFilePath}`
    );
    logger.info('[ProjectService] :: Saving agreement file');
    const agreementFileHash = await this.storageService.saveStorageData({
      data: agreementFile
    });
    logger.info('[ProjectService] :: Reading proposal file');
    const proposalFile = files.getFileFromPath(
      `${currentWorkingDir}${project.proposalFilePath}`
    );
    logger.info('[ProjectService] :: Saving proposal file');
    const proposalFileHash = await this.storageService.saveStorageData({
      data: proposalFile
    });
    logger.info(
      '[ProjectService] :: Saving project meetadata to storage service'
    );

    // NOTE: The notarization should be generated after update status in DB.
    const projectData = await this.mapProjectToPublish(project);
    
    const {metadataHash, metadataFile} = await this.uploadProjectMetadataToIPFS({project: {
      ...projectData,
      agreementFileHash,
      proposalFileHash,
      status: previousStatus ? previousStatus : projectStatuses.PUBLISHED
    }}, userId);

    logger.info('[ProjectService] :: Saving project metadata');

    await files.saveProjectMetadataFile({
      projectId: project.parent || project.id,
      revisionId: project.revision,
      data: { ...metadataFile, hash: metadataHash }
    });
    try {
      logger.info(`[ProjectService] :: Updating project with id ${project.id}`);
      if (!previousStatus) {
        await this.updateProject(project.id, {
          status: projectStatuses.PENDING_PUBLISHED,
          agreementFileHash,
          proposalFileHash,
          ipfsHash: metadataHash
        });
        logger.info(
          `[ProjectService] :: Calling COA createProject with ${JSON.stringify({
            projectId,
            metadataHash
          })}`
        );
        let transaction;
  
        try {
          transaction = await coa.createProject({
            projectId,
            metadataHash
          });
        } catch (error) {
          await this.updateProject(projectId, {
            status: projectStatuses.DRAFT,
          });
          logger.error(
            `[ProjectService] :: Error: ${error.message}`
          );
          throw new COAError(error.message)
        }
        

        logger.info('[ProjectService] :: About to create changelog');
        const changelog = await this.changelogService.createChangelog({
          project: project.id,
          user: userId,
          revision: project.revision,
          action: ACTION_TYPE.PUBLISH_PROJECT,
          transaction: transaction.hash,
          status: TX_STATUS.PENDING
        });

        logger.info('[ProjectService] :: About to create tx');
        await this.txDao.createTx({
          hash: transaction.hash,
          action: ACTION_TYPE.PUBLISH_PROJECT,
          status: TX_STATUS.PENDING,
          data: { projectId, changelogId: changelog.id, userEmail }
        });
      }
    } catch (error) {
      logger.error(
        '[ProjectService] :: There was an error trying to update project',
        error
      );
      throw new COAError(errors.project.CantUpdateProject(project.id));
    }

    const toReturn = { projectId, ipfsHash: metadataHash };

    return toReturn;
  },

  async mapProjectToPublish(project) {
    logger.info(`[ProjectService] :: mapProjectToPublish projectId: ${project.id}`);

    return {
      id: project.parent || project.id,
      projectId: project.id,
      name: project.name,
      problem: project.problem,
      projectName: project.projectName,
      mission: project.mission,
      problemAddressed: project.problemAddressed,
      agreementFileHash: project.agreementFileHash,
      proposalFileHash: project.proposalFileHash,
      revision: project.revision,
      type: project.type,
      isCancelRequested: project.isCancelRequested,
      currency: project.currency,
      currencyType: project.currencyType,
      goalAmount: project.goalAmount,
      timeframe: project.timeframe,
      timeframeUnit: project.timeframeUnit,
      location: project.location,
      status: project.status,
      users: [],
      milestones: await this.milestoneService.getAllMilestonesByProject(project.id),
      lastIPFSMetadata: project.lastIPFSMetadata
    }
  },

  async mapUsersToPublish(users, projectId) {
    return this.sortById(await Promise.all(users.map(async (user) => (await this.mapUserToPublish(user, projectId)))));
  },

  async mapUserToPublish(user, projectId) {
    logger.info(`[ProjectService] :: mapUserToPublish userId: ${user.id}`);
    const userMapped = {
      firstName: user.firstName,
      lastName: user.lastName,
      email: format(user.email),
      createdAt: user.createdAt,
      id: user.id,
      publicKey: user.address,
      country: user.country,
      role: user.role.description
    }
    await this.populateCountryInUsers([userMapped]);

    const userProjects = await this.userProjectDao.getRolesOfUser({
      user: user.id,
      project: projectId
    });

    if (userProjects) {
      userMapped.role = userProjects[0].role.description
    }

    return userMapped;
  },

  async mapMilestonesToPublish(milestones, projectId, userId, lastMilestonesState, activityId)  {
    const mappedMilestones = await Promise.all(milestones.map(async (m) => {
      logger.info(`[ProjectService] :: mapMilestonesToPublish milestoneId: ${m.id}`);

      const lastMilestoneState = lastMilestonesState && lastMilestonesState.find(lastMilestone => lastMilestone.id === m.publicId);

      return {
        id: m.id,
        publicId: m.publicId,
        title: m.title,
        description: m.description,
        status: m.status,
        activities: await this.mapActivitiesToPublish(m.tasks, projectId, userId, lastMilestoneState && lastMilestoneState.activities, activityId),
      }
    }));
    return this.sortById(mappedMilestones);
  },

  async mapActivitiesToPublish(activities, projectId, userId, lastActivitiesState, activityId) {
    const mappedActivities = await Promise.all(
      activities.map(async (activity) => {
        logger.info(`[ProjectService] :: mapActivitiesToPublish activityId: ${activity.id}`);

        let proposer = null;
        let auditedBy = null;
        let auditedAt = activity.approvalDate
        let sentToReviewAt = activity.revisionDate

        if (activityId && activity.id === activityId) {
          // if an activity is being audited we retrived the data from database
          if (activity.status === ACTIVITY_STATUS.PENDING_TO_REVIEW) {
            proposer = await this.mapUserToPublish(await this.userService.getUserById(activity.proposer), projectId);
          }

          if (activity.status === ACTIVITY_STATUS.PENDING_TO_REJECT || activity.status === ACTIVITY_STATUS.PENDING_TO_APPROVE) {
            auditedBy = await this.mapUserToPublish(await this.userService.getUserById(activity.auditorId), projectId) 
          }
        } else {
          // otherwise we looking for the last state stored
          const lastActivityState = lastActivitiesState && lastActivitiesState.find(lastActivity => lastActivity.id === activity.publicId);
  
          
          if (lastActivityState) {
            if (lastActivityState.proposer) {
              proposer = lastActivityState.proposer
            } else if (activity.status === ACTIVITY_STATUS.PENDING_TO_REVIEW) {
              proposer = await this.mapUserToPublish(await this.userService.getUserById(userId), projectId);
            }
  
            if (lastActivityState.auditedBy) {
              auditedBy = lastActivityState.auditedBy
            } else if (activity.status === ACTIVITY_STATUS.PENDING_TO_REJECT || activity.status === ACTIVITY_STATUS.PENDING_TO_APPROVE) {
              auditedBy = await this.mapUserToPublish(await this.userService.getUserById(userId), projectId) 
            }
            
            if (lastActivityState.auditedAt) {
              auditedAt = lastActivityState.auditedAt
            }
  
            if (lastActivityState.sentToReviewAt) {
              sentToReviewAt = lastActivityState.sentToReviewAt
            }
          }

        }

        const mappedActivity = {
          id: activity.id,
          publicId: activity.publicId,
          title: activity.title,
          description: activity.description,
          acceptanceCriteria: activity.acceptanceCriteria,
          type: activity.type,
          budget: activity.budget,
          status: this.mapActivityStatusForAudit(activity.status),
          auditedBy,
          auditedAt,
          assignedAuditor: await this.mapUserToPublish(await this.userService.getUserById(activity.auditor), projectId),
          proposer,
          sentToReviewAt,
          evidences: [],
        }

        return mappedActivity
      })
    );

    const mappedActivitiesSorted =  this.sortById(mappedActivities)
    return mappedActivitiesSorted;
  },

  validateDataComplete({ dataComplete }) {
    logger.info('[ProjectService] :: Entering validateDataComplete method');
    if (dataComplete !== STEPS_COMPLETED) {
      logger.info('[ProjectService] :: There are some incomplete step');
      throw new COAError(errors.project.IncompleteStep());
    }
  },

  validateProjectUsersAreVerified({ users }) {
    logger.info(
      '[ProjectService] :: Entering validateProjectUsersAreVerified method'
    );
    if (users.some(user => user.first)) {
      logger.info('[ProjectService] :: Not all users are verified');
      throw new COAError(errors.project.SomeUserIsNotVerified());
    }
  },

  async getUsersByProjectId({ projectId }) {
    logger.info('[ProjectService] :: Entering getUsersByProjectId method');

    const users = await this.userProjectDao.getUserProjects(
      projectId
    );
    return users.map(u => ({  ...u.user, role: u.role }));
  },
  async populateCountryInUsers(users) {
    const countries = users.map((u) => u.country);
    const uniqueCountries = [... new Set(countries)];
    (await countryService.getAll({id: uniqueCountries})).forEach(c => {
      users.forEach(user => user.country === c.id && (user.country = c.name))
    });

  },
  async deleteProject(projectId) {
    const project = await checkExistence(this.projectDao, projectId, 'project');
    if (project.status !== projectStatuses.DRAFT) {
      logger.error(`Project with id ${projectId} is not in Draft status`);
      throw new COAError(errors.project.ProjectInvalidStatus(projectId));
    }
    const userProjects = await this.userProjectDao.getUserProjects(projectId);

    try {
      logger.info('[ProjectService] :: About to delete user projects');
      await Promise.all(
        userProjects.map(userProject =>
          this.userProjectDao.removeUserProject(userProject.id)
        )
      );
      const milestones = await this.milestoneService.getAllMilestonesByProject(
        projectId
      );
      logger.info(
        '[ProjectService] :: About to delete milestones and activities'
      );
      await Promise.all(
        milestones.map(async milestone => {
          await Promise.all(
            milestone.tasks.map(async task => {
              const evidences = await this.activityEvidenceDao.getEvidencesByTaskId(
                task.id
              );
              await Promise.all(
                evidences.map(evidence =>
                  this.activityEvidenceDao.deleteEvidence(evidence.id)
                )
              );
              this.activityDao.deleteActivity(task.id);
            })
          );
          return this.milestoneDao.deleteMilestone(milestone.id);
        })
      );
      logger.info('[ProjectService] :: About to delete changelogs');
      await this.changelogService.deleteProjectChangelogs(projectId);
    } catch (error) {
      logger.error(
        '[ProjectService] :: There was an error deleting project ',
        error
      );
      throw new COAError(errors.server.InternalServerError);
    }
    logger.info(
      `[ProjectService] :: About to delete project with id ${projectId}`
    );
    const deletedProject = await this.projectDao.deleteProject({ projectId });
    if (!deletedProject) {
      logger.info('[ProjectService] :: Project could not be deleted');
      throw new COAError(errors.common.ErrorDeleting('project'));
    }
    logger.info('[ProjectService] :: Project successfully deleted');
    return deletedProject;
  },

  async mapUserData(users) {
    if (!users) return users;
    const userIds = users.flatMap((eachRole) => eachRole.users.map((eachUser) => eachUser.id));
  
    const walletHistory = await Promise.all(
      userIds.map((id) => this.walletHistoryDao.findAddressesByUser(id))
    );
    const walletHistoryMap = new Map(
      userIds.map((id, i) => [id, walletHistory[i]])
    );

    const usersData = await Promise.all(
      userIds.map((id) => this.userDao.findById(id))
    );
    const usersDataMap = new Map(
      userIds.map((id, i) => [id, usersData[i]])
    );
  
    return users.map((eachRole) => ({
      ...eachRole,
      users: eachRole.users.map((eachUser) => {
        const userData = usersDataMap.get(eachUser.id);
        return ({
        ...eachUser,
        email: format(eachUser.email),
        walletHistory: walletHistoryMap.get(eachUser.id),
        isActive: userData.isActive,
      })}),
    }));
  },

  async getProject(id, user) {
    const checkProject = await checkExistence(this.projectDao, id, 'project');
    if (!checkProject.parent) {
      const project = await checkExistence(
        this.projectDao,
        id,
        'project',
        this.projectDao.getLastPublicRevisionProject(id)
      );
      if (!user) {
        const projectWithPublicFields = pick(project, projectPublicFields);

        return {
          ...projectWithPublicFields,
          users: await this.mapUserData(projectWithPublicFields.users),
          milestones: projectWithPublicFields.milestones.map(milestone => ({
            ...milestone,
            activities: milestone.activities.map(activity => ({
              ...activity,
              evidences: activity.evidences.filter(
                evidence => evidence.status === evidenceStatus.APPROVED
              )
            }))
          }))
        };
      }
      // This is for the admin panel. The admin user can see full emails
      if (user.isAdmin) return project;
      const userProjects = await this.userProjectDao.getProjectsOfUser(user.id);
      const existsUserProjectRelationship = userProjects
        .map(up => up.project.id)
        .includes(project.id);
      if (!existsUserProjectRelationship) {
        logger.error(
          '[ProjectService] User not related to this project, throwing'
        );
        throw new COAError(errors.user.UserNotRelatedToTheProject);
      }
      project.users = await this.mapUserData(project.users);
      if (project.status === projectStatuses.DRAFT)
        return {
          id: project.id,
          status: project.status,
          basicInformation: project.basicInformation,
          revision: project.revision
        };
      return omit(project, projectSensitiveDataFields);
    }

    if (!user) {
      throw new COAError(errors.user.UserCanNotAccessInformation);
    }

    if (!user.isAdmin) {
      await this.userProjectService.validateUserWithRoleInProject({
        user: user.id,
        descriptionRoles: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR],
        project: id,
        error: errors.user.UserCanNotAccessInformation
      });
    }

    const project = await checkExistence(
      this.projectDao,
      id,
      'project',
      this.projectDao.getProjectWithAllData(id)
    );

    project.users = await this.mapUserData(project.users);

    if (project.step === PROJECT_STEPS.PENDING_SIGNATURE_AUTHORIZATION) {
      return {
        ...project,
        toSign: getMessageHash(
          ['uint256', 'string', 'string', 'string'],
          [
            project.proposerNonce,
            project.parent,
            project.ipfsHash,
            project.proposer.email
          ]
        )
      };
    }

    return project;
  },

  async setProjectVisibility(projectId, visibility) {
    logger.info(`[ProjectService] :: entering setProjectVisibility method for ${projectId}`);

    if (!Object.values(PROJECT_VISIBILITY_TYPES).includes(visibility)) {
      logger.error('[ProjectService] :: Provided visibility type does not exist');
      throw new COAError(errors.project.CantUpdateProject(projectId));
    }

    const project = await checkExistence(this.projectDao, projectId, 'project');
    let toUpdate;
    switch (visibility) {
      case PROJECT_VISIBILITY_TYPES.PRIVATE:
        if (!project.published && !project.highlighted) break;
        toUpdate = { published: false, highlighted: false, visibilityUpdatedAt: new Date() };
        break;
      case PROJECT_VISIBILITY_TYPES.PUBLISHED:
        if (project.published && !project.highlighted) break;
        toUpdate = { published: true, highlighted: false, visibilityUpdatedAt: new Date() };
        break;
      case PROJECT_VISIBILITY_TYPES.HIGHLIGHTED:
        if (project.published && project.highlighted) break;
        toUpdate = { published: true, highlighted: true, visibilityUpdatedAt: new Date() };
        break;
      default:
        break;
    }
    if (!toUpdate) {
      return {
        success: true,
      };
    }
    const response = await this.projectDao.updateProject(toUpdate, projectId);
    if (!response) {
      logger.error('[ProjectService] :: Error sending update request to database');
      throw new COAError(errors.project.CantUpdateProject(projectId));
    }

    return {
      success: !!response
    };
  },

  async getPublicProjects(highlighted, limit) {
    logger.info(`[ProjectService] :: entering getPublicProjects method for ${highlighted ? 'highlighted' : 'public' } projects`);
    const genesisProjects = await this.projectDao.findGenesisProjects();

    const filter = {
      status: {
        in: [projectStatuses.IN_PROGRESS, projectStatuses.PUBLISHED, projectStatuses.CANCELLED, projectStatuses.COMPLETED]
      },
    };

    const projects = (await Promise.all(
      genesisProjects.map(async project => {
        return this.projectDao.getLastProjectByFilter(project.id, filter)
      })
    )).filter(project => project && project.published && (highlighted ? project.highlighted : true));

    projects.splice(limit);

    const beneficiaryRole = await this.roleService.getRoleByDescription(
      rolesTypes.BENEFICIARY
    );

    for (const proj of projects) {
      const beneficiary = proj.users.find(user => user.role === beneficiaryRole.id);

      let user;
      if (beneficiary) {
        user = await this.userService.getUserById(beneficiary.user);
        user = {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName
        }
      }
      proj.beneficiary = user;
    }

    projects.sort((a, b) => b.visibilityUpdatedAt - a.visibilityUpdatedAt);
    return projects;
  },

  async getProjects({ status } = {}) {
    // TODO: implement pagination
    logger.info(
      `Getting all the projects ${status ? `with status ${status}` : ''}`
    );
    const genesisProjects = await this.projectDao.findGenesisProjects();
    const projects = (await Promise.all(
      genesisProjects.map(async project => {
        const lastProjectRevision = await this.projectDao.getLastValidReview(
          project.id
        );
        let visibility = PROJECT_VISIBILITY_TYPES.PRIVATE;
        if (lastProjectRevision.published) {
          visibility = lastProjectRevision.highlighted
            ? PROJECT_VISIBILITY_TYPES.HIGHLIGHTED
            : PROJECT_VISIBILITY_TYPES.PUBLISHED;
        }
        const projectToReturn = { ...lastProjectRevision, visibility };
        const clone = await this.projectDao.findActiveProjectClone(project.id);
        if (clone) return { ...projectToReturn, cloneStatus: clone.status };
        return projectToReturn;
      })
    )).sort(sortByCreatedAt);

    const beneficiaryRole = await this.roleService.getRoleByDescription(
      rolesTypes.BENEFICIARY
    );
    const projectsWithBeneficiary = await Promise.all(
      projects.map(async project => {
        const beneficiary = await this.userProjectService.getBeneficiaryByProjectId(
          {
            projectId: project.id,
            role: beneficiaryRole
          }
        );
        if (beneficiary) {
          const projectWithBeneficiary = { ...project, beneficiary };
          return projectWithBeneficiary;
        }
        return project;
      })
    );
    return projectsWithBeneficiary;
  },

  validateCurrencyType(currencyType) {
    if (currencyType.toLowerCase() === currencyTypes.FIAT)
      throw new COAError(errors.project.ProjectIsNotFundedCrypto);
  },

  async getProjectTransactions({ projectId, type }) {
    logger.info('[ProjectService] :: Entering getProjectTransactions method');
    const project = await checkExistence(this.projectDao, projectId, 'project');
    const { currencyType, currency, additionalCurrencyInformation } = project;
    this.validateCurrencyType(currencyType);
    return this.blockchainService.getTransactions({
      currency,
      address: additionalCurrencyInformation.trim(),
      type
    });
  },

  buildChangelogFilters(projects, paramObj) {
    const filters = {};

    // Filter changelogs by milestone if present
    if (paramObj.milestone) {
      filters.milestoneId = Number.parseInt(paramObj.milestone, 10);
      projects.forEach(project => {
        project.milestones.forEach(milestone => {
          if (milestone.id === Number.parseInt(paramObj.milestone, 10)) {
            filters.parentMilestoneId = milestone.parent;
          }
        });
      });
    }

    // Filter changelogs by activity if present
    if (paramObj.activity) {
      filters.activityId = Number.parseInt(paramObj.activity, 10);
      projects.forEach(project => {
        project.milestones.forEach(milestone => {
          milestone.activities.forEach(activity => {
            if (activity.id === Number.parseInt(paramObj.activity, 10)) {
              filters.parentActivityId = activity.parent;
            }
          });
        });
      });
    }

    // Filter changelogs by evidenceId if present
    if (paramObj.evidence) {
      filters.evidenceId = Number.parseInt(paramObj.evidence, 10);
      projects.forEach(project => {
        project.milestones.forEach(milestone => {
          milestone.activities.forEach(activity => {
            activity.evidences.forEach(evidence => {
              if (evidence.id === Number.parseInt(paramObj.evidence, 10)) {
                filters.parentEvidenceId = evidence.parent;
              }
            });
          });
        });
      });
    }

    return filters;
  },

  async getProjectChangelog(paramObj) {
    logger.info('[ProjectService] :: Entering getProjectChangelog method');

    // Get project
    const mainProject = await this.projectDao.getProjectWithAllData(
      paramObj.project
    );

    // Get all project clones
    const projects = [mainProject];

    const childrenProjects = await this.projectDao.getAllProjectsByParentIdWithAllData(
      paramObj.project
    );

    childrenProjects.forEach(project => projects.push(project));

    // Get changelogs
    return await this.changelogService.getChangelog(
      paramObj.project,
      childrenProjects.map(project => project.id),
      this.buildChangelogFilters(projects, paramObj)
    );
  },

  async updateProjectStatus({
    user,
    projectId,
    newStatus,
    validateAction,
    action
  }) {
    logger.info('[ProjectService] :: Entering updateProjectStatus method');

    validateRequiredParams({
      method: 'updateProjectStatus',
      params: { projectId, user }
    });

    const project = await checkExistence(this.projectDao, projectId, 'project');

    this.validateProjectStep({
      project,
      step: PROJECT_STEPS.OPEN_REVIEW_PROJECT
    });

    logger.info(
      `[Project Service] :: Validate project ${projectId} status transition from ${
        project.status
      } to  review`
    );

    await validateProjectStatusChange({
      user,
      newStatus,
      project
    });

    logger.info(
      `[Project Service] :: Validate if user ${
        user.id
      } has beneficiary or investor role`
    );

    if (!user.isAdmin) {
      await validateAction();
    }

    const isSendToReview = newStatus === projectStatuses.PENDING_REVIEW;
    const proposerNonce = isSendToReview ? generateNonceRandom() : null;
     // NOTE: The notarization should be generated after update status in DB.
    const projectData = await this.mapProjectToPublish(project);
    const toUpdate = isSendToReview
      ? {
          status: newStatus,
          ipfsHash: (await this.uploadProjectMetadataToIPFS({ project: { ...projectData, status: projectStatuses.IN_REVIEW }}, user.id)).metadataHash,
          proposer: user.id,
          step: PROJECT_STEPS.PENDING_SIGNATURE_AUTHORIZATION,
          isCancelRequested: project.isCancelRequested,
          proposerNonce
        }
      : { status: newStatus, step: PROJECT_STEPS.AUDITED_PROJECT_REVIEW };

    const projectUpdated = await this.updateProject(projectId, toUpdate);

    if (!isSendToReview) {
      logger.info('[ProjectService] :: About to create changelog');
      await this.changelogService.createChangelog({
        project: project.parent || projectId,
        revision: project.revision,
        action,
        user: user.id
      });
    }

    const toReturn = {
      success: !!projectUpdated
    };

    if (isSendToReview) {
      const projectParentId = project.parent;
      const proposedIpfsHash = toUpdate.ipfsHash;
      const proposerEmail = user.email;
      const toSign  = {
        projectId: projectParentId,
        proposedIpfsHash,
        proposerEmail,
        messageHash: getMessageHash(
          ['uint256', 'string', 'string', 'string'],
          [proposerNonce, projectParentId, proposedIpfsHash, proposerEmail]
        )
      };
      logger.info('[ProjectService] :: Message hash information:', toSign);
      return {
        ...toReturn,
        toSign: toSign.messageHash
      };
    }

    return toReturn;
  },

  async sendProjectToReview({ user, projectId }) {
    logger.info('[ProjectService] :: Entering sendProjectToReview method');

    const project = await checkExistence(this.projectDao, projectId, 'project');
    if (project.proposer && project.proposer !== user.id) {
      logger.error(`[ProjectService] User is not change request proposer`);
      throw new COAError(errors.project.OnlyProposerCanSendProposeProjectEditTransaction);
    }

    const userData = await this.userDao.findById(user.id);
    const currentPinStatus = userData.pinStatus;
    if (!user.isAdmin && (currentPinStatus === pinStatus.APPROVED || currentPinStatus === pinStatus.INACTIVE)) {
      logger.error(`[ProjectService] Current pin status doesn't match expected status`);
      throw new COAError(errors.user.InvalidPinStatus);
    }

    return this.updateProjectStatus({
      user,
      projectId,
      newStatus: projectStatuses.PENDING_REVIEW,
      action: ACTION_TYPE.SEND_PROJECT_TO_REVIEW,
      validateAction: async () =>
        this.userProjectService.validateUserWithRoleInProject({
          user: user.id,
          descriptionRoles: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR],
          project: projectId,
          error: errors.project.UserCanNotMoveProjectToReview
        })
    });
  },

  async cancelProjectReview({ user, projectId }) {
    logger.info('[ProjectService] :: Entering cancelProjectReview method');

    return this.updateProjectStatus({
      user,
      projectId,
      newStatus: projectStatuses.CANCELLED_REVIEW,
      action: ACTION_TYPE.CANCEL_REVIEW,
      validateAction: async () =>
        this.userProjectService.validateUserWithRoleInProject({
          user: user.id,
          descriptionRoles: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR],
          project: projectId,
          error: errors.project.UserCanNotMoveProjectToCancelReview
        })
    });
  },

  async updateProjectReview({
    userId,
    userEmail,
    approved,
    projectId,
    reason
  }) {
    logger.info('[ProjectService] :: Entering updateProjectReview method');
    const project = await checkExistence(
      this.projectDao,
      projectId,
      'project',
      this.projectDao.getProjectWithProposer(projectId)
    );
    if (!project.parent) {
      throw new COAError(errors.project.GivenProjectIsNotAClone(projectId));
    }
    if (project.status !== projectStatuses.IN_REVIEW) {
      throw new COAError(errors.project.CantUpdateReview(project.status));
    }
    
    let ipfsHash;
    if (approved) {
      const status = await this.getLastRevisionStatus(project.parent);
      ({ ipfsHash } = await this.publishProject({
        projectId,
        userId,
        userEmail,
        previousStatus: status
      }));
    } else {
      const projectData = await this.mapProjectToPublish(project);
      const { metadataHash} = await this.uploadProjectMetadataToIPFS({ project: {
        ...projectData,
        status: projectStatuses.CANCELLED_REVIEW
      }}, userId);
      ipfsHash = metadataHash;
    }

    await this.updateProject(projectId, {
      ipfsHash,
      status: approved
        ? projectStatuses.PENDING_APPROVE_REVIEW
        : projectStatuses.PENDING_CANCEL_REVIEW
    });

    const submitProjectEditAuditResultParams = {
      projectId: project.parent,
      proposalIpfsHash: project.ipfsHash,
      auditIpfsHash: ipfsHash,
      proposerAddress: project.proposer.address,
      approved
    };

    logger.info(
      '[ProjectService] :: Call submitProjectEditAuditResultParams method with following params',
      submitProjectEditAuditResultParams
    );

    let transaction;
    try {
      transaction = await coa.submitProjectEditAuditResult(
        submitProjectEditAuditResultParams
      );
      
    } catch (error) {
      await this.updateProject(projectId, {
        status: projectStatuses.IN_REVIEW,
      });
      logger.error(
        `[ProjectService] :: Error: ${error.message}`
      );
      throw new COAError(error.message)
    }

    logger.info(
      '[ProjectService] :: Information about the transaction sent',
      transaction
    );

    const action = approved
      ? ACTION_TYPE.APPROVE_REVIEW
      : ACTION_TYPE.CANCEL_REVIEW;

    const extraData = {
      isCancelRequested: project.isCancelRequested,
      ...(!approved && reason) && { reason }
    };
    logger.info('[ProjectService] :: Creating changelog');
    const changelog = await this.changelogService.createChangelog({
      project: project.parent,
      revision: project.revision,
      action,
      user: userId,
      extraData,
      transaction: transaction.hash,
      status: TX_STATUS.PENDING
    });

    logger.info('[ProjectService] :: Creating tx');
    await this.txDao.createTx({
      hash: transaction.hash,
      action,
      status: TX_STATUS.PENDING,
      data: { projectId, changelogId: changelog.id, userEmail }
    });

    return { projectId };
  },

  async updateStatusIfProjectIsComplete(projectId) {
    const isCompleted = await this.isProjectComplete(projectId);
    logger.info('[ProjectService] :: Project is complete: ', isCompleted);
    if (isCompleted) {
      await this.updateProject(projectId, {
        status: projectStatuses.COMPLETED
      });
    }
  },

  async isProjectComplete(projectId) {
    logger.info('[ProjectService] :: Entering isComplete method');
    const milestones = await this.milestoneService.getMilestonesByProject(
      projectId
    );
    return milestones.every(
      milestone => milestone.status === MILESTONE_STATUS.APPROVED
    );
  },

  async sendProjectReviewTransaction({
    user,
    projectId,
    authorizationSignature
  }) {
    logger.info(
      '[ProjectService] :: Entering sendProjectReviewTransaction method'
    );

    const userData = await this.userDao.findById(user.id);
    const currentPinStatus = userData.pinStatus;
    if (!user.isAdmin && (currentPinStatus === pinStatus.APPROVED || currentPinStatus === pinStatus.INACTIVE)) {
      logger.error(`[ProjectService] Current pin status doesn't match expected status`);
      throw new COAError(errors.user.InvalidPinStatus);
    }

    const project = await checkExistence(
      this.projectDao,
      projectId,
      'project',
      this.projectDao.getProjectWithProposer(projectId)
    );

    this.validateProjectStep({
      project,
      step: PROJECT_STEPS.PENDING_SIGNATURE_AUTHORIZATION
    });

    if (project.status !== projectStatuses.PENDING_REVIEW) {
      throw new COAError(errors.project.CantSendProposeProjectEditTransaction);
    }

    validateUsersAreEqualsOrThrowError({
      firstUserId: user.id,
      secondUserId: project.proposer.id,
      error: errors.project.OnlyProposerCanSendProposeProjectEditTransaction
    });

    const proposeProjectEditParams = {
      projectId: project.parent,
      proposedIpfsHash: project.ipfsHash,
      proposerEmail: user.email,
      proposerAddress: project.proposer.address,
      authorizationSignature,
      proposerNonce: project.proposerNonce
    };
    logger.info(
      '[ProjectService] :: Call proposeProjectEdit method with following params',
      proposeProjectEditParams
    );

    let transaction;
    try {
      transaction = await coa.proposeProjectEdit(proposeProjectEditParams);
      
    } catch (error) {
      await this.updateProject(projectId, {
        status: projectStatuses.OPEN_REVIEW,
        step: PROJECT_STEPS.OPEN_REVIEW_PROJECT
      });
      logger.error(
        `[ProjectService] :: Error: ${error.message}`
      );
      throw new COAError(error.message)
    }

    logger.info(
      '[ProjectService] :: Information about the transaction sent',
      transaction
    );

    await this.updateProject(projectId, {
      step: PROJECT_STEPS.AUDITED_PROJECT_REVIEW
    });

    const extraData = {
      isCancelRequested: project.isCancelRequested
    };
    logger.info('[ProjectService] :: About to insert changelog');
    const changelog = await this.changelogService.createChangelog({
      project: project.parent || project.id,
      revision: project.revision,
      user: user.id,
      action: ACTION_TYPE.SEND_PROJECT_TO_REVIEW,
      status: TX_STATUS.PENDING,
      transaction: transaction.hash,
      extraData
    });

    logger.info('[ProjectService] :: About to insert tx');
    await this.txDao.createTx({
      hash: transaction.hash,
      action: ACTION_TYPE.SEND_PROJECT_TO_REVIEW,
      status: TX_STATUS.PENDING,
      data: { projectId, changelogId: changelog.id, userEmail: user.email }
    });

    const toReturn = { txHash: transaction.hash };

    return toReturn;
  },

  async uploadProjectMetadataToIPFS({ project }, userId, activityId) {
    logger.info('[ProjectService] :: Entering uploadProjectFileToIPFS method');

    const lastProjectState = project.lastIPFSMetadata ? JSON.parse(project.lastIPFSMetadata) : null;
    let projectMetadata = await this.buildAuditMetadata(project, userId, lastProjectState, activityId)

    // Both properties are used to build current metadata but dont belong to the metadata.
    // We should delete them
    delete projectMetadata.projectId;
    delete projectMetadata.lastIPFSMetadata;

    // Replace id by publicId
    projectMetadata.milestones.forEach(milestone => {
      if (milestone.publicId) {
        milestone.id = milestone.publicId;
        delete milestone.publicId;        
      }

      milestone.activities.forEach(activity => {
        if (activity.publicId) {
          activity.id = activity.publicId;
          delete activity.publicId;
        }

        activity.evidences.forEach(evidence => {
          if (evidence.publicId) {
            evidence.id = evidence.publicId;
            delete evidence.publicId;
          }
        })
      })
    })
  
    projectMetadata.milestones.forEach(milestone => {
      const allActivitiesAreApproved = milestone.activities.every(
        _activity => _activity.status === ACTIVITY_STATUS.APPROVED
      );

      if (allActivitiesAreApproved) {
        milestone.status = MILESTONE_STATUS.APPROVED
      }
    })

    const allMilestonesAreApproved = projectMetadata.milestones.every(
      milestone => milestone.status === MILESTONE_STATUS.APPROVED
    );

    if (allMilestonesAreApproved) {
      projectMetadata.status = projectStatuses.COMPLETED
    }

    // snapshot of the project to track users
    const toUpdate = {
      lastIPFSMetadata: JSON.stringify(projectMetadata)
    }

    // projectMetadata.id = project.id
    const response = await this.projectDao.updateProject(toUpdate, project.projectId);
    if (!response) {
      logger.error('[ProjectService] :: Error sending update request to database');
      throw new COAError(errors.project.CantUpdateProject(projectId));
    }

    logger.info(
      '[ProjectService] :: Saving project metadata to storage service'
    );
    return {
      metadataHash: await this.storageService.saveStorageData({
      data: JSON.stringify(projectMetadata)
    }), metadataFile: projectMetadata };
  },

  async getLastRevisionStatus(projectId) {
    logger.info('[ProjectService] :: Getting project last revision status');
    const project = await this.projectDao.getLastProjectWithValidStatus(
      projectId
    );
    return project.status;
  },

  validateProjectStep({ project, step }) {
    logger.info('[ProjectService] :: Entering validateProjectStep method');
    logger.info('[ProjectService] :: Project step is:', project.step);
    if (project.step !== step) {
      throw new COAError(errors.common.InvalidStep);
    }
  },

  validateProjectType(type) {
    if (!Object.values(PROJECT_TYPES).includes(type)) {
      throw new COAError(errors.project.InvalidProjectType);
    }
  },

  async validateProjectBudget(projectId) {
    const activities = await this.activityService.getActivitiesByProject(
      projectId
    );

    const fundingActivitiesBudget = this.activityService.getActivitiesBudget({
      activities,
      type: ACTIVITY_TYPES.FUNDING
    });

    const spendingActivitiesBudget = this.activityService.getActivitiesBudget({
      activities,
      type: ACTIVITY_TYPES.SPENDING
    });

    if (!fundingActivitiesBudget.isEqualTo(spendingActivitiesBudget)) {
      throw new COAError(errors.project.InvalidActivitiesBudget);
    }
  },

  async getProjectEvidences({ projectId: id, limit }) {
    let project = await checkExistence(this.projectDao, id, 'project');
    if (!project.parent) {
      project = await checkExistence(
        this.projectDao,
        id,
        'project',
        this.projectDao.getLastPublicRevisionProject(id)
      );
    }
    const projectId = project.id;
    logger.info(
      '[ProjectService] :: Entering getProjectEvidences method with projectId ',
      projectId
    );
    const evidences = await this.activityService.getApprovedEvidencesByProject({
      projectId,
      limit
    });
    const mappedEvidences = evidences.map(_evidence => ({
      amount: _evidence.amount,
      activityType: _evidence.activity.type,
      destinationAccount: _evidence.destinationAccount,
      userName: `${_evidence.user.firstName} ${_evidence.user.lastName}`,
      date: _evidence.createdAt,
      role: _evidence.roleDescription
    }));

    const toReturn = { evidences: mappedEvidences };
    return toReturn;
  },

  async getProjectMovements({ projectId: id, page, type, withLinkedActivities }) {
    let project = await checkExistence(this.projectDao, id, 'project');

    const projectId = project.parent ? project.parent : project.id;
    logger.info(
      '[ProjectService] :: Entering getMovementsByProject method with projectId ',
      projectId
    );
    const movements = await this.movementService.getMovementsByProject(projectId, page, type, withLinkedActivities);

    const toReturn = { movements };
    return toReturn;
  },

  async validateThereAreNoPendingActivityTransactions(projectId) {
    const thereAreSomePendingActivityTransactions = await this.activityService.thereArePendingActivityTransactions(
      projectId
    );
    if (thereAreSomePendingActivityTransactions) {
      throw new COAError(errors.project.PendingTransaction);
    }
  },

  async cancelProject({ user, projectId }) {
    logger.info('[ProjectService] :: Entering cancelProject method');

    const userData = await this.userDao.findById(user.id);
    const currentPinStatus = userData.pinStatus;
    if (!user.isAdmin && (currentPinStatus === pinStatus.APPROVED || currentPinStatus === pinStatus.INACTIVE)) {
      logger.error(`[ProjectService] Current pin status doesn't match expected status`);
      throw new COAError(errors.user.InvalidPinStatus);
    }

    validateRequiredParams({
      method: 'cancelProject',
      params: { projectId, user }
    });

    if (!user.isAdmin) {
      await this.userProjectService.validateUserWithRoleInProject({
        user: user.id,
        descriptionRoles: [rolesTypes.BENEFICIARY, rolesTypes.INVESTOR],
        project: projectId,
        error: errors.project.UserCanNotRequestAProjectCancellation
      });
    }

    const project = await checkExistence(this.projectDao, projectId, 'project');

    logger.info('[ProjectService] :: Validating project status');
    if (project.status !== projectStatuses.OPEN_REVIEW) {
      throw new COAError(errors.project.InvalidStatusForCancelProject(project.status));
    }

    try {
      await this.updateProject(projectId, {
        isCancelRequested: true
      });

      logger.info('[ProjectService] :: About to insert changelog');
      await this.changelogService.createChangelog({
        project: project.parent || project.id,
        revision: project.revision,
        user: user.id,
        action: ACTION_TYPE.SEND_CANCEL_TO_REVIEW,
        extraData: { isCancelRequested: true }
      });

      ACTION_TYPE.SEND_PROJECT_TO_REVIEW
      return { projectId }
    } catch (error) {
      await this.updateProject(projectId, {
        isCancelRequested: false
      });

      throw error;
    }
  },

  async buildAuditMetadata(project, userId, lastProjectState, activityId) {
    logger.info('[ProjectService] :: buildAuditMetadata');
    const users = await this.getUsersByProjectId({ projectId: project.projectId });


    const milestones = await this.mapMilestonesToPublish(project.milestones, project.projectId, userId, lastProjectState && lastProjectState.milestones, activityId)
    for await (const milestone of milestones) {
      const lastMilestoneState = lastProjectState && lastProjectState.milestones.find(m => m.id === milestone.publicId);

      await this.getEvidences(milestone.activities, project.projectId, userId, lastMilestoneState && lastMilestoneState.activities)
    }

    return {
      ...project,
      users: await this.mapUsersToPublish(users, project.projectId),
      milestones,
    }
  },

  async getEvidences(activities, projectId, userId, lastActivitiesState) {
    const evidences = activities.map(activity => {
      logger.info(`[ProjectService] :: getEvidences activityId: ${activity.id}`);
      return this.activityEvidenceDao.getEvidencesByActivityId(
        activity.id
      );
    })
    
    const ev = await Promise.all(evidences);

    const mappedEvidences = await Promise.all(
      activities.map(async (activity, i) => {
        const lastActivityState = lastActivitiesState && lastActivitiesState.find(lastActivity => lastActivity.id === activity.publicId);
        activity.evidences = await this.mapEvidences(ev[i], projectId, userId, lastActivityState && lastActivityState.evidences);
      })
    )

    return mappedEvidences
  },

  async mapEvidences(evidences, projectId, userId, lastEvidencesState) {
    const mappedEvidences = await Promise.all(
      evidences.map(async (evidence) => {
        const lastEvidenceState = lastEvidencesState && lastEvidencesState.find(lastEvidence => lastEvidence.id === evidence.publicId);

        let auditor = null;
        let createdBy = null;
        let createdAt = evidence.createdAt
        let auditedAt = evidence.auditedAt
        
        if (lastEvidenceState) {
          if (lastEvidenceState.auditor) {
            auditor = lastEvidenceState.auditor
          } else if (evidence.auditor) {
            auditor = await this.mapUserToPublish(await this.userService.getUserById(userId), projectId);
          }

          if (lastEvidenceState.createdBy) {
            createdBy = lastEvidenceState.createdBy
          }
          
          if (lastEvidenceState.createdAt) {
            createdAt = lastEvidenceState.createdAt
          }

          if (lastEvidenceState.auditedAt) {
            auditedAt = lastEvidenceState.auditedAt
          }
        }

        // CreatedBy must always exist. In case lastEvidenceState doesnt exist we take the value from db
        if (createdBy === null) {
          const createdUser = await this.userService.getUserById(evidence.user);
          createdBy = await this.mapUserToPublish({ ...createdUser, role: { description: evidence.roleDescription }}, projectId)
        }

        logger.info(`[ProjectService] :: mapEvidences evidenceId: ${evidence.id}`);
        const mappedFile = await Promise.all(
          evidence.files.map(async (file) => {
            const fileRecord = await this.fileDao.getFileById(file.file);
            return { hash: fileRecord.hash, path: fileRecord.path }
          })
        )

        const mappedEvidence = {
          id: evidence.id,
          publicId: evidence.publicId,
          title: evidence.title,
          description: evidence.description,
          type: evidence.type,
          status: evidence.status,
          createdAt,
          createdBy,
          amount: evidence.amount,
          destinationAccount: evidence.destinationAccount,
          auditedAt,
          auditor,
          hashEvidence: mappedFile.map(file => file.hash),
          linkEvidence: mappedFile.map(file => `${process.env.NEXT_PUBLIC_URL_HOST}${file.path}` )
        }
        return mappedEvidence;
      })
      
    )
    const mappedEvidencesSorted = this.sortById(mappedEvidences);
    return mappedEvidencesSorted;
  },

  mapActivityStatusForAudit(activityStatus) {
    const map = {
      [ACTIVITY_STATUS.APPROVED]: ACTIVITY_STATUS.APPROVED,
      [ACTIVITY_STATUS.IN_PROGRESS]: ACTIVITY_STATUS.IN_PROGRESS,
      [ACTIVITY_STATUS.IN_REVIEW]: 'in review',
      [ACTIVITY_STATUS.NEW]: ACTIVITY_STATUS.NEW,
      [ACTIVITY_STATUS.PENDING_TO_APPROVE]: ACTIVITY_STATUS.APPROVED,
      [ACTIVITY_STATUS.PENDING_TO_REJECT]: ACTIVITY_STATUS.REJECTED,
      [ACTIVITY_STATUS.PENDING_TO_REVIEW]: 'in review',
      [ACTIVITY_STATUS.REJECTED]: ACTIVITY_STATUS.REJECTED
    };
    return map[activityStatus];
  },

  sortById(arr) {
    return arr.sort((p1, p2) => (p1.id > p2.id) ? 1 : (p1.id < p2.id) ? -1 : 0)
  }
};
