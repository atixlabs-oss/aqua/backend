/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const axios = require('axios');
const COAError = require('../errors/COAError');
const errors = require('../errors/exporter/ErrorExporter');
const logger = require('../logger');
const { txTypes } = require('../util/constants');
const { dateFormat } = require('../util/dateFormatters');
const tokenService = require('./tokenService');

const fetchGetAPI = async ({ apiBaseUrl, queryParams, errorToThrow }) => {
  const response = await axios
    .get(`${apiBaseUrl}?${queryParams}`)
    .catch(error => {
      logger.error(
        '[BlockchainService] :: Error when fetch external API',
        error
      );
      throw new COAError(errorToThrow);
    });

  if (!response.data.result) {
    logger.error(
      '[BlockchainService] :: Result of fetch API is null or undefined'
    );
    throw new COAError({ message: response.data.message });
  }

  return response;
};

const fetchGetTransactions = async ({ queryParams, tokenSymbol }) => {
  logger.info('[BlockchainService] :: Entering fetchGetTransactions method');
  const token = await tokenService.getTokenBySymbol(tokenSymbol);
  if (!token) throw new COAError(errors.token.TokenNotFound);
  const contractAddressQueryParam = token.contractAddress
    ? `&contractaddress=${token.contractAddress}`
    : '';

  const response = await fetchGetAPI({
    apiBaseUrl: token.apiBaseUrl,
    queryParams: `${queryParams}${contractAddressQueryParam}`,
    errorToThrow: errors.transaction.CanNotGetTransactions
  });
  return response.data.result
    .filter(
      transaction =>
        transaction.value !== '0' &&
        (transaction.error === undefined || transaction.isError === '0')
    )
    .map(transaction => ({
      ...transaction,
      tokenSymbol: token.symbol,
      decimals: token.decimals
    }));
};

const getTransactionsWithToken = async ({ address, token }) => {
  logger.info(`[BlockchainService] :: Entering getTransactions method for ${token} token`);

  const { action } = await tokenService.getTokenBySymbol(token);

  return fetchGetTransactions({
    queryParams: `module=account&action=${action}&address=${address}&sort=desc&page=0&offset=100`,
    tokenSymbol: token
  })
}

const fetchGetTransaction = async ({ txHash, tokenSymbol }) => {
  logger.info('[BlockchainService] :: Entering fetchGetTransactions method');
  const token = await tokenService.getTokenBySymbol(tokenSymbol);
  if (!token) throw new COAError(errors.token.TokenNotFound);

  const response = await fetchGetAPI({
    apiBaseUrl: token.apiBaseUrl,
    queryParams: `module=transaction&action=gettxinfo&txhash=${txHash}`,
    errorToThrow: errors.transaction.CanNotGetTransaction(txHash)
  });

  const transactionWithTokenInfo = {
    ...response.data.result,
    tokenSymbol: token.symbol,
    decimals: token.decimals
  };

  return transactionWithTokenInfo;
};

const filterByType = ({ transactions, address, type }) => {
  logger.info('[BlockchainService] :: Entering filterByType method');
  const isSentType = type === txTypes.SENT;
  return transactions.filter(
    transaction =>
      (isSentType
        ? transaction.from.toLowerCase()
        : transaction.to.toLowerCase()) === address.toLowerCase()
  );
};

const formatTransaction = ({
  hash,
  value,
  timeStamp,
  decimals,
  tokenSymbol,
  from,
  to
}) => ({
  txHash: hash,
  value: Number(value) / 10 ** decimals,
  tokenSymbol,
  from,
  to,
  timestamp: dateFormat(timeStamp)
});

const formatTransactions = transactions => {
  logger.info('[BlockchainService] :: Entering formatTransactions method');
  return transactions.map(formatTransaction);
};

module.exports = {
  async getTransactions({ currency, address, type }) {
    logger.info('[BlockchainService] :: Entering getTransactions method');
    logger.info('[BlockchainService] :: About params to get transactions', {
      currency,
      address,
      type
    });

    const transactions = await getTransactionsWithToken({ address, token: currency });
    logger.info(
      `[BlockchainService] :: ${transactions.length} transactions were obtained`
    );
    if (!type) return { transactions: formatTransactions(transactions) };
    const transactionsFiltered = filterByType({ transactions, address, type });
    return { transactions: formatTransactions(transactionsFiltered) };
  },
  async getTransaction({ currency, txHash }) {
    logger.info('[BlockchainService] :: Entering getTransaction method');
    logger.info('[BlockchainService] :: About params to get transactions', {
      currency,
      txHash
    });
    const transaction = await fetchGetTransaction({
      tokenSymbol: currency,
      txHash
    });
    logger.info(`[BlockchainService] :: Transaction ${txHash} was obtained`);
    return formatTransaction(transaction);
  },

  async isNativeToken(symbol) {
    logger.info(
      `[BlockchainService] :: Entering isNativeToken method with symbol ${symbol}`
    );
    const token = await tokenService.getTokenBySymbol(symbol);
    return !token.contractAddress;
  }
};
