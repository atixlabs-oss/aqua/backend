/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../logger');
const Joi = require('joi');
const configs = require('config');
const { MOVEMENT_TYPES, evidenceStatus } = require('../util/constants');

module.exports = {
  async registerMovements(movements, middlewareId) {
    logger.info('[MovementService] :: Entering registerMovements method');

    // Format validation
    const movementValidationSchema = Joi.object({
      uid: Joi.string().required(),
      txId: Joi.string().max(configs.movement.validationLimits.txid).required(),
      amount: Joi.number().positive().precision(configs.movement.validationLimits.decimals).required(),
      movementType: Joi.number().valid(0, 1).required(),
      accountId: Joi.string().max(configs.movement.validationLimits.accounts).required(),
      otherAccount: Joi.string().max(configs.movement.validationLimits.accounts).required(),
      date: Joi.date().iso().required()
    });
    const movementsValidationSchema = Joi.array().unique('uid').items(movementValidationSchema);
    const { error } = movementsValidationSchema.validate(movements);
    if (error) throw new Error(`[MovementService] :: There was an error: ${error}`);

    // Definition of movement filters
    const middlewareAccounts = await this.accountService.getAccountsByMiddleware(middlewareId);
    const filterMiddlewareAccountMovements = (movement) => {
      return middlewareAccounts.map(account => account.id).includes(movement.accountId);
    };
    let filteredMovements = movements.filter(movement => filterMiddlewareAccountMovements(movement));
    const movementsAccountIds = filteredMovements.map(movement => movement.accountId);
    const movementsExternalIds = filteredMovements.map(movement => movement.uid);
    const duplicateMovements = await this.movementDao.findMovements({
      accountId: {
        in: movementsAccountIds
      },
      externalId: {
        in: movementsExternalIds
      }
    });
    const filterDuplicateMovements = (movement) => {
      return duplicateMovements.some((dupMovement) => {
        return movement.accountId == dupMovement.accountId && movement.uid == dupMovement.externalId
      });
    };
   filteredMovements = filteredMovements.filter(movement => !filterDuplicateMovements(movement));
   
    // Register filtered movements (sequential to avoid multiple DB connections)
    for(const movement of filteredMovements) {
      const movDate = new Date(movement.date);
      const m = {
        externalId: movement.uid,
        displayId: movement.txId,
        amount: movement.amount,
        type: MOVEMENT_TYPES[movement.movementType],
        accountId: movement.accountId,
        otherAccount: movement.otherAccount,
        date: movDate.toISOString()
      };
      await this.movementDao.createMovement(m);
    }
    const unregisteredMovementsByAccount = movements
    .filter(movement => !filterMiddlewareAccountMovements(movement));
    const unregisteredMovementsByDuplication = movements
      .filter(movement => filterDuplicateMovements(movement));
    if (unregisteredMovementsByAccount.length > 0 || unregisteredMovementsByDuplication.length > 0) {
      logger.warn(`[MovementService] :: The following movements were weren't registered`);
      logger.warn(`[MovementService] :: Due to wrong account:`);
      logger.warn(unregisteredMovementsByAccount);
      logger.warn(`[MovementService] :: Due to duplicate movement:`);
      logger.warn(unregisteredMovementsByDuplication);
    }
    return;
  },
  async getMovementsByProject(projectId, page, type, withLinkedActivities) {
    logger.info('[MovementService] :: Entering getMovementsByProject method');
    const accounts = await this.accountService.getAccountsByProject(projectId);
    let movementsWithActivities;
    if (withLinkedActivities != undefined) {
      const evidences = await this.activityEvidenceDao.getEvidencesBy({
        status: { in: [evidenceStatus.APPROVED, evidenceStatus.NEW] }
      });
      movementsWithActivities = evidences.map(evidence => evidence.movement);
    }
    // WARNING: The following solution trades off proper query pagination for date filtering.
    // In order to allow fetching movements from multiple accounts with different dates,
    // proper query pagination must be sacrificed to accept date filtering.
    const movements = await Promise.all(accounts.map(async (account) => {
      let where = {
        accountId: account.id,
        date: {
          '>=': account.dateFrom,
          '<=': account.dateTo
        }
      };
      if (withLinkedActivities != undefined) {
        const id = withLinkedActivities === "true" ? { in: movementsWithActivities } : { nin: movementsWithActivities };
        where = {
          ...where,
          id
        }
      }
      if (type && MOVEMENT_TYPES[type]) {
        where = {
          ...where,
          type: MOVEMENT_TYPES[type]
        };
      }
      return this.movementDao.getMovementByProject(where, page);
    }));
    return movements.flat();
  }
};
