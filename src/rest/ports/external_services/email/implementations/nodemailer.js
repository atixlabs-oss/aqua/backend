/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const nodemailer = require('nodemailer');
const logger = require('../../../../logger');
const { EmailInterface } = require('../interface');

class NodeMailerConnection extends EmailInterface {
    client = null;
    constructor(host, port, name, user, pass) {
        super();
        try {
            this.client = nodemailer.createTransport({
                host,
                port,
                name,
                secure: true,
                requireTLS: true,
                auth: {
                  user,
                  pass
                }
            });
        } catch (error) {
            logger.error(`[EmailClient] :: Error connecting to nodemailer. `, error);
            throw Error(error);
        }
    }

    sendMail(args) {
        if (this.client.transporter) {
            return this.client.sendMail(args);
        }
        logger.error(`[EmailClient] :: Email Client transport not working`);
        throw Error("Error sending mail");
    }

    isNodeMailer() {
        return true;
    }
}

module.exports = {
    NodeMailerConnection
}