/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../../../../logger');
const { EmailInterface } = require('../interface');

class PrintConnection extends EmailInterface {
    sendMail(args) {
        logger.info('Email links: ');
        logger.info(args.html.split("\n").filter((line) => line.includes("http")));
        return {};
    }

    isNodeMailer() {
        return false;
    }
}

module.exports = {
    PrintConnection
}