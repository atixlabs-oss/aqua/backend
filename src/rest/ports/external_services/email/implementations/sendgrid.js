/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// import { setApiKey, send } from '@sendgrid/mail';
const sendgrid = require('@sendgrid/mail');
const { EmailInterface } = require('../interface');

class SendgridConnection extends EmailInterface {
    constructor(apiKey) {
        super();
        if (apiKey) {
            sendgrid.setApiKey(apiKey);
        } else {
            throw Error("Missing API Key");
        }
    }

    sendMail(args) {
        return sendgrid.send(args);
    }

    isNodeMailer() {
        return false;
    }
}

module.exports = {
    SendgridConnection
}