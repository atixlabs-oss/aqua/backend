/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const config = require('config');
const { SendgridConnection } = require('./implementations/sendgrid');
const { EmptyConnection } = require('./implementations/empty');
const { PrintConnection } = require('./implementations/print');
const { NodeMailerConnection } = require('./implementations/nodemailer');
const { connectionType, host, port, user, pass, apiKey, name } = config.email;

module.exports = {
    getEmailConnection() {
        switch (connectionType) {
            case 'sendgrid':
                return new SendgridConnection(apiKey);
            case 'nodemailer':
                return new NodeMailerConnection(host, port, name, user, pass);
            case 'print':
                return new PrintConnection();
            case 'disabled':
                return new EmptyConnection();
            default:
                throw Error("Email connection type missing.")
        }
    }
};