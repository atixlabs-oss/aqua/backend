/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
global.TextEncoder = require('util').TextEncoder;
global.TextDecoder = require('util').TextDecoder;
const pinataSDK = require('@pinata/sdk');
const { v4: uuidv4 } = require('uuid');
const tmp = require('tmp');
const fs = require('fs');
const filesUtil = require('../../../../util/files');

const logger = require('../../../../logger');
const { IPFSStorageInterface } = require('../interface');

class PinataConnection extends IPFSStorageInterface {
    pinata = null;
    constructor(apiKey, secretKey) {
        super();
        if (apiKey && secretKey) {
            this.pinata = new pinataSDK({ pinataApiKey: apiKey, pinataSecretApiKey: secretKey });
        } else {
            throw Error("Missing API Keys");
        }
    }

    async pinFileToIPFS(name, file) {
        const options = { pinataMetadata: { name: name } };
        logger.info(`[Pinata IPFS] About to upload image to IPFS, with name ${name}`);
      
        try {
          // This workaround has to be done since Pinata SDK isn't prepared to upload buffers
          // so it complains if a filename is not provided.
          // Ref: https://github.com/PinataCloud/Pinata-SDK/issues/28#issuecomment-816439078
          file.path = uuidv4();
          const { IpfsHash } = await this.pinata.pinFileToIPFS(file, options);
          logger.info(`[Pinata IPFS] Upload image to IPFS was successful, with hash ${IpfsHash}`);
          return IpfsHash;
        } catch (error) {
          logger.error(
            `[Pinata IPFS] There was an error trying to upload file to IPFS: ${error}`
          );
          throw error
        }
      }

    async generateStorageHash(data) {
        logger.info('[Pinata IPFS] :: Entering generateStorageHash method');

        if (typeof data === 'string') {
            // data is a string
            const {name : path, removeCallback} = tmp.fileSync();
            fs.writeFileSync(path, data);

            const name = uuidv4();
            const file = filesUtil.getFileFromPath(path);

            const ipfsHash = await this.pinFileToIPFS(name, file);

            removeCallback();
            return ipfsHash;
        } else {
            // data is a file
            const name = data.path;
            const readableStream = data;

            return await this.pinFileToIPFS(name, readableStream)
        }
    }

    async getStorageData(fileHash) {
        logger.info('[Pinata IPFS] :: Entering getStorageData method');
        try {
            const filters = {
                hashContains: fileHash
            }

            const retrievedData = await this.pinata.pinList(filters);
            return retrievedData;
        } catch (error) {
            logger.error('[Pinata IPFS] :: An error has occurred', error);
            throw error;
        }
    }
}

module.exports = {
    PinataConnection
}