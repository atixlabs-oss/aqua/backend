/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const mailService = require('./services/mailService');
const userService = require('./services/userService');
const projectService = require('./services/projectService');
const countryService = require('./services/countryService');
const fileService = require('./services/fileService');
const activityService = require('./services/activityService');
const userProjectService = require('./services/userProjectService');
const milestoneService = require('./services/milestoneService');
const storageService = require('./services/storageService');
const roleService = require('./services/roleService');
const evidenceFileService = require('./services/evidenceFileService');
const blockchainService = require('./services/blockchainService');
const tokenService = require('./services/tokenService');
const changelogService = require('./services/changelogService');
const txService = require('./services/txService');
const movementService = require('./services/movementService');
const middlewareService = require('./services/middlewareService');
const accountService = require('./services/accountService');

const cronjobService = require('./services/cronjob/cronjobService');
const emailClient = require('./services/helpers/emailClient');

const milestoneBudgetStatusDao = require('./dao/milestoneBudgetStatusDao');
const projectDao = require('./dao/projectDao');
const countryDao = require('./dao/countryDao');
const fileDao = require('./dao/fileDao');
const activityDao = require('./dao/activityDao');
const userProjectDao = require('./dao/userProjectDao');
const milestoneDao = require('./dao/milestoneDao');
const userDao = require('./dao/userDao');
const passRecoveryService = require('./services/passRecoveryService');
const passRecoveryDao = require('./dao/passRecoveryDao');
const activityEvidenceDao = require('./dao/activityEvidenceDao');
const transactionDao = require('./dao/transactionDao');
const userWalletDao = require('./dao/userWalletDao');
const roleDao = require('./dao/roleDao');
const evidenceFileDao = require('./dao/evidenceFileDao');
const tokenDao = require('./dao/tokenDao');
const changelogDao = require('./dao/changelogDao');
const txDao = require('./dao/txDao');
const walletHistoryDao = require('./dao/walletHistoryDao');
const accountDao = require('./dao/accountDao');
const movementDao = require('./dao/movementDao');
const middlewareDao = require('./dao/middlewareDao');

const { injectDependencies } = require('./util/injection');

module.exports = fastify => {
  // Injects a model into a dao instance as the property `model`
  const injectModel = (daoInstance, model) => {
    injectDependencies(daoInstance, { model });
  };

  function configureFileService(service) {
    const dependencies = {
      fileDao
    };

    injectDependencies(service, dependencies);
  }

  // Configure the mail service.
  function configureMailService(service) {
    const dependencies = {
      emailClient,
      projectService
    };

    injectDependencies(service, dependencies);
  }

  function configureUserService(service) {
    const dependencies = {
      userDao,
      userWalletDao,
      mailService,
      projectService,
      countryService,
      userProjectDao,
      passRecoveryDao,
      userProjectService,
      projectDao,
      roleService,
      walletHistoryDao
    };

    injectDependencies(service, dependencies);
  }

  function configureCountryService(service) {
    const dependencies = {
      countryDao
    };

    injectDependencies(service, dependencies);
  }

  function configureProjectService(service) {
    const dependencies = {
      fileServer: fastify.configs.fileServer,
      projectDao,
      milestoneService,
      userService,
      activityService,
      cronjobService,
      mailService,
      userProjectDao,
      roleDao,
      userDao,
      activityDao,
      storageService,
      userProjectService,
      roleService,
      activityEvidenceDao,
      milestoneDao,
      blockchainService,
      changelogService,
      evidenceFileDao,
      txDao,
      fileDao,
      walletHistoryDao,
      movementService
    };

    injectDependencies(service, dependencies);
  }
  // TODO: delete unnecessary dependencies
  function configureActivityService(service) {
    const dependencies = {
      activityDao,
      activityEvidenceDao,
      userProjectDao,
      roleDao,
      fileService,
      activityFileDao: undefined,
      userService,
      milestoneService,
      projectService,
      roleService,
      evidenceFileService,
      userProjectService,
      storageService,
      milestoneDao,
      blockchainService,
      projectDao,
      changelogService,
      userWalletDao,
      txService,
      userDao,
      movementDao,
      accountService
    };
    injectDependencies(service, dependencies);
  }

  function configureUserProjectService(service) {
    const dependencies = {
      userProjectDao,
      userDao,
      roleDao,
      projectDao,
      roleService,
      changelogService,
      milestoneDao
    };

    injectDependencies(service, dependencies);
  }

  function configurePassRecoveryService(service) {
    const dependencies = {
      mailService,
      passRecoveryDao,
      userDao,
      userWalletDao,
      userProjectDao,
      projectDao
    };
    injectDependencies(service, dependencies);
  }

  function configureMilestoneService(service) {
    const dependencies = {
      milestoneDao,
      activityService,
      milestoneBudgetStatusDao,
      projectService,
      userService,
      activityEvidenceDao,
      changelogService,
      projectDao,
      userProjectService
    };
    injectDependencies(service, dependencies);
  }

  function configureCronjobService(service) {
    const dependencies = {
      projectService,
      activityService,
      mailService
    };
    injectDependencies(service, dependencies);
  }

  function configureRoleService(service) {
    const dependencies = {
      roleDao
    };

    injectDependencies(service, dependencies);
  }

  function configureEvidenceFileService(service) {
    const dependencies = {
      evidenceFileDao
    };

    injectDependencies(service, dependencies);
  }

  function configureTokenService(service) {
    const dependencies = {
      tokenDao
    };

    injectDependencies(service, dependencies);
  }

  function configureChangelogService(service) {
    const dependencies = {
      userProjectService,
      changelogDao,
      userService,
      milestoneService,
      projectService,
      activityService
    };

    injectDependencies(service, dependencies);
  }

  function configureTxService(service) {
    const dependencies = {
      txDao,
      changelogService,
      mailService
    };

    injectDependencies(service, dependencies);
  }

  function configureMovementService(service) {
    const dependencies = {
      movementDao,
      accountService,
      activityEvidenceDao
    };

    injectDependencies(service, dependencies);
  }

  function configureMiddlewareService(service) {
    const dependencies = {
      middlewareDao
    };

    injectDependencies(service, dependencies);
  }

  function configureAccountService(service) {
    const dependencies = {
      accountDao
    };
    injectDependencies(service, dependencies);
  }

  function configureDAOs(models) {
    injectModel(userDao, models.user);
    injectModel(fileDao, models.file);
    injectModel(milestoneDao, models.milestone);
    injectModel(projectDao, models.project);
    injectModel(countryDao, models.country);
    injectModel(milestoneBudgetStatusDao, models.milestoneBudgetStatus);
    injectModel(passRecoveryDao, models.pass_recovery);
    injectModel(activityDao, models.activity);
    // TODO: delete this when dao and model deleted
    injectModel(activityEvidenceDao, models.activity_evidence);
    injectModel(transactionDao, models.transaction);
    injectModel(userWalletDao, models.user_wallet);
    injectModel(userProjectDao, models.user_project);
    injectModel(roleDao, models.role);
    injectModel(evidenceFileDao, models.evidence_file);
    injectModel(tokenDao, models.token);
    injectModel(changelogDao, models.changelog);
    injectModel(txDao, models.tx);
    injectModel(walletHistoryDao, models.wallet_history);
    injectModel(accountDao, models.account);
    injectModel(movementDao, models.movement);
    injectModel(middlewareDao, models.middleware);
  }

  function configureServices() {
    configureMailService(mailService);
    configureUserProjectService(userProjectService);
    configureUserService(userService);
    configureCountryService(countryService);
    configureMilestoneService(milestoneService);
    configureProjectService(projectService);
    configureFileService(fileService);
    configureActivityService(activityService);
    configurePassRecoveryService(passRecoveryService);
    configureCronjobService(cronjobService);
    configureRoleService(roleService);
    configureEvidenceFileService(evidenceFileService);
    configureTokenService(tokenService);
    configureChangelogService(changelogService);
    configureTxService(txService);
    configureMovementService(movementService);
    configureMiddlewareService(middlewareService);
    configureAccountService(accountService);
  }

  function init({ models }) {
    configureDAOs(models);
    configureServices();
  }

  init(fastify);
};
