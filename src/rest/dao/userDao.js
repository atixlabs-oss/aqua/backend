/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const uuid = require('uuid');

module.exports = {
  async findById(id) {
    const user = await this.model.findOne({ id }).populate('wallets', {
      where: { active: true }
    });
    if (!user) {
      return;
    }
    if (!user.wallets.length) {
      return { withNoWallets: true, ...user };
    }
    const { address, encryptedWallet} = user.wallets[0];
    delete user.address;
    delete user.encryptedWallet;
    delete user.wallets;
    return { address, encryptedWallet, ...user };
  },

  async getUserByEmail(email) {
    const user = await this.model
      .findOne({ email, blocked: false })
      .populate('roles')
      .populate('wallets', {
        where: { active: true }
      });
    if (!user) {
      return;
    }
    if (!user.wallets.length) {
      return { withNoWallets: true, ...user };
    }
    const { address, encryptedWallet } = user.wallets[0];
    delete user.address;
    delete user.encryptedWallet;
    delete user.wallets;
    return { address, encryptedWallet, ...user };
  },

  async getUserByAPIKey(apiKey) {
    const user = await this.model
      .findOne({ apiKey, blocked: false })
      .populate('roles')
      .populate('wallets', {
        where: { active: true }
      });
    if (!user) {
      return;
    }
    if (!user.wallets.length) {
      return { withNoWallets: true, ...user };
    }
    const { address, encryptedWallet } = user.wallets[0];
    delete user.address;
    delete user.encryptedWallet;
    delete user.wallets;
    return { address, encryptedWallet, ...user };
  },

  async createUser(user) {
    return this.model.create({
      id: uuid.v4(),
      role: 'entrepreneur',
      ...user
    });
  },

  async getFollowedProjects(id) {
    return this.model.findOne({ id }).populate('following');
  },

  async getAppliedProjects(id) {
    return this.model
      .findOne({ id })
      .populate('funding')
      .populate('monitoring');
  },

  async updateUser(id, user) {
    const updatedUser = await this.model.updateOne({ id }).set({ ...user });
    return updatedUser;
  },

  async updateUserByEmail(email, user) {
    const updatedUser = await this.model
      .updateOne({ where: { email: email.toLowerCase() } })
      .set({ ...user });
    return updatedUser;
  },

  async updatePasswordByMail(email, pwd) {
    return this.model
      .updateOne({ where: { email: email.toLowerCase() } })
      .set({ pwd });
  },

  /* eslint-disable no-param-reassign */
  async getUsers() {
    const users = await this.model
      .find({
        where: {
          blocked: false
        }
      })
      .populate('roles')
      .populate('wallets', {
        where: { active: true }
      });
    return users.map(user => {
      if (!user.wallets.length) {
        return user;
      }
      const { address, encryptedWallet } = user.wallets[0];
      delete user.address;
      delete user.encryptedWallet;
      delete user.wallets;
      return { address, encryptedWallet, ...user };
    });
  },

  async getPaginatedUsers(page, filterPinStatus) {
    const where = {
      isAdmin: false
    };

    if (filterPinStatus) {
      where['pinStatus'] = filterPinStatus
    };

    const pageSize = 10;
    const dbUsers = await this.model
      .find({
        where,
        limit: pageSize,
        skip: (page - 1) * pageSize
      }).sort('email ASC');
    return { dbUsers, pageSize };
  },

  async countUsers(countConditions = {}) {
    const users = await this.model.count({
      where: {
        isAdmin: false,
        ...countConditions
      },
    });

    return users;
  },

  async updateTransferBlockchainStatus(userId, status) {
    return this.model
      .updateOne({ id: userId })
      .set({ transferBlockchainStatus: status });
  },

  async removeUserById(id) {
    return this.model.destroy({ id });
  },

  async getUsersByProject(projectId) {
    return this.model
      .find({ blocked: false })
      .populate('wallets')
      .populate('roles', {
        where: { project: projectId }
      });
  },

  async findByUserProject({ roleId, projectId }) {
    return this.model.find({ blocked: false }).populate('roles', {
      where: { project: projectId, role: roleId }
    });
  },

  async getUsersByIds(userIds) {
    return this.model.find({
      id: { in: userIds }
    });
  },

  async getProfileData(id) {
    const user = await this.model.findOne({ id })
    .populate('country')
 
    if (!user) {
      return;
    }

    return user;
  },

  async findAdmins() {
    return this.model.find().where({ isAdmin : true })
  }
};
