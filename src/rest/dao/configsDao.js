/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const coaAccountAddressKey = 'coa_bank_account_address';
const coaAccountBankKey = 'coa_bank_account_bank_name';
const coaAccountOwnerKey = 'coa_bank_account_owner_name';

const ConfigsDao = ({ configsModel }) => ({
  async getCoaBankAccount() {
    const response = {};
    response.address = (await configsModel.findByKey({
      key: coaAccountAddressKey
    })).value;
    response.bank = (await configsModel.findByKey({
      key: coaAccountBankKey
    })).value;
    response.owner = (await configsModel.findByKey({
      key: coaAccountOwnerKey
    })).value;
    return response;
  }
});

module.exports = ConfigsDao;
