/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const uuid = require('uuid');

module.exports = {
  async findById(milestoneId) {
    const milestone = await this.model.findOne({
      id: milestoneId,
      deleted: false
    });
    return milestone;
  },
  async getMilestoneByIdWithProject(milestoneId) {
    const milestone = await this.model
      .findOne({ id: milestoneId, deleted: false })
      .populate('project');

    return milestone;
  },
  async getMilestonesByProjectId(project) {
    const milestones = await this.model
      .find({ project, deleted: false })
      .populate('tasks', { where: { deleted: false }, sort: 'id ASC' })
      .sort('id ASC');
    return milestones || [];
  },
  async saveMilestone({ milestone, projectId }) {
    const toSave = {
      ...milestone,
      project: projectId,
      publicId: uuid.v4()
    };
    const createdMilestone = await this.model.create(toSave);
    return createdMilestone;
  },
  async createMilestone(milestone) {
    const createdMilestone = await this.model.create(milestone);
    return createdMilestone;
  },
  async updateMilestone(milestone, milestoneId) {
    const toUpdate = { ...milestone };

    const savedMilestone = await this.model
      .updateOne({ id: milestoneId, deleted: false })
      .set({ ...toUpdate });

    return savedMilestone;
  },
  async deleteMilestone(milestoneId) {
    const deleted = await this.model
      .updateOne({ id: milestoneId })
      .set({ deleted: true });
    return deleted;
  },

  async getMilestoneTasks(milestoneId) {
    const milestone = await this.model
      .findOne({ id: milestoneId, deleted: false })
      .populate('tasks');

    if (!milestone) return;
    return milestone.tasks || [];
  },
  async updateMilestoneStatus(milestoneId, status) {
    this.model.update(milestoneId).set({ status });
  },
  async getMilestones(filters) {
    const milestones = await this.model
      .find()
      .where(filters)
      .populate('project')
      .populate('tasks')
      .sort('createdAt DESC');

    return milestones || [];
  },
  async updateCreationTransactionHash(milestoneId, transactionHash) {
    this.model.updateOne({ id: milestoneId }).set({ transactionHash });
  },

  async removeMilestonesByProps(filter) {
    const deletedMilestones = await this.model.destroy(filter).fetch();
    return deletedMilestones.map(oracle => oracle.id);
  },
  async getMilestonesByProject(project) {
    return this.model.find({ project, deleted: false });
  }
};
