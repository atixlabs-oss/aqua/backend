/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const uuid = require('uuid');
const { blockchainStatus } = require('../util/constants');

module.exports = {
  async saveActivity(activity, milestoneId) {
    const toSave = {
      ...activity,
      milestone: milestoneId,
      publicId: uuid.v4()
    };
    const createdActivity = await this.model.create(toSave);
    return createdActivity;
  },

  async createActivity(activity) {
    const createdActivity = await this.model.create(activity);
    return createdActivity;
  },

  async findById(id) {
    const activity = await this.model.findOne({ id, deleted: false });
    return activity;
  },

  async updateActivity(activity, activityId) {
    const toUpdate = { ...activity };

    const savedActivity = await this.model
      .updateOne({ id: activityId, deleted: false })
      .set({ ...toUpdate });

    return savedActivity;
  },

  async deleteActivity(activityId) {
    const deleted = await this.model
      .updateOne({ id: activityId, deleted: false })
      .set({ deleted: true });
    return deleted;
  },

  async updateTransactionHash(activityId, transactionHash) {
    return this.model
      .updateOne({ id: activityId, deleted: false })
      .set({ transactionHash });
  },

  async whichUnconfirmedActivities(activitiesIds) {
    return this.model.find({
      where: {
        id: activitiesIds,
        blockchainStatus: { '!=': blockchainStatus.CONFIRMED },
        deleted: false
      }
    });
  },

  async updateCreationTransactionHash(activityId, transactionHash) {
    return this.model
      .updateOne({ id: activityId, deleted: false })
      .set({ transactionHash });
  },

  async getActivityByIdWithMilestone(id) {
    const task = await this.model
      .findOne({ id, deleted: false })
      .populate('milestone');
    return task;
  },

  getTaskByMilestones(milestoneIds) {
    return this.model.find({
      where: { milestone: { in: milestoneIds }, deleted: false }
    });
  },

  getActivitiesByMilestones(milestoneIds) {
    return this.model.find({ milestone: { in: milestoneIds }, deleted: false });
  },

  getTasksByMilestone(milestoneId) {
    return this.model
      .find({
        where: { milestone: milestoneId, deleted: false }
      })
      .sort('id ASC')
      .populate('auditor');
  },

  findActivities(where) {
    return this.model.find({ where });
  }
};
