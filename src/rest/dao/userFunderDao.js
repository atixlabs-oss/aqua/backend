/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const create = userFunderModel => async userFunder =>
  userFunderModel.create(userFunder);

const getById = userFunderModel => async id => {
  const userFunder = userFunderModel.findOne({
    id
  });
  return userFunder;
};

const getByUserId = userFunderModel => async userId => {
  const userFunder = userFunderModel.findOne({
    user: userId
  });
  return userFunder;
};

module.exports = userFunderModel => ({
  create: create(userFunderModel),
  getById: getById(userFunderModel),
  getByUserId: getByUserId(userFunderModel)
});
