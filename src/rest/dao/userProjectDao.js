/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = {
  async findUserProject(where) {
    const userProject = await this.model.findOne(where);
    return userProject;
  },
  async updateStatus({ userProject, newStatus }) {
    const updatedUserProject = await this.model
      .update({ id: userProject.id })
      .set({ status: newStatus });

    return updatedUserProject;
  },
  async getUserProjects(projectId) {
    const userProjects = await this.model
      .find({ project: projectId })
      .populate('user')
      .populate('role');
    return userProjects;
  },
  findUserProjectWithUser(where) {
    return this.model.findOne(where).populate('user');
  },
  async getUserProject(where) {
    return this.model.find(where);
  },
  async createUserProject(userProject) {
    return this.model.create(userProject);
  },
  async getProjectsOfUser(userId) {
    return this.model.find({ user: userId }).populate('project');
  },
  async findUserProjectById(userProjectId) {
    const userProject = await this.model.findOne({ id: userProjectId });
    return userProject;
  },
  async removeUserProject(userProjectId) {
    return this.model.destroy({ id: userProjectId }).fetch();
  },
  async getRolesOfUser({ user, project }) {
    return this.model.find({ user, project }).populate('role');
  },
  async updateConfirmed({ projectId, confirmed }) {
    const updatedUserProject = await this.model
      .update({ project: projectId })
      .set({ confirmed: confirmed });

    return updatedUserProject;
  }
};
