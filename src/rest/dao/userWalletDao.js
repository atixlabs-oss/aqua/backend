/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-destructuring */
module.exports = {
  async findActiveById(id) {
    return this.model.findOne({ id }).populate('user');
  },

  findActiveByUserId(userId) {
    return this.model.findOne({ user: userId, active: true }).populate('user');
  },

  findByUserId(userId) {
    return this.model.findOne({ user: userId }).populate('user');
  },

  updateWallet(filter, data) {
    return this.model.updateOne(filter).set(data);
  },

  createUserWallet(userWallet, isActive) {
    return this.model.create({
      active: isActive,
      ...userWallet
    });
  },
  /* eslint-disable no-param-reassign */
  async findByAddress(address) {
    let userWalletSelected;
    const userWallets = await this.model
      .find({ address })
      .sort([{ createdAt: 'desc' }])
      .populate('user');
    if (!userWallets.length) {
      return;
    }
    if (userWallets.some(wallet => wallet.active)) {
      userWalletSelected = userWallets.find(wallet => wallet.active);
    } else {
      userWalletSelected = userWallets[0];
    }
    const { user, encryptedWallet } = userWalletSelected;
    delete user.address;
    delete user.encryptedWallet;
    return { address, encryptedWallet, ...user };
  },

  async findByAddresses(addresses) {
    const uniqueAddresses = [];
    const userWallets = await this.model
      .find({ address: addresses })
      .populate('user');
    return userWallets
      ? userWallets.reduce(
          (result, { user, encryptedWallet, address }) => {
            if (!uniqueAddresses.includes(address)) {
              uniqueAddresses.push(address);
              delete user.address;
              delete user.encryptedWallet;
              result.push({
                address,
                encryptedWallet,
                ...user
              });
            }
            return result;
          },
          []
        )
      : [];
  },

  async removeUserWalletByUser(userId) {
    return this.model.destroy({ user: userId });
  }
};
