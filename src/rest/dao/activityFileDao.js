/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const saveActivityFile = activityFileModel => async (
  activityId,
  fileId,
  fileHash
) => {
  const activityFile = await activityFileModel.create({
    activity: activityId,
    file: fileId,
    fileHash
  });
  return activityFile;
};

const getActivityFileByActivityAndFile = activityFileModel => async (
  activityId,
  fileId
) => {
  const activityFile = await activityFileModel.findOne({
    activity: activityId,
    file: fileId
  });
  return activityFile;
};

const getActivityFileByActivity = activityFileModel => async activityId => {
  const activityFiles = await activityFileModel
    .find({
      activity: activityId
    })
    .populate('file');
  return activityFiles;
};

const deleteActivityFile = activityFileModel => async activityFileId => {
  const deleted = activityFileModel.destroy(activityFileId).fetch();
  return deleted;
};

module.exports = activityFileModel => ({
  saveActivityFile: saveActivityFile(activityFileModel),
  deleteActivityFile: deleteActivityFile(activityFileModel),
  getActivityFileByActivityAndFile: getActivityFileByActivityAndFile(
    activityFileModel
  ),
  getActivityFileByActivity: getActivityFileByActivity(activityFileModel)
});
