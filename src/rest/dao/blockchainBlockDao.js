/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const updateLastBlock = blockchainBlockModel => async (
  blockNumber,
  transactionHash
) => {
  const lastBlock = (await blockchainBlockModel.find())[0];
  if (!lastBlock) {
    const updatedBlock = await blockchainBlockModel.create({
      blockNumber,
      transactionHash
    });
    return updatedBlock;
  }
  const updatedBlock = await blockchainBlockModel
    .update({ blockNumber: lastBlock.blockNumber })
    .set({ blockNumber, transactionHash });
  return updatedBlock;
};

const getLastBlock = blockchainBlockModel => async () => {
  const lastBlock = (await blockchainBlockModel.find())[0];
  return lastBlock;
};

module.exports = blockchainBlockModel => ({
  updateLastBlock: updateLastBlock(blockchainBlockModel),
  getLastBlock: getLastBlock(blockchainBlockModel)
});
