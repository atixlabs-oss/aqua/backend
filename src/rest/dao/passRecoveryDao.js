/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = {
  async createRecovery(email, token, expirationDate) {
    const recover = await this.model.find({ email });
    if (recover) await this.model.destroyOne({ email });
    return this.model.create({ email, token, expirationDate });
  },
  async findRecoverBytoken(token) {
    const found = await this.model.findOne({ where: { token } });
    return found;
  },
  async deleteRecoverByToken(token) {
    await this.model.destroyOne({ where: { token } });
  }
};
