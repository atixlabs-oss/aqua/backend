/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../logger');

module.exports = {
  async getChangelogBy(where) {
    logger.info('[ChangelogDao] :: Entering getChangelogBy method');
    return this.model
      .find(where)
      .populate('milestone')
      .populate('activity')
      .populate('evidence')
      .populate('user')
      .populate('project')
      .sort('id DESC');
  },
  async findDeletedEvidenceInChangelog(evidenceId) {
    logger.info('[ChangelogDao] :: Entering findDeletedEvidence method');
    return this.model.find({ evidence: evidenceId }).sort('id DESC')
  },
  async createChangelog(newChangelog) {
    return this.model.create(newChangelog);
  },
  async deleteProjectChangelogs(projectId) {
    return this.model.destroy({ project: projectId });
  },
  async updateChangelog({ id, toUpdate }) {
    return this.model.updateOne({ id }).set(toUpdate);
  }
};
