/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const { BigNumber } = require('bignumber.js');
const { isEmpty } = require('lodash');
const uuid = require('uuid');
const {
  projectStatuses,
  decimalBase,
  ACTIVITY_STATUS,
  ACTIVITY_TYPES
} = require('../util/constants');
const userDao = require('./userDao');
const activityDao = require('./activityDao');
const activityService = require('../services/activityService');
const activityEvidenceDao = require('./activityEvidenceDao');
const userProjectService = require('../services/userProjectService');
const roleDao = require('./roleDao');

const calculateTotalCurrent = (activities, type) =>
  activities
    .filter(activity => activity.type === type)
    .reduce(
      (accum, activity) => accum.plus(BigNumber(activity.current)),
      BigNumber('0')
    );

const buildProjectWithBasicInformation = async project => {
  const {
    projectName,
    location,
    timeframe,
    timeframeUnit,
    cardPhotoPath,
    goalAmount,
    published,
    highlighted,
    ...rest
  } = project;

  const basicInformation = {
    projectName,
    location,
    timeframe,
    timeframeUnit,
    published,
    highlighted,
    thumbnailPhoto: cardPhotoPath,
    beneficiary: await userProjectService.getBeneficiaryByProjectId({
      projectId: project.id
    })
  };
  return { ...rest, budget: goalAmount, basicInformation };
};

const buildProjectWithUsers = async project => {
  const usersByProject = await userDao.getUsersByProject(project.id);
  const dbRoles = await roleDao.getAllRoles();
  const rolesWithUser = usersByProject
    .map(({ id, firstName, lastName, email, country, first, roles, wallets, isActive }) =>
      roles.map(({ role }) => ({
        id,
        firstName,
        lastName,
        email,
        country,
        first,
        role,
        address: wallets.length > 0 ? wallets[0].address : undefined,
        isActive
      }))
    )
    .flat();

  const usersByRole = rolesWithUser.reduce(
    (mapUserByRole, { role, ...user }) => ({
      ...mapUserByRole,
      [role]: mapUserByRole[role] ? [...mapUserByRole[role], user] : [user]
    }),
    {}
  );
  const users = Object.entries(usersByRole).flatMap(
    ([role, usersWithRole]) => ({
      role,
      users: usersWithRole
    })
  );
  const usersWithRoleDescription = users.map(user => ({
    roleDescription: dbRoles.find(
      r => r.id === Number.parseInt(user.role, decimalBase)
    ).description,
    ...user
  }));
  const toReturn = { ...project, users: usersWithRoleDescription };
  return toReturn;
};

const buildProjectWithMilestonesAndActivitiesAndDetails = async project => {
  const {
    mission,
    problemAddressed,
    currency,
    currencyType,
    additionalCurrencyInformation,
    agreementFilePath,
    proposalFilePath,
    ...rest
  } = project;

  const milestonesWithActivities = await Promise.all(
    project.milestones.map(
      async ({ id, title, description, status, parent: milestoneParent }) => {
        const activitiesByMilestone = await activityDao.getTasksByMilestone(id);
        const activities = activitiesByMilestone.map(
          ({
            id: activityId,
            title: activityTitle,
            description: activityDescription,
            acceptanceCriteria,
            budget,
            auditor: { id: auditorId, firstName, lastName },
            status: activityStatus,
            type,
            current,
            parent: activityParent
          }) => ({
            id: activityId,
            title: activityTitle,
            description: activityDescription,
            acceptanceCriteria,
            budget,
            currency,
            auditor: { id: auditorId, firstName, lastName },
            status: activityStatus,
            type,
            current,
            parent: activityParent
          })
        );

        const funding = activityService.getActivitiesBudgetAndCurrentByType({
          activities,
          type: ACTIVITY_TYPES.FUNDING
        });

        const spending = activityService.getActivitiesBudgetAndCurrentByType({
          activities,
          type: ACTIVITY_TYPES.SPENDING
        });

        const payback = activityService.getActivitiesBudgetAndCurrentByType({
          activities,
          type: ACTIVITY_TYPES.PAYBACK
        });

        const milestone = {
          id,
          title,
          description,
          funding,
          spending,
          payback,
          status,
          activities,
          parent: milestoneParent
        };
        return milestone;
      }
    )
  );

  const totalActivities = milestonesWithActivities.flatMap(
    milestone => milestone.activities
  );
  const completedActivities = totalActivities.filter(
    activity => activity.status === ACTIVITY_STATUS.APPROVED
  );
  const fundingActivities = totalActivities.filter(
    activity => activity.type === ACTIVITY_TYPES.FUNDING
  );
  const totalBudget = fundingActivities.reduce(
    (accum, activity) => accum.plus(BigNumber(activity.budget)),
    BigNumber('0')
  );
  const totalFunding = calculateTotalCurrent(
    completedActivities,
    ACTIVITY_TYPES.FUNDING
  );
  const totalSpending = calculateTotalCurrent(
    completedActivities,
    ACTIVITY_TYPES.SPENDING
  );
  const totalPayback = calculateTotalCurrent(
    completedActivities,
    ACTIVITY_TYPES.PAYBACK
  );

  const completedMilestonesLength = milestonesWithActivities.filter(milestone =>
    milestone.activities.every(act => act.status === ACTIVITY_STATUS.APPROVED)
  ).length;

  const status = {
    milestones: {
      completed: completedMilestonesLength,
      incompleted: milestonesWithActivities.length - completedMilestonesLength
    },
    activities: {
      completed: completedActivities.length,
      incompleted: totalActivities.length - completedActivities.length
    },
    budget: totalBudget,
    funding: totalFunding,
    spending: totalSpending,
    payback: totalPayback
  };

  const details = {
    mission,
    problemAddressed,
    currency,
    currencyType,
    additionalCurrencyInformation,
    legalAgreementFile: agreementFilePath,
    projectProposalFile: proposalFilePath,
    status
  };

  const projectWithMilestonesAndDetails = {
    ...rest,
    details,
    milestones: milestonesWithActivities
  };
  return projectWithMilestonesAndDetails;
};

const buildProjectWithEvidences = async project => {
  const milestonesWithEvidences = await Promise.all(
    project.milestones.map(async milestone => ({
      ...milestone,
      activities: await Promise.all(
        milestone.activities.map(async activity => {
          const evidences = await activityEvidenceDao.getEvidencesByTaskId(
            activity.id
          );
          const activityWithEvidences = { ...activity, evidences };
          return activityWithEvidences;
        })
      )
    }))
  );
  const projectWithEvidences = {
    ...project,
    milestones: milestonesWithEvidences
  };
  return projectWithEvidences;
};

module.exports = {
  async saveProject(project) {
    const createdProject = await this.model.create({
      ...project,
      id: uuid.v4()
    });
    return createdProject;
  },

  async findById(id) {
    const project = await this.model.findOne({ id });
    if (!isEmpty(project)) return project;
  },

  async deleteProject({ projectId }) {
    const deletedProject = this.model.destroy({ id: projectId }).fetch();
    return deletedProject;
  },

  async getProjectWithAllData(id) {
    const project = await this.model
      .findOne({ id })
      .populate('milestones', {
        where: {
          deleted: false
        },
        sort: 'id ASC'
      })
      .populate('proposer');
    if (!project) return project;
    return this.buildProjectWithEditingFields(
      await buildProjectWithEvidences(
        await buildProjectWithMilestonesAndActivitiesAndDetails(
          await buildProjectWithUsers(
            await buildProjectWithBasicInformation(project)
          )
        )
      )
    );
  },

  async getAllProjectsByParentIdWithAllData(parentId) {
    const projects = await this.model
      .find({
        where: { parent: parentId }
      })
      .populate('milestones', {
        where: {
          deleted: false
        },
        sort: 'id ASC'
      })
      .populate('proposer');
    if (!projects) return projects;

    return Promise.all(
      projects.map(async project =>
        this.buildProjectWithEditingFields(
          await buildProjectWithEvidences(
            await buildProjectWithMilestonesAndActivitiesAndDetails(
              await buildProjectWithUsers(
                await buildProjectWithBasicInformation(project)
              )
            )
          )
        )
      )
    );
  },

  async getLastProjectByFilter(id, filter) {
    const project = await this.model
    .find({
      or: [{ id }, { parent: id }],
        ...filter
      })
      .populate('users')
      .sort('revision DESC')
      .limit(1);

    return project[0];
  },

  async getLastProjectWithValidStatus(id) {
    const project = await this.model
      .find({
        or: [{ id }, { parent: id }],
        status: {
          nin: [
            projectStatuses.OPEN_REVIEW,
            projectStatuses.IN_REVIEW,
            projectStatuses.CANCELLED_REVIEW,
            projectStatuses.PENDING_REVIEW,
            projectStatuses.PENDING_APPROVE_REVIEW,
            projectStatuses.PENDING_CANCEL_REVIEW
          ]
        }
      })
      .sort('revision DESC')
      .limit(1);
    return project[0];
  },

  async getLastPublicRevisionProject(id) {
    const project = await this.model
      .find()
      .where({
        or: [{ id }, { parent: id }],
        status: {
          nin: [
            projectStatuses.OPEN_REVIEW,
            projectStatuses.IN_REVIEW,
            projectStatuses.CANCELLED_REVIEW,
            projectStatuses.PENDING_REVIEW,
            projectStatuses.PENDING_APPROVE_REVIEW,
            projectStatuses.PENDING_CANCEL_REVIEW
          ]
        }
      })
      .populate('milestones', {
        where: {
          deleted: false
        },
        sort: 'id ASC'
      })
      .populate('proposer')
      .sort('revision DESC')
      .limit(1);
    if (!project) return project;
    return this.buildProjectWithEditingFields(
      await buildProjectWithEvidences(
        await buildProjectWithMilestonesAndActivitiesAndDetails(
          await buildProjectWithUsers(
            await buildProjectWithBasicInformation(project[0])
          )
        )
      )
    );
  },

  async findActiveProjectClone(id) {
    return this.model.findOne().where({
      parent: id,
      status: [
        projectStatuses.OPEN_REVIEW,
        projectStatuses.IN_REVIEW,
        projectStatuses.PENDING_REVIEW,
        projectStatuses.PENDING_APPROVE_REVIEW,
        projectStatuses.PENDING_CANCEL_REVIEW
      ]
    });
  },

  async buildProjectWithEditingFields(project) {
    const projectId = project.parent || project.id;
    const activeProjectClone = await this.findActiveProjectClone(projectId);
    const editing = !!activeProjectClone;
    const cloneId = activeProjectClone ? activeProjectClone.id : null;
    const cloneStatus = activeProjectClone
      ? activeProjectClone.status
      : undefined;
    const editProposer = activeProjectClone
      ? activeProjectClone.proposer
      : undefined;
    const projectWithEditingFields = {
      ...project,
      editing,
      cloneId,
      cloneStatus,
      editProposer
    };
    return projectWithEditingFields;
  },

  async findGenesisProjects() {
    const projects = await this.model.find({
      where: { parent: null },
      sort: 'createdAt DESC'
    });
    return projects;
  },

  async findInReviewProjects() {
    const projects = await this.model.find({
      status: {
        in: [projectStatuses.IN_REVIEW]
      }
    });
    return projects;
  },

  async updateProject(project, id) {
    const toUpdate = { ...project };

    delete toUpdate.id;
    delete toUpdate.ownerId;

    const savedProject = await this.model
      .updateOne({ id })
      .set({ ...toUpdate });

    return savedProject;
  },

  async getProjectPhotos(projectId) {
    return this.model.findOne({
      where: { id: projectId },
      select: ['coverPhoto', 'cardPhoto']
    });
  },

  async getLastValidReview(id) {
    const project = await this.model
      .find({
        or: [{ id }, { parent: id }],
        status: {
          nin: [
            projectStatuses.CANCELLED_REVIEW,
            projectStatuses.OPEN_REVIEW,
            projectStatuses.PENDING_REVIEW
          ]
        }
      })
      .sort('revision DESC')
      .limit(1);

    return project[0];
  },

  async getProjectWithProposer(id) {
    return this.model.findOne({ id }).populate('proposer');
  },

  async getLastValidReviewWithUsers(id) {
    const project = await this.model
      .find({
        or: [{ id }, { parent: id }],
        status: {
          nin: [
            projectStatuses.CANCELLED_REVIEW,
            projectStatuses.OPEN_REVIEW,
            projectStatuses.PENDING_REVIEW
          ]
        }
      })
      .populate('users')
      .sort('revision DESC')
      .limit(1);

    return project[0];
  }
};
