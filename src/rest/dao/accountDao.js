/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const uuid = require('uuid');

/* eslint-disable prefer-destructuring */
module.exports = {
    async findById(id) {
      // TODO: populate doesn't work because the token table in db has no primary key
      return this.model.findOne({ id }).populate('currency');
    },

    async getAccountBy(where) {
      return this.model.find({
        where
      });
    },
  
    updateAccount(filter, data) {
      return this.model.updateOne(filter).set(data);
    },
  
    createAccount(account) {
      return this.model.create({
        ...account,
        id: uuid.v4()
      });
    },
  
    async removeAccountById(id) {
      return this.model.destroy({ id });
    }
  };
  