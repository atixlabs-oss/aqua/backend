/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const uuid = require('uuid');
const {
  txEvidenceStatus,
  evidenceStatus,
  evidenceTypes
} = require('../util/constants');

module.exports = {
  async findById(id) {
    return this.model
      .findOne({ id })
      .populate('activity')
      .populate('files')
      .populate('user')
      .populate('auditor')
      .populate('movement');
  },
  async findByTxHash(txHash) {
    return this.model.findOne({ txHash });
  },

  async saveActivityEvidence(evidence) {
    const toSave = {
      ...evidence,
      publicId: uuid.v4()
    };
    const evidenceCreated = await this.model.create(toSave);
    return evidenceCreated;
  },

  async addActivityEvidence(data) {
    const evidenceCreated = await this.model.create({
      ...data,
    });
    return evidenceCreated;
  },

  async getEvidencesByTaskId(taskId) {
    return this.model.find({ activity: taskId });
  },

  async updateTaskEvidence(evidenceId, data) {
    const updatedEvidence = await this.model
      .updateOne({ id: evidenceId })
      .set(data);
    return updatedEvidence;
  },

  async findAllSentTxs() {
    const txs = await this.model.find({
      select: ['id', 'txHash'],
      where: { status: txEvidenceStatus.SENT }
    });
    return txs;
  },

  async findAllPendingVerificationTxs() {
    const txs = await this.model.find({
      select: ['id', 'txHash'],
      where: { status: txEvidenceStatus.PENDING_VERIFICATION }
    });
    return txs;
  },

  async deleteEvidence(evidenceId) {
    return this.model.destroyOne(evidenceId);
  },

  async getEvidencesByActivityId(activityId) {
    return this.model.find({ activity: activityId }).populate('files').populate('movement');
  },

  async getApprovedEvidences({ tasksIds, limit }) {
    const criteria = {
      where: {
        activity: { in: tasksIds },
        status: evidenceStatus.APPROVED,
        type: evidenceTypes.TRANSFER
      }
    };
    return this.model
      .find(limit ? { ...criteria, limit } : criteria)
      .populate('activity')
      .populate('user')
      .sort('createdAt ASC');
  },

  async getEvidencesByMovementId(movementId, where = {}) {
    const criteria = {
      where: {
        movement: movementId,
        ...where
      }
    };
    return this.model.find(criteria);
  },

  async getEvidencesBy(where) {
    return this.model.find(where);
  }
};
