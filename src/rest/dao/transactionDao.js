/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  async findById(id) {
    return this.model.findOne({ id });
  },
  async findOneByProps(filters, populate) {
    return this.model.findOne(filters, populate);
  },
  async findAllByProps(filters, populate) {
    return this.model.find(filters, populate);
  },
  async findByTxHash(txHash) {
    return this.model.findOne({ txHash });
  },
  async findLastTxBySender(sender) {
    const [tx] = await this.model.find({
      where: { sender },
      limit: 1,
      sort: 'nonce DESC'
    });
    return tx;
  },
  async findAllBySender(sender) {
    return this.model.find({ sender });
  },
  async save(transaction) {
    return this.model.create(transaction);
  },
  async delete(id) {
    return this.model.destroyOne({ id });
  }
};
