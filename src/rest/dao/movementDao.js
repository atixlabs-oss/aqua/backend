/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-destructuring */
const { MAXIMUM_MOVEMENTS_PER_REQUEST } = require('../util/constants')

module.exports = {
    async findById(id) {
      return this.model.findOne({ id });
    },

    async findMovements(where) {
      return this.model.find(where);
    },

    async getMovementByProject(where, page) {
      return this.model.find({
        where,
        limit: MAXIMUM_MOVEMENTS_PER_REQUEST,
        skip: (page - 1) * MAXIMUM_MOVEMENTS_PER_REQUEST
      })
      .populate('accountId')
      .sort('date DESC');
    },
  
    updateMovement(filter, data) {
      return this.model.updateOne(filter).set(data);
    },
  
    createMovement(movement) {
      return this.model.create(movement);
    },
  
    async removeMovementById(id) {
      return this.model.destroy({ id });
    }
  };
  