/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const logger = require('../logger');
const { TX_STATUS } = require('../util/constants');

module.exports = {
  async createTx(tx) {
    return this.model.create(tx);
  },
  async findAllPendingTransactions() {
    logger.info('[TxDao] :: Entering findAllPendingTransactions method');
    return this.model.find({ status: TX_STATUS.PENDING });
  },
  async updateTransaction({ id, toUpdate }) {
    logger.info('[TxDao] :: Entering updateTransaction method');
    return this.model.updateOne({ id }).set(toUpdate);
  }
};
