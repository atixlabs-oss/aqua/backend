/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const XLSX = require('xlsx');
const { remove, invert } = require('lodash');
const logger = require('../logger');
const COAError = require('../errors/COAError');
const errors = require('../errors/exporter/ErrorExporter');
const { xlsxConfigs } = require('../util/constants');

exports.readExcelData = data => {
  let workbook;
  logger.info('[ExcelParser] :: Reading Milestone excel');
  try {
    workbook = XLSX.read(data, { raw: true });
  } catch (err) {
    logger.error('[ExcelParser] :: Error reading excel file:', err);
    throw new COAError(errors.milestone.ErrorProcessingMilestonesFile);
  }

  if (!workbook) {
    logger.error('[ExcelParser] :: Error reading Milestone excel file');
    throw new COAError(errors.milestone.ErrorProcessingMilestonesFile);
  }

  const sheetNameList = workbook.SheetNames;
  const worksheet = workbook.Sheets[sheetNameList[0]];

  const nameMap = invert({ ...xlsxConfigs.keysMap });

  delete worksheet['!autofilter'];
  delete worksheet['!merges'];
  delete worksheet['!margins'];
  delete worksheet['!ref'];

  const cellKeys = Object.keys(worksheet);
  remove(cellKeys, k => k.slice(1) <= xlsxConfigs.startRow);

  return {
    worksheet,
    cellKeys,
    nameMap
  };
};
