/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const crypto = require('crypto');
const { v4: uuidv4 } = require('uuid');
const { encryption } = require('./constants');

module.exports = {
  generateAPIKeyAndSecret() {
    return {
      apiKey: uuidv4(),
      apiSecret: crypto
        .randomBytes(encryption.apiSecretSize)
        .toString('base64'),
    };
  },
};
