/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const fs = require('fs');

/**
 * Method to convert image file to base64
 * @param path absolute path to image file
 * @returns a base64 string
 */
exports.getBase64htmlFromPath = path => {
  try {
    const bitmap = fs.readFileSync(path);
    const format = path.split('.').slice(-1)[0];
    // convert binary data to base64 encoded string
    const base64 = new Buffer.from(bitmap).toString('base64');
    const res = `data:image/${format};base64, ${base64}`;
    return res;
  } catch (error) {
    return '';
  }
};
