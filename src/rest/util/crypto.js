/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const crypto = require('crypto');

module.exports = {
  async encrypt(data, key) {
    const iv = await crypto.randomBytes(16);
    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let encryptedText = cipher.update(data);
    encryptedText = Buffer.concat([encryptedText, cipher.final()]);
    return {
      encryptedData: encryptedText.toString('hex'),
      iv: iv.toString('hex'),
    };
  },

  decrypt(encryptedData, key, iv) {
    const ivBuff = Buffer.from(iv, 'hex');
    const encryptedDataBuff = Buffer.from(encryptedData, 'hex');
    const decipher = crypto.createDecipheriv('aes-256-cbc', key, ivBuff);

    let decrypted = decipher.update(encryptedDataBuff);
    decrypted = Buffer.concat([decrypted, decipher.final()]);

    // returns data after decryption
    return decrypted.toString();
  },
};
