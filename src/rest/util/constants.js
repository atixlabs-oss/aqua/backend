/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const txTypes = {
  SENT: 'sent',
  RECEIVED: 'received'
};

const txStatusType = {
  PENDING: 'pending',
  CONFIRMED: 'confirmed',
  CANCELLED: 'cancelled'
};

const TX_STATUS = {
  PENDING: 'pending',
  CONFIRMED: 'confirmed',
  FAILED: 'failed'
};

const currencyType = {
  FIAT: 'fiat',
  CRYPTO: 'crypto'
};

const evidenceTypes = {
  TRANSFER: 'transfer',
  IMPACT: 'impact'
};

const evidenceStatus = {
  NEW: 'new',
  APPROVED: 'approved',
  REJECTED: 'rejected'
};

const lastEvidenceStatus = [evidenceStatus.APPROVED, evidenceStatus.REJECTED];

const validStatusToChange = [evidenceStatus.APPROVED, evidenceStatus.REJECTED];

const projectSections = {
  BASIC_INFORMATION: 1,
  DETAILS: 2,
  USERS: 3,
  MILESTONES: 4
};

const rolesTypes = {
  BENEFICIARY: 'beneficiary',
  AUDITOR: 'auditor',
  INVESTOR: 'investor'
};

const projectSensitiveDataFields = [];

const projectPublicFields = [
  'id',
  'status',
  'basicInformation',
  'details',
  'milestones',
  'users',
  'budget',
  'inReview',
  'revision',
  'type',
  'parent'
];

const evidenceFileTypes = {
  FILE: 'File',
  PHOTO: 'Photo'
};

const transferStatus = {
  RECONCILIATION: 1,
  PENDING_VERIFICATION: 0,
  CANCELLED: 3,
  VERIFIED: 2
};

const projectStatus = {
  // TODO delete this one
  REJECTED: 2,
  DRAFT: 0,
  PENDING_APPROVAL: 1,
  PUBLISHED: 3,
  IN_PROGRESS: 4
};

const projectStatuses = {
  DRAFT: 'draft',
  PUBLISHED: 'published',
  IN_PROGRESS: 'in progress',
  IN_REVIEW: 'in review',
  COMPLETED: 'completed',
  CANCELLED: 'cancelled',
  OPEN_REVIEW: 'open review',
  CANCELLED_REVIEW: 'cancelled review',
  PENDING_PUBLISHED: 'pending published',
  PENDING_REVIEW: 'pending review',
  PENDING_APPROVE_REVIEW: 'pending approve review',
  PENDING_CANCEL_REVIEW: 'pending cancel review'
};

const pendingProjectStatus = [
  projectStatuses.PENDING_PUBLISHED,
  projectStatuses.PENDING_APPROVE_REVIEW,
  projectStatuses.PENDING_CANCEL_REVIEW,
  projectStatuses.PENDING_REVIEW
];

const activityStatus = {
  PENDING: 1,
  STARTED: 2,
  VERIFIED: 3,
  COMPLETED: 4
};

const claimMilestoneStatus = {
  PENDING: 'pending',
  CLAIMABLE: 'claimable',
  CLAIMED: 'claimed',
  TRANSFERRED: 'transferred'
};

const userRoles = {
  COA_ADMIN: 'admin',
  ENTREPRENEUR: 'entrepreneur',
  PROJECT_SUPPORTER: 'supporter',
  PROJECT_CURATOR: 'curator',
  BANK_OPERATOR: 'bankoperator'
};

const currencyTypes = {
  FIAT: 'fiat',
  CRYPTO: 'crypto'
};

const milestoneBudgetStatus = {
  CLAIMABLE: 1,
  CLAIMED: 2,
  FUNDED: 3,
  BLOCKED: 4
};

const blockchainStatus = {
  PENDING: 1,
  SENT: 2,
  CONFIRMED: 3,
  ABORTED: 4
};

const xlsxConfigs = {
  keysMap: {
    A: 'quarter',
    C: 'tasks',
    D: 'impact',
    E: 'impactCriterion',
    F: 'signsOfSuccess',
    G: 'signsOfSuccessCriterion',
    H: 'budget',
    I: 'category',
    J: 'keyPersonnel'
  },
  columnNames: {
    quarter: 'Timeline',
    tasks: 'Tasks',
    impact: 'Expected Changes/ Social Impact Targets',
    impactCriterion: 'Review Criterion for the Expected Changes',
    signsOfSuccess: 'Signs of Success',
    signsOfSuccessCriterion: 'Review Criterion for the Signs of Success',
    budget: 'Budget needed',
    category: 'Expenditure Category',
    keyPersonnel: 'Key Personnel Responsible'
  },
  typeColumnKey: 'B',
  startRow: 4
};

const transactionTypes = {
  projectCreation: 'projectCreation',
  milestoneCreation: 'milestoneCreation',
  activityCreation: 'activityCreation',
  milestoneClaimed: 'milestoneClaimed',
  projectStarted: 'projectStarted',
  milestoneFunded: 'milestoneFunded',
  validateActivity: 'validateActivity',
  updateEvidence: 'updateEvidence'
};

const voteEnum = {
  NULL: 0,
  YES: 1,
  NO: 2
};
const proposalTypeEnum = {
  NEW_MEMBER: 0,
  NEW_DAO: 1,
  ASSIGN_BANK: 2,
  ASSIGN_CURATOR: 3
};
const daoMemberRoleEnum = {
  NORMAL: 0,
  BANK: 1,
  CURATOR: 2
};
const daoMemberRoleNames = ['Normal', 'Bank Operator', 'Project Curator'];

const txEvidenceStatus = {
  NOT_SENT: 'notsent',
  SENT: 'sent',
  CONFIRMED: 'confirmed',
  FAILED: 'failed',
  PENDING_VERIFICATION: 'pending_verification'
};

const txProposalStatus = {
  NOT_SENT: 'notsent',
  SENT: 'sent',
  CONFIRMED: 'confirmed',
  FAILED: 'failed'
};

const encryption = {
  saltOrRounds: 10,
  apiSecretSize: 32
};

const allowDeleteProjectStatuses = [projectStatuses.DRAFT];

const ACTIVITY_STATUS = {
  NEW: 'new',
  IN_PROGRESS: 'in_progress',
  IN_REVIEW: 'to-review',
  APPROVED: 'approved',
  REJECTED: 'rejected',
  PENDING_TO_REVIEW: 'pending to review',
  PENDING_TO_APPROVE: 'pending to approve',
  PENDING_TO_REJECT: 'pending to reject'
};

const pendingActivityStatus = [
  ACTIVITY_STATUS.PENDING_TO_REVIEW,
  ACTIVITY_STATUS.PENDING_TO_APPROVE,
  ACTIVITY_STATUS.PENDING_TO_REJECT
];

const ACTIVITY_STATUS_TRANSITION = {
  [ACTIVITY_STATUS.NEW]: [],
  [ACTIVITY_STATUS.IN_PROGRESS]: [ACTIVITY_STATUS.IN_REVIEW],
  [ACTIVITY_STATUS.IN_REVIEW]: [
    ACTIVITY_STATUS.REJECTED,
    ACTIVITY_STATUS.APPROVED
  ],
  [ACTIVITY_STATUS.APPROVED]: [],
  [ACTIVITY_STATUS.REJECTED]: [ACTIVITY_STATUS.IN_REVIEW]
};

const MILESTONE_STATUS = {
  NOT_STARTED: 'not started',
  IN_PROGRESS: 'in progress',
  APPROVED: 'approved'
};

const decimalBase = 10;

const TIMEFRAME_DECIMALS = 3;

const ACTION_TYPE = {
  CREATE_PROJECT: 'create_project',
  PUBLISH_PROJECT: 'publish_project',
  SEND_PROJECT_TO_REVIEW: 'send_project_to_review',
  EDIT_PROJECT_BASIC_INFORMATION: 'edit_project_basic_information',
  EDIT_PROJECT_DETAILS: 'edit_project_details',
  ADD_USER_PROJECT: 'add_user_project',
  REMOVE_USER_PROJECT: 'remove_user_project',
  ADD_MILESTONE: 'add_milestone',
  REMOVE_MILESTONE: 'remove_milestone',
  ADD_ACTIVITY: 'add_activity',
  REMOVE_ACTIVITY: 'remove_activity',
  ADD_EVIDENCE: 'add_evidence',
  DELETE_EVIDENCE: 'delete_evidence',
  REJECT_ACTIVITY: 'reject_activity',
  APPROVE_ACTIVITY: 'approve_activity',
  SEND_ACTIVITY_TO_REVIEW: 'activity_to_review',
  REJECT_EVIDENCE: 'reject_evidence',
  APPROVE_EVIDENCE: 'approve_evidence',
  PROJECT_CLONE: 'project_clone',
  CANCEL_REVIEW: 'cancel_review',
  APPROVE_REVIEW: 'approve_review',
  UPDATE_MILESTONE: 'update_milestone',
  UPDATE_ACTIVITY: 'update_activity',
  APPROVE_CANCELLATION: 'approve_cancellation',
  REJECT_CANCELLATION: 'reject_cancellation',
  SEND_CANCEL_TO_REVIEW: 'send_cancel_to_review',
};

const projectStatusToClone = [
  projectStatuses.IN_PROGRESS,
  projectStatuses.PUBLISHED
];

const EDITABLE_ACTIVITY_STATUS = [
  ACTIVITY_STATUS.IN_PROGRESS,
  ACTIVITY_STATUS.NEW,
  ACTIVITY_STATUS.IN_REVIEW
];

const ACTIVITY_STEPS = {
  CAN_UPDATE_ACTIVITY_STATUS: 0,
  WAITING_SIGNATURE_AUTHORIZATION: 1,
  SIGNATURE_AUTHORIZATION_SENT: 2
};

const PROJECT_STEPS = {
  OPEN_REVIEW_PROJECT: 0,
  PENDING_SIGNATURE_AUTHORIZATION: 1,
  AUDITED_PROJECT_REVIEW: 2
};

const PROJECT_TYPES = {
  GRANT: 'grant',
  LOAN: 'loan'
};

const ACTIVITY_TYPES = {
  FUNDING: 'funding',
  SPENDING: 'spending',
  PAYBACK: 'payback'
};

const pinStatus = {
  ACTIVE: 'active',
  IN_REVIEW: 'in review',
  APPROVED: 'approved',
  REJECTED: 'rejected',
  INACTIVE: 'inactive',
}

const PROJECT_VISIBILITY_TYPES = {
  PRIVATE: 'private',
  PUBLISHED: 'published',
  HIGHLIGHTED: 'highlighted'
};

const MAXIMUM_MOVEMENTS_PER_REQUEST = 10;

const MOVEMENT_TYPES = [
  'incoming',
  'outgoing'
];

module.exports = {
  ACTION_TYPE,
  ACTIVITY_STATUS,
  ACTIVITY_STATUS_TRANSITION,
  MILESTONE_STATUS,
  allowDeleteProjectStatuses,
  decimalBase,
  currencyTypes,
  lastEvidenceStatus,
  projectPublicFields,
  projectSensitiveDataFields,
  evidenceFileTypes,
  transferStatus,
  projectStatus,
  projectStatuses,
  projectSections,
  activityStatus,
  userRoles,
  milestoneBudgetStatus,
  blockchainStatus,
  xlsxConfigs,
  transactionTypes,
  voteEnum,
  proposalTypeEnum,
  daoMemberRoleNames,
  daoMemberRoleEnum,
  claimMilestoneStatus,
  txEvidenceStatus,
  txProposalStatus,
  encryption,
  rolesTypes,
  currencyType,
  evidenceTypes,
  evidenceStatus,
  validStatusToChange,
  txStatusType,
  txTypes,
  TIMEFRAME_DECIMALS,
  projectStatusToClone,
  EDITABLE_ACTIVITY_STATUS,
  ACTIVITY_STEPS,
  PROJECT_STEPS,
  PROJECT_TYPES,
  ACTIVITY_TYPES,
  TX_STATUS,
  pendingProjectStatus,
  pendingActivityStatus,
  pinStatus,
  PROJECT_VISIBILITY_TYPES,
  MAXIMUM_MOVEMENTS_PER_REQUEST,
  MOVEMENT_TYPES
};
