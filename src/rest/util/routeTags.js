/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module.exports = {
  USER: { name: 'USER: ', description: 'User related end-points' },
  COUNTRY: { name: 'COUNTRY: ', description: 'Country related end-points' },
  ACTIVITY: { name: 'ACTIVITY: ', description: 'Activity related end-points' },
  FILE: { name: 'FILE: ', description: 'File related end-points' },
  PHOTO: { name: 'PHOTO: ', description: 'Photo related end-points' },
  GENERAL: { name: 'GENERAL: ', description: 'General related end-points' },
  MILESTONE: {
    name: 'MILESTONE: ',
    description: 'Milestone related end-points'
  },
  PROJECT: { name: 'PROJECT: ', description: 'Project related end-points' },
  QUESTIONNAIRE: {
    name: 'QUESTIONNAIRE: ',
    description: 'Questionnaire related end-points'
  },
  USER_PROJECT: {
    name: 'USER_PROJECT: ',
    description: 'UserProject related end-points'
  },
  TRANSFER: { name: 'TRANSFER: ', description: 'Transfer related end-points' },
  DAO: { name: 'DAO: ', description: 'DAO related end-points' },
  GET: { name: 'GET: ', description: 'GET end-points' },
  POST: { name: 'POST: ', description: 'POST end-points' },
  DELETE: { name: 'DELETE: ', description: 'DELETE end-points' },
  PUT: { name: 'PUT: ', description: 'PUT end-points' },
  TOKEN: { name: 'TOKEN: ', description: 'Token related end-points' },
  MOVEMENT: { name: 'MOVEMENT: ', description: 'Movement related end-points' },
  ACCOUNT: { name: 'ACCOUNT: ', description: 'Account related end-points' },
};
