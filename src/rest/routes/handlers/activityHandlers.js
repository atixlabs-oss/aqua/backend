/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const httpStatus = require('http-status');
const activityService = require('../../services/activityService');
const userService = require('../../services/userService');

const COAError = require('../../errors/COAError');
const errors = require('../../errors/exporter/ErrorExporter');

module.exports = {
  createActivity: () => async (request, reply) => {
    const { milestoneId } = request.params;
    const {
      title,
      description,
      acceptanceCriteria,
      budget,
      auditor,
      type
    } = request.body;
    const response = await activityService.createActivity({
      milestoneId,
      title,
      description,
      acceptanceCriteria,
      budget,
      auditor,
      user: request.user,
      type
    });
    reply.status(httpStatus.CREATED).send(response);
  },

  updateActivity: () => async (req, reply) => {
    const response = await activityService.updateActivity({
      activityId: req.params.activityId,
      title: req.body.title,
      description: req.body.description,
      acceptanceCriteria: req.body.acceptanceCriteria,
      budget: req.body.budget,
      auditor: req.body.auditor,
      user: req.user,
      type: req.body.type
    });
    reply.status(httpStatus.OK).send(response);
  },

  updateActivityStatus: () => async (request, reply) => {
    const { status, txId, reason } = request.body;
    const response = await activityService.updateActivityStatus({
      user: request.user,
      activityId: request.params.activityId,
      status,
      txId,
      reason
    });
    reply.status(httpStatus.OK).send(response);
  },

  deleteActivity: () => async (request, reply) => {
    const { taskId } = request.params;
    const response = await activityService.deleteActivity(taskId, request.user);
    reply.status(httpStatus.OK).send(response);
  },

  addEvidence: () => async (request, reply) => {
    const response = await activityService.addEvidence({
      activityId: request.params.activityId,
      userId: request.user.id,
      title: request.raw.body.title,
      description: request.raw.body.description,
      type: request.raw.body.type,
      movementId: request.raw.body.movementId,
      files: request.raw.files
    });
    reply.status(httpStatus.OK).send(response);
  },

  updateEvidenceStatus: () => async (request, reply) => {
    const { evidenceId } = request.params;
    const { status, reason } = request.body;
    const response = await activityService.updateEvidenceStatus({
      evidenceId,
      newStatus: status.toLowerCase(),
      userId: request.user.id,
      reason
    });
    reply.status(httpStatus.OK).send(response);
  },

  getActivityEvidences: fastify => async (request, reply) => {
    const token = request.headers.authorization;
    let existentUser;
    if (token) {
      if (!token.startsWith('Bearer ')) throw new Error('Invalid token format');
      const [_, bearerToken] = token.split(' ');
      const user = await fastify.jwt.verify(bearerToken);
      existentUser = await userService.getUserById(user.id);
      if (!existentUser || existentUser.blocked) {
        fastify.log.error(
          '[Project Handler] :: Unathorized access for user:',
          user
        );
        throw new COAError(errors.server.UnauthorizedUser);
      }
    }
    const response = await activityService.getActivityEvidences({
      activityId: request.params.activityId,
      user: existentUser
    });
    reply.status(httpStatus.OK).send(response);
  },

  getEvidence: () => async (request, reply) => {
    const response = await activityService.getEvidence(
      request.params.evidenceId
    );
    reply.status(httpStatus.OK).send(response);
  },

  sendActivityTransaction: () => async (request, reply) => {
    const response = await activityService.sendActivityTransaction({
      user: request.user,
      activityId: request.params.activityId,
      authorizationSignature: request.body.authorizationSignature
    });
    reply.status(httpStatus.OK).send(response);
  },

  deleteEvidence: () => async (request, reply) => {
    const { activityId, evidenceId } = request.params;
    const userId = request.user.id;
    const response = await activityService.deleteEvidence(activityId, evidenceId, userId);
    reply.status(httpStatus.OK).send(response);
  }
};
