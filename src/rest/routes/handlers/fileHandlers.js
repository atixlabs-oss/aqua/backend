/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const fileService = require('../../services/fileService');

module.exports = {
  deleteFile: fastify => async (request, reply) => {
    const { fileId } = request.params;
    fastify.log.info(`[File Routes] :: Deleting file ID ${fileId}`);

    try {
      const res = await fileService.deleteFile(fileId);

      if (res && res.error) {
        fastify.log.error(
          `[File Routes] :: Error deleting file ID ${fileId}:`,
          res.error
        );
        reply.status(res.status).send(res.error);
      } else {
        fastify.log.info('[File Routes] :: deleting file ID', fileId);

        reply.send(res);
      }
    } catch (error) {
      fastify.log.error(
        `[File Routes] :: Error deleting file ID ${fileId}:`,
        error
      );
      reply.status(500).send({ error: 'Error deleting file' });
    }
  },

  getMilestonesTemplateFile: () => async (_request, reply) => {
    const response = await fileService.getMilestonesTemplateFile();

    reply.header('file', response.filename);
    reply.header('Access-Control-Expose-Headers', 'file');
    reply.status(200).send(response.filestream);
  }
};
