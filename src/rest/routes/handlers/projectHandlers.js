/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const httpStatus = require('http-status');
const projectService = require('../../services/projectService');
const userService = require('../../services/userService');
const COAError = require('../../errors/COAError');
const errors = require('../../errors/exporter/ErrorExporter');

module.exports = {
  createProject: () => async (request, reply) => {
    const ownerId = request.user.id;
    const response = await projectService.createProject({ ownerId });
    reply.status(200).send(response);
  },
  cloneProject: () => async (request, reply) => {
    const { user } = request;
    const { projectId } = request.params;
    const response = await projectService.cloneProject({ projectId, user });
    reply.status(200).send(response);
  },
  updateBasicProjectInformation: () => async (request, reply) => {
    const body = request.raw.body || {};
    const files = request.raw.files || {};

    const { user } = request;
    const { projectId } = request.params;
    const { projectName, location, timeframe, timeframeUnit } = body;
    const { thumbnailPhoto } = files;
    const response = await projectService.updateBasicProjectInformation({
      projectId,
      projectName,
      location,
      timeframe,
      timeframeUnit,
      file: thumbnailPhoto,
      user
    });
    reply.status(200).send(response);
  },
  updateProjectDetails: () => async (request, reply) => {
    const body = request.raw.body || {};
    const files = request.raw.files || {};

    const { projectId } = request.params;
    const {
      mission,
      problemAddressed,
      currencyType,
      currency,
      additionalCurrencyInformation,
      type
    } = body;
    const { legalAgreementFile, projectProposalFile } = files;
    const response = await projectService.updateProjectDetails({
      projectId,
      mission,
      problemAddressed,
      currencyType,
      currency,
      additionalCurrencyInformation,
      legalAgreementFile,
      projectProposalFile,
      user: request.user,
      type
    });
    reply.status(200).send(response);
  },

  getProjectMilestones: () => async (request, reply) => {
    const { projectId } = request.params;
    const response = await projectService.getProjectMilestones(projectId);
    reply.status(200).send(response);
  },

  sendProjectToReview: () => async (request, reply) => {
    const { projectId } = request.params;
    const { user } = request;
    const response = await projectService.sendProjectToReview({
      user,
      projectId
    });
    reply.status(httpStatus.OK).send(response);
  },

  deleteProject: () => async (request, reply) => {
    const { projectId } = request.params;
    const response = await projectService.deleteProject(projectId);
    reply.send(response);
  },

  publishProject: () => async (request, reply) => {
    const userId = request.user.id;
    const userEmail = request.user.email;
    const response = await projectService.publishProject({
      projectId: request.params.projectId,
      userId,
      userEmail
    });
    reply.send(response);
  },

  getProjects: props => async (_, reply) => {
    const projects = await projectService.getProjects(props);
    reply.status(200).send(projects);
  },

  getPublicProjects: () => async (request, reply) => {
    const getPublicProjectMaxQuota = 100;
    const highlighted = request.query && request.query.highlighted === 'true' || false;
    const limit = request.query && request.query.limit <= getPublicProjectMaxQuota && request.query.limit || getPublicProjectMaxQuota;
    const projects = await projectService.getPublicProjects(highlighted, limit);
    reply.status(200).send(projects);
  },

  getProject: fastify => async (request, reply) => {
    const { projectId } = request.params;

    if (!projectId) {
      throw new COAError(errors.project.ProjectIdNotProvided);
    }

    const token = request.headers.authorization;
    let existentUser;
    if (token) {
      if (!token.startsWith('Bearer ')) throw new Error('Invalid token format');
      const [_, bearerToken] = token.split(' ');
      const user = await fastify.jwt.verify(bearerToken);
      existentUser = await userService.getUserById(user.id);
      if (!existentUser || existentUser.blocked) {
        fastify.log.error(
          '[Project Handler] :: Unathorized access for user:',
          user
        );
        throw new COAError(errors.server.UnauthorizedUser);
      }
    }
    const response = await projectService.getProject(projectId, existentUser);
    reply.status(200).send(response);
  },

  getProjectTransactions: () => async (request, reply) => {
    const { projectId } = request.params;
    const { type } = request.query;
    const response = await projectService.getProjectTransactions({
      projectId,
      type
    });
    reply.status(httpStatus.OK).send(response);
  },

  getProjectChangelog: () => async (request, reply) => {
    const { projectId } = request.params;
    const {
      activityId,
      milestoneId,
      revisionId,
      evidenceId,
      userId
    } = request.query;
    const response = await projectService.getProjectChangelog({
      project: projectId,
      activity: activityId,
      milestone: milestoneId,
      revision: revisionId,
      user: userId,
      evidence: evidenceId
    });
    reply.status(httpStatus.OK).send(response);
  },

  cancelProjectReview: () => async (request, reply) => {
    const { projectId } = request.params;
    const { user } = request;
    const response = await projectService.cancelProjectReview({
      user,
      projectId
    });
    reply.status(httpStatus.OK).send(response);
  },

  updateProjectReview: () => async (request, reply) => {
    const { projectId } = request.params;
    const { user } = request;
    const { approved, reason } = request.body;
    const response = await projectService.updateProjectReview({
      projectId,
      approved,
      userId: user.id,
      userEmail: user.email,
      reason
    });
    reply.status(httpStatus.OK).send(response);
  },

  sendProjectReviewTransaction: () => async (request, reply) => {
    const response = await projectService.sendProjectReviewTransaction({
      user: request.user,
      projectId: request.params.projectId,
      authorizationSignature: request.body.authorizationSignature
    });
    reply.status(httpStatus.OK).send(response);
  },

  getProjectEvidences: () => async (request, reply) => {
    const response = await projectService.getProjectEvidences({
      projectId: request.params.projectId,
      limit: request.query.limit
    });
    reply.status(httpStatus.OK).send(response);
  },

  getProjectMovements: () => async (request, reply) => {
    const response = await projectService.getProjectMovements({
      projectId: request.params.projectId,
      page: request.query.page || 1,
      type: request.query.type,
      withLinkedActivities: request.query.withLinkedActivities
    });
    reply.status(httpStatus.OK).send(response);
  },

  cancelProject: () => async (request, reply) => {
    const { projectId } = request.params;
    const { user } = request;
    const response = await projectService.cancelProject({
      user,
      projectId
    });
    reply.status(httpStatus.OK).send(response);
  },

  setProjectVisibility: () => async (request, reply) => {
    const { projectId } = request.params;
    const { visibility } = request.body;
    const response = await projectService.setProjectVisibility(
      projectId,
      visibility
    );
    reply.status(httpStatus.OK).send(response);
  }
};
