/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const bcrypt = require('bcrypt');
const COAError = require('../../errors/COAError');
const errors = require('../../errors/exporter/ErrorExporter');
const httpStatus = require('http-status');

const accountService = require('../../services/accountService');
const middlewareService = require('../../services/middlewareService');
const { MAXIMUM_MOVEMENTS_PER_REQUEST } = require('../../util/constants');

module.exports = {
    getAccountsByMiddleware: () => async (request, reply) => {
        const { middlewareId, middlewareKey } = request.query;
        const middleware = await middlewareService.getMiddlewareById(middlewareId);
        if (!middleware) {
            throw new COAError(errors.middleware.MissingMiddleware);
        }
        const match = await bcrypt.compare(middlewareKey, middleware.apiKey);
        if (!match) {
            throw new COAError(errors.middleware.UnauthorizedMiddleware);
        }
        const response = await accountService.getAccountsByMiddleware(middlewareId);
        reply.status(httpStatus.OK).send(response);
    }
};
