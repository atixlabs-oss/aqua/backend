/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const httpStatus = require('http-status');
const milestoneService = require('../../services/milestoneService');

module.exports = {
  getMilestones: () => async (request, reply) => {
    const filters = request.query;
    const milestones = await milestoneService.getMilestones(filters);
    reply.status(200).send(milestones);
  },

  createMilestone: () => async (request, reply) => {
    const { projectId } = request.params;
    const { title, description } = request.body;
    const response = await milestoneService.createMilestone({
      projectId,
      title,
      description,
      user: request.user
    });
    reply.status(httpStatus.CREATED).send(response);
  },

  updateMilestone: () => async (request, reply) => {
    const { milestoneId } = request.params;
    const { title, description } = request.body;
    const { user } = request;
    const response = await milestoneService.updateMilestone({
      milestoneId,
      title,
      description,
      user
    });
    reply.status(httpStatus.OK).send(response);
  },

  deleteMilestone: () => async (request, reply) => {
    const { milestoneId } = request.params;
    const userId = request.user.id;
    const response = await milestoneService.deleteMilestone(
      milestoneId,
      userId
    );
    reply.status(httpStatus.OK).send(response);
  },

  claimMilestone: () => async (request, reply) => {
    const { milestoneId } = request.params;
    const userId = request.user.id;

    const response = await milestoneService.claimMilestone({
      milestoneId,
      userId
    });

    reply.status(200).send(response);
  },

  transferredMilestone: () => async (request, reply) => {
    const { milestoneId } = request.params;
    const { claimReceiptFile } = request.raw.files || {};
    const userId = request.user.id;

    const response = await milestoneService.transferredMilestone({
      milestoneId,
      userId,
      claimReceiptFile
    });

    reply.status(200).send(response);
  }
};
