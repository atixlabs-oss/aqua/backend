/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const userProjectService = require('../../services/userProjectService');

module.exports = {
  signAgreement: fastify => async (request, reply) => {
    fastify.log.info('[User Project Routes] :: Signing Agreement');
    const newUserProject = await userProjectService.signAgreement({
      userProjectId: request.params.userProjectId,
      status: request.body.status
    });

    if (newUserProject.error) {
      reply.status(newUserProject.status).send({ error: newUserProject.error });
    } else {
      reply.send(newUserProject);
    }
  },

  getUsers: fastify => async (request, reply) => {
    const { projectId } = request.params;

    fastify.log.info(
      '[User Project Routes] :: Getting User associated to Project ID:',
      projectId
    );
    const userProjects = await userProjectService.getUsers(projectId);

    if (userProjects.error) {
      reply.status(userProjects.status).send(userProjects.error);
    } else {
      reply.send(userProjects);
    }
  },

  createUserProject: fastify => async (request, reply) => {
    const { userId, projectId } = request.body;

    fastify.log.info(
      `[User Project Routes] :: Associating User ID ${userId} to Project ID 
      ${projectId}`
    );

    try {
      const userProject = await userProjectService.createUserProject(
        userId,
        projectId
      );

      if (userProject.error) {
        fastify.log.error(
          '[User Project Routes] :: Error creating user-project relation: ',
          userProject.error
        );
        reply.status(userProject.status).send(userProject.error);
      } else {
        fastify.log.info(
          '[User Routes Service] :: User-Project relation created succesfully: ',
          userProject
        );
        reply
          .status(200)
          .send({ success: 'User-project relation created successfully!' });
      }
    } catch (error) {
      fastify.log.error(
        '[User Project Routes] :: Error creating user-project relation: ',
        error
      );
      reply.status(500).send({ error: 'Error creating user-project relation' });
    }
  },

  relateUserWithProject: fastify => async (request, reply) => {
    const userIdAction = request.user.id;
    const { userId, projectId, roleId } = request.body;

    fastify.log.info(
      `[User Project Routes] :: Associating User ID ${userId} to Project ID 
      ${projectId} with role with id ${roleId}`
    );
    const userProject = await userProjectService.relateUserWithProject({
      userId,
      projectId,
      roleId,
      userIdAction
    });

    fastify.log.info(
      '[User Routes Service] :: User-Project relation created succesfully: ',
      userProject
    );
    reply.status(200).send(userProject);
  },

  removeUserProject: fastify => async (request, reply) => {
    const { userId, projectId, roleId } = request.body;
    const adminUserId = request.user.id;

    fastify.log.info(
      `[User Project Routes] :: Deleting User ID ${userId} to Project ID 
      ${projectId} with role with id ${roleId}`
    );
    const userProject = await userProjectService.removeUserProject({
      adminUserId,
      userId,
      projectId,
      roleId
    });

    fastify.log.info(
      '[User Routes Service] :: User-Project relation created succesfully: ',
      userProject
    );
    reply.status(200).send(userProject);
  }
};
