/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const basePath = '/projects';
const handlers = require('./handlers/projectHandlers');
const routeTags = require('../util/routeTags');
const {
  successResponse,
  serverErrorResponse,
  clientErrorResponse
} = require('../util/responses');
const { txTypes } = require('../util/constants');
const { idParam } = require('../util/params');

const basicInformationProperties = {
  projectName: { type: 'string' },
  location: { type: 'string' },
  timeframe: { type: 'number' },
  timeframeUnit: { type: 'string' },
};

const projectDetailProperties = {
  mission: { type: 'string' },
  problemAddressed: { type: 'string' }
};

const projectDetailsProperties = {
  ...projectDetailProperties,
  currencyType: { type: 'string' },
  currency: { type: 'string' },
  additionalCurrencyInformation: { type: 'string' },
  type: { type: 'string' }
};

const milestonesResponse = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      category: { type: 'string' },
      description: { type: 'string' },
      claimStatus: { type: 'string' },
      tasks: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: { type: 'integer' },
            activityHash: { type: 'string' },
            description: { type: 'string' },
            reviewCriteria: { type: 'string' },
            category: { type: 'string' },
            keyPersonnel: { type: 'string' },
            oracle: { type: ['string', 'null'] },
            budget: { type: 'string' },
            verified: { type: 'boolean' }
          }
        }
      }
    }
  },
  description: 'Returns all milestones of a project'
};

const projectIdParam = idParam('Project identification', 'projectId', 'string');

const successWithProjectIdResponse = {
  type: 'object',
  properties: {
    projectId: { type: 'string' }
  },
  description: 'Returns the id of the project'
};

const userProperties = {
  id: { type: 'string' },
  firstName: { type: 'string' },
  lastName: { type: 'string' },
  role: { type: 'string' },
  email: { type: 'string' }
};

const userResponse = {
  type: 'object',
  properties: userProperties,
  description: 'Returns and object with the user information'
};

const projectsResponse = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      projectName: { type: 'string' },
      mission: { type: 'string' },
      problemAddressed: { type: 'string' },
      location: { type: 'string' },
      timeframe: { type: 'string' },
      timeframeUnit: { type: 'string' },
      proposal: { type: 'string' },
      faqLink: { type: 'string' },
      coverPhotoPath: { type: 'string' },
      cardPhotoPath: { type: 'string' },
      milestonePath: { type: 'string' },
      goalAmount: { type: 'number' },
      currency: { type: 'string' },
      status: { type: 'string' },
      revision: { type: 'number' },
      owner: userResponse,
      createdAt: { type: 'string' },
      proposalFilePath: { type: 'string' },
      agreementFilePath: { type: 'string' },
      id: { type: 'string' },
      nextStatusUpdateAt: { type: 'string' },
      parent: { type: 'string' },
      cloneStatus: { type: 'string' },
      visibility: { type: 'string'},
      beneficiary: {
        type: 'object',
        properties: {
          id: {
            type: 'string'
          },
          firstName: {
            type: 'string'
          },
          lastName: {
            type: 'string'
          }
        }
      },
      type: { type: 'string' }
    }
  },
  description: 'Returns all projects'
};

const sucessProjectTransactions = {
  type: 'object',
  properties: {
    transactions: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          txHash: { type: 'string' },
          value: { type: 'string' },
          tokenSymbol: { type: 'string' },
          from: { type: 'string' },
          to: { type: 'string' },
          timestamp: { type: 'string' }
        }
      }
    }
  }
};

const successProjectEvidences = {
  type: 'object',
  properties: {
    evidences: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          date: { type: 'string' },
          role: { type: 'string' },
          userName: { type: 'string' },
          activityType: { type: 'string' },
          amount: { type: 'string' },
          destinationAccount: { type: 'string' }
        }
      }
    }
  }
};

const successProjectMovements = {
  type: 'object',
  properties: {
    movements: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'number' },
          externalId: { type: 'string' },
          displayId: { type: 'string'},
          amount: { type: 'number' },
          type: { type: 'string' },
          accountId: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              platformName: { type: 'string' },
              currency: { type: 'number' },
              data: { type: 'object' },
              middlewareId: { type: 'number' },
              projectId: { type: 'string' }
            }
          },
          otherAccount: { type: 'string' },
          date: { type: 'string' }
        }
      }
    }
  }
};

const changelogResponse = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      id: { type: 'string' },
      project: {
        type: 'object',
        properties: {
          id: { type: 'string' },
          projectName: { type: ['string', 'null'] }
        }
      },
      revision: { type: ['integer', 'null'] },
      milestone: {
        type: 'object',
        properties: {
          id: { type: 'integer' },
          title: { type: 'string' }
        },
        nullable: true
      },
      activity: {
        type: 'object',
        properties: {
          id: { type: 'integer' },
          title: { type: 'string' },
          auditor: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              firstName: { type: 'string' },
              lastName: { type: 'string' }
            }
          }
        },
        nullable: true
      },
      evidence: {
        type: 'object',
        properties: {
          id: { type: 'integer' },
          title: { type: 'string' }
        },
        nullable: true
      },
      user: {
        type: 'object',
        properties: {
          id: { type: 'string' },
          firstName: { type: 'string' },
          lastName: { type: 'string' },
          isAdmin: { type: 'boolean' },
          role: { type: 'string' },
        },
        nullable: true
      },
      transaction: { type: ['string', 'null'] },
      description: { type: ['string', 'null'] },
      action: { type: ['string', 'null'] },
      extraData: {
        type: 'object',
        nullable: true,
        additionalProperties: true
      },
      status: {
        type: 'string'
      },
      datetime: { type: 'string' }
    }
  },
  description: 'Returns all changelogs of a project'
};

const successBooleanResponse = {
  type: 'object',
  properties: {
    success: {
      type: 'boolean'
    }
  }
};

const successProjectToReviewResponse = {
  type: 'object',
  properties: {
    success: {
      type: 'boolean'
    },
    toSign: { type: 'string' }
  }
};

const sendTransactionResponse = {
  type: 'object',
  properties: {
    txHash: {
      type: 'string'
    }
  }
};

const publicProjectsResponse = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      id: { type: 'string' },
      projectName: { type: 'string' },
      cardPhotoPath: { type: 'string' },
      parent: { type: ['string', 'null'] },
      location: { type: 'string' },
      timeframe: { type: 'string' },
      timeframeUnit: { type: 'string' },
      type: { type: 'string' },
      beneficiary: {
        type: 'object',
        properties: {
          id: { type: 'string' },
          firstName: { type: 'string' },
          lastName: { type: 'string' },
        }
      },
      goalAmount: { type: 'string' },
      currency: { type: 'string' },
      revision: { type: 'number' },
      status: { type: 'string' },
    }
  }
};

const basicInformationRoutes = {
  createProject: {
    method: 'post',
    path: `${basePath}`,
    options: {
      beforeHandler: ['withUser', 'adminAuth'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.POST.name],
        description: 'Creates new project with default title.',
        summary: 'Create new project',
        type: 'multipart/form-data',
        raw: {
          files: { type: 'object' },
          body: {
            type: 'object',
            properties: basicInformationProperties
          }
        },
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.createProject
  },
  cloneProject: {
    method: 'post',
    path: `${basePath}/:projectId/clone`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.POST.name],
        description: 'Clones project with all its relations.',
        summary: 'Clones a project',
        params: projectIdParam,
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.cloneProject
  },
  updateBasicProjectInformation: {
    method: 'put',
    path: `${basePath}/:projectId/basic-information`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.POST.name],
        description: 'Update basic project information.',
        summary: 'Update basic information',
        type: 'multipart/form-data',
        raw: {
          files: { type: 'object' },
          body: {
            type: 'object',
            properties: basicInformationProperties
          }
        },
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.updateBasicProjectInformation
  }
};

const projectDetailsRoutes = {
  updateProjectDetails: {
    method: 'put',
    path: `${basePath}/:projectId/details`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.POST.name],
        description: 'Updates the details of an existing project.',
        summary: 'Updates a project details',
        raw: {
          files: { type: 'object' },
          body: {
            type: 'object',
            properties: projectDetailsProperties
          }
        },
        params: projectIdParam,
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.updateProjectDetails
  }
};

const projectMilestonesRoute = {
  getMilestones: {
    method: 'get',
    path: `${basePath}/:projectId/milestones`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.POST.name],
        description: 'Returns the milestones of an existing project',
        summary: 'Gets milestones of a project',
        params: projectIdParam,
        response: {
          ...successResponse(milestonesResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getProjectMilestones
  }
};

const projectStatusRoutes = {
  sendProjectToReview: {
    method: 'put',
    path: `${basePath}/:projectId/in-review`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.PUT.name],
        description: 'Send a project to be reviewed',
        summary: 'Send a project to be reviewed',
        params: projectIdParam,
        response: {
          ...successResponse(successProjectToReviewResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.sendProjectToReview
  },

  cancelProjectReview: {
    method: 'put',
    path: `${basePath}/:projectId/cancel-review`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.PUT.name],
        description: 'Cancel project review',
        summary: 'Cancel project review',
        params: projectIdParam,
        response: {
          ...successResponse(successBooleanResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.cancelProjectReview
  },

  publishProject: {
    method: 'put',
    path: `${basePath}/:projectId/publish`,
    options: {
      beforeHandler: ['adminAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.PUT.name],
        description: 'Publish a project',
        summary: 'Publish a project',
        params: projectIdParam,
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.publishProject
  },

  updateProjectReview: {
    method: 'put',
    path: `${basePath}/:projectId/review`,
    options: {
      beforeHandler: ['adminAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.PUT.name],
        description: 'Update project review',
        summary: 'Update project review',
        body: {
          type: 'object',
          properties: {
            approved: {
              type: 'boolean'
            },
            reason: {
              type: 'string'
            }
          },
          required: ['approved']
        },
        type: 'object',
        params: projectIdParam,
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.updateProjectReview
  },

  sendProjectReviewTransaction: {
    method: 'post',
    path: `${basePath}/:projectId/signature`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.POST.name],
        description:
          'Send propose project edit transaction with signature of params',
        summary: 'Send propose project edit transaction',
        params: projectIdParam,
        body: {
          type: 'object',
          properties: {
            authorizationSignature: { type: 'string' }
          },
          additionalProperties: false
        },
        required: ['authorizationSignature'],
        response: {
          ...successResponse(sendTransactionResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.sendProjectReviewTransaction
  },

  cancelProject: {
    method: 'put',
    path: `${basePath}/:projectId/cancel`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.PUT.name],
        description: 'Request to cancel project',
        summary: 'Request to cancel project',
        params: projectIdParam,
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.cancelProject
  }
};

const commonProjectRoutes = {
  getProjects: {
    method: 'get',
    path: `${basePath}`,
    options: {
      beforeHandler: ['adminAuth'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.GET.name],
        description: 'Gets all projects.',
        summary: 'Gets all project',
        response: {
          ...successResponse(projectsResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getProjects
  },

  getProject: {
    method: 'get',
    path: `${basePath}/:projectId`,
    options: {
      beforeHandler: []
    },
    handler: handlers.getProject
  },

  setProjectVisibility: {
    method: 'put',
    path: `${basePath}/:projectId/set-visibility`,
    options: {
      beforeHandler: ['adminAuth', 'withUser'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.PUT.name],
        description: 'Set project visibility',
        summary: 'Set project visibility',
        params: projectIdParam,
        body: {
          type: 'object',
          properties: {
            visibility: {
              type: 'string'
            },
          },
          required: ['visibility']
        },
        type: 'object',
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.setProjectVisibility
  },

  getPublicProjects: {
    method: 'get',
    path: `${basePath}/public`,
    options: {
      beforeHandler: [],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.GET.name],
        description: 'Gets public or highlighted projects.',
        summary: 'Gets public or highlighted project',
        querystring: {
          highlighted: { type: 'string' },
          limit: { type: 'number' }
        },
        response: {
          ...successResponse(publicProjectsResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getPublicProjects
  }
};

const adminRoutes = {
  deleteProject: {
    method: 'delete',
    path: `${basePath}/:projectId`,
    options: {
      beforeHandler: ['adminAuth'],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.DELETE.name],
        description: 'Deletes a project',
        summary: 'Deletes a project',
        params: projectIdParam,
        response: {
          ...successResponse(successWithProjectIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.deleteProject
  }
};

const projectTransactionsRoutes = {
  getProjectTransactions: {
    method: 'get',
    path: `${basePath}/:projectId/transactions`,
    options: {
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.GET.name],
        description: 'Get transactions of address associated to project',
        summary: 'Get transactions of project',
        params: {
          type: 'object',
          properties: {
            projectId: {
              type: 'string'
            },
            type: { type: 'string', enum: Object.values(txTypes) }
          }
        },
        response: {
          ...successResponse(sucessProjectTransactions),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getProjectTransactions
  }
};

const changelogRoutes = {
  getProjectChangelog: {
    method: 'get',
    path: `${basePath}/:projectId/changelog`,
    options: {
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.GET.name],
        description: 'Get changelog of a project.',
        summary: 'Get project changelog.',
        params: {
          type: 'object',
          properties: {
            projectId: {
              type: 'string'
            },
            milestoneId: { type: 'integer' },
            activityId: { type: 'integer' },
            revisionId: { type: 'integer' },
            evidenceId: { type: 'integer' },
            userId: { type: 'integer' }
          }
        },
        response: {
          ...successResponse(changelogResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getProjectChangelog
  }
};

const evidencesRoutes = {
  getProjectEvidences: {
    method: 'get',
    path: `${basePath}/:projectId/evidences`,
    options: {
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.GET.name],
        description: 'Get approved evidences of given project',
        summary: 'Get evidences of project',
        params: {
          type: 'object',
          properties: {
            projectId: {
              type: 'string'
            },
            limit: { type: 'integer' }
          }
        },
        response: {
          ...successResponse(successProjectEvidences),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getProjectEvidences
  }
};

const movementRoutes = {
  getProjectMovements: {
    method: 'get',
    path: `${basePath}/:projectId/movement`,
    options: {
      parameters: [
        {
          name: 'page',
          in: 'query',
          description:
            'Page value optional that should need to be considered for filter',
          required: false,
          schema: {
            type: 'number',
          }
        },
        {
          name: 'type',
          in: 'query',
          required: false,
          schema: {
            type: 'number',
          }
        },
        {
          name: 'withLinkedActivities',
          in: 'query',
          required: false,
          schema: {
            type: 'boolean'
          }
        }
      ],
      schema: {
        tags: [routeTags.PROJECT.name, routeTags.GET.name],
        description: 'Get approved evidences of given project',
        summary: 'Get evidences of project',
        params: {
          type: 'object',
          properties: {
            projectId: {
              type: 'string'
            }
          }
        },
        response: {
          ...successResponse(successProjectMovements),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getProjectMovements
  }
};

const routes = {
  ...basicInformationRoutes,
  ...projectDetailsRoutes,
  ...projectMilestonesRoute,
  ...commonProjectRoutes,
  ...projectStatusRoutes,
  ...adminRoutes,
  ...projectTransactionsRoutes,
  ...changelogRoutes,
  ...evidencesRoutes,
  ...movementRoutes
};

module.exports = routes;
