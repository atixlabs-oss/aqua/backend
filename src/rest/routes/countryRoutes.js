/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const basePath = '/countries';
const handlers = require('./handlers/countryHandlers');
const routeTags = require('../util/routeTags');
const {
  successResponse,
  serverErrorResponse,
  clientErrorResponse
} = require('../util/responses');

const countriesResponse = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      id: { type: 'number' },
      name: { type: 'string' },
      callingCode: { type: 'number' }
    }
  },
  description: 'Returns all countries'
};

const routes = {
  getAllCountries: {
    method: 'get',
    path: `${basePath}`,
    options: {
      schema: {
        tags: [routeTags.COUNTRY.name, routeTags.GET.name],
        description: 'Gets all countries.',
        summary: 'Gets all countries',
        response: {
          ...successResponse(countriesResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getCountries
  }
};

module.exports = routes;
