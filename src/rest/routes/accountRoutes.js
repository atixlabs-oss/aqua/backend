/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const basePath = '/account';
const handlers = require('./handlers/accountHandlers');
const routeTags = require('../util/routeTags');
const {
  successResponse,
  clientErrorResponse,
  serverErrorResponse
} = require('../util/responses');

const successGetAccountsByMiddleware = {
    type: 'array',
    items: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        platformName: { type: 'string' },
        currency: { type: 'number' },
        data: { type: 'object' },
        middlewareId: { type: 'number' },
        projectId: { type: 'string' },
        dateFrom: { type: 'string' },
        dateTo: { type: 'string' }
      }
    }
};

const routes = {
  getAccountsByMiddleware: {
    method: 'get',
    path: `${basePath}/middleware`,
    options: {
        parameters: [
            {
              name: 'middlewareId',
              in: 'query',
              required: true,
              schema: {
                type: 'number'
              }
            },
            {
                name: 'middlewareKey',
                in: 'query',
                required: true,
                schema: {
                  type: 'string'
                }
            }
        ],
        schema: {
            tags: [routeTags.ACCOUNT.name, routeTags.GET.name],
            description: 'Returns all accounts matching middleware',
            summary: 'Get accounts',
            response: {
            ...successResponse(successGetAccountsByMiddleware),
            ...clientErrorResponse(),
            ...serverErrorResponse()
            }
        }
    },
    handler: handlers.getAccountsByMiddleware
  }
};

module.exports = routes;
