/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const basePath = '/movement';
const handlers = require('./handlers/movementHandlers');
const routeTags = require('../util/routeTags');
const {
  successResponse,
  clientErrorResponse,
  serverErrorResponse
} = require('../util/responses');

const successRegisterMovements = {
  type: 'object',
  properties: {
    success: { type: 'string' },
  }
};

const routes = {
  registerMovements: {
    method: 'put',
    path: `${basePath}`,
    options: {
        parameters: [
            {
              name: 'middlewareId',
              in: 'query',
              required: true,
              schema: {
                type: 'number'
              }
            },
            {
                name: 'middlewareKey',
                in: 'query',
                required: true,
                schema: {
                  type: 'string'
                }
              }
        ],
        schema: {
            tags: [routeTags.MOVEMENT.name, routeTags.PUT.name],
            description: 'Returns all available tokens',
            summary: 'Get tokens',
            body: {
            type: 'object',
            properties: {
                movements: { 
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            accountId: { type: 'string' },
                            otherAccount: { type: 'string' },
                            amount: { type: 'number' },
                            movementType: { type: 'number' },
                            date: { type: 'string' },
                            txId: { type: 'string' },
                            uid: { type: 'string'}
                        }
                    }
                },
            }
            },
            response: {
            ...successResponse(successRegisterMovements),
            ...clientErrorResponse(),
            ...serverErrorResponse()
            }
        }
    },
    handler: handlers.registerMovements
  }
};

module.exports = routes;
