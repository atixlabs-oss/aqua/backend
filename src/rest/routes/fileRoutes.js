/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const basePath = '/files';
const handlers = require('./handlers/fileHandlers');
const routeTags = require('../util/routeTags');
const {
  successResponse,
  serverErrorResponse,
  clientErrorResponse
} = require('../util/responses');

const fileResponse = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    createdAt: { type: 'string' },
    updatedAt: { type: 'string' }
  },
  description: 'Returns the file object'
};

const fileStreamResponse = {
  type: 'string',
  description: 'Template file stream'
};

const routes = {
  deleteFile: {
    method: 'delete',
    path: `${basePath}/:fileId`,
    options: {
      beforeHandler: ['adminAuth'],
      schema: {
        tags: [routeTags.FILE.name, routeTags.DELETE.name],
        description: 'Deletes an existing file',
        summary: 'Delete file',
        params: {
          type: 'object',
          properties: {
            fileId: { type: 'integer', description: 'File to delete' }
          }
        },
        response: {
          ...successResponse(fileResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.deleteFile
  },

  getMilestonesTemplateFile: {
    method: 'get',
    path: `${basePath}/milestones/template`,
    options: {
      beforeHandler: ['generalAuth'],
      schema: {
        tags: [routeTags.FILE.name, routeTags.GET.name],
        description: 'Returns milestones template file',
        summary: 'Returns milestones template file',
        response: {
          ...successResponse(fileStreamResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getMilestonesTemplateFile
  }
};

module.exports = routes;
