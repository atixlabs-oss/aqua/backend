/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const basePath = '/tokens';
const handlers = require('./handlers/tokenHandlers');
const routeTags = require('../util/routeTags');
const {
  successResponse,
  clientErrorResponse,
  serverErrorResponse
} = require('../util/responses');

const successGetTokens = {
  type: 'object',
  properties: {
    tokens: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string' },
          name: { type: 'string' },
          symbol: { type: 'string' }
        }
      }
    }
  }
};

const routes = {
  getPhoto: {
    method: 'get',
    path: `${basePath}`,
    options: {
      schema: {
        tags: [routeTags.TOKEN.name, routeTags.GET.name],
        description: 'Returns all available tokens',
        summary: 'Get tokens',
        params: {
          type: 'object',
          properties: {
            photoId: { type: 'integer', description: 'Photo to get' }
          }
        },
        response: {
          ...successResponse(successGetTokens),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getTokens
  }
};

module.exports = routes;
