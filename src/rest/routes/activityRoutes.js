/**
 * AGUA Copyright (C) 2023  Atix Labs
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const basePath = '/activities';
const handlers = require('./handlers/activityHandlers');
const routeTags = require('../util/routeTags');
const { idParam } = require('../util/params');
const {
  successResponse,
  serverErrorResponse,
  clientErrorResponse
} = require('../util/responses');

const taskIdParam = idParam('Task identification', 'taskId');
const activityIdParam = idParam('Activity identification', 'activityId');
const milestoneIdParam = idParam('Milestone identification', 'milestoneId');
const evidenceIdParam = idParam('Evidence identification', 'evidenceId');

const activityProperties = {
  title: { type: 'string' },
  description: { type: 'string' },
  acceptanceCriteria: { type: 'string' },
  budget: { type: 'string' },
  auditor: { type: 'string' },
  type: { type: 'string' }
};

const successWithTaskIdResponse = {
  type: 'object',
  properties: {
    taskId: { type: 'integer' }
  },
  description: 'Returns the id of the task'
};

const successWithActivityIdResponse = {
  type: 'object',
  properties: {
    activityId: { type: 'integer' }
  },
  description: 'Returns the id of the activity'
};

const successWithEvidenceIdResponse = {
  type: 'object',
  properties: {
    evidenceId: { type: 'integer' }
  },
  description: 'Returns the id of the evidence'
};

const sendTransactionResponse = {
  type: 'object',
  properties: {
    txHash: {
      type: 'string'
    }
  }
};

const successEvidence = {
  description: 'Returns evidence information',
  type: 'object',
  properties: {
    movement: {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        externalId: { type: 'string' },
        displayId: { type: 'string' },
        amount: { type: 'integer' },
        type: { type: 'string' },
        accountId: { type: 'string' },
        otherAccount: { type: 'string' },
        date: { type: 'string' }
      }
    },
    user: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        address: { type: 'string' }
      },
      nullable: true
    },
    activity: {
      type: 'object',
      properties: {
        status: { type: 'string' },
        id: { type: 'integer' },
        title: { type: 'string' },
        description: { type: 'string' },
        auditor: { 
          type: 'object',
          properties: {
            id: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            address: { type: 'string' }
          }
        },
        type: { type: 'string' },
      },
    },
    // successDeletedEvidence fields
    id: { type: 'number' },
    evidenceTitle: { type: 'string' },
    activityTitle: { type: 'string' },
    milestoneTitle: { type: 'string' },
    deleted: { type: 'boolean' }
  },
  additionalProperties: true
};

const successWithActivityEvidences = {
  description: 'Returns an array with the activity evidences',
  type: 'object',
  properties: {
    milestone: {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        title: { type: 'string' }
      }
    },
    activity: {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        title: { type: 'string' },
        description: { type: 'string' },
        acceptanceCriteria: { type: 'string' },
        status: { type: 'string' },
        auditor: { type: 'string' },
        budget: { type: 'string' },
        current: { type: 'string' },
        step: { type: 'integer' }
      },
      additionalProperties: true
    },
    evidences: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'integer' },
          title: { type: 'string' },
          description: { type: 'string' },
          type: { type: 'string' },
          amount: { type: 'string' },
          destinationAccount: { type: 'string' },
          txHash: { type: 'string' },
          status: { type: 'string' },
          reason: { type: 'string' },
          files: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                id: { type: 'integer' },
                name: { type: 'string' },
                path: { type: 'string' }
              }
            }
          },
          movement: {
            type: 'object',
            properties: {
              id: { type: 'integer' },
              externalId: { type: 'string' },
              displayId: { type: 'string' },
              amount: { type: 'integer' },
              type: { type: 'string' },
              accountId: { type: 'string' },
              otherAccount: { type: 'string' },
              date: { type: 'string' }
            }
          },
          createdAt: { type: 'string' }
        }
      }
    }
  }
};

const activityRoutes = {
  createActivity: {
    method: 'post',
    path: `/milestones/:milestoneId${basePath}`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.POST.name],
        description: 'Creates a new activity for an existing milestone',
        summary: 'Creates new activity',
        params: { milestoneIdParam },
        body: {
          type: 'object',
          properties: activityProperties,
          required: [
            'title',
            'description',
            'acceptanceCriteria',
            'budget',
            'auditor',
            'type'
          ],
          additionalProperties: false
        },
        response: {
          ...successResponse(successWithActivityIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.createActivity
  },
  updateActivity: {
    method: 'put',
    path: `${basePath}/:activityId`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.PUT.name],
        description: 'Update the information of an existing activity',
        summary: 'Update activity information',
        params: { activityIdParam },
        body: {
          type: 'object',
          properties: activityProperties,
          additionalProperties: false
        },
        response: {
          ...successResponse(successWithActivityIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.updateActivity
  },
  updateTaskStatus: {
    method: 'put',
    path: `${basePath}/:activityId/status`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.PUT.name],
        description: 'Update the status of an existing activity',
        summary: 'Update activity status',
        params: { activityIdParam },
        body: {
          type: 'object',
          properties: {
            status: { type: 'string' },
            reason: { type: 'string' }
          },
          additionalProperties: false
        },
        required: ['status'],
        response: {
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.updateActivityStatus
  },
  deleteActivity: {
    method: 'delete',
    path: `${basePath}/:taskId`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.DELETE.name],
        description: 'Deletes an existing task',
        summary: 'Deletes task',
        params: { taskIdParam },
        response: {
          ...successResponse(successWithTaskIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.deleteActivity
  },

  sendActivityTransaction: {
    method: 'post',
    path: `${basePath}/:activityId/signature`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.POST.name],
        description:
          'Send propose claim transaction or submit claim audit result with signature of params',
        summary: 'Send propose claim transaction or submit claim audit result ',
        params: { activityIdParam },
        body: {
          type: 'object',
          properties: {
            authorizationSignature: { type: 'string' }
          },
          additionalProperties: false
        },
        required: ['authorizationSignature'],
        response: {
          ...successResponse(sendTransactionResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.sendActivityTransaction
  }
};

const evidencesRoutes = {
  addEvidence: {
    method: 'post',
    path: `${basePath}/:activityId/evidences`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.POST.name],
        description: 'Add evidence to activity for an existing project',
        summary: 'Add evidence to activity',
        params: { activityIdParam },
        type: 'multipart/form-data',
        response: {
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.addEvidence
  },
  getEvidence: {
    method: 'get',
    path: '/evidences/:evidenceId',
    options: {
      beforeHandler: [],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.GET.name],
        description: 'Returns the evidence with the given id',
        summary: 'Returns an evidence',
        params: evidenceIdParam,
        response: {
          ...successResponse(successEvidence),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getEvidence
  },
  updateEvidenceStatus: {
    method: 'put',
    path: '/evidences/:evidenceId',
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.PUT.name],
        description: 'Updates evidence status',
        summary: 'Updates evidence status',
        params: evidenceIdParam,
        body: {
          type: 'object',
          properties: {
            status: {
              type: 'string'
            },
            reason: {
              type: 'string'
            }
          },
          additionalProperties: false,
          required: ['status', 'reason']
        },
        response: {
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.updateEvidenceStatus
  },
  getActivityEvidences: {
    method: 'get',
    path: `${basePath}/:activityId/evidences`,
    options: {
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.GET.name],
        description: 'Get all the evidences uploaded for a specific activity',
        summary: 'Get activity evidences',
        params: { activityIdParam },
        response: {
          ...successResponse(successWithActivityEvidences),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.getActivityEvidences
  },
  deleteEvidence: {
    method: 'delete',
    path: `${basePath}/:activityId/evidence/:evidenceId`,
    options: {
      beforeHandler: ['generalAuth', 'withUser'],
      schema: {
        tags: [routeTags.ACTIVITY.name, routeTags.DELETE.name],
        description: 'Deletes a NEW evidence for an IN_PROGRESS activity',
        summary: 'Deletes evidence',
        params: { activityIdParam, evidenceIdParam },
        response: {
          ...successResponse(successWithEvidenceIdResponse),
          ...clientErrorResponse(),
          ...serverErrorResponse()
        }
      }
    },
    handler: handlers.deleteEvidence
  }
};

const routes = {
  ...activityRoutes,
  ...evidencesRoutes
};

module.exports = routes;
