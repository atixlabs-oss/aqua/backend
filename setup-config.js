module.exports = {
    "email": "admin@agua.com",
    "password": "admin",
    "db": {
        "name": process.env.DB_NAME || "name",
        "password": process.env.DB_PASSWORD || "password",
        "port": process.env.DB_PORT || 5432,
        "host": process.env.DB_HOST || "localhost",
        "user": process.env.DB_USER || "postgres"
    }
};
